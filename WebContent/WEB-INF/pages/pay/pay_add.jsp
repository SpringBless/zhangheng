<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加家庭档案-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加内容</strong></div>
		<div class="body-content">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${addAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>县级编码：</label>
					</div>
					<div class="field">
						<select id="countyCode" name="countyCode" class="input" data-validate="required:请输入县级编码" >
							<option value="0">请选择</option>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>乡镇编码：</label>
					</div>
					<div class="field">
						<select id="townsCode" name="townsCode" class="input" data-validate="required:请输入乡镇编码" >
							<option value="0">请选择</option>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>村编码：</label>
					</div>
					<div class="field">
						<select id="villageCode" name="villageCode" class="input" data-validate="required:请输入村编码" >
							<option value="0">请选择</option>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>组编码：</label>
					</div>
					<div class="field">
						<select id="groupCode" name="groupCode" class="input" data-validate="required:请输入组编码" >
							<option value="0">请选择</option>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>户属性：</label>
					</div>
					<%--
					<div class="field">
					    <input type="text" id="familyProperty" name="familyProperty" class="input" data-validate="required:请输入户属性" />
						<div class="tips"></div>
					</div>
					--%>
					<div class="field">
					    <select id="familyProperty" name="familyProperty" class="input" data-validate="required:请输入户属性" >
					    	<c:if test="${!empty familyProperty }">
					    		<c:forEach items="${ familyProperty }" var="item">					    		
									<option value="<c:out value="${item }"/>" ><c:out value="${item }"/></option>				    	
					    		</c:forEach>
					    	</c:if>
					    	<c:if test="${empty familyProperty }">				    	
								<option value="null" >请先在家庭人员资源管理添加户类型</option>
					    	</c:if>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="label">
						<label>家庭人口数：</label>
					</div>
					<div class="field">
					    <input type="number" class="input" name="familyNumber" placeholder="请输入家庭人口数" data-validate="required:请输入家庭人口数" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>农业人口数：</label>
					</div>
					<div class="field">
					    <input type="number" class="input" name="agricultural" placeholder="请输入农业人口数" data-validate="required:请输入农业人口数" />
						<div class="tips"></div>
					</div>
				</div> -->
				<div class="form-group">
					<div class="label">
						<label>家庭住址：</label>
					</div>
					<div class="field">
						<textarea class="input" name="address" style="height:90px;"></textarea>
					</div>
				</div>
				<hr/>
				<!-- 户主信息 -->
				<div class="form-group">
					<div class="label">
						<label>户主信息：</label>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>户主姓名：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="householder" placeholder="请输入户主姓名" data-validate="required:请输入户主姓名" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>农合证号：</label>
					</div>
					<div class="field">
						<input name="nhCode" class="input" data-validate="required:请输入农合证号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>医疗证卡号：</label>
					</div>
					<div class="field">
						<input name="ylCode" class="input" data-validate="required:请输入医疗证卡号" />
						<div class="tips"></div>
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="name" placeholder="请输入姓名" data-validate="required:请输入姓名" />
						<div class="tips"></div>
					</div>
				</div>
				 -->
				<div class="form-group">
					<div class="label">
						<label>与户主关系：</label>
					</div>
					<div class="field">
					    <select id="fmRelation" class="input" name="fmRelation" placeholder="请输入家庭人口数" data-validate="required:请输入与户主关系" >
					    	<option></option>
					    </select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>身份证号：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" id="cardCode" name="cardCode" placeholder="请输入身份证号" data-validate="required:请输入身份证号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>性别：</label>
					</div>
					<div class="field">
						<select id="gender" class="input" name="gender" placeholder="请输入性别" data-validate="required:请输入性别" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>健康状况：</label>
					</div>
					<div class="field">
						<select id="sfHealth" class="input" name="sfHealth" placeholder="请输入健康状况" data-validate="required:请输入健康状况" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>民族：</label>
					</div>
					<div class="field">
						<select id="sfNational" class="input" name="sfNational" placeholder="请输入民族" data-validate="required:请输入民族" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>文化程度：</label>
					</div>
					<div class="field">
						<select id="sfEducation" class="input" name="sfEducation" placeholder="请输入文化程度" data-validate="required:请输入文化程度" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>人员属性：</label>
					</div>
					<div class="field">
						<select id="rAttribute" class="input" name="rAttribute" placeholder="请输入人员属性"  >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>是否农村户口：</label>
					</div>
					<div class="field">
						<select id="hkType" class="input" name="hkType" placeholder="请输入是否农村户口" data-validate="required:请输入是否农村户口" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>职业：</label>
					</div>
					<div class="field">
						<select id="sfProfessional" class="input" name="sfProfessional" placeholder="请输入职业"  >
					    	<option></option>
					    </select>
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="label">
						<label>出生日期：</label>
					</div>
					<div class="field">
					    <input type="date" class="input" name="birthday" placeholder="请输入出生日期" data-validate="required:请输入出生日期" />
						<div class="tips"></div>
					</div>
				</div>
				 -->
				<div class="form-group">
					<div class="label">
						<label>工作单位：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="workunits" placeholder="请输入工作单位" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系电话：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="phone" placeholder="请输入联系电话" data-validate="required:请输入联系电话" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>常住地址：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="infoAddress" placeholder="请输入常住地址" data-validate="required:请输入常住地址" ></textarea>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		var fn = {
				URL : '${pageContext.request.contextPath}${initCodeURL}',// 获取区域编码 
				initValueURL : '${pageContext.request.contextPath}${initValueURL}',//户主信息下拉框数据获取
				cardCodeAction :'${pageContext.request.contextPath}${cardCodeAction}',//身份证校验
				initCode : function(datas,callBack){
					this.ajaxRequest(this.URL,datas,function(res){
						console.log(res)
						if(res != null && Array.isArray(res)){
							var html = '<option value="0">请选择</option>';
							for(var i = 0; i < res.length; i++){
								html += '<option value='+res[i].areacode+'>'+res[i].areaname+'</option>';
							}
							callBack && callBack.call(this,html);
						}
					});
				},
				ajaxRequest : function(url,datas,callBack){
					var This = this;
					$.ajax({
						type:'POST',
						url: url, 
						dataType:'json',
						data:datas,
						async: false,
						success:function(res){//与服务器交互成功调用的回调函数
							callBack && callBack.call(This,res);
						},
						error:function(data , c){
							alert("服务器异常");
						}
					});	
				},
				initValue : function(datas,callBack){
					this.ajaxRequest(this.initValueURL,datas,function(res){
						if(res != null && Array.isArray(res)){
							var html = '<option value="0">请选择</option>';
							for(var i = 0; i < res.length; i++){
								html += '<option value='+res[i]+'>'+res[i]+'</option>';
							}
							callBack && callBack.call(this,html);
						}
					});
				},
				ajaxRequestForHuZ : function(url,datas,callBack){
					var This = this;
					$.ajax({
						type:'POST',
						url: url, 
						dataType:'json',
						data:datas,
						async: false,
						success:function(res){//与服务器交互成功调用的回调函数
							console.log(res);
							callBack && callBack.call(This,res);
						},
						error:function(data , c){
							alert("服务器异常");
						}
					});	
				}
		};
		$(document).ready(function(){
			fn.initCode({areaCode:"",grade:1}, function(html){
				$("#countyCode").html(html);
			});
			$("#countyCode").change(function(){
				var code = $(this).val(),
					grade = 2;
				var datas = {"areaCode":code,"grade":grade};
				console.log(datas);
				fn.initCode(datas, function(html){
					$("#townsCode").html(html);
				});
			}); 
			$("#townsCode").change(function(){
				var code = $(this).val(),
					grade = 3;
				var datas = {"areaCode":code,"grade":grade};
				console.log(datas);
				fn.initCode(datas, function(html){
					$("#villageCode").html(html);
				});
			});
			$("#villageCode").change(function(){
				var code = $(this).val(),
					grade = 4;
				var datas = {"areaCode":code,"grade":grade};
				console.log(datas);
				fn.initCode(datas, function(html){
					$("#groupCode").html(html);
				});
			});
			
			//户主信息
			$("#cardCode").blur(function(){
				var value = $(this).val();
				fn.ajaxRequest(fn.cardCodeAction,{"cardCode":value},function(res){
					fn.flag = !res.flag;
					if(!fn.flag){
						alert('该身份证号已存在');
					}
				});
			});
			// 与户主关系
			fn.initValue({column:"fm_relation"}, function(html){
				$("#fmRelation").html(html);
			});
			
			fn.initValue({column : "sf_professional"},function(html){
				$("#sfProfessional").html(html);
			});
			// 性别
			fn.initValue({column:"gender"}, function(html){
				$("#gender").html(html);
			});
		
			//健康状况
			fn.initValue({column:"sf_health"}, function(html){
				$("#sfHealth").html(html);
			});
			// 民族
			fn.initValue({column:"sf_national"}, function(html){
				$("#sfNational").html(html);
			});
			// 文化程度
			fn.initValue({column:"sf_education"}, function(html){
				$("#sfEducation").html(html);
			});
			// 人员属性
			fn.initValue({column:"r_attribute"}, function(html){
				$("#rAttribute").html(html);
			});
			// 是否是农村户口
			fn.initValue({column:"hk_type"}, function(html){
				$("#hkType").html(html);
			});
		});
	</script>
</body>
</html>