<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加参合交费-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		.form-group{display:inline-block;width:45%;}
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加内容</strong></div>
		<div class="body-content" style="position:relative;">
			<c:choose>
				<c:when test="${not empty payInfo and curentTimeLong gt payInfo.endDate.getTime() }">
					<div style="color:red;height: 40px;line-height: 30px;padding: 0 10px;font-size: 16px;font-weight: 600;">当前缴费时间已经截止</div>		
				</c:when>
				<c:otherwise>
					<div style="height: 40px;line-height: 30px;padding: 0 10px;font-size: 16px;font-weight: 600;">
						距离缴费截止时间还有:<span style="padding-left:10px;" id="dateTime"></span>
						<script type="text/javascript">
							let endDate = ${payInfo.endDate.getTime()};
							let timer = null;
							function flushDate(){
								if(timer)clearInterval(timer);
								timer = setInterval(function(){									
									var crtTime = new Date(endDate - new Date());
									if(crtTime > 0){										
								    	$("#dateTime").html(dateFtt("MM月dd天hh时mm分ss秒",crtTime));//直接调用公共JS里面的时间类处理的办法 
									}else{
										if(timer)clearInterval(timer);
										$("#dateTime").html("缴费时间已经截止");
										$("#formAction").remove();
									}
									//$("#dateTime").html(dateFtt("MM月dd天hh时mm分ss秒",crtTime));//直接调用公共JS里面的时间类处理的办法 
								},1000);
							}
							
							flushDate();
							/**************************************时间格式化处理************************************/
							function dateFtt(fmt,date){ //author: meizz   
								var o = {   
								    "M+" : date.getMonth(),                 //月份   
								    "d+" : date.getDate(),                    //日   
								    "h+" : date.getHours(),                   //小时   
								    "m+" : date.getMinutes(),                 //分   
								    "s+" : date.getSeconds(),                 //秒   
								    "q+" : Math.floor((date.getMonth()+3)/3), //季度   
								    "S"  : date.getMilliseconds()             //毫秒   
								};   
								if(/(y+)/.test(fmt))   
								    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));   
								for(var k in o)   
								    if(new RegExp("("+ k +")").test(fmt))   
								fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
								return fmt;   
							}
						</script>
					</div>		
					<form id="formAction" method="post" class="form-x" onsubmit="return check();" action="${pageContext.request.contextPath}${doAddAction }">  
						<input type="hidden" name="tgId" value="${tgId }"/>
						<input type="hidden" name="tgName" value="${tgName }"/>
						<input id="mingdao" type="hidden" name="cardCode" value=""/>
						<input id="familyCode" type="hidden" name="familyCode" value=""/>
						<div class="form-group">
							<div class="label">
								<label>当前年份：</label>
							</div>
							<div class="field">
								<input id="playYear" name="playYear" type="year" readonly="readonly" value="${curentYear }" class="input" data-validate="required:当前缴费年份是必须的" />
								<div class="tips"></div>
							</div>
						</div>
						<c:choose>
							<c:when test="${ not empty payInfo and payInfo.years eq curentYear}">
								<div class="form-group">
									<div class="label">
										<label>当前年份每人应缴：</label>
									</div>
									<div class="field">
										<input readonly="readonly" id="playPrice" data-flag="true" value="${ payInfo.price }" name="price" class="input" />
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="form-group">
									<div class="label">
										<label>当前还没有设置标准：</label>
									</div>
									<div class="field">
										<a id="playPrice" data-flag="false" href="${pageContext.request.contextPath}${addPayInfoHref}" name="price" class="button bg-main icon-check-square-o" >设置当年标准</a>
									</div>
								</div>
							</c:otherwise>
						</c:choose>
						<br/>
						<div class="form-group">
							<div class="label">
								<label>当前选择待缴费名单：</label>
							</div>
							<div class="field">
								<div id="daijiaofei" style="height: 100px;" readonly="readonly" class="input"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="label">
								<label>家庭成员名单：</label>
							</div>
							<div class="field">
								<div id="chengyuan" style="height: 100px;" readonly="readonly" class="input"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="label">
								<label>缴费总金额：</label>
							</div>
							<div class="field">
								<input id="allPrice" type="text" value="0.00" readonly="readonly" class="input" placeholder="缴费总金额" />
								<div class="tips"></div>
							</div>
						</div>
						<br/>
						<div class="form-group">
							<div class="label">
								<label></label>
							</div>
							<div class="field">
								<input class="button bg-main icon-check-square-o" type="submit" value="确认缴费"/>
							</div>
						</div>
					</form>
					<form method="post" onsubmit="return printCheck(this);" class="form-x" target="_blank" action="${pageContext.request.contextPath}${downLoadFP }">  
						<input id="mingdao1" type="hidden" name="cardCode" value=""/>
						<input id="playYear1" name="playYear" type="hidden" value="${curentYear }" />
						<input id="playPrice1" name="price" type="hidden" value="${ payInfo.price }" />
						<input style="position: absolute;bottom: 30px;left: 300px;" value="打印" type="submit" class="button bg-main icon-check-square-o"/>
					</form>
				</c:otherwise>
			</c:choose>
		</div>
		<div style="clear:both;"></div>
		<hr/>
		<div class="panel-head" ><strong><span class="icon-pencil-square-o"></span>模糊搜索</strong></div>
		<div class="body-content">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${limitPageAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<input type="hidden" name="curentYear" value="${curentYear }"/>
				<div class="form-group">
					<div class="label">
						<label>关键字检索：</label>
					</div>
					<div class="field">
						<input name="keywords" type="text" value="${keywords }" class="input" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>所属区域：</label>
					</div>
					<div class="field">
						<c:if test="${not empty areaAll }">
							<select name="areaCode" class="input" style="display: inline-block;width:200px !important;margin-right: 50px;">
								<option value="">---请选择---</option>
								<c:forEach items="${areaAll }" var="item">
									<c:choose>
										<c:when test="${ item.areacode eq areaCode }">
											<option selected="selected" value='<c:out value="${item.areacode }"/>'><c:out value="${item.areaname }"/></option>										
										</c:when>									
										<c:otherwise>
											<option value='<c:out value="${item.areacode }"/>'><c:out value="${item.areaname }"/></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</c:if>
						<input class="button bg-main icon-check-square-o" type="submit" value="搜索"/>
					</div>
				</div>
			</form>
		</div>
		<hr/>
		<!-- 家庭信息 -->
		<div style="apdding: 0 20px;">
			<table class="table table-hover text-center">
				<tr>
					<th>选择</th>
					<th>县级编号</th>
					<th>乡/镇/村/组编号</th>
					<th>家庭编号</th>
					<th>户属性</th>
					<th>户主姓名</th>
					<th>家庭人口数</th>
					<th>农业人口数</th>
					<th>家庭住址</th>
					<th>创建档案时间</th>
					<th>登记员</th>
				</tr>
				<!-- 华丽分割线 -->	
				<c:if test="${ !empty limitPage and !empty limitPage.data }">
					<c:forEach items="${limitPage.data }" var="model">
						<tr class="tr_familyCode">
							<td><input type="radio" name="familyCode" class="familyCode" value="<c:out value="${ model.familyCode }"/>"/></td>
							<td><c:out value="${ model.countyCode }"/></td>
							<td><c:out value="${ model.groupCode }"/></td>
							<td><c:out value="${ model.familyCode }"/></td>
							<td><c:out value="${ model.familyProperty }"/></td>
							<td><c:out value="${ model.householder }"/></td>
							<td><c:out value="${ model.familyNumber }"/></td>
							<td><c:out value="${ model.agricultural }"/></td>
							<td><c:out value="${ model.address }"/></td>
							<td><fmt:formatDate value="${ model.createTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td><c:out value="${ model.greffier }"/></td>
						</tr>
					</c:forEach>
				</c:if>
				
				<!-- 分页条 -->
				<tr>
					<td colspan="11">
						<div class="pagelist">
							<c:if test="${!empty limitPage }">
								<p style="float:left;">当前显示第${ limitPage.begRow}至  ${limitPage.endRow}</p>
								<p style="float:left;">,一共${limitPage.totalRow }条,一共${limitPage.totalPage }页</p>
								<p style="float:left;">,当前显示第${limitPage.pageNo }页</p>
								<c:choose>
									<c:when test="${ limitPage.firstPage  }">
										<span onclick="alert('已经是第一页了！');" class="current">1</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=1&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">首页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.prePage }&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">上一页</a>
										<span onclick="alert('已是当前页了！');" class="current">${limitPage.pageNo }</span>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.totalPage gt 1 and limitPage.pageNo + 5 lt limitPage.totalPage }">
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.pageNo + 5 }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">${item }</a>												
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.totalPage }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">${item }</a>												
										</c:forEach>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.lastPage  }">
										<span onclick="alert('已经是最后一页了！');" class="current" >尾页</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.nextPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">下一页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.totalPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}&keywords=${keywords}&areaCode=${areaCode}">尾页</a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- 弹出层 -->
    <%@ include file="/commom/sframe.jsp" %>
	${flag }
	<script type="text/javascript">
		function printCheck(el){
			let mingdao1 = $("#mingdao1").val();
			if(mingdao1 == "" || mingdao1.length <= 0){
				alert("当前未选择缴费信息，不能打印");
				return false;
			}
			return true;
		};
		function check(){
			let mingdao = $("#mingdao").val();
			let familyCode = $("#familyCode").val();
			let playPrice = $("#playPrice").attr("data-flag");
			if(playPrice == false || playPrice == "false"){
				alert("当前未设置当年标准线，请先设置标准线");
				return false;
			}
			if(mingdao == "" || mingdao.length <= 0){
				alert("当前为选择");
				return false;
			}
			if(familyCode == "" || familyCode.length <= 0){
				alert("当前为选择");
				return false;
			}
			return true;
		};
		function sAjax(url,data,callBack){
			$.ajax({
				type:'POST',
				url: url, 
				dataType:'json',
				data:data,
				async: false,
				success:function(data){//与服务器交互成功调用的回调函数
					callBack && callBack.call(this,data);
				},
				error:function(data , c){
					alert("服务器异常");
				}
			});	
		};
		var URL = "${pageContext.request.contextPath}${ajaxAction}";
		$(document).ready(function(){
			$(".tr_familyCode").click(function(){
				//let index = $(this).index();
				$("#familyCode").val("");
				$(this).find("input.familyCode").attr({"checked":"checked"});
				let code = $(this).find("input.familyCode").val();
				$("#familyCode").val(code);
				sAjax(URL,{"familyCode":code},function(data){
					console.log(data);
					if(data.code == 200){
						var all = data.all;
						var pay = data.pay;
						var html = '';
						let allPrice = 0;
						let playPrice = $("#playPrice").val();
						let cp = playPrice == null || playPrice.length <= 0 ?  0: playPrice;
						var sf = '';
						if(all != null && all.length > 0){
							for(let i=0;i<all.length; i++){
								html += all[i].name + "<br/>";
								if(all[i].isPay){
									sf += '<ul class="s-ul">'; 
									sf += '<li class="s-ul-x"><input type="checkbox" data-s="1" data-name="'+all[i].name+'" value="'+ all[i].cardCode +'" name="sCode"></li>';
									sf += '<li>'+ all[i].familyCode +'</li>';
									sf += '<li>'+ all[i].cardCode +'</li>';
									sf += '<li>'+ all[i].name+'</li>';
									sf += '</ul>';
								}else{
									allPrice += cp * 1;
									sf += '<ul class="s-ul">'; 
									sf += '<li class="s-ul-x"><input type="checkbox" data-name="'+all[i].name+'" value="'+ all[i].cardCode +'" name="sCode"></li>';
									sf += '<li>'+ all[i].familyCode +'</li>';
									sf += '<li>'+ all[i].cardCode +'</li>';
									sf += '<li>'+ all[i].name+'</li>';
									sf += '</ul>';
								}
							}
						}
						$("#allPrice").val(allPrice);
						$("#allPrice1").val(allPrice);
						$("#chengyuan").html(html);
						new SFrame(sf,function(data){
							$("#daijiaofei").html('');
							$("#mingdao").val("");
							$("#mingdao1").val("");
							var codes = [];
				            if(data != null && data.length>0){
				            	//{name: "张积建", code: "4504210203040005"}
				            	let html = '';
				            	for(let i=0; i<data.length;i++){
				            		html += '<p>'+data[i].name + "-" + data[i].code +'</p>';
				            		codes.push(data[i].code);
				            	}
				            	$("#daijiaofei").html(html);
				            }else{
				            	alert("未选择数据");
				            }
				            $("#mingdao").val(codes);
				            $("#mingdao1").val(codes);
				        });
					}else{
						alert("系统内部异常！");
					}
				});
			});
		});
	</script>
</body>
</html>