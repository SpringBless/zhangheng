<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>参合交费管理列表-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
</head>
<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
		</div>
		<div class="padding border-bottom">
			<ul class="search" style="padding-left: 10px;">
				<c:if test="${ !empty actionMenu }">
					<c:forEach items="${ actionMenu }" var="menu">
						<c:if test="${ menu.name eq '添加' or menu.name eq '增加' or menu.name eq '添加参合交费'}">
							<li>
								<a class="button border-main icon-plus-square-o" href="${pageContext.request.contextPath}${ menu.href }&tgId=${tgId}&tgName=${tgName}">&nbsp;${ menu.name }</a>
							</li>
						</c:if>
					</c:forEach>
				</c:if>
			</ul>
			<c:if test="${ !empty actionMenu }">
				<form action="${pageContext.request.contextPath}${limitPageAction }&tgId=${tgId}&tgName=${tgName}" onsubmit="return checkedForm();" method="post">
					<ul class="search" style="margin-top:10px;padding-left: 10px;">
						<li>按行政区域：</li>
						<li>
							<c:if test="${ not empty areaAll }">
								<select id="areacode" class="input" name="areacode"  style="width: 100px; line-height: 17px; display: inline-block">
									<option value="" selected="selected">请选择</option>
									<c:forEach items="${areaAll }" var="area">
										<option value="<c:out value="${area.areacode }"/>"><c:out value="${area.areaname }"/></option>
									</c:forEach>
								</select> 
							</c:if>
						</li>
						<li>查询年份：</li>
						<li>
							<c:choose>
								<c:when test="${ not empty allYears }">
									<select id="year" class="input" name="year"  style="width: 60px; line-height: 17px; display: inline-block">
										<option value="" selected="selected">请选择</option>
										<c:forEach items="${allYears }" var="year">
											<c:choose>
												<c:when test="${year eq curentYear}">
													<option value="<c:out value="${year }"/>"><c:out value="${year }"/></option>
												</c:when>
												<c:otherwise>
													<option value="<c:out value="${year }"/>"><c:out value="${year }"/></option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select> 
								</c:when>
								<c:otherwise>
									<select id="year" name="year" class="input"  style="width: 60px; line-height: 17px; display: inline-block">
										<option value="" selected="selected">请选择</option>
										<option value="<c:out value="${curentYear }"/>"><c:out value="${curentYear }"/></option>
									</select>
								</c:otherwise>
							</c:choose>
						</li>
						<li>起始时间：</li>
						<li><input id="startDate" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="startDate" type="text" class="input" style="width: 200px; line-height: 17px; display: inline-block"></li>
						<li>结束时间：</li>
						<li><input id="endDate" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="endDate" type="text" class="input" style="width: 200px; line-height: 17px; display: inline-block"></li>
						<c:forEach items="${ actionMenu }" var="menu">
							<c:if test="${ menu.name eq '关键字检索' or menu.name eq '搜索' and menu.delFlag eq 0}">
								<li>搜索：</li>
								<li>
									<input type="text" placeholder="请输入搜索关键字" id="keywords" name="keywords" class="input" style="width: 200px; line-height: 17px; display: inline-block" />
									<input type="submit" class="button border-main icon-search" value="搜索"/>
								</li>
							</c:if>
							<c:if test="${ menu.name eq '下载' or menu.name eq '导出EXCEL' and menu.delFlag eq 0}">
								<li>
									<a class="button border-main icon-search" target="_blank" href="${pageContext.request.contextPath}${ menu.href }${curentSearch}">${menu.name }</a>
								</li>
							</c:if>
						</c:forEach>
					</ul>
				</form>
			</c:if>
		</div>
		<table class="table table-hover text-center">
			<tr>
				<th>农合证号</th>
				<th>发票编号</th>
				<th>姓名</th>
				<th>家庭编号</th>
				<th>身份证号</th>
				<th>交费金额</th>
				<th>交费年度</th>
				<th>交费时间</th>
				<th>登记员</th>
				<!--  <th width="200">操作</th> -->
			</tr>
			<!-- 华丽分割线 -->	
			<c:if test="${ !empty limitPage and !empty limitPage.data }">
				<c:forEach items="${limitPage.data }" var="model">
					<tr>
						<td><c:out value="${ model.nhCode }"/></td>
						<td><c:out value="${ model.fapiao }"/></td>
						<td><c:out value="${ model.name }"/></td>
						<td><c:out value="${ model.familyCode }"/></td>
						<td><c:out value="${ model.cardCode }"/></td>
						<td><c:out value="${ model.playPrice }"/></td>
						<td><c:out value="${ model.playYear.substring(0,4) }"/></td>
						<td><fmt:formatDate value="${ model.playDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><c:out value="${ model.userId }"/></td>
						<%--
							<td>
								<div class="button-group">
									<c:if test="${ !empty actionMenu }">
										<c:forEach items="${ actionMenu }" var="acMenu">
											<c:if test="${ acMenu.name eq '修改' and acMenu.delFlag eq 0}">
												<a class="button border-main" href="${pageContext.request.contextPath}${ acMenu.href }&familyCode=${model.familyCode}&tgId=${tgId}&tgName=${tgName}">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
											<c:if test="${ acMenu.name eq '删除' and acMenu.delFlag eq 0}">
												<a class="button border-red" href="javascript:;" onclick="return del('${pageContext.request.contextPath}${ acMenu.href }','${model.familyCode}')">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
										</c:forEach>
									</c:if>
								</div>
							</td>
						 --%>
					</tr>
				</c:forEach>
			</c:if>
			
			<!-- 分页条 -->
			<tr>
				<td colspan="10">
					<div class="pagelist">
						<c:if test="${!empty limitPage }">
							<p style="float:left;">当前显示第${ limitPage.begRow}至  ${limitPage.endRow}</p>
							<p style="float:left;">,一共${limitPage.totalRow }条,一共${limitPage.totalPage }页</p>
							<p style="float:left;">,当前显示第${limitPage.pageNo }页</p>
							<c:choose>
								<c:when test="${ limitPage.firstPage  }">
									<span onclick="alert('已经是第一页了！');" class="current">1</span>
								</c:when>
								<c:otherwise>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=1&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">首页</a>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.prePage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">上一页</a>
									<span onclick="alert('已是当前页了！');" class="current">${limitPage.pageNo }</span>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${ limitPage.totalPage gt 1 and limitPage.pageNo + 5 lt limitPage.totalPage }">
									<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.pageNo + 5 }" var="item">
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">${item }</a>												
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.totalPage }" var="item">
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">${item }</a>												
									</c:forEach>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${ limitPage.lastPage  }">
									<span onclick="alert('已经是最后一页了！');" class="current" >尾页</span>
								</c:when>
								<c:otherwise>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.nextPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">下一页</a>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.totalPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${pageInfos}">尾页</a>
								</c:otherwise>
							</c:choose>
						</c:if>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
		function checkedForm(){
			let start = $('#startDate').val();
			let end = $('#endDate').val();
			if(start.length > 0 || end.length>0){				
				let sDate = new Date(start);
				let eDate = new Date(end);
				let n = new Date();
				if(sDate > eDate){
					alert("起始时间不能大于结束时间！");
					return false;
				}
				if(eDate > n){
					alert("结束时间不能大于当前日期！");
					return false;
				}
				console.log(sDate, eDate,n);
			}
			return true;
		}
		//单个删除
		function del(uri,id){
			if(confirm("您确定要删除吗?这可能会带来不可描述的后果。")){
				$.ajax({
					type:'POST',
					url: uri, 
					dataType:'json',
					data:{
						id : id
					},
					async: false,
					success:function(data){//与服务器交互成功调用的回调函数
						//console.log(data);
						if(data != null && data.code == 200){
							alert(data.msg);
							window.location.reload();
						}else{
							alert(data.msg);
						}
					},
					error:function(data , c){
						alert("服务器异常");
					}
				});	
			}
		}
</script>
</body>
</html>