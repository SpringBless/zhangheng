<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改家庭档案-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改内容</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${editAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>	
				<input type="hidden" name="agricultural" value="${editModel.agricultural }"/>	
				<input type="hidden" name="familyNumber" value="${editModel.familyNumber }"/>	
				<div class="form-group">
					<div class="label">
						<label>县级编码：</label>
					</div>
					<div class="field">
						<input type="text" id="countyCode" value="${editModel.countyCode }" name="countyCode" readonly="readonly" class="input" data-validate="required:请输入县级编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>乡镇编码：</label>
					</div>
					<div class="field">
						<input type="text" id="townsCode" value="${editModel.townsCode }" name="townsCode" readonly="readonly" class="input" data-validate="required:请输入乡镇编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>村编码：</label>
					</div>
					<div class="field">
						<input type="text" id="villageCode" value="${editModel.villageCode }" name="villageCode" readonly="readonly" class="input" data-validate="required:请输入村编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>组编码：</label>
					</div>
					<div class="field">
						<input type="text" id="groupCode" value="${editModel.groupCode }" name="groupCode" readonly="readonly" class="input" data-validate="required:请输入组编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>家庭编码：</label>
					</div>
					<div class="field">
						<input type="text" id="familyCode" value="${editModel.familyCode }" name="familyCode" readonly="readonly" class="input" data-validate="required:请输入家庭编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>户属性：</label>
					</div>
					<%--
					<div class="field">
					    <input type="text" id="familyProperty" name="familyProperty" value="${editModel.familyProperty}" class="input" data-validate="required:请输入户属性" />
						<div class="tips"></div>
					</div>
					 --%>
					<div class="field">
					    <select id="familyProperty" name="familyProperty" class="input" data-validate="required:请输入户属性" >
					    	<c:if test="${!empty familyProperty }">
					    		<c:forEach items="${ familyProperty }" var="item">
						    		<c:choose>
							    		<c:when test="${editModel.familyProperty eq item }">
							    			<option value="<c:out value="${item }"/>" selected="selected"><c:out value="${item }"/></option>	
							    		</c:when>
							    		<c:otherwise>
											<option value="<c:out value="${item }"/>" ><c:out value="${item }"/></option>				    	
							    		</c:otherwise>
						    		</c:choose>
					    		</c:forEach>
					    	</c:if>
					    	<c:if test="${empty familyProperty }">				    	
								<option value="${editModel.familyProperty }" >${editModel.familyProperty }</option>
					    	</c:if>
						</select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>户主姓名：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" value="${editModel.householder }" name="householder" placeholder="请输入疾病名称" data-validate="required:请输入户主姓名" />
						<div class="tips"></div>
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="label">
						<label>家庭人口数：</label>
					</div>
					<div class="field">
					    <input type="number" class="input" value="${editModel.familyNumber }" name="familyNumber" placeholder="请输入家庭人口数" data-validate="required:请输入家庭人口数" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>农业人口数：</label>
					</div>
					<div class="field">
					    <input type="number" class="input" value="${editModel.agricultural }" name="agricultural" placeholder="请输入农业人口数" data-validate="required:请输入农业人口数" />
						<div class="tips"></div>
					</div>
				</div> -->
				<div class="form-group">
					<div class="label">
						<label>家庭住址：</label>
					</div>
					<div class="field">
						<textarea class="input" name="address" style="height:90px;">${editModel.address }</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		
	</script>
</body>
</html>