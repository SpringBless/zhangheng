<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:forEach items="${menu }" var="item1">
	 <c:choose>
	 	<c:when test="${item1.parentId eq 0 and item1.isShow eq 1}">
	 		<h2><c:out value="${item1.name }"/></h2>
	 		<div style="padding-left:30px;">
		 		<c:forEach items="${menu }" var="item2">
		 			<c:choose>
		 				<c:when test="${item2.parentId eq item1.id and item2.isShow eq 1}">
		 					<h6><c:out value="${item2.name }"/></h6>
		 				</c:when>
		 			</c:choose>
		 		</c:forEach>
	 		</div>
	 	</c:when>
	 </c:choose>
</c:forEach>
</body>
</html>