<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改参合缴费配置信息-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改内容</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${editAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>	
				<div class="form-group">
					<div class="label">
						<label>编号：</label>
					</div>
					<div class="field">
						<input type="text" value="${editModel.id }" name="id" readonly="readonly" class="input" data-validate="required:请输入" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>所属年份：</label>
					</div>
					<div class="field">
						<input type="number" id="years" value="${editModel.years }" name="years"  class="input" data-validate="required:请输入" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>缴费截至时间：</label>
					</div>
					<div class="field">
						<input type="text" id="endDate" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<fmt:formatDate value="${ editModel.endDate}" pattern="yyyy-MM-dd HH:mm:ss" />" name="endDate"  class="input" data-validate="required:请输入" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>金额/每人：</label>
					</div>
					<div class="field">
						<input type="number" value="${editModel.price }" name="price"  class="input" data-validate="required:请输入" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		$(document).ready(function(){
			$('#years').change(function(){
				let val = $(this).val();
				let len = $("#endDate").val();
				len = len.substring(0,4);
				if(val > len){
					alert('截止时间大于所属年份');
				}
				if(val < len){
					alert('截止时间小于所属年份');
				}
			});
			$('#endDate').change(function(){
				let val = $(this).val();
				let len = $('#years').val();
				val = val.substring(0,4);
				if(val > len){
					alert('截止时间大于所属年份');
				}
				if(val < len){
					alert('接值时间小于所属年份');
				}
			});
		});
	</script>
</body>
</html>