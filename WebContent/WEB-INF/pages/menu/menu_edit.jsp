<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改菜单-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改内容</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${editAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="id" value="${editMenu.id }"/>
				<input type="hidden" name="lavel" value="${editMenu.lavel }"/>				
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					  <div class="label">
					    	<label>上级菜单：</label>
					  </div>
					  <div class="field">
					    	<select name="parentId" class="input">
					    		<c:if test="${ !empty menuList }">
					    			<c:forEach items="${ menuList }" var="menu">
						    				<c:choose>
							    				<c:when test="${menu.id eq editMenu.parentId }">
													<option selected value='<c:out value="${ menu.id }"/>' >
							    				</c:when>
							    				<c:otherwise>
							    					<option value='<c:out value="${ menu.id }"/>' >
							    				</c:otherwise>
						    				</c:choose>
											<c:choose>
												<c:when test="${menu.lavel eq 0 }">
													|-&nbsp;<c:out value="设为一级菜单"/>
												</c:when>
												<c:when test="${menu.lavel eq 1 }">
													|-&nbsp;<c:out value="${ menu.name }"/>
												</c:when>
												<c:when test="${menu.lavel eq 2 }">
													|----&nbsp;<c:out value="${ menu.name }"/>
												</c:when>
												<c:when test="${menu.lavel eq 3 }">
													|-------&nbsp;<c:out value="${ menu.name }"/>
												</c:when>
												<c:when test="${menu.lavel eq 4 }">
													|----------&nbsp;<c:out value="${ menu.name }"/>
												</c:when>
											</c:choose>
										</option>
					    			</c:forEach>
					    		</c:if>
							</select>
					  </div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>菜单名称：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="name" value="${editMenu.name }" placeholder="请输入菜单名称" data-validate="required:请输入菜单名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>排序：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="sort" value="${editMenu.sort }" placeholder="请输入排序编号" data-validate="number:请输入排序编号" />
					    <div style="color:#5bc0de">*排序一般为：10的整数倍</div>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>目标地址：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" value="${editMenu.href }" name="href" placeholder="请输入目标地址"/>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>显示图标：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" value="${editMenu.icon }" name="icon" placeholder="请输入显示图标"/>
					    <div style="color:#5bc0de">*显示图标非必须</div>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label" >
						<label >当前状态：</label>
					</div>
					<div class="field" style="padding-top:5px;">
						<c:choose>
							<c:when test="${editMenu.delFlag eq 0 }">
								<input id="radio_01" type="radio" checked="checked" value="${editMenu.delFlag }" name="delFlag"/>
								<label for="radio_01" style="color:#5bc0de"><c:out value="正常"/></label>&nbsp;&nbsp;
								<input id="radio_02" type="radio" value="1" name="delFlag"/>
								<label for="radio_02" style="color:#ac2925"><c:out value="停用"/></label>
							</c:when>
							<c:otherwise>
								<input id="radio_01" type="radio" value="0" name="delFlag"/>
								<label for="radio_01" style="color:#5bc0de"><c:out value="正常"/></label>&nbsp;&nbsp;
								<input id="radio_02" checked="checked" type="radio" value="${editMenu.delFlag }" name="delFlag"/>
								<label for="radio_02" style="color:#ac2925"><c:out value="停用"/></label>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>打开方式：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="target" value="${editMenu.target }" placeholder="请输入打开方式" data-validate="required:请输入排序编号"/>
					    <div style="color:#5bc0de">*打开方式一般为：_self(当前),_blank(新标签页),right(系统内部)</div>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>备注信息：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="remarks" style=" height:90px;">${editMenu.remarks }</textarea>
		          		<div class="tips" title=""></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="float:right;width:30%;min-width:350px;">
			<p>当前权限树</p>
			<ul id="treeDemo" class="ztree"></ul>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		var setting = {
			check: {
				enable: true,
				nocheckInherit: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};

		var zNodes = JSON.parse('${menuTree}');
		
		function nocheckNode(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nocheck = e.data.nocheck,
			nodes = zTree.getSelectedNodes();
			if (nodes.length == 0) {
				alert("请先选择一个节点");
			}

			for (var i=0, l=nodes.length; i<l; i++) {
				nodes[i].nocheck = nocheck;
				zTree.updateNode(nodes[i]);
			}
		};
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			$("#treeDemo a").each(function(index, el){
	    		$(this).attr({"href" : "javascript:;"});
	    		$(this).removeAttr("target");
	    	});
			//$("#nocheckTrue").bind("click", {nocheck: true}, nocheckNode);
			//$("#nocheckFalse").bind("click", {nocheck: false}, nocheckNode);
		});
	</script>
</body>
</html>