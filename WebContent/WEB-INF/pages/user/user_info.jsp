<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>个人信息-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|个人信息</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<div class="form-x">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>用户名称：</label>
					</div>
					<div class="field">
					    <p class="input" ><c:out value="${SESSION_USER.name }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>登录账号：</label>
					</div>
					<div class="field">
					    <p class="input" ><c:out value="${SESSION_USER.loginName }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>用户编号：</label>
					</div>
					<div class="field">
						<p class="input" ><c:out value="${SESSION_USER.no }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>电话：</label>
					</div>
					<div class="field">
					    <p class="input" ><c:out value="${SESSION_USER.phone }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>邮箱：</label>
					</div>
					<div class="field">
					    <p class="input" ><c:out value="${SESSION_USER.email }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label" >
						<label >当前状态：</label>
					</div>
					<div class="field" style="padding-top:8px;">
						<c:choose>
							<c:when test="${SESSION_USER.delFlag eq 0 }">
								<span style="color:#5bc0de"><c:out value="正常"/></span>
							</c:when>
							<c:otherwise>
								<label style="color:#ac2925"><c:out value="停用"/></label>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>备注信息：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="remarks" style=" height:90px;">${editUser.remarks }</textarea>
		          		<div class="tips" title=""></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>授予角色：</label>
					</div>
					<div class="field input" style="padding-top: 10px;">
						<c:if test="${ !empty curentRoles }">
							<c:forEach items="${ curentRoles }" var="role">
								<span style="float:left;color:#5bc0de;border:1px solid #5bc0de;padding:3px;margin-right:10px;">
									<c:out value="${role.name }"/>
								</span>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</body>
</html>