<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加用户-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加用户</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${addUserAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>用户名称：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="name" placeholder="请输入用户名称" data-validate="required:请输入用户名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>登录账号：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="loginName" placeholder="请输入登录账号" data-validate="required:请输入登录账号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>登录密码：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="password" placeholder="请输入登录密码" data-validate="required:请输入登录密码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>用户编号：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="no" placeholder="请输入用户编号" data-validate="required:请输入用户编号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>所在区域：</label>
					</div>
					<div class="field">
						<c:choose>
							<c:when test="${ not empty areas }">
								<select class="input" name="officeId">
									<c:forEach items="${areas }" var="item">
										<option value="${item.areacode }">${item.areaname }</option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<span class="input">当前还没有行政区域</span>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>电话：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="phone" placeholder="请输入电话" data-validate="required:请输入电话" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>邮箱：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="email" placeholder="请输入邮箱" data-validate="required:请输入邮箱" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>备注信息：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="remarks" style=" height:90px;"></textarea>
		          		<div class="tips" title=""></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>授予角色：</label>
					</div>
					<div class="field input" style="padding-top: 10px;">
						<c:if test="${ !empty roleAll }">
							<c:forEach items="${ roleAll }" var="role">
								<span style="float:left;">
							    	<input type="checkbox" id='ch_<c:out value="${role.id }"/>' name="roleIds" value='<c:out value="${role.id }"/>'/>
							    	<label style="margin-right: 30px;" for="ch_<c:out value="${role.id }"/>"><c:out value="${role.name }"/></label>
								</span>
							</c:forEach>
						</c:if>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">

	</script>
</body>
</html>