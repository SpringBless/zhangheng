<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改个人信息-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改个人信息</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${changeUserAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="id" value="${SESSION_USER.id }" />
				<input type="hidden" name="officeId" value="${SESSION_USER.officeId }" />
				<input type="hidden" name="companyId" value="${SESSION_USER.companyId }" />
				<input type="hidden" name="password" value="${SESSION_USER.password }" />
				<input type="hidden" name="loginName" value="${SESSION_USER.loginName }" />
				<input type="hidden" name="delFlag" value="${SESSION_USER.delFlag }" />
				<input type="hidden" name="no" value="${SESSION_USER.no}" />
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>用户名称：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" value="${SESSION_USER.name }" name="name"  placeholder="请输入用户名称" data-validate="required:请输入用户名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>登录账号：</label>
					</div>
					<div class="field">
						<p class="input" onclick="alert('该属性不能修改');" ><c:out value="${SESSION_USER.loginName }"/></p>
					    <div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>登录密码：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="nPassword" placeholder="如需更改密码请输入" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>用户编号：</label>
					</div>
					<div class="field">
					    <p class="input" onclick="alert('该属性不能修改');"><c:out value="${SESSION_USER.no }"/></p>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>电话：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="phone" value="${SESSION_USER.phone }" placeholder="请输入电话" data-validate="required:请输入电话" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>邮箱：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="email" value="${SESSION_USER.email }" placeholder="请输入邮箱" data-validate="required:请输入邮箱" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label" >
						<label >当前状态：</label>
					</div>
					<div class="field" style="padding-top:8px;">
						<c:choose>
							<c:when test="${SESSION_USER.delFlag eq 0 }">
								<span style="color:#5bc0de"><c:out value="正常"/></span>
							</c:when>
							<c:otherwise>
								<label style="color:#ac2925"><c:out value="停用"/></label>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>备注信息：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="remarks" style=" height:90px;">${SESSION_USER.remarks }</textarea>
		          		<div class="tips" title=""></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
</body>
</html>