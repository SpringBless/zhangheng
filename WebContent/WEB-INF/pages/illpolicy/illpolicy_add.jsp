<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加慢性病政策-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加内容</strong></div>
		<div class="body-content">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${addAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>疾病编码：</label>
					</div>
					<div class="field">
						<c:choose>
							<c:when test="${!empty list }">							
								<select class="input" name="illcode" placeholder="请输入疾病编码" data-validate="required:请输入疾病编码" >
									<c:forEach items="${list }" var="item">
										<option value='<c:out value="${item.illcode }"/>'><c:out value="${item.illcode }" />--<c:out value="${item.illname }"/></option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
						    	<input type="text" class="input" name="illcode" placeholder="请输入疾病编码" data-validate="required:请输入疾病编码" />								
							</c:otherwise>
						</c:choose>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>报销年份：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="cyear" placeholder="请输入报销年份" data-validate="required:请输入报销年份" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>封顶线：</label>
					</div>
					<div class="field">
					    <input type="number" class="input" name="topLine" placeholder="请输入封顶线" data-validate="required:请输入封顶线" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>报销比例：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="cprop" placeholder="请输入报销比例" data-validate="required:请输入报销比例" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
</body>
</html>