<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 403拒绝访问</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="${pageContext.request.contextPath}/static/third/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/code/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/code/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/code/code.css?v=4.1.0" rel="stylesheet">
</head>
<body>
    <div class="middle-box text-center animated fadeInDown">
        <h1>403</h1>
        <h3 class="font-bold">服务器拒绝访问</h3>
        <div class="error-desc">
            您好像没有访问的权限...
            <br/>您可以返回主页看看
            <br/><a href="${pageContext.request.contextPath}/login.do?method=login" class="btn btn-primary m-t">主页</a>
        </div>
    </div>
</body>
</html>