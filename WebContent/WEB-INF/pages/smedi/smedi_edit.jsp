<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改医疗卫生机构-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改内容</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${editAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>卫生机构编码：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" readonly="readonly" name="mcode" value="${editModel.mcode }" placeholder="请输入卫生机构编码" data-validate="required:请输入卫生机构编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>卫生机构名称：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="mname" value="${editModel.mname }" placeholder="请输入卫生机构名称" data-validate="required:请输入卫生机构名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>卫生机构地址：</label>
					</div>
					<div class="field">
						<textarea name="address" class="input" style=" height:90px;" placeholder="请输入卫生机构名称" data-validate="required:请输入卫生机构名称" >${editModel.address }</textarea>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
</body>
</html>