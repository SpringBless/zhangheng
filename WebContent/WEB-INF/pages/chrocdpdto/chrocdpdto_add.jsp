<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加慢性病证-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/autocomplete/jquery.autocomplete.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/autocomplete/jquery.autocomplete.js"></script>  
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
	<script type="text/javascript">
		$(document).ready(function() {
			//姓名自动匹配
			var urlUname="${pageContext.request.contextPath}${queryUserByAJAX}";
			
		    $("#userName").autocomplete(urlUname, { 
			   dataType:"json",
			   type: "post",
		       max: 50, //列表里的条目数 
		       minChars: 0, //自动完成激活之前填入的最小字符 
		       scrollHeight: 200, //提示的高度
		       matchContains: true, //包含匹配
		       autoFill: false, //自动填充
			   extraParams: {"name":$("#userName").val()},
			   parse: function(data) { 
				   console.log(data);
		               var rows = [];
		               for(var i=0; i<data.length; i++){	
		                 rows[rows.length] = 
		                 { data:data[i],
		                   result:data[i].address,
		                   value:data[i].name
		                 };
		               }           
		               return rows;
		           }, 
		       formatMatch: function(row, i, max) { 
		          return row.name ; 
		       },  
		       formatItem: function(row, i, max) { 
			      return  row.address+"-"+row.name; 
		       }
		    }).result(function(event, row, formatted) { 
			   $("#userName").val(row.name);
			   $("#cpIdCard").val(row.cardCode);
			   $("#cpNhCode").val(row.nhCode);
		    });
		
			var urlB="${pageContext.request.contextPath}${queryBinByAJAX}";
	    	$("#cpNames").autocomplete(urlB, { 
			   dataType:"json",
			   type: "post",
		       max: 50, //列表里的条目数 
		       minChars: 0, //自动完成激活之前填入的最小字符 
		       scrollHeight: 200, //提示的高度
		       matchContains: true, //包含匹配
		       autoFill: false, //自动填充
			   parse: function(data) { 
				   console.log(data);
		               var rows = [];
		               for(var i=0; i<data.length; i++){	
		                 rows[rows.length] = 
		                 { data:data[i],
		                   result:data[i].illcode,
		                   value:data[i].illname
		                 };
		               }           
		               return rows;
		           }, 
		       formatMatch: function(row, i, max) { 
		          return row.illname ; 
		       },  
		       formatItem: function(row, i, max) { 
			      return  row.illname; 
		       }
		    }).result(function(event, row, formatted) { 
			   $("#cpNames").val(row.illname);
		    }); 
		});
		
		function checkSubmit(el){
			let cpIdCard = $("#cpIdCard").val();
			let cpNhCode = $("#cpNhCode").val();
			if($.trim(cpIdCard).length <= 0 ){
				alert("当前还未选择");
				return false;
			}
			if($.trim(cpNhCode).length <= 0 ){
				alert("当前还未选择");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加内容</strong></div>
		<div class="body-content">
			<form method="post" onsubmit="return checkSubmit(this);" class="form-x" action="${pageContext.request.contextPath}${addModleAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<input type="hidden" id="cpIdCard" name="cpIdCard" value=""/>
				<input type="hidden" id="cpNhCode" name="cpNhCode" value=""/>
				<div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
					    <input type="text" id="userName" class="input" placeholder="请输入姓名" data-validate="required:请输入姓名" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>慢病名称：</label>
					</div>
					<div class="field">
					    <input type="text" id="cpNames" class="input" name="cpNames" placeholder="请输入慢病名称" data-validate="required:请输入慢病名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>起始时间：</label>
					</div>
					<div class="field">
					    <input type="text" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="input" name="cpStart" placeholder="请输入起始时间" data-validate="required:请输入起始时间" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>结束时间：</label>
					</div>
					<div class="field">
					    <input type="text" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="input" name="cpEnd" placeholder="请输入结束时间" data-validate="required:请输入结束时间" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
</body>
</html>