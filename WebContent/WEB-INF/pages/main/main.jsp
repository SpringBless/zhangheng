<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>  
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/static/images/logo.jpg">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
</head>
<body style="background-color:#f2f9fd;">

	<div class="header bg-main">
		<div class="logo margin-big-left fadein-top">
			<h1><img src="${pageContext.request.contextPath}/static/images/logo.jpg" class="radius-circle rotate-hover" height="50" alt="" />慢性病报销系统</h1>
		</div>
		<div class="head-l" >
			<a href="" target="_blank" style="color:#FFF" ><span class="icon-user"></span> 欢迎 ${SESSION_USER.name }</a>&nbsp;&nbsp;
			<a class="button button-little bg-green" href="" target="_SELF" ><span class="icon-home"></span> 首页</a> &nbsp;&nbsp;
			<a class="button button-little bg-red" href="${pageContext.request.contextPath}/login.do?method=logout"><span class="icon-power-off"></span> 退出登录</a> 
		</div>
	</div>
	<div class="leftnav">
		<div class="leftnav-title">
			<strong><span class="icon-list"></span>菜单列表</strong>
		</div>
		
		<!-- 迭代菜单 -->
		<c:choose>
			<c:when test="${!empty sessionScope.SESSION_MENU }">
				<c:forEach items="${sessionScope.SESSION_MENU }" var="menu1">
				 	<c:if test="${menu1.parentId eq 1 and menu1.isShow eq 1}">
						<h2><span class="${menu1.icon }"></span><c:out value="${menu1.name }"/></h2>
						<ul>
					 		<c:forEach items="${sessionScope.SESSION_MENU }" var="menu2">
				 				<c:if test="${menu2.parentId eq menu1.id and menu2.isShow eq 1}">
									<li>
										<a href="${pageContext.request.contextPath}${menu2.href}&tgId=${menu2.id}&tgName=${menu2.name}" target="${ menu2.target }">
											<span class="${ menu2.icon }"></span>
											<c:out value="${menu2.name }"/>
										</a>
									</li>
				 				</c:if>
					 		</c:forEach>
						</ul>
				 	</c:if>
				</c:forEach> 
			</c:when>
			<c:otherwise>
				<h2 onclick="alert('您还没有权限，请联系管理员');"><span class="icon-pencil-square-o" ></span><c:out value="您还没有权限，请联系管理员"/></h2>
			</c:otherwise>
		</c:choose>
	</div>
	
	<script type="text/javascript">
		$(function(){
		  $(".leftnav ul").each(function(index, item){
			  if(index == 0){
				  $(item).css({"display" : "block"});
			  }
		  });
		  $(".leftnav h2").click(function(){
			  $(this).next().slideToggle(200);	
			  $(this).toggleClass("on"); 
		  })
		  $(".leftnav ul li a").click(function(){
			    $("#a_leader_txt").text($(this).text());
		  		$(".leftnav ul li a").removeClass("on");
				$(this).addClass("on");
		  })
		});
	</script>
	
	<ul class="bread">
		<li><a href="{:U('Index/info')}" target="right" class="icon-home"> 首页</a></li>
		<li><a href="##" id="a_leader_txt">欢迎界面</a></li>
		<li><b>当前语言：</b><span style="color:red;">中文</php></span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;切换语言：<a href="##">中文</a> &nbsp;&nbsp;<a href="##">英文</a> </li>
	</ul>
	
	<div class="admin">
		<iframe scrolling="auto" rameborder="0" src="" name="right" width="100%" height="100%"></iframe>
	</div>
	
</body>
</html>