<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>修改家庭成员档案-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
		.form-x .form-group .label{width:14%;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|修改内容</strong></div>
		<div class="body-content">
			<form method="post" class="form-x" action="${pageContext.request.contextPath}${editAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<input type="hidden" name="oldHk" value="${model.hkType eq '是' ? '1' : '0' }"/>
				<div class="form-group">
					<div class="label">
						<label>家庭编码：</label>
					</div>
					<div class="field">
						<input id="familyCode" name="familyCode" class="input" value="${model.familyCode }" readonly="readonly" data-validate="required:请输入县级编码" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>农合证号：</label>
					</div>
					<div class="field">
						<input name="nhCode" class="input" value="${model.nhCode }" readonly="readonly" data-validate="required:请输入农合证号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>医疗证卡号：</label>
					</div>
					<div class="field">
						<input name="ylCode" class="input" value="${model.ylCode }" readonly="readonly" data-validate="required:请输入医疗证卡号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>户内编号：</label>
					</div>
					<div class="field">
						<input name="hnCode" class="input" value="${model.hnCode }" readonly="readonly"/>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>姓名：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="name" value="${model.name }"  placeholder="请输入姓名" data-validate="required:请输入姓名" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>与户主关系：</label>
					</div>
					<div class="field">
					    <select id="fmRelation" class="input" name="fmRelation" data-value="${model.fmRelation }"  placeholder="请输入家庭人口数" data-validate="required:请输入与户主关系" >
					    	<option></option>
					    </select>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>身份证号：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" id="cardCode" name="cardCode" value="${model.cardCode }" readonly="readonly"  placeholder="请输入身份证号" data-validate="required:请输入身份证号" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>性别：</label>
					</div>
					<div class="field">
						<select id="gender" class="input" name="gender" data-value="${model.gender }"  placeholder="请输入性别" data-validate="required:请输入性别" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>健康状况：</label>
					</div>
					<div class="field">
						<select id="sfHealth" class="input" name="sfHealth" data-value="${model.sfHealth }" placeholder="请输入健康状况" data-validate="required:请输入健康状况" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>民族：</label>
					</div>
					<div class="field">
						<select id="sfNational" class="input" name="sfNational" data-value="${model.sfNational }" placeholder="请输入民族" data-validate="required:请输入民族" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>文化程度：</label>
					</div>
					<div class="field">
						<select id="sfEducation" class="input" name="sfEducation" data-value="${model.sfEducation }" placeholder="请输入文化程度" data-validate="required:请输入文化程度" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>人员属性：</label>
					</div>
					<div class="field">
						<select id="rAttribute" class="input" name="rAttribute" data-value="${model.rAttribute }" placeholder="请输入人员属性"  >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>是否农村户口：</label>
					</div>
					<div class="field">
						<select id="hkType" class="input" name="hkType" data-value="${model.hkType }" placeholder="请输入是否农村户口" data-validate="required:请输入是否农村户口" >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>职业：</label>
					</div>
					<div class="field">
						<select id="sfProfessional" class="input" data-value="${model.sfProfessional }" name="sfProfessional" placeholder="请输入职业"  >
					    	<option></option>
					    </select>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>出生日期：</label>
					</div>
					<div class="field">
					    <input type="date" readonly="readonly" class="input" name="birthday" value="${model.birthday }" placeholder="请输入出生日期" data-validate="required:请输入出生日期" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>工作单位：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="workunits" value="${model.workunits }" placeholder="请输入工作单位" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>联系电话：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="phone" value="${model.phone }" placeholder="请输入联系电话" data-validate="required:请输入联系电话" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>常住地址：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="address" placeholder="请输入常住地址" data-validate="required:请输入常住地址" >${model.address }</textarea>
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		var fn = {
				CARD_CODE_URL: '${pageContext.request.contextPath}${cardCodeAction}',
				URL : '${pageContext.request.contextPath}${initCodeURL}',
				initCode : function(datas,callBack){
					this.ajaxRequest(this.URL,datas,function(res){
						if(res != null && Array.isArray(res)){
							var html = '<option value="0">请选择</option>';
							for(var i = 0; i < res.length; i++){
								html += '<option value='+res[i]+'>'+res[i]+'</option>';
							}
							callBack && callBack.call(this,html);
						}
					});
				},
				ajaxRequest : function(url,datas,callBack){
					var This = this;
					$.ajax({
						type:'POST',
						url: url, 
						dataType:'json',
						data:datas,
						async: false,
						success:function(res){//与服务器交互成功调用的回调函数
							console.log(res);
							callBack && callBack.call(This,res);
						},
						error:function(data , c){
							alert("服务器异常");
						}
					});	
				},
				falg : true,
				timer : null,
				check : function(el){
					return fn.flag ? true : false;
				}
		};
		$(document).ready(function(){
			//$("#cardCode").blur(function(){
			//	var value = $(this).val();
			//	fn.ajaxRequest(fn.CARD_CODE_URL,{"cardCode":value},function(res){
			//		fn.flag = !res.flag;
			//		if(!fn.flag){
			//			alert('该身份证号已存在');
			//		}
			//	});
			//});
			// 与户主关系
			fn.initCode({column:"fm_relation"}, function(html){
				$("#fmRelation").html(html);
				var val = $("#fmRelation").attr("data-value");
				$("#fmRelation option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			
			fn.initCode({column : "sf_professional"},function(html){
				$("#sfProfessional").html(html);
				var val = $("#sfProfessional").attr("data-value");
				$("#sfProfessional option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			// 性别
			fn.initCode({column:"gender"}, function(html){
				$("#gender").html(html);
				var val = $("#gender").attr("data-value");
				$("#gender option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
		
			//健康状况
			fn.initCode({column:"sf_health"}, function(html){
				$("#sfHealth").html(html);
				var val = $("#sfHealth").attr("data-value");
				$("#sfHealth option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			// 民族
			fn.initCode({column:"sf_national"}, function(html){
				$("#sfNational").html(html);
				var val = $("#sfNational").attr("data-value");
				$("#sfNational option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			// 文化程度
			fn.initCode({column:"sf_education"}, function(html){
				$("#sfEducation").html(html);
				var val = $("#sfEducation").attr("data-value");
				$("#sfEducation option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			// 人员属性
			fn.initCode({column:"r_attribute"}, function(html){
				$("#rAttribute").html(html);
				var val = $("#rAttribute").attr("data-value");
				$("#rAttribute option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
			// 是否是农村户口
			fn.initCode({column:"hk_type"}, function(html){
				$("#hkType").html(html);
				var val = $("#hkType").attr("data-value");
				$("#hkType option").each(function(idx, el){
					if($(el).val() == val){
						$(el).attr({"selected":"selected"});
					}
				});
			});
		});
	</script>
</body>
</html>