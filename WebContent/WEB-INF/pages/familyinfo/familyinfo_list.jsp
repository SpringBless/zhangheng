<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>家庭档案管理列表-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
</head>
<body>
	<form method="post" action="" id="listform">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left: 10px;">
					<c:if test="${ !empty actionMenu }">
						<c:forEach items="${ actionMenu }" var="menu">
							<c:if test="${ menu.name eq '添加' or menu.name eq '增加'}">
								<li>
									<a class="button border-main icon-plus-square-o" href="${pageContext.request.contextPath}${ menu.href }&tgId=${tgId}&tgName=${tgName}">&nbsp;${ menu.name }</a>
								</li>
							</c:if>
						</c:forEach>
					</c:if>
					<c:if test="${ !empty curentMenu }">
						<c:forEach items="${ curentMenu }" var="menu">
							<c:if test="${ menu.name eq '添加' or menu.name eq '增加'}">
								<li>
									<a class="button border-main icon-plus-square-o" href="${pageContext.request.contextPath}${ menu.href }&tgId=${tgId}&tgName=${tgName}">&nbsp;${ menu.name }</a>
								</li>
							</c:if>
							<c:if test="${ menu.name eq '成员列表' and menu.delFlag eq 0}">
								<li>搜索：</li>
								<li>
									<input type="text" placeholder="请输入搜索关键字" id="keywords" name="keywords" class="input" style="width: 250px; line-height: 17px; display: inline-block" />
									<a href="javascript:void(0)" class="button border-main icon-search" 
										onclick="fn.changesearch('${pageContext.request.contextPath}${ menu.href }&tgId=${menu.id}&tgName=${menu.name}')">搜索</a>
								</li>
							</c:if>
						</c:forEach>
					</c:if>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th>家庭编号</th>
					<th>农合证号</th>
					<th>医疗证卡号</th>
					<th>户内编号</th>
					<th>姓名</th>
					<th>与户主关系</th>
					<th>身份证号</th>
					<th>性别</th>
					<th>年龄</th>
					<th>健康状况</th>
					<th>民族</th>
					<th>文化程度</th>
					<th>出生日期</th>
					<th>人员属性</th>
					<th>是否农村户口</th>
					<th>职业</th>
					<th>工作单位</th>
					<th>联系电话</th>
					<th>常住地址</th>
					<th width="200">操作</th>
				</tr>
				<!-- 华丽分割线 -->	
				<c:if test="${ !empty limitPage and !empty limitPage.data }">
					<c:forEach items="${limitPage.data }" var="model">
						<tr>
							<td><c:out value="${ model.familyCode }"/></td>
							<td><c:out value="${ model.nhCode }"/></td>
							<td><c:out value="${ model.ylCode }"/></td>
							<td><c:out value="${ model.hnCode }"/></td>
							<td><c:out value="${ model.name }"/></td>
							<td><c:out value="${ model.fmRelation }"/></td>
							<td><c:out value="${ model.cardCode }"/></td>
							<td><c:out value="${ model.gender }"/></td>
							<td><c:out value="${ model.age }"/></td>
							<td><c:out value="${ model.sfHealth }"/></td>
							<td><c:out value="${ model.sfNational }"/></td>
							<td><c:out value="${ model.sfEducation }"/></td>
							<td><c:out value="${ model.birthday }"/></td>
							<td><c:out value="${ model.rAttribute }"/></td>
							<td><c:out value="${ model.hkType }"/></td>
							<td><c:out value="${ model.sfProfessional }"/></td>
							<td><c:out value="${ model.workunits }"/></td>
							<td><c:out value="${ model.phone }"/></td>
							<td><c:out value="${ model.address }"/></td>
							<td>
								<div class="button-group">
									<c:if test="${ !empty actionMenu }">
										<c:forEach items="${ actionMenu }" var="acMenu">
											<c:if test="${ acMenu.name eq '修改' and acMenu.delFlag eq 0}">
												<a class="button border-main" href="${pageContext.request.contextPath}${ acMenu.href }&cardCode=${model.cardCode}&tgId=${tgId}&tgName=${tgName}">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
											<c:if test="${ acMenu.name eq '删除' and acMenu.delFlag eq 0}">
												<a class="button border-red" href="javascript:;" onclick="return del('${pageContext.request.contextPath}${ acMenu.href }','${model.cardCode}')">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
										</c:forEach>
									</c:if>
								</div>
							</td>
						</tr>
					</c:forEach>
				</c:if>
				
				<!-- 分页条 -->
				<tr>
					<td colspan="20">
						<div class="pagelist">
							<c:if test="${!empty limitPage }">
								<p style="float:left;">当前显示第${ limitPage.begRow}至  ${limitPage.endRow}</p>
								<p style="float:left;">,一共${limitPage.totalRow }条,一共${limitPage.totalPage }页</p>
								<p style="float:left;">,当前显示第${limitPage.pageNo }页</p>
								<c:choose>
									<c:when test="${ limitPage.firstPage  }">
										<span onclick="alert('已经是第一页了！');" class="current">1</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=1&pageSize=10&tgId=${tgId}&tgName=${tgName}">首页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.prePage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">上一页</a>
										<span onclick="alert('已是当前页了！');" class="current">${limitPage.pageNo }</span>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.totalPage gt 1 and limitPage.pageNo + 5 lt limitPage.totalPage }">
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.pageNo + 5 }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}">${item }</a>												
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.totalPage }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}">${item }</a>												
										</c:forEach>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.lastPage  }">
										<span onclick="alert('已经是最后一页了！');" class="current" >尾页</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.nextPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">下一页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.totalPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">尾页</a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
	<script type="text/javascript">
		var fn = {
				changesearch : function(url){
					var val = $("#keywords").val();
					if($.trim(val+"").length>0){
						open(url + "&keywords=" + val, "_self");
					}
				}
		};
		//单个删除
		function del(uri,id){
			if(confirm("您确定要删除吗?这可能会带来不可描述的后果。")){
				$.ajax({
					type:'POST',
					url: uri, 
					dataType:'json',
					data:{
						id : id
					},
					async: false,
					success:function(data){//与服务器交互成功调用的回调函数
						//console.log(data);
						if(data != null && data.code == 200){
							alert(data.msg);
							window.location.reload();
						}else{
							alert(data.msg);
						}
					},
					error:function(data , c){
						alert("服务器异常");
					}
				});	
			}
		}
</script>
</body>
</html>