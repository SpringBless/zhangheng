<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>角色列表-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
</head>
<body>
	<form method="post" action="" id="listform">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
			</div>
			<div class="padding border-bottom">
				<ul class="search" style="padding-left: 10px;">
					<c:if test="${ !empty actionMenu }">
						<c:forEach items="${ actionMenu }" var="menu">
							<c:if test="${ menu.name eq '添加' or menu.name eq '增加'}">
								<li>
									<a class="button border-main icon-plus-square-o" href="${pageContext.request.contextPath}${ menu.href }&tgId=${tgId}&tgName=${tgName}">&nbsp;${ menu.name }</a>
								</li>
							</c:if>
						</c:forEach>
					</c:if>
					<%--
						<li>搜索：</li>
						<li>首页 
							<select name="s_ishome" class="input" onchange="changesearch()" style="width: 60px; line-height: 17px; display: inline-block">
								<option value="">选择</option>
								<option value="1">是</option>
								<option value="0">否</option>
							</select> 
							&nbsp;&nbsp; 推荐 
							<select name="s_isvouch" class="input" onchange="changesearch()" style="width: 60px; line-height: 17px; display: inline-block">
								<option value="">选择</option>
								<option value="1">是</option>
								<option value="0">否</option>
							</select> 
							&nbsp;&nbsp; 置顶 
							<select name="s_istop" class="input" onchange="changesearch()" style="width: 60px; line-height: 17px; display: inline-block">
								<option value="">选择</option>
								<option value="1">是</option>
								<option value="0">否</option>
							</select>
						</li>
						<li>
							<input type="text" placeholder="请输入搜索关键字" name="keywords" class="input" style="width: 250px; line-height: 17px; display: inline-block" />
							<a href="javascript:void(0)" class="button border-main icon-search" onclick="changesearch()">搜索</a>
						</li>
					 --%>
				</ul>
			</div>
			<table class="table table-hover text-center">
				<tr>
					<th width="100" style="text-align: left; padding-left: 20px;">ID</th>
					<th>角色名称</th>
					<th>描述</th>
					<th width="310">操作</th>
				</tr>
			
				<!-- 华丽分割线 -->	
				<c:if test="${ !empty limitPage }">
					<c:forEach items="${limitPage.data }" var="role">
						<tr>
							<td style="text-align: left; padding-left: 20px;">
								<input type="checkbox" name="id[]" value="<c:out value="${ role.id }"/>" /><c:out value="${ role.id }"/></td>
							<td>
								<c:out value="${ role.name }"/>
							</td>
							<td>
								<c:choose>
									<c:when test="${ !empty role.remarks }"><c:out value="${ role.remarks }"/></c:when>
									<c:otherwise>
										<c:out value="暂无"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<div class="button-group">
									<c:if test="${ !empty actionMenu }">
										<c:forEach items="${ actionMenu }" var="acMenu">
											<c:if test="${ acMenu.name eq '修改' and acMenu.delFlag eq 0}">
												<a class="button border-main" href="${pageContext.request.contextPath}${ acMenu.href }&id=${role.id}&tgId=${tgId}&tgName=${tgName}">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
											<c:if test="${ acMenu.name eq '删除' and acMenu.delFlag eq 0}">
												<a class="button border-red" href="javascript:;" onclick="return del('${pageContext.request.contextPath}${ acMenu.href }',${role.id})">
													<span class="${acMenu.icon }"></span>&nbsp;${ acMenu.name }
												</a>
											</c:if>
										</c:forEach>
									</c:if>
								</div>
							</td>
						</tr>
					</c:forEach>
				</c:if>
				
				<!-- 华丽分割线 -->
				<tr>
					<td style="text-align: left; padding: 19px 0; padding-left: 20px;"><input
						type="checkbox" id="checkall" /> 全选</td>
					<td colspan="3" style="text-align: left; padding-left: 20px;"><a
						href="javascript:void(0)" class="button border-red icon-trash-o"
						style="padding: 5px 15px;" onclick="DelSelect()"> 删除</a> <a
						href="javascript:void(0)"
						style="padding: 5px 15px; margin: 0 10px;"
						class="button border-blue icon-edit" onclick="sorts()"> 排序</a> 操作：
						<select name="ishome"
						style="padding: 5px 15px; border: 1px solid #ddd;"
						onchange="changeishome(this)">
							<option value="">首页</option>
							<option value="1">是</option>
							<option value="0">否</option>
					</select> <select name="isvouch"
						style="padding: 5px 15px; border: 1px solid #ddd;"
						onchange="changeisvouch(this)">
							<option value="">推荐</option>
							<option value="1">是</option>
							<option value="0">否</option>
					</select> <select name="istop"
						style="padding: 5px 15px; border: 1px solid #ddd;"
						onchange="changeistop(this)">
							<option value="">置顶</option>
							<option value="1">是</option>
							<option value="0">否</option>
					</select> &nbsp;&nbsp;&nbsp; 移动到： <select name="movecid"
						style="padding: 5px 15px; border: 1px solid #ddd;"
						onchange="changecate(this)">
							<option value="">请选择分类</option>
							<option value="">产品分类</option>
							<option value="">产品分类</option>
							<option value="">产品分类</option>
							<option value="">产品分类</option>
					</select> <select name="copynum"
						style="padding: 5px 15px; border: 1px solid #ddd;"
						onchange="changecopy(this)">
							<option value="">请选择复制</option>
							<option value="5">复制5条</option>
							<option value="10">复制10条</option>
							<option value="15">复制15条</option>
							<option value="20">复制20条</option>
					</select></td>
				</tr>
				
				<!-- 分页条 -->
				<tr>
					<td colspan="9">
						<div class="pagelist">
							<c:if test="${!empty limitPage }">
								<p style="float:left;">当前显示第${ limitPage.begRow}至  ${endRow}</p>
								<p style="float:left;">,一共${limitPage.totalRow }条,一共${limitPage.totalPage }页</p>
								<p style="float:left;">,当前显示第${limitPage.pageNo }页</p>
								<c:choose>
									<c:when test="${ limitPage.firstPage  }">
										<span onclick="alert('已经是第一页了！');" class="current">1</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=1&pageSize=10&tgId=${tgId}&tgName=${tgName}">首页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.prePage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">上一页</a>
										<span onclick="alert('已是当前页了！');" class="current">${limitPage.pageNo }</span>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.totalPage gt 1 and limitPage.pageNo + 5 lt limitPage.totalPage }">
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.pageNo + 5 }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}">${item }</a>												
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.totalPage }" var="item">
											<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}">${item }</a>												
										</c:forEach>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${ limitPage.lastPage  }">
										<span onclick="alert('已经是最后一页了！');" class="current" >尾页</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.nextPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">下一页</a>
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.totalPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}">尾页</a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
	<script type="text/javascript">

		//搜索
		function changesearch(){	
				
		}

		//单个删除
		function del(uri,id){
			if(confirm("您确定要删除吗?这可能会带来不可描述的后果。")){
				$.ajax({
					type:'POST',
					url: uri, 
					dataType:'json',
					data:{
						id : id
					},
					async: false,
					success:function(data){//与服务器交互成功调用的回调函数
						//console.log(data);
						if(data != null && data.code == 200){
							alert(data.msg);
							window.location.reload();
						}else{
							alert(data.msg);
						}
					},
					error:function(data , c){
						alert("服务器异常");
					}
				});	
			}
		}

		//全选
		$("#checkall").click(function(){ 
		  $("input[name='id[]']").each(function(){
			  if (this.checked) {
				  this.checked = false;
			  }
			  else {
				  this.checked = true;
			  }
		  });
		})

		//批量删除
		function DelSelect(){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){
				var t=confirm("您确认要删除选中的内容吗？");
				if (t==false) return false;		
				$("#listform").submit();		
			}
			else{
				alert("请选择您要删除的内容!");
				return false;
			}
		}

		//批量排序
		function sorts(){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){	
				
				$("#listform").submit();		
			}
			else{
				alert("请选择要操作的内容!");
				return false;
			}
		}


		//批量首页显示
		function changeishome(o){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){
				
				$("#listform").submit();	
			}
			else{
				alert("请选择要操作的内容!");		
			
				return false;
			}
		}

		//批量推荐
		function changeisvouch(o){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){
				
				
				$("#listform").submit();	
			}
			else{
				alert("请选择要操作的内容!");	
				
				return false;
			}
		}

		//批量置顶
		function changeistop(o){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){		
				
				$("#listform").submit();	
			}
			else{
				alert("请选择要操作的内容!");		
			
				return false;
			}
		}
		
		
		//批量移动
		function changecate(o){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){		
				
				$("#listform").submit();		
			}
			else{
				alert("请选择要操作的内容!");
				
				return false;
			}
		}
		
		//批量复制
		function changecopy(o){
			var Checkbox=false;
			 $("input[name='id[]']").each(function(){
			  if (this.checked==true) {		
				Checkbox=true;	
			  }
			});
			if (Checkbox){	
				var i = 0;
			    $("input[name='id[]']").each(function(){
			  		if (this.checked==true) {
						i++;
					}		
			    });
				if(i>1){ 
			    	alert("只能选择一条信息!");
					$(o).find("option:first").prop("selected","selected");
				}else{
				
					$("#listform").submit();		
				}	
			}
			else{
				alert("请选择要复制的内容!");
				$(o).find("option:first").prop("selected","selected");
				return false;
			}
		}
</script>
</body>
</html>