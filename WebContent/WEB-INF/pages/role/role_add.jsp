<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>添加角色-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/zTree_v3/js/jquery.ztree.excheck.js"></script>
	<style type="text/css">
		ul.ztree{background-color:#fff;height:auto;}
		.input{width:60%!important;}
	</style>
</head>
<body>
	<div class="panel admin-panel" style="padding-bottom:50px;">
		<div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>&nbsp;${tgName }|增加角色</strong></div>
		<div class="body-content" style="float:left;width:70%;min-width:500px;">
			<form method="post" class="form-x" onsubmit="return _fnAdd();" action="${pageContext.request.contextPath}${addRoleAction }">  
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input id="menuIds" name="menuIds" type="hidden" value="">
				<input type="hidden" name="tgName" value="${tgName }"/>
				<div class="form-group">
					<div class="label">
						<label>角色名称：</label>
					</div>
					<div class="field">
					    <input type="text" class="input" name="name" placeholder="请输入菜单名称" data-validate="required:请输入菜单名称" />
						<div class="tips"></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label>备注信息：</label>
					</div>
					<div class="field">
					    <textarea class="input" name="remarks" style=" height:90px;"></textarea>
		          		<div class="tips" title=""></div>
					</div>
				</div>
				<div class="form-group">
					<div class="label">
						<label></label>
					</div>
					<div class="field">
						<input class="button bg-main icon-check-square-o" type="submit" value="提交"/>
					</div>
				</div>
			</form>
		</div>
		<div style="float:right;width:30%;min-width:350px;">
			<p style="color:red;">请选择权限</p>
			<ul id="treeDemo" class="ztree"></ul>
		</div>
		<div style="clear:both;"></div>
	</div>
	${flag }
	<script type="text/javascript">
		var setting = {
			check : {
				enable: true,
				nocheckInherit: true
			},
		    view : {
		        selectedMulti: false
		    },
			data : {
				simpleData: {
					enable: true
				}
			}
		};

		var zNodes = JSON.parse('${menuTree}');
		$(document).ready(function(){
			var tree = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
			// 不选择父节点
	    	tree.setting.check.chkboxType = { "Y" : "ps", "N" : "s" };
			window._fnAdd = function fnAdd(){
	   			var ids = [], nodes = tree.getCheckedNodes(true);
	   			for(var i=0; i<nodes.length; i++) {
	   				ids.push(nodes[i].id);
	   			}
	   			console.log(ids)
	   			var flag = false;
	   			//if(ids.length <= 0){
				//	alert("当前未选择节点");
				//	return false;
				//}
	   			if(!flag){
	    			$("#menuIds").val(ids);
	    			return true;
	   			}
	   			return false;
	    	};
	    	$("#treeDemo a").each(function(index, el){
	    		$(this).attr({"href" : "javascript:;"});
	    		$(this).removeAttr("target");
	    	});
		});
	</script>
</body>
</html>