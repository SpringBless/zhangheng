<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
</head>
<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
		</div>
		<style type="text/css">
			.info .search{float:left;width:50%;margin-top:10px;margin-bottom:10px;}
		</style>
		<div class="padding border-bottom info">
			<c:if test="${not empty shenheModle }">
				<form onsubmit="return checkApply(this);" action="${pageContext.request.contextPath}${action }" method="post">
					<input type="hidden" name="tgId" value="${tgId }"/>
					<input type="hidden" name="tgName" value="${tgName }"/>
					<input type="hidden" name="aId" value="${shenheModle.aId }"/>
					<ul class="search" style="padding-left: 10px;">
						<li style="width:15%;">家庭编号:</li>
						<li style="width:70%;"><input type="text" class="input" name="aFamilyCode" value="${shenheModle.familyCode }" readonly="readonly"/></li>
					</ul>
					<ul class="search" style="padding-left: 10px;">
						<li style="width:15%;">农民姓名:</li>
						<li style="width:70%;"><input type="text" name="name" class="input" value="${shenheModle.name }" readonly="readonly"/></li>
					</ul>
					<ul class="search" style="padding-left: 10px;">
						<li style="width:15%;">身份证号:</li>
						<li style="width:70%;"><input type="text" class="input" name="aIdCard" value="${shenheModle.cardCode }" readonly="readonly"/></li>
					</ul>
					<ul class="search" style="padding-left: 10px;">
						<li style="width:15%;">现居住地:</li>
						<li style="width:70%;"><input type="text" class="input" value="${shenheModle.address }" readonly="readonly"/></li>
					</ul>
					<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">慢病名称:</li>
							<li style="width:70%;"><input type="text" value="${shenheModle.aIllname }" name="aIllname" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">报销金:</li>
							<li style="width:70%;"><input type="text" value="${shenheModle.aApply }" name="aApply" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">申请报销年份:</li>
							<li style="width:70%;"><input type="text" value="${shenheModle.aYear }" name="aYear" class="input" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">申请报销时间:</li>
							<li style="width:70%;"><input type="text" value="<fmt:formatDate value="${shenheModle.aCreateDate }" pattern="yyyy-MM-dd HH:mm:ss" />" name="aCreateDate" class="input" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">医院发票号:</li>
							<li style="width:70%;"><input type="text" readonly="readonly" value="${shenheModle.aFapiao }" name="aFapiao" class="input" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">就诊时间:</li>
							<li style="width:70%;"><input type="text" readonly="readonly" value="<fmt:formatDate value="${shenheModle.aSeeDate}" pattern="yyyy-MM-dd HH:mm:ss" />" name="aSeeDate" class="input"  /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">&nbsp;</li>
							<li style="width:70%;"><input type="submit" value="同意申请" class="button border-main icon-plus-square-o"  /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;"><a href="${pageContext.request.contextPath}${action}&tgName=${tgName }&tgId=${tgId }&aId=${shenheModle.aId }&type=1" class="button border-red" >回拒申请</a></li>
							<li style="width:70%;"></li>
						</ul>
					<hr/>
				</form>
			</c:if>	
		</div>
	</div>
</body>
</html>