<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
</head>
<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
		</div>
		<style type="text/css">
			.info .search{float:left;width:50%;margin-top:10px;margin-bottom:10px;}
		</style>
		<div class="padding border-bottom info">
			<c:choose>
				<c:when test="${not empty apply }">
					<form id="print"  onsubmit="return checkApply(this);" action="${pageContext.request.contextPath}${addAction }" method="post">
						<input type="hidden" name="tgId" value="${tgId }"/>
						<input type="hidden" name="tgName" value="${tgName }"/>					
						<input type="hidden" name="aCreateDate" value="<fmt:formatDate value="${apply.aCreateDate}" pattern="yyyy-MM-dd HH:mm:ss" />"/>					
						<p>报销政策：</p>
						<c:choose>
							<c:when test="${not empty illPolicy }">
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">慢病名称:</li>
									<li style="width:70%;"><input type="text" value="${illPolicy.illname }" class="input" readonly="readonly" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">封顶线金额:</li>
									<li style="width:70%;"><input type="text" value="${illPolicy.topLine }" class="input" readonly="readonly" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">报销比例:</li>
									<li style="width:70%;"><input id="bili" type="text" value="${illPolicy.cprop }%" class="input" readonly="readonly" /></li>
								</ul>
							</c:when>
							<c:otherwise>
								<p>当前还没有慢性政策，请先添加慢病政策。 </p>
							</c:otherwise>
						</c:choose>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">当前剩余可报金额:</li>
							<li style="width:70%;"><input id="kbaoxiao" type="text" value="${illPolicy.topLine.subtract(decimal) }" class="input" readonly="readonly" /></li>
						</ul>
						<p>慢病报销登记详情：</p>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">家庭编号:</li>
							<li style="width:70%;"><input type="text" value="${apply.aFamilyCode }" name="aFamilyCode" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">身份证号:</li>
							<li style="width:70%;"><input type="text" value="${apply.aIdCard }" name="aIdCard" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">姓名:</li>
							<li style="width:70%;"><input type="text" value="${name}" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">慢病名称:</li>
							<li style="width:70%;"><input type="text" value="${apply.aIllname }" name="aIllname" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">申请报销金:</li>
							<li style="width:70%;"><input id="shenqing" type="text" value="${apply.aApply }" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">报销金:</li>
							<li style="width:70%;"><input type="text" value="0.00" id="baoxiaoBili" name="aApply" class="input" readonly="readonly" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">申请报销年份:</li>
							<li style="width:70%;"><input type="text" value="${apply.aYear }" name="aYear" class="input" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">申请报销时间:</li>
							<li style="width:70%;"><input type="text" value="<fmt:formatDate value="${apply.aCreateDate }" pattern="yyyy-MM-dd HH:mm:ss" />" name="aCreateDate" class="input" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">医院发票号:</li>
							<li style="width:70%;"><input type="text" readonly="readonly" value="${apply.aFapiao }" name="aFapiao" class="input" /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">就诊时间:</li>
							<li style="width:70%;"><input type="text" readonly="readonly" value="<fmt:formatDate value="${apply.aSeeDate}" pattern="yyyy-MM-dd HH:mm:ss" />" name="aSeeDate" class="input"  /></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:25%;"><span>&nbsp;</span></li>
							<li style="width:15%;"><input class="button bg-main icon-check-square-o" type="submit" value="提交"/></li>
						</ul>
					</form>
					<ul class="search" style="padding-left: 10px;">
						<li style="width:25%;"><span onclick="printWeb(this);" class="button bg-main icon-check-square-o" >打印</span></li>
						<li style="width:15%;"></li>
					</ul>
					<script type="text/javascript">
						var commonPrintDirection = "horizontal";
						function printWeb(el){
							if(confirm("您确定要打印吗")){
								window.print();
							}
						}
						function checkApply(el){
							let kbaoxiao = $("#kbaoxiao") && $("#kbaoxiao").val();
							let shenqing = $("#shenqing") && $("#shenqing").val();
							let bili = $("#bili") && $("#bili").val().substr(0,$("#bili").val().length -1);
							bili = bili ? bili : 0.00;
							console.log(bili);
							if(kbaoxiao <= 0){
								alert("报销金额一超，已经不能再报销。");
								return false;
							}
							if((shenqing* bili / 100) <= kbaoxiao){
								let num = (shenqing * bili/ 100) + "";
								num = num.match(/^[0-9][0-9]*\.[0-9]{2}/)[0];
								console.log(num);
								$("#baoxiaoBili").val(num);
								if(el != 1 && confirm("若当前报销金额大于可报销金额，确认则按最高报销金报销，取消则结束")){
									return true;
								}else{									
									return false;
								}
							}else{
								$("#baoxiaoBili").val(kbaoxiao);
								if(el != 1 && confirm("若当前报销金额大于可报销金额，确认则按最高报销金报销，取消则结束")){
									return true;
								}else{									
									return false;
								}
							}
							return true;
						}
						checkApply(1);
					</script>
				</c:when>
				<c:otherwise>
					<p style="text-align:center;">当前暂无记录</p>
				</c:otherwise>				
			</c:choose>
			<hr/>
		</div>
	</div>
</body>
</html>