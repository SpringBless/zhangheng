<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销统计-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/autocomplete/jquery.autocomplete.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/third/autocomplete/jquery.autocomplete.js"></script>  
	<script type="text/javascript">
		$(document).ready(function() {
			//姓名自动匹配
			var urlUname="${pageContext.request.contextPath}${queryAreaByAJAX}";
			
		    $("#areas").autocomplete(urlUname, { 
			   dataType:"json",
			   type: "post",
		       max: 50, //列表里的条目数 
		       minChars: 0, //自动完成激活之前填入的最小字符 
		       scrollHeight: 200, //提示的高度
		       matchContains: true, //包含匹配
		       autoFill: false, //自动填充
			   //extraParams: {"name":$("#userName").val()},
			   parse: function(data) { 
				   console.log(data);
		               var rows = [];
		               for(var i=0; i<data.length; i++){	
		                 rows[rows.length] = 
		                 { data:data[i],
		                   result:data[i].areacode,
		                   value:data[i].areaname
		                 };
		               }           
		               return rows;
		           }, 
		       formatMatch: function(row, i, max) { 
		          return row.value ; 
		       },  
		       formatItem: function(row, i, max) { 
			      return  row.areacode+"-"+row.areaname; 
		       }
		    }).result(function(event, row, formatted) { 
			   $("#areaCode").val(row.areacode);
			   $("#areas").val(row.areaname);
		    });
		});
	</script>
</head>
<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
		</div>
		<form method="post" action="${pageContext.request.contextPath}${limitPageAction}" >
			<input type="hidden" name="tgId" value="${tgId }"/>
			<input type="hidden" name="tgName" value="${tgName }"/>
			<input type="hidden" id="areaCode" value="" name="areaCode" /> 
			<div class="padding border-bottom">
				<ul class="search" style="padding-left: 10px;">
					<li>行政区域：</li>
					<li><input type="text" id="areas" class="input" placeholder="行政区域" style="width:200px;"/></li>
					<li>姓名：</li>
					<li><input type="text" name="name" class="input" placeholder="姓名" style="width:200px;"/></li>
					<li>报销起始时间：</li>
					<li><input type="text" name="startDate" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="input" placeholder="报销起始时间" style="width:200px;"/></li>
					<li>报销结束时间：</li>
					<li><input type="text" name="endDate" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="input" placeholder="报销结束时间" style="width:200px;"/></li>
					<li>检索类型</li>
					<li>
						<select class="input" name="aStatus" >
							<option value="0">待审核</option>
							<option value="1">审核通过</option>
							<option value="2">审核不通过</option>
							<option value="3">已汇款</option>
							<option value="4">未汇款</option>
						</select>
					</li>
					<li>疾病类型</li>
					<li>
						<select class="input" name="aIllname" >
							<c:if test="${not empty Chronicdis }">
								<c:forEach items="${Chronicdis }" var="item">
									<option value="${item.illname }">${item.illname }</option>
								</c:forEach>
							</c:if>
						</select>
					</li>
					<li>
						<input type="submit" class="button border-main icon-plus-square-o" value="检索" />
					</li>
					<li>
						<a target="_blank" href="${pageContext.request.contextPath}${outExcelAction}${tempStr}" class="button border-main icon-plus-square-o" >导出Excel</a>
					</li>
				</ul>
			</div>
		</form>
		<table class="table table-hover text-center">
			<tr>
				<th>家庭编号</th>
				<th>姓名</th>
				<th>身份证号</th>
				<th>慢病名称</th>
				<th>报销金额</th>
				<th>申报时间</th>
				<th>医院发票号</th>
				<th>就诊时间</th>
				<th>状态</th>
			</tr>
			<!-- 华丽分割线 -->	
			<c:if test="${ !empty limitPage and !empty limitPage.data }">
				<c:forEach items="${limitPage.data }" var="model">
					<tr>
						<td><c:out value="${ model.aFamilyCode }"/></td>
						<td><c:out value="${ model.name }"/></td>
						<td><c:out value="${ model.aIdCard }"/></td>
						<td><c:out value="${ model.aIllname }"/></td>
						<td>￥<c:out value="${ model.aApply }"/>元</td>
						<td><fmt:formatDate value="${ model.aCreateDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><c:out value="${ model.aFapiao }"/></td>
						<td><fmt:formatDate value="${ model.aSeeDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>
							<c:choose>
								<c:when test="${model.aStatus eq 0 }">待审核</c:when>
								<c:when test="${model.aStatus eq 1}">审核通过</c:when>
								<c:when test="${model.aStatus eq 2}">审核不通过</c:when>
								<c:when test="${model.aStatus eq 3}">已汇款</c:when>
								<c:when test="${model.aStatus eq 4}">未回款</c:when>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:if>
			
			<!-- 分页条 -->
			<tr>
				<td colspan="10">
					<div class="pagelist">
						<c:if test="${!empty limitPage }">
							<p style="float:left;">当前显示第${ limitPage.begRow}至  ${limitPage.endRow}</p>
							<p style="float:left;">,一共${limitPage.totalRow }条,一共${limitPage.totalPage }页</p>
							<p style="float:left;">,当前显示第${limitPage.pageNo }页</p>
							<c:choose>
								<c:when test="${ limitPage.firstPage  }">
									<span onclick="alert('已经是第一页了！');" class="current">1</span>
								</c:when>
								<c:otherwise>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=1&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">首页</a>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.prePage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">上一页</a>
									<span onclick="alert('已是当前页了！');" class="current">${limitPage.pageNo }</span>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${ limitPage.totalPage gt 1 and limitPage.pageNo + 5 lt limitPage.totalPage }">
									<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.pageNo + 5 }" var="item">
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">${item }</a>												
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach begin="${limitPage.pageNo + 1 }" end="${ limitPage.totalPage }" var="item">
										<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${item }&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">${item }</a>												
									</c:forEach>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${ limitPage.lastPage  }">
									<span onclick="alert('已经是最后一页了！');" class="current" >尾页</span>
								</c:when>
								<c:otherwise>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.nextPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">下一页</a>
									<a href="${pageContext.request.contextPath}${limitPageAction}&startPage=${limitPage.totalPage }&pageSize=10&tgId=${tgId}&tgName=${tgName}${tempStr}">尾页</a>
								</c:otherwise>
							</c:choose>
						</c:if>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>