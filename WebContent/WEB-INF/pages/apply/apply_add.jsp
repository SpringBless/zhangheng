<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销-新农合慢性病报销系统V1.0</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/scroll.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/admin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/third/My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/admin.js"></script>  
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/third/My97DatePicker/WdatePicker.js"></script>  
</head>
<body>
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong class="icon-reorder"> &nbsp;${tgName } | 内容列表</strong> 
		</div>
		<div class="padding border-bottom">
			<form action="${pageContext.request.contextPath}${checkAction }" method="post">
				<input type="hidden" name="tgId" value="${tgId }"/>
				<input type="hidden" name="tgName" value="${tgName }"/>
				<ul class="search" style="padding-left: 10px;">
					<li>身份证号：</li>
					<li><input type="text" class="input" value="${cardCode }" name="cardCode" style="width:200px;" data-validate="required:必须的身份证号" placeholder="请输入身份证号"/></li>
					<li><span class="tips"></span></li>
					<li><input type="submit" class="button bg-main icon-check-square-o" value="检索"/></li>
				</ul>
			</form>
		</div>
		<style type="text/css">
			.info .search{float:left;width:50%;margin-top:10px;margin-bottom:10px;}
		</style>
		<div class="padding border-bottom info">
			<c:choose>
				<c:when test="${not empty payDto }">
					<form onsubmit="return checkApply(this);" action="${pageContext.request.contextPath}${addAction }" method="post">
						<input type="hidden" name="tgId" value="${tgId }"/>
						<input type="hidden" name="tgName" value="${tgName }"/>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">家庭编号:</li>
							<li style="width:70%;"><input type="text" class="input" name="aFamilyCode" value="${payDto.familyCode }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">户主姓名:</li>
							<li style="width:70%;"><input type="text" class="input" value="${payDto.householder }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">农民姓名:</li>
							<li style="width:70%;"><input type="text" name="name" class="input" value="${payDto.name }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">身份证号:</li>
							<li style="width:70%;"><input type="text" class="input" name="aIdCard" value="${payDto.cardCode }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">县/镇/村:</li>
							<li style="width:70%;"><input type="text" class="input" value="${payDto.groupCode }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">户属性:</li>
							<li style="width:70%;"><input type="text" class="input" value="${payDto.familyProperty }" readonly="readonly"/></li>
						</ul>
						<ul class="search" style="padding-left: 10px;">
							<li style="width:15%;">现居住地:</li>
							<li style="width:70%;"><input type="text" class="input" value="${payDto.address }" readonly="readonly"/></li>
						</ul>
						<hr/>
						<c:choose>
							<c:when test="${not empty chrocdp }">
								<input type="hidden" id="cpStart" value="${chrocdp.cpStart.getTime() }"/> 
								<input type="hidden" id="cpEnd" value="${chrocdp.cpEnd.getTime() }"/> 
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">可报慢病:</li>
									<li style="width:70%;"><input type="text" id="kcpNames" class="input" value="${chrocdp.cpNames }" readonly="readonly"/></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">有效时间:</li>
									<li style="width:70%;"><input type="text" class="input" 
										value="<fmt:formatDate value="${chrocdp.cpStart}" pattern="yyyy-MM-dd HH:mm:ss" /> 至 <fmt:formatDate value="${chrocdp.cpEnd}" pattern="yyyy-MM-dd HH:mm:ss" />" 
										readonly="readonly"/></li>
								</ul>
								<hr/>
								<p>慢病报销登记：</p>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">慢病名称:</li>
									<li style="width:70%;"><input type="text" id="caIllname" name="aIllname" class="input" data-validate="required:请输入慢病名称" placeholder="请输入慢病名称" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">报销金额:</li>
									<li style="width:70%;"><input type="text" id="aApply" name="aApply" class="input" data-validate="required:请输入报销金额" placeholder="请输入报销金额" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">医院发票号:</li>
									<li style="width:70%;"><input type="text"  name="aFapiao" class="input" data-validate="required:请输入医院发票号" placeholder="请输入医院发票号" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:15%;">就诊时间:</li>
									<li style="width:70%;"><input type="text" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="aSeeDate" class="input" data-validate="required:请输入就诊时间" placeholder="请输入就诊时间" /></li>
								</ul>
								<ul class="search" style="padding-left: 10px;">
									<li style="width:25%;"><span>&nbsp;</span></li>
									<li style="width:15%;"><input class="button bg-main icon-check-square-o" type="submit" value="提交"/></li>
								</ul>
							</c:when>
							<c:otherwise>
								<p>当前还没有慢性病证，请先添加慢病证。 <a class="button bg-main icon-check-square-o" href="${pageContext.request.contextPath}/sys/chrocdpdto.do?method=toAddChrocdpDtoPage&tgId=129&tgName=慢性病证管理">点击添加慢病</a></p>
							</c:otherwise>
						</c:choose>
					</form>
					<script type="text/javascript">
						function checkApply(el){
							let cpStart = $("#cpStart") && $("#cpStart").val();
							let cpEnd = $("#cpEnd") && $("#cpEnd").val();
							let kcpNames = $("#kcpNames") && $("#kcpNames").val();
							let caIllname = $("#caIllname") && $("#caIllname").val();
							let aApply = $("#aApply") && $("#aApply").val();
							
							if(!(/^[1-9][0-9]*\.[0-9]{2}$/.test(aApply))){
								alert("非法输入，请输入正确的金额");
								return false;
							}
							if(kcpNames != caIllname){
								alert("您输入的慢病名称不再报销范围。");
								return false;
							}
							if((new Date() - cpStart) < 0){
								alert("当前时间不在报销时间段");
								return false;
							}
							if((new Date() - cpEnd) > 0){
								alert("当前时间不在报销时间段");
								return false;
							}
							return true;
						}
					</script>
				</c:when>
				<c:otherwise>
					<p style="text-align:center;">当前暂无记录,或者未参合</p>
				</c:otherwise>				
			</c:choose>
			<hr/>
		</div>
		${flag }
	</div>
</body>
</html>