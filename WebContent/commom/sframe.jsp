<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style type="text/css">
*{margin: 0;padding: 0;}
a{text-decoration: none;color: #333;}
ul,li{list-style: none;}
body{font-size: 14px;font-family: "微软雅黑";}
/*** 弹出层 ***/
.s-iframe{
	display:none;
    position: fixed;
    top:0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.5);
    z-index: 999;
}
.s-iframe-wrap{
    position: absolute;
    padding: 10px 10px 40px;
    top: 50%;
    left: 50%;
    width: 580px;
    height: 300px;
    margin-left: -300px;
    margin-top: -150px;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 0 10px 0 skyblue;
    overflow: hidden;
}
.s-title{height: 30px;line-height: 30px;font-size: 14px;color: #666;}
.s-close{position: absolute;top: 10px;right: 10px;font-size: 16px;
    font-weight: bold;color: #999;cursor: pointer;}
.s-close:hover{color: #333;}
.s-ul{
    display: block;
    height: 30px;
    width: 100%;
    text-align: center;
    cursor: pointer;
}
.s-ul li {
    float: left;
    width: 170px;
    line-height: 30px;
    height: 30px;
    border-bottom: 1px solid #eee;
}
.s-content{display:block;width: 100%;height: 170px;overflow-y: auto;}
.s-ul .s-ul-x{width: 50px;}
.s-btn{position:absolute;left:10px;bottom:-10px;display:block;width: 100%;height: 50px;border-top: 1px solid #999;}
.s-btn-w{
    display:block;
    width: 80px;
    height: 30px;
    background-color: #55b929;
    text-align: center;
    line-height: 30px;
    margin: 5px auto 0;
    border-radius: 10px;
    color: #fff;
    cursor: pointer;
}
.s-btn-w:hover{background-color: #22b929;color: #eee;}
</style>
<div id="s-iframe" class="s-iframe">
    <div class="s-iframe-wrap">
        <div class="s-title">谁家的 <span id="s-close" class="s-close">X</span></div>
        <hr/>
        <ul class="s-ul">
            <li class="s-ul-x">选择</li>
            <li>家庭编号</li>
            <li>身份证号</li>
            <li>姓名</li>
        </ul>
        <div id="s-content" class="s-content">
            <ul class="s-ul">
                <li class="s-ul-x"><input type="checkbox" value="1" name="sCode"></li>
                <li>家庭编号</li>
                <li>身份证号</li>
                <li>姓名</li>
            </ul>
            <ul class="s-ul">
                <li class="s-ul-x"><input type="checkbox" data-s='1' value="1" name="sCode"></li>
                <li>家庭编号</li>
                <li>身份证号</li>
                <li>姓名</li>
            </ul>
        </div>
        <div class="s-btn">
            <span id="s-btn-w" class="s-btn-w">确定</span>
        </div>
    </div>
</div>
<script type="text/javascript"> 
    let SURL = '${pageContext.request.contextPath}${printURL}';
	var sOpen = function sOpen(key,data){
		this.key = key;
		this.data = data;
		this.open();
	}; 
	sOpen.prototype.open = function(){
		let url =SURL + "&" + this.key + "=" + this.data;
		console.log(url);
		let old = document.getElementById("sOpenForm");
		if(old){
			old.parentNode.removeChild(old);
		}
		let form = document.createElement("form");
		form.setAttribute("target","_blank");
		form.setAttribute("id","sOpenForm");
		form.setAttribute("method","post");
		form.setAttribute("action",url);		
		this.form = form;
		document.getElementById("s-iframe").appendChild(this.form);
		//window.open(url,"_blank");
	}
	
    var SFrame = function SFrame(data, callBack){
        this.data = data;
        this.callBack = callBack;
        this.init();
    };
    SFrame.prototype.init = function(){
        let This =  this;
        let sIframe = document.getElementById("s-iframe");
        let sClose = document.getElementById("s-close");
        let sBtnW = document.getElementById("s-btn-w");
        let sContent = document.getElementById("s-content");
        sContent.innerHTML = This.data;
        let uls = sContent.getElementsByTagName("ul");
        let inputs = sContent.getElementsByTagName("input");
        sIframe.style.display = "block";
        sClose.onclick = function(){
            sIframe.style.display = "none";
        };
        for(let i=0; i<inputs.length; i++){
            if(inputs[i].getAttribute("data-s") == 1){
                inputs[i].checked = true;
            }
        }
        for(let i=0; i<uls.length; i++){
            uls[i].i=i;
            uls[i].onclick = function(){
                if(inputs[this.i].checked && inputs[this.i].getAttribute("data-s") != 1){
                    inputs[this.i].checked = false;
                }else if(!inputs[this.i].checked && !inputs[this.i].getAttribute("data-s")){
                    inputs[this.i].checked = true;
                }
            };
        }
        This.val = [];
        sBtnW.onclick = function(){
            This.val = [];
            for(let i=0; i<inputs.length; i++){
                if(inputs[i].checked && inputs[i].getAttribute("data-s") != 1){
                	let name =  inputs[i].getAttribute("data-name");
                	let data = {"name": name, "code":inputs[i].value};
                    This.val.push(data);
                }
            }
            //console.log(This.val)
            sIframe.style.display = "none";
            This.callBack && This.callBack.call(This, This.val);
        };
    };
    
    //new SFrame(html,function(data){
    //    console.log(data);
    //});
</script>