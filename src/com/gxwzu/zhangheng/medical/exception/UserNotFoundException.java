package com.gxwzu.zhangheng.medical.exception;

/**
 * @ClassName:  UserNotFoundException   
 * @Description:TODO(这里用一句话描述这个类的作用)  没有找到用户异常类 
 * @author: zhangheng
 * @date:   2019年3月27日 下午2:13:48     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class UserNotFoundException extends Exception {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserNotFoundException(String message) {
		super(message);
	}

}
