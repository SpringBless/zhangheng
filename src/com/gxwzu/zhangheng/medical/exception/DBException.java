package com.gxwzu.zhangheng.medical.exception;

/**
 * @ClassName:  DBException   
 * @Description:TODO(这里用一句话描述这个类的作用)   自定义数据库连接异常类
 * @author: zhangheng
 * @date:   2019年3月27日 下午2:11:41     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class DBException extends Exception {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	public DBException(String message, Throwable cause) {
		super(message, cause);
	}

	public DBException(String message) {
		super(message);
	}

}
