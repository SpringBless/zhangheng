package com.gxwzu.zhangheng.medical.exception;

/**
 * @ClassName:  MenuException   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年3月28日 下午7:49:43     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MenuException extends Exception {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	public MenuException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public MenuException(String message) {
		super(message);
	}

}
