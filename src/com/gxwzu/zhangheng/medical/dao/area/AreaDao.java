package com.gxwzu.zhangheng.medical.dao.area;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  AreaDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年4月8日 下午7:07:04     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class AreaDao extends BaseDao {
	
	protected static final String COUNT_TB_AREA = "SELECT COUNT(*) AS total FROM tb_area";
	
	/**
	 * @Title: findAreaAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有行政区域  
	 */
	public List<Area> findAreaAll(){
		String sql = "SELECT * FROM tb_area WHERE 1=1";
		try {
			List<Area> areaAll = super.findAll(sql, null, Area.class);
			return areaAll;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findAreaLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  行政区域分页 
	 */
	public PageObject<Area> findAreaLimit(PageObject<Area> pageObject){
		int totals = getCountTotal(COUNT_TB_AREA);
		String sql = "SELECT * FROM tb_area WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Area> areas = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
							Area.class);
			pageObject.setData(areas);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
