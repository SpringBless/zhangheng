package com.gxwzu.zhangheng.medical.dao.familydata;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.familydata.FamilyData;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyDataDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭成员档案信息资源类， table name ： tb_family_data
 * @author: zhangheng
 * @date:   2019年4月15日 下午10:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyInfoDataDao extends BaseDao {

	protected static final String COUNT_TB_FAMILY_DATA = "SELECT distinct COUNT(*) AS total FROM tb_family_data";
	
	/**
	 * @Title: findFamilyDataLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询FamilyData  
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<FamilyInfo>      
	 */
	public PageObject<FamilyData> findFamilyDataLimit(PageObject<FamilyData> pageObject){
		int totals = getCountTotal(COUNT_TB_FAMILY_DATA);
		String sql = "SELECT distinct * FROM tb_family_data WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<FamilyData> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					FamilyData.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
