package com.gxwzu.zhangheng.medical.dao.pay;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.pay.Pay;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  PayDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月7日 下午7:07:04     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PayDao extends BaseDao {
	
	protected static final String COUNT_TB_AREA = "SELECT COUNT(*) AS total FROM tb_pay";
	
	/**
	 * @Title: findPayAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有交费登记信息
	 */
	public List<Pay> findPayAll(){
		String sql = "SELECT * FROM tb_pay WHERE 1=1";
		try {
			List<Pay> all = super.findAll(sql, null, Pay.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findPayLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合交费分页 
	 */
	public PageObject<Pay> findPayLimit(PageObject<Pay> pageObject){
		int totals = getCountTotal(COUNT_TB_AREA);
		String sql = "SELECT * FROM tb_pay WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Pay> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					Pay.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
