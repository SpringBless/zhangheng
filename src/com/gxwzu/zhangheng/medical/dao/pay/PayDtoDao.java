package com.gxwzu.zhangheng.medical.dao.pay;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.RowMapper;
import com.gxwzu.zhangheng.medical.domain.pay.PayDto;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  PayDtoDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月7日 下午7:07:04     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PayDtoDao extends BaseDao {
	
protected static final String COUNT_TB_AREA = "SELECT COUNT(*) AS total FROM tb_pay";
	
	/**
	 * @Title: findPayDtoAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有交费登记信息
	 */
	public List<PayDto> findPayDtoAll(){
		String sql = "SELECT * FROM tb_pay WHERE 1=1";
		try {
			List<PayDto> all = super.findAll(sql, null, PayDto.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findPayDtoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合交费分页 
	 */
	public PageObject<PayDto> findPayDtoLimit(PageObject<PayDto> pageObject){
		int totals = getCountTotal(COUNT_TB_AREA);
		String sql = "SELECT a.*, b.name AS name FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findPayDtoKeywordsAndLikeLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合交费分页 
	 */
	public PageObject<PayDto> findPayDtoKeywordsAndLikeLimit(PageObject<PayDto> pageObject,String keywords){
		int totals = getCountTotal("SELECT count(*) AS total FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and b.name LIKE '%"+keywords+"%'");
		String sql = "SELECT a.*, b.name AS name FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and b.name LIKE '%"+keywords+"%' LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 行政区域，参合交费分页 
	 * @Title: findPayDtoAreaCodeAndLikeLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param pageObject
	 * @param: @param areaCode
	 * @return: PageObject<PayDto>      
	 */
	public PageObject<PayDto> findPayDtoAreaCodeAndLikeLimit(PageObject<PayDto> pageObject,String areaCode){
		int totals = getCountTotal("SELECT count(*) AS total FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.family_code LIKE '"+areaCode+"%'");
		String sql = "SELECT a.*, b.name AS name FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.family_code LIKE '"+areaCode+"%' LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 时间查询参合交费分页 
	 * @Title: findPayDtoDateAndLikeLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param pageObject
	 * @param: @param startDate
	 * @param: @param endDate
	 * @return: PageObject<PayDto>      
	 */
	public PageObject<PayDto> findPayDtoDateAndLikeLimit(PageObject<PayDto> pageObject,String startDate, String endDate){
		int totals = getCountTotal("SELECT count(*) AS total FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.play_date between '"+startDate+"' and '" + endDate +"'");
		String sql = "SELECT a.*, b.name AS name FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.play_date between '"+startDate+"' and '" + endDate +"' LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findPayDtoYearAndLikeLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合交费分页 
	 */
	public PageObject<PayDto> findPayDtoYearAndLikeLimit(PageObject<PayDto> pageObject,String year){
		int totals = getCountTotal("SELECT count(*) AS total FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.play_year =" + year);
		String sql = "SELECT a.*, b.name AS name FROM tb_pay a,tb_family_info b WHERE 1=1 and a.card_code=b.card_code and a.play_year =" + year + " LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findAllYears   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   查询当前有的年份
	 * @return: List<String>      
	 */
	public List<String> findAllYears(){
		String sql = "SELECT distinct play_year FROM tb_pay WHERE 1=1 ";
		List<String> findAll = BaseDao.findAll(new RowMapper<String>() {

			@Override
			public String mapping(ResultSet resultSet) throws SQLException {
				String reString = resultSet.getString("play_year");
				return reString.length() > 4? reString.substring(0, 4) : reString;
			}
		}, sql, null);
		return findAll;
	}
	
	/**
	 * @Title: findCodeByCurentYear   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取当前年份已经参合缴费的家庭成员信息
	 * @param: @param year, familyCode
	 * @return: List<String>      
	 */
	public List<String> findCodeByCurentYear(int year,String familyCode){
		String sql = "SELECT card_code FROM tb_pay WHERE family_code=? AND play_year=? ";
		List<String> all = super.findAll(new RowMapper<String>() {

			@Override
			public String mapping(ResultSet resultSet) throws SQLException {
				String card_code = resultSet.getString("card_code");
				return card_code;
			}
		}, sql, new Object[]{familyCode, year});
		return all;
	}
	
	/**
	 * @Title: queryPayDtoByYearCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  查询缴费详细信息   
	 * @param: @param year
	 * @param: @param cardCode
	 * @return: PayDto      
	 */
	public PayDto queryPayDtoByYearCode(String year, String cardCode){
		StringBuffer sql = new StringBuffer();
		sql.append("   SELECT ")
			.append("      t_p.*, t_f_i.*, t_f_r.* ")
			.append("  FROM  ")
			.append("      tb_pay t_p, tb_family_info t_f_i, tb_family_rchives t_f_r")
			.append("  WHERE ")
			.append("      t_f_r.family_code = t_f_i.family_code AND t_f_i.card_code = t_p.card_code AND ")
			.append("      t_p.play_year = ? AND t_p.card_code = ? ");
		try {
			PayDto dto = super.getObject(sql.toString(), new Object[]{year, cardCode},  PayDto.class);
			return dto;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
