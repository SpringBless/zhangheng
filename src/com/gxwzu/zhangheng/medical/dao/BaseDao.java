package com.gxwzu.zhangheng.medical.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gxwzu.zhangheng.medical.database.DBUtil;
import com.gxwzu.zhangheng.medical.database.EntityMapper;
import com.gxwzu.zhangheng.medical.database.RowMapper;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @Description:TODO medical 基础数据层封装
 * classpath：xin.spring.medical.dao.BaseDao.java
 * @author zhangheng
 * @date 2019-03-01 22:17:00
 */
public abstract class BaseDao {

	private static Connection connection;
	private static PreparedStatement preparedStatement;
	private static ResultSet resultSet;
	
	protected static final String SELECT = " SELECT * FROM ";
	protected static final String DELETE = " DELETE FROM ";
	protected static final String INSERT = " INSERT INTO ";
	protected static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * Gson解析
	 */
	protected static Gson GSON = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
	
	/**
	 * 设置preparedStatement的sql语句值
	 * @param pStatement
	 * @param object
	 * @throws SQLException
	 */
	protected static void setProperty(PreparedStatement pStatement, Object[] object) throws SQLException{
		if(pStatement != null && object != null && object.length > 0){
			for(int i = 0; i < object.length; i++){
				pStatement.setObject(i + 1, object[i]);
			}
		}
	}
	
	/**
	 * @Title: getCountTotal   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  查询总记录数 
	 */
	protected int getCountTotal(String countSql){
		Integer total = (Integer) getObject(new EntityMapper<Integer>() {
			@Override
			public Integer getEntity(ResultSet resultSet) throws SQLException {
				Integer total = resultSet.getInt("total");
				return total;
			}
		}, countSql, null);
		if(total == null){
			total = 0;
		}
		return total;
	}
	
	/**
	 * toUpdate();增删改操作
	 * @param sql
	 * @param object
	 * @return int 影响记录条数
	 */
	public static int toUpdate(String sql, Object[] object){
		int i = 0;
		try{
			connection = DBUtil.getConnection();
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			i = preparedStatement.executeUpdate();
			connection.commit();
			return i;
		}catch(SQLException e){
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
		return i;
	}
	
	/**
	 * 判断真假
	 * @param msg
	 * @return true or false
	 */
	public static boolean getBoolean(int msg){
		return (msg > 0)? true:false;
	}
	
	/**
	 * 通过匿名内部类实现数据库连接和执行业务, 如想使用类加载器解析请使用同名重载函数
	 * @param mapper
	 * @param sql
	 * @param object
	 * @return <T> List<T>
	 */
	public static <T> List<T> findAll(RowMapper<T> mapper, String sql, Object[] object){
		List<T> arrayList = null;
		try {
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			resultSet = preparedStatement.executeQuery();
			if(resultSet != null){
				arrayList = new ArrayList<T>();
				while(resultSet.next()){
					arrayList.add(mapper.mapping(resultSet));
				}
			}
			return arrayList;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
		return arrayList;
	}
	
	/**
	 * 通过匿名内部类获取实体，如想使用类加载器获取数据请使用同名重载函数
	 * @param @param entityMapper
	 * @param @param object
	 * @param @param resultSet
	 * @param @throws SQLException   
	 * @return T  
	 * @date 2018年11月8日
	 */
	public static <T> Object getObject(EntityMapper<T> entityMapper, String sql, Object[] object){
		Object t = null;
		try {
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			resultSet = preparedStatement.executeQuery();
			if(resultSet != null){
				while(resultSet.next()){
					t = entityMapper.getEntity(resultSet);
				}
			}
			return t;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
		return t;		
	}
	
	/**
	 * if success return true
	 * else return false 
	 * 执行多条sql，增删改
	 * @param sqls
	 * @param parameter
	 * @return true or false 
	 */
	public static boolean branch(String[] sqls, Object[]...parameter){
		boolean flag = false;
		Object[][] parameters = parameter;
		if(sqls == null || sqls.length < 0){
			return flag;
		}
		if(parameters == null || parameters.length < 0){
			return flag;
		}
		if(sqls.length != parameters.length){
			return flag;
		}
		
		try{
			connection = DBUtil.getConnection();
			connection.setAutoCommit(false);
			for(int i=0; i<sqls.length; i++){
				preparedStatement = connection.prepareStatement(sqls[i]);
				setProperty(preparedStatement, parameters[i]);
				preparedStatement.executeUpdate();
			}
			connection.commit();
			flag = true;
			return flag;
		}catch(SQLException e){
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
		return flag;
	}
	
	
	/**
	 * 使用java反射设计模式返回数据集合
	 * @param sql sql
	 * @param object 参数数组
	 * @param t 泛型类.class
	 * @throws DBException 
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> findAll(String sql, Object[] object, Class<?> t) throws DBException{
		try {
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			resultSet = preparedStatement.executeQuery();
			// 取得ResultSet的列名
			String[][] column = getColumn(resultSet);
			String[] columnNames = column[0];
			String[] columnValues = column[1];
			List<T> list = new ArrayList<T>();
			while(resultSet.next()){
				Map<String, T> map = new HashMap<String, T>();
				for(int i=0; i<columnValues.length; i++){
					map.put(columnNames[i], (T) resultSet.getObject(columnValues[i]));
				}
				String resultJson = GSON.toJson(map);
				T fromJson = GSON.fromJson(resultJson, t);
				list.add((T) fromJson);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException("获取数据列表异常");
		}finally{
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
	}
	
	/**
	 * @Title: findSetAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   使用java反射设计模式返回数据集合
	 * @param: @param sql sql语句
	 * @param: @param object 参数数组
	 * @param: @param t 泛型.class
	 * @return: Set<T>     结果集
	 */
	@SuppressWarnings("unchecked")
	public static <T> Set<T> findSetAll(String sql, Object[] object, Class<?> t) throws DBException{
		try {
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			resultSet = preparedStatement.executeQuery();
			// 取得ResultSet的列名
			String[][] column = getColumn(resultSet);
			String[] columnNames = column[0];
			String[] columnValues = column[1];
			Set<T> list = new HashSet<T>();
			while(resultSet.next()){
				Map<String, T> map = new HashMap<String, T>();
				for(int i=0; i<columnValues.length; i++){
					map.put(columnNames[i], (T) resultSet.getObject(columnValues[i]));
				}
				String resultJson = GSON.toJson(map);
				T fromJson = GSON.fromJson(resultJson, t);
				list.add((T) fromJson);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException("持久层数据与实体映射异常！");
		}finally{
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}		
	}
	
	/**
	 * 使用java反射机制，通过自动类识别获取数据
	 * @param sql sql
	 * @param object 参数数组
	 * @param t 泛型类.class
	 */
	public static <T> T getObject(String sql, Object[] object, Class<?> t) throws DBException{
		try {
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			setProperty(preparedStatement, object);
			resultSet = preparedStatement.executeQuery();
			if(resultSet != null && resultSet.next()){//下移了一个下标
				// 取得ResultSet的列名
				String[][] column = getColumn(resultSet);
				String[] columnNames = column[0];
				String[] columnValues = column[1];
				Map<String, Object> map = new HashMap<>();
				resultSet.previous();//所以这里要上移一个下标
				while(resultSet.next()){
					for(int i=0; i<columnValues.length; i++){
						map.put(columnNames[i], resultSet.getObject(columnValues[i]));
					}
				}
				String resultJson = GSON.toJson(map);
				T fromJson = GSON.fromJson(resultJson, t);
				return fromJson;
			}else{
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("数据持久层与实体映射异常 ！");
		}finally{
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
	}
	
	/**
	 * @Title: getCount   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获取表记录总数 
	 * @param: @param tableName
	 * @return: long      
	 * @throws DBException 
	 */
	public static long getCount(String tableName) throws DBException{
		long totle = 0L;
		try{
			String sql = "SELECT count(1) AS totle FROM " + tableName;
			connection = DBUtil.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			if(resultSet != null && resultSet.next()){
				totle = resultSet.getLong("totle");
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new DBException("SELECT count(1) AS totle 查询异常！");
		}finally {
			DBUtil.closeAll(connection, preparedStatement, resultSet);
		}
		return totle;
	}
	
	/**
	 * 获取数据表字段，java属性名（成员）
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	protected static String[][] getColumn(ResultSet resultSet) throws SQLException{
		ResultSetMetaData rsmd = resultSet.getMetaData();
		int columnsCount = rsmd.getColumnCount();
		String[] columnNames = new String[columnsCount];
		String[] columnValues = new String[columnsCount];
		for (int i = 0; i < columnsCount; i++) {
			String label = rsmd.getColumnLabel(i + 1);
			columnValues[i] = label;
			if(label.indexOf("_") != -1){
				String[] arr = label.split("_");
				StringBuffer sbf = new StringBuffer();
				sbf.append(arr[0]);
				for(int j=1; j<arr.length; j++){
	    			sbf.append(String.valueOf(arr[j].charAt(0)).toUpperCase().concat(arr[j].substring(1)));
				}
				columnNames[i] = sbf.toString();
			}else{
				columnNames[i] = label;
			}
		}
		return new String[][]{columnNames, columnValues};
	}
}