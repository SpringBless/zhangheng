package com.gxwzu.zhangheng.medical.dao.institution;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.institution.Institution;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  AreaDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   农合机构
 * @author: zhangheng
 * @date:   2019年4月8日 下午7:07:04     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class InstitutionDao extends BaseDao {
	
	protected static final String COUNT_TB_INSTITUTION = "SELECT COUNT(*) AS total FROM tb_institution";
	
	/**
	 * @Title: findInstitutionAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有农合机构 
	 */
	public List<Institution> findInstitutionAll(){
		String sql = "SELECT * FROM tb_institution WHERE 1=1";
		try {
			List<Institution> areaAll = super.findAll(sql, null, Institution.class);
			return areaAll;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findInstitutionLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  农合机构分页 
	 */
	public PageObject<Institution> findInstitutionLimit(PageObject<Institution> pageObject){
		int totals = getCountTotal(COUNT_TB_INSTITUTION);
		String sql = "SELECT * FROM tb_institution WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Institution> areas = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					Institution.class);
			pageObject.setData(areas);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
