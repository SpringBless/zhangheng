package com.gxwzu.zhangheng.medical.dao.menu;

import java.util.ArrayList;
import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;
import com.gxwzu.zhangheng.medical.domain.menu.MenuDto;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  MenuDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   菜单（权限）数据持久层操作类
 * @author: zhangheng
 * @date:   2019年3月28日 下午2:23:37     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MenuDao extends BaseDao {
	
	protected static final String COUNT_TB_MENU = "SELECT COUNT(*) AS total FROM tb_menu WHERE id != 0";
	
	/**
	 * @Title: findMenusLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  菜单分页 
	 */
	public PageObject<MenuDto> findMenusLimit(PageObject<MenuDto> pageObject){
		int totals = getCountTotal(COUNT_TB_MENU);
		String sql = "SELECT * FROM tb_menu WHERE 1=1 AND id != 0 LIMIT ?,?";
		try {
			List<Menu> menuAll = findAllMenuNameAndId();//先获取所有菜单名和id
			pageObject.setTotalRow(totals);
			List<Menu> menus = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
							Menu.class);
			List<MenuDto> menuDtos = new ArrayList<MenuDto>();
			if(menus != null && menus.size() > 0){
				for(Menu m : menus){
					for(Menu ms : menuAll){
						if(m.getParentId().longValue() == ms.getId().longValue()){
							MenuDto mDto = new MenuDto();
							mDto.setParentName(ms.getName());
							mDto.setName(m.getName());
							mDto.setId(m.getId());
							mDto.setParentId(m.getParentId());
							mDto.setSort(m.getSort());
							mDto.setHref(m.getHref());
							mDto.setTarget(m.getTarget());
							mDto.setIcon(m.getIcon());
							mDto.setIsShow(m.getIsShow());
							mDto.setLavel(m.getLavel());
							mDto.setRemarks(m.getRemarks());
							mDto.setDelFlag(m.getDelFlag());
							menuDtos.add(mDto);
							break;
						}
					}
					
				}
			}
			pageObject.setData(menuDtos);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findMenuAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有菜单名，id  
	 */
	protected List<Menu> findAllMenuNameAndId(){
		String sql = "SELECT id, parent_id, name FROM tb_menu WHERE 1=1";
		try {
			List<Menu> menuAll = super.findAll(sql, null, Menu.class);
			return menuAll;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
