package com.gxwzu.zhangheng.medical.dao.medical;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.medical.Medical;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  MedicalDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   医疗卫生机构管理数据层
 * @author: zhangheng
 * @date:   2019年4月14日 下午1:39:39     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MedicalDao extends BaseDao {
	
	protected static final String COUNT_TB_MEDICAL = "SELECT COUNT(*) AS total FROM tb_medical";
	
	/**
	 * @Title: findMedicalAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有医疗卫生机构
	 */
	public List<Medical> findMedicalAll(){
		String sql = "SELECT * FROM tb_medical WHERE 1=1";
		try {
			List<Medical> all = super.findAll(sql, null, Medical.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findMedicalLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  医疗卫生机构分页 
	 */
	public PageObject<Medical> findMedicalLimit(PageObject<Medical> pageObject){
		int totals = getCountTotal(COUNT_TB_MEDICAL);
		String sql = "SELECT * FROM tb_medical WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Medical> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					Medical.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
