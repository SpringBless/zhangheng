package com.gxwzu.zhangheng.medical.dao.user;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.user.User;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  UserDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   用户数据池就层通讯类
 * @author: zhangheng
 * @date:   2019年3月27日 下午4:48:19     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class UserDao extends BaseDao {
	
	protected static final String COUNT_TB_USER = "SELECT COUNT(*) AS total FROM tb_user";
	
	/**
	 * @Title: findUserAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有用户信息，不包括自己  
	 */
	public List<User> findUserAll(Long id){
		String sql = "SELECT * FROM tb_user WHERE id != ?";
		try {
			List<User> userAll = super.findAll(sql, new Object[]{id}, User.class);
			return userAll;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findUsersLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)分页 获取所有用户信息，不包括自己  
	 */
	public PageObject<User> findUsersLimit(Long id, PageObject<User> pageObject){
		int totals = getCountTotal(COUNT_TB_USER);
		String sql = "SELECT * FROM tb_user WHERE 1=1 AND id != ? LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<User> users = super.findAll(sql, 
					new Object[]{
							id,
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
							User.class);
			pageObject.setData(users);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
