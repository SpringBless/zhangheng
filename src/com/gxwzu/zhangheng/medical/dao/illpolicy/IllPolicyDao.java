package com.gxwzu.zhangheng.medical.dao.illpolicy;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.illpolicy.IllPolicy;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChronicdisDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病政策管理类 数据访问层
 * @author: zhangheng
 * @date:   2019年4月14日 上午11:59:33     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class IllPolicyDao extends BaseDao {
	
	protected static final String COUNT_TB_ILLPOLICY = "SELECT COUNT(*) AS total FROM tb_ill_policy i, tb_chronicdis c WHERE i.illcode = c.illcode";
	
	/**
	 * @Title: findIllPolicyAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取所有慢性病政策
	 */
	public List<IllPolicy> findIllpolicyAll(){
		String sql = "SELECT * FROM tb_ill_policy WHERE 1=1";
		try {
			List<IllPolicy> all = super.findAll(sql, null, IllPolicy.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findIllPolicyLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询IllPolicy  
	 * @param: @param pageObject
	 * @return: PageObject<Chronicdis>      
	 */
	public PageObject<IllPolicy> findIllPolicyLimit(PageObject<IllPolicy> pageObject){
		int totals = getCountTotal(COUNT_TB_ILLPOLICY);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT i.id AS id, i.illcode AS illcode, i.cyear AS cyear, i.top_line AS top_line,  ")
			.append(" i.cprop AS cprop, c.illname AS illname, c.pycode AS pycode, c.wbcode as wbcode ")
			.append(" FROM tb_ill_policy i, tb_chronicdis c WHERE i.illcode = c.illcode LIMIT ?,? ");
		try {
			pageObject.setTotalRow(totals);
			List<IllPolicy> list = super.findAll(sql.toString(), 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					IllPolicy.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
