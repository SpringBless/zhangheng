package com.gxwzu.zhangheng.medical.dao.chrocdp;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.chrocdp.ChrocdpDto;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChrocdpDtoDtoDtoDtoDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病证:tb_chrocd_p
 * @author: zhangheng
 * @date:   2019年6月3日 下午4:16:59     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class ChrocdpDtoDao extends BaseDao {
	
	protected static final String COUNT_TB_CHRONICDIS = "SELECT COUNT(*) AS total FROM tb_chrocd_p a, tb_family_info b WHERE a.cp_id_card = b.card_code ";
	
	/*
	 * CREATE TABLE `tb_chrocd_p` (
		  `cp_nh_code` varchar(128) NOT NULL COMMENT '农合证号、外键',
		  `cp_id` varchar(32) NOT NULL COMMENT '慢病证号、 主键',
		  `cp_id_card` varchar(32) NOT NULL COMMENT '身份证号、 外键',
		  `cp_names` text COMMENT '疾病名称',
		  `cp_start` datetime NOT NULL COMMENT '起始时间、 ',
		  `cp_end` datetime NOT NULL COMMENT '终止时间。',
		  PRIMARY KEY (`cp_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='慢性病证表';
	 */
	
	/**
	 * @Title: findChrocdpDtoAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取所有慢性病证
	 */
	public List<ChrocdpDto> findChrocdpDtoAll(){
		String sql = "SELECT a.*, b.name AS userName FROM tb_chrocd_p a, tb_family_info b WHERE a.cp_id_card = b.card_code ";
		try {
			List<ChrocdpDto> all = super.findAll(sql, null, ChrocdpDto.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findChrocdpDtoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询findChrocdpDtoLimit  
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<ChrocdpDtoDtoDtoDto>      
	 */
	public PageObject<ChrocdpDto> findChrocdpDtoLimit(PageObject<ChrocdpDto> pageObject){
		int totals = getCountTotal(COUNT_TB_CHRONICDIS);
		String sql = "SELECT a.*, b.name AS userName FROM tb_chrocd_p a, tb_family_info b WHERE a.cp_id_card = b.card_code LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<ChrocdpDto> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					ChrocdpDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
