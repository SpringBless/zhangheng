package com.gxwzu.zhangheng.medical.dao.familyinfo;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfo;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfoDto;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyInfoDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭成员档案信息类， table name ： tb_family_info
 * @author: zhangheng
 * @date:   2019年4月15日 下午10:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyInfoDao extends BaseDao {

	protected static final String COUNT_TB_FAMILY_INFO = "SELECT COUNT(*) AS total FROM tb_family_info WHERE 1=1 ";
	
	/**
	 * @Title: findFamilyInfoAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取所有家庭成员档案
	 */
	public List<FamilyInfo> findFamilyInfoAll(){
		String sql = "SELECT * FROM tb_family_info WHERE 1=1";
		try {
			List<FamilyInfo> all = super.findAll(sql, null, FamilyInfo.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: queryByAjax   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 家庭区域 + 用户名   
	 * @param: @param areaCode
	 * @param: @param name
	 * @return: List<FamilyInfo>      
	 */
	public List<FamilyInfo> queryByAjax(String areaCode, String name){
		try {
			String sql = "SELECT * FROM tb_family_info WHERE family_code LIKE CONCAT('%', ? , '%') ";
			Object[] param = new Object[]{areaCode};
			if(name != null && !"".equals(name)){
				sql += " and name = ? ";
				param = new Object[]{areaCode, name};
			}
			List<FamilyInfo> list = super.findAll(sql, param, FamilyInfo.class);
			return list;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findFamilyInfoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询FamilyInfo  
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<FamilyInfo>      
	 */
	public PageObject<FamilyInfo> findFamilyInfoLimit(PageObject<FamilyInfo> pageObject){
		int totals = getCountTotal(COUNT_TB_FAMILY_INFO);
		String sql = "SELECT * FROM tb_family_info WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<FamilyInfo> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					FamilyInfo.class);
			if(list != null){
				for(int i=0; i<list.size(); i++){
					FamilyInfo familyInfo = list.get(i);
					int nyear = DateUtils.getYearByUtilDate();
					int byear = DateUtils.getYearBySqlDate(familyInfo.getBirthday());
					familyInfo.setAge(nyear - byear);
				}
			}
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findFamilyInfoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询FamilyInfo  
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<FamilyInfo>      
	 */
	public PageObject<FamilyInfo> findFamilyInfoLimit(PageObject<FamilyInfo> pageObject, String familyCode){
		int totals = getCountTotal(COUNT_TB_FAMILY_INFO + " AND family_code=" + familyCode);
		String sql = "SELECT * FROM tb_family_info WHERE 1=1 AND family_code = ? LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<FamilyInfo> list = super.findAll(sql, 
					new Object[]{
							familyCode,
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					FamilyInfo.class);
			if(list != null){
				for(int i=0; i<list.size(); i++){
					FamilyInfo familyInfo = list.get(i);
					int nyear = DateUtils.getYearByUtilDate();
					int byear = DateUtils.getYearBySqlDate(familyInfo.getBirthday());
					familyInfo.setAge(nyear - byear);
				}
			}
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findFamilyInfoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   模糊搜索
	 * @param: @param pageObject
	 * @param: @param name
	 * @return: PageObject<FamilyInfo>      
	 */
	public PageObject<FamilyInfo> findFamilyInfoLimitLike(PageObject<FamilyInfo> pageObject, String name, String familyCode){
		if(!name.startsWith("%")){
			name = "%" + name;
		}
		if(!name.endsWith("%")){
			name = name + "%";
		}
		int totals = 0;
		String sql = null;
		Object[] parmas = null;
		if(familyCode != null && !"".equals(pageObject)){			
			totals = getCountTotal(COUNT_TB_FAMILY_INFO + " AND family_code = "+familyCode+" AND name LIKE '" + name + "'");
			sql = "SELECT * FROM tb_family_info WHERE 1=1 AND family_code =?  AND name LIKE ? LIMIT ?,?";
			parmas = new Object[]{
					familyCode,
					name,
					pageObject.getPageSize() * (pageObject.getPageNo() - 1),
					pageObject.getPageSize()};
		}else {
			totals = getCountTotal(COUNT_TB_FAMILY_INFO + " AND name LIKE '" + name + "'");			
			sql = "SELECT * FROM tb_family_info WHERE 1=1 AND name LIKE ? LIMIT ?,?";
			parmas = new Object[]{
					name,
					pageObject.getPageSize() * (pageObject.getPageNo() - 1),
					pageObject.getPageSize()};
		}
		try {
			pageObject.setTotalRow(totals);
			List<FamilyInfo> list = super.findAll(sql, parmas,FamilyInfo.class);
			if(list != null){
				for(int i=0; i<list.size(); i++){
					FamilyInfo familyInfo = list.get(i);
					int nyear = DateUtils.getYearByUtilDate();
					int byear = DateUtils.getYearBySqlDate(familyInfo.getBirthday());
					familyInfo.setAge(nyear - byear);
				}
			}
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Title: getFamilyInfoByFamilyCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   参合缴费使用
	 * @param: @param familyCode
	 * @return: List<String>      
	 */
	public List<FamilyInfoDto> getFamilyInfoByFamilyCode(String familyCode){
		String sql = "SELECT * FROM tb_family_info WHERE family_code = ?";
		try {
			List<FamilyInfoDto> all = super.findAll(sql, new Object[]{familyCode}, FamilyInfoDto.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: getFamilyInfoInCardCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   IN查询家庭成员信息
	 * @param: @param familyCode
	 * @return: List<FamilyInfo>      
	 */
	public List<FamilyInfo> getFamilyInfoInCardCode(String cardCodes){
		String sql = null;
		if(cardCodes.indexOf(",") != -1){
			String[] split = cardCodes.split(",");
			cardCodes = "";
			for(int i=0;i<split.length;i++){
				cardCodes += "'" + split[i] + "',";
			}
			cardCodes = cardCodes.substring(0, cardCodes.length() - 1);
			sql = "SELECT * FROM tb_family_info WHERE card_code IN ("+cardCodes+")";
		}else {			
			sql = "SELECT * FROM tb_family_info WHERE card_code IN ('"+cardCodes+"')";
		}
		try {
			List<FamilyInfo> all = super.findAll(sql, null, FamilyInfo.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
}
