package com.gxwzu.zhangheng.medical.dao.apply;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.apply.ApplyDto;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ApplyDao   
 * @Description:TODO(这里用一句话描述这个类的作用) 慢病报销   
 * @author: zhangheng
 * @date:   2019年6月4日 下午1:53:42     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class ApplyDtoDao extends BaseDao {
	
	/**
	 * @Title: findApplyLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 慢病报销审核分页   
	 * @param: @param pageObject
	 * @return: PageObject<Apply>      
	 */
	public PageObject<ApplyDto> findApplyLimit(PageObject<ApplyDto> pageObject, String areaCode,
			String name, String startDate, String endDate, String aStatus){
		StringBuffer sql = new StringBuffer();
		sql.append("  SELECT ")
			.append("     distinct t_f_i.*, t_p.* ")
			.append(" FROM ")
			.append("     tb_family_info t_f_i, tb_apply t_p ")
			.append(" WHERE ")
			.append("     t_f_i.card_code = t_p.a_id_card AND t_p.a_status = " + aStatus);
		if(name != null && !"".equals(name)){
			sql.append("  AND t_f_i.name LIKE CONCAT('%','" + name + "','%')  ");
		}
		if(startDate != null && !"".equals(startDate) && endDate != null && !"".equals(endDate)){
			if("3".equals(aStatus)){				
				sql.append("  AND t_p.a_apply_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}else {
				sql.append("  AND t_p.a_create_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}
		}
		if(areaCode != null && !"".equals(areaCode)){			
			sql.append("      AND t_p.a_family_code LIKE CONCAT('%','" + areaCode + "','%') ");
		}
		try {
			int totals = getCountTotal(" SELECT COUNT(*) AS total FROM ( " + sql.toString() + " ) t_table_t");
			sql.append(" LIMIT ?,? ");
			pageObject.setTotalRow(totals);
			List<ApplyDto> list = super.findAll(sql.toString(), 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					ApplyDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public PageObject<ApplyDto> findApplyLimit(PageObject<ApplyDto> pageObject, String areaCode,
			String name, String startDate, String endDate, String aStatus, String bingz){
		StringBuffer sql = new StringBuffer();
		sql.append("  SELECT ")
			.append("     distinct t_f_i.*, t_p.* ")
			.append(" FROM ")
			.append("     tb_family_info t_f_i, tb_apply t_p ")
			.append(" WHERE ")
			.append("     t_f_i.card_code = t_p.a_id_card AND t_p.a_status = " + aStatus);
		if(bingz != null && !"".equals(bingz)){
			sql.append(" AND t_p.a_illname = '" + bingz + "' ");
		}
		if(name != null && !"".equals(name)){
			sql.append("  AND t_f_i.name LIKE CONCAT('%','" + name + "','%')  ");
		}
		if(startDate != null && !"".equals(startDate) && endDate != null && !"".equals(endDate)){
			if("3".equals(aStatus)){				
				sql.append("  AND t_p.a_apply_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}else {
				sql.append("  AND t_p.a_create_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}
		}
		if(areaCode != null && !"".equals(areaCode)){			
			sql.append("      AND t_p.a_family_code LIKE CONCAT('%','" + areaCode + "','%') ");
		}
		try {
			int totals = getCountTotal(" SELECT COUNT(*) AS total FROM ( " + sql.toString() + " ) t_table_t");
			sql.append(" LIMIT ?,? ");
			pageObject.setTotalRow(totals);
			List<ApplyDto> list = super.findAll(sql.toString(), 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					ApplyDto.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ApplyDto> findApplyLimitExcel(String areaCode,
			String name, String startDate, String endDate, String aStatus, String bingz){
		StringBuffer sql = new StringBuffer();
		sql.append("  SELECT ")
			.append("     distinct t_f_i.*, t_p.* ")
			.append(" FROM ")
			.append("     tb_family_info t_f_i, tb_apply t_p ")
			.append(" WHERE ")
			.append("     t_f_i.card_code = t_p.a_id_card AND t_p.a_status = " + aStatus);
		if(bingz != null && !"".equals(bingz)){
			sql.append(" AND t_p.a_illname = '" + bingz + "' ");
		}
		if(name != null && !"".equals(name)){
			sql.append("  AND t_f_i.name LIKE CONCAT('%','" + name + "','%')  ");
		}
		if(startDate != null && !"".equals(startDate) && endDate != null && !"".equals(endDate)){
			if("3".equals(aStatus)){				
				sql.append("  AND t_p.a_apply_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}else {
				sql.append("  AND t_p.a_create_date BETWEEN '" +startDate + "'  AND '" + endDate + "'  ");
			}
		}
		if(areaCode != null && !"".equals(areaCode)){			
			sql.append("      AND t_p.a_family_code LIKE CONCAT('%','" + areaCode + "','%') ");
		}
		try {
			List<ApplyDto> list = super.findAll(sql.toString(), new Object[]{}, ApplyDto.class);
			return list;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
