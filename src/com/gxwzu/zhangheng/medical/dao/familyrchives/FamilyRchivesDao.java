package com.gxwzu.zhangheng.medical.dao.familyrchives;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.familyrchives.FamilyRchives;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyRchivesDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭档案管理
 * @author: zhangheng
 * @date:   2019年4月16日 下午12:37:24     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyRchivesDao extends BaseDao {
	
	protected static final String COUNT_TB_FAMILY = "SELECT COUNT(*) AS total FROM tb_family_rchives";
	
	/**
	 * @Title: findFamilyRchivesAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有家庭档案管理
	 */
	public List<FamilyRchives> findFamilyRchivesAll(){
		String sql = "SELECT * FROM tb_family_rchives WHERE 1=1";
		try {
			List<FamilyRchives> list = super.findAll(sql, null, FamilyRchives.class);
			return list;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findFamilyRchivesLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  家庭档案管理分页 
	 */
	public PageObject<FamilyRchives> findFamilyRchivesLimit(PageObject<FamilyRchives> pageObject){
		int totals = getCountTotal(COUNT_TB_FAMILY);
		String sql = "SELECT * FROM tb_family_rchives WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<FamilyRchives> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					FamilyRchives.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 模糊搜索,区域编码
	 * @Title: findFamilyRchivesLikeLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<FamilyRchives>      
	 * @throws
	 */
	public PageObject<FamilyRchives> findFamilyRchivesLikeLimit(PageObject<FamilyRchives> pageObject, String keywords, String areaCode){
		String toSql = "SELECT COUNT(*) AS total FROM tb_family_rchives WHERE 1=1 ";
		String sql = "SELECT * FROM tb_family_rchives WHERE 1=1 ";
		if(keywords != null && !"".equals(keywords)){
			toSql =  toSql + "and householder LIKE '%"+ keywords +"%' ";
			sql = sql + " AND householder LIKE '%" + keywords + "%' ";
		}
		if(areaCode != null && !"".contentEquals(areaCode)){
			toSql = toSql + " and family_code LIKE '"+ areaCode +"%'";
			sql = sql + " and family_code LIKE '"+ areaCode +"%'";
		}
		sql += " LIMIT ?,? ";
		int totals = getCountTotal(toSql);
		try {
			pageObject.setTotalRow(totals);
			List<FamilyRchives> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					FamilyRchives.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
}
