package com.gxwzu.zhangheng.medical.dao.role;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.role.Role;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  RoleDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年3月29日 下午12:29:14     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class RoleDao extends BaseDao {
	
	protected static final String COUNT_TB_ROLE = "SELECT COUNT(*) AS total FROM tb_role";
	
	/**
	 * @Title: findRoleAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有角色  
	 */
	public List<Role> findRoleAll(){
		String sql = " SELECT * FROM  tb_role WHERE 1=1";
		try {
			List<Role> roleAll = super.findAll(sql, null, Role.class);
			return roleAll;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Title: findRolesLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  角色分页 
	 */
	public PageObject<Role> findRolesLimit(PageObject<Role> pageObject){
 		int totals = getCountTotal(COUNT_TB_ROLE);
		String sql = "SELECT * FROM tb_role WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Role> roles = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
							Role.class);
			pageObject.setData(roles);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
}
