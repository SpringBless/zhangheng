package com.gxwzu.zhangheng.medical.dao.payinfo;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.payinfo.PayInfo;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * 参合缴费配置信息
 * @ClassName:  PayInfoDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月18日 上午11:49:49     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PayInfoDao extends BaseDao {
	
	protected static final String COUNT_TB_AREA = "SELECT COUNT(*) AS total FROM tb_pay_info";
	
	/**
	 * @Title: findPayInfoAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取所有交
	 */
	public List<PayInfo> findPayInfoAll(){
		String sql = "SELECT * FROM tb_pay_info WHERE 1=1";
		try {
			List<PayInfo> all = super.findAll(sql, null, PayInfo.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findPayInfoInfoLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合交费配置信息分页 
	 */
	public PageObject<PayInfo> findPayLimit(PageObject<PayInfo> pageObject){
		int totals = getCountTotal(COUNT_TB_AREA);
		String sql = "SELECT * FROM tb_pay_info WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<PayInfo> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					PayInfo.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
