package com.gxwzu.zhangheng.medical.dao.chronicdis;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChronicdisDao   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病分类管理类 数据访问层
 * @author: zhangheng
 * @date:   2019年4月14日 上午11:59:33     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class ChronicdisDao extends BaseDao {
	
	protected static final String COUNT_TB_CHRONICDIS = "SELECT COUNT(*) AS total FROM tb_chronicdis";
	
	/**
	 * @Title: findChronicdisAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取所有慢性病分类
	 */
	public List<Chronicdis> findChronicdisAll(){
		String sql = "SELECT * FROM tb_chronicdis WHERE 1=1";
		try {
			List<Chronicdis> all = super.findAll(sql, null, Chronicdis.class);
			return all;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findChronicdisLimit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 分页查询Chronicdis  
	 * @param: @param pageObject
	 * @param: @return      
	 * @return: PageObject<Chronicdis>      
	 */
	public PageObject<Chronicdis> findChronicdisLimit(PageObject<Chronicdis> pageObject){
		int totals = getCountTotal(COUNT_TB_CHRONICDIS);
		String sql = "SELECT * FROM tb_chronicdis WHERE 1=1 LIMIT ?,?";
		try {
			pageObject.setTotalRow(totals);
			List<Chronicdis> list = super.findAll(sql, 
					new Object[]{
							pageObject.getPageSize() * (pageObject.getPageNo() - 1),
							pageObject.getPageSize()},
					Chronicdis.class);
			pageObject.setData(list);
			return pageObject;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
