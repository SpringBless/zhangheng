package com.gxwzu.zhangheng.medical.domain.user;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.RowMapper;
import com.gxwzu.zhangheng.medical.domain.role.Role;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.EncryptUtil;

/**
 * @ClassName:  User   
 * @Description:TODO(这里用一句话描述这个类的作用)   用户信息实体
 * @author: zhangheng
 * @date:   2019年3月27日 下午4:14:03     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class User implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * CREATE TABLE `tb_user` (
		  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '编号',
		  `company_id` int(32) NOT NULL COMMENT '归属公司',
		  `office_id` int(32) NOT NULL COMMENT '归属部门',
		  `login_name` varchar(100) NOT NULL COMMENT '登录名',
		  `password` varchar(100) NOT NULL COMMENT '密码',
		  `no` varchar(100) DEFAULT NULL COMMENT '工号',
		  `name` varchar(100) NOT NULL COMMENT '姓名',
		  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
		  `phone` varchar(200) DEFAULT NULL COMMENT '电话',
		  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
		  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
		  PRIMARY KEY (`id`),
		  KEY `tb_user_office_id` (`office_id`),
		  KEY `tb_user_login_name` (`login_name`),
		  KEY `tb_user_company_id` (`company_id`),
		  KEY `tb_user_del_flag` (`del_flag`)
		) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='用户表';
	 */

	/**
	 * `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '编号',
	 */
	private Long id;
	
	/**
	 * `company_id` int(32) NOT NULL COMMENT '归属公司',
	 */
	private Long companyId;
	
	/**
	 *  `office_id` int(32) NOT NULL COMMENT '归属部门',
	 */
	private Long officeId;
	
	/**
	 * `login_name` varchar(100) NOT NULL COMMENT '登录名',
	 */
	private String loginName;
	
	/**
	 * `password` varchar(100) NOT NULL COMMENT '密码',
	 */
	private String password;
	
	/**
	 *  `no` varchar(100) DEFAULT NULL COMMENT '工号',
	 */
	private String no;
	
	/**
	 * `name` varchar(100) NOT NULL COMMENT '姓名',
	 */
	private String name;
	
	/**
	 * `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
	 */
	private String email;
	
	/**
	 * `phone` varchar(200) DEFAULT NULL COMMENT '电话',
	 */
	private String phone;
	
	/**
	 * `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
	 */
	private String remarks;
	
	/**
	 * `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
	 */
	private String delFlag;
	
	/**
	 * 用户对应角色列表
	 */
	private Set<Role> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Long officeId) {
		this.officeId = officeId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	/**
	 * @Title: hasRoles   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获取当前用户id为userId的角色信息 
	 */
	public Set<Role> hasRoles(Long userId){
		StringBuffer sql = new StringBuffer();
			sql.append("SELECT distinct t_r.* FROM tb_role t_r, ")
			.append("			(SELECT role_id FROM tb_user_role WHERE user_id = ?) temp ")
			.append("	WHERE t_r.id = temp.role_id");
			/**
			 * SELECT distinct t_r.* FROM tb_role t_r, (SELECT role_id FROM tb_user_role WHERE user_id = 1) temp  
			 * WHERE t_r.id = temp.role_id
			 */
			try {
				Set<Role> roles = BaseDao.findSetAll(sql.toString(), new Object[]{userId}, Role.class);
				return roles;
			} catch (DBException e) {
				e.printStackTrace();
			}
		return null;
	}
	
	/**
	 * @Title: getRoles   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取用户角色
	 */
	public List<Long> findUserRoles(Long userId) {
		String sql = "SELECT role_id FROM tb_user_role WHERE user_id=?";
		List<Long> curentRoles = BaseDao.findAll(new RowMapper<Long>() {
			@Override
			public Long mapping(ResultSet resultSet) throws SQLException {
				return resultSet.getLong("role_id");
			}
		}, sql, new Object[]{userId});
		return curentRoles;
	}

	public User() {}

	@Override
	public String toString() {
		return "User [id=" + id + ", companyId=" + companyId + ", officeId=" + officeId + ", loginName=" + loginName
				+ ", password=" + password + ", no=" + no + ", name=" + name + ", email=" + email + ", phone=" + phone
				+ ", remarks=" + remarks + ", delFlag=" + delFlag + ", roles=" + roles + "]";
	}
	
	/**
	 * @Title: deleteUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  删除用户（使用户处于停用状态） 
	 */
	public boolean deleteUser(Long userId){
		String userSql = "UPDATE tb_user SET del_flag = 1 WHERE id = ?";
		String userRole = "DELETE FROM tb_user_role WHERE user_id = ?";
		boolean branch = BaseDao.branch(
				new String[]{userSql, userRole}, 
				new Object[]{userId}, 
				new Object[]{userId});
		return branch;
	}
	
	/**
	 * @Title: deleteRoleByUserId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除用户id为userId的用户角色表中记录
	 */
	public boolean deleteRoleByUserId(Long userId){
		String sql = "DELETE FROM tb_user_role WHERE user_id = ?";
		int update = BaseDao.toUpdate(sql, new Object[]{userId});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: updateUserAndRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新用户及其角色
	 */
	public boolean updateUserAndRole(User user, String[] roleIds){
		StringBuffer userSql = new StringBuffer();
		userSql.append("UPDATE tb_user SET company_id=?, office_id=?, login_name=?, ")
			.append("		password=?, no=?, name=?, email=?, phone=?, remarks=?, del_flag=? ")
			.append("	WHERE id=? ");
		String[] sql = null;
		Object[][] params = null;
		if(roleIds !=  null && roleIds.length > 0){
			sql = new String[roleIds.length+1];
			sql[0] = userSql.toString();
			params = new Object[roleIds.length+1][];
			params[0] = new Object[11];
			params[0][0] = user.getCompanyId(); 
			params[0][1] = user.getOfficeId(); 
			params[0][2] = user.getLoginName(); 
			params[0][3] = user.getPassword(); 
			params[0][4] = user.getNo(); 
			params[0][5] = user.getName(); 
			params[0][6] = user.getEmail(); 
			params[0][7] = user.getPhone(); 
			params[0][8] = user.getRemarks(); 
			params[0][9] = user.getDelFlag(); 
			params[0][10] = user.getId(); 
			for(int i=0;i<roleIds.length;i++){
				String roleMenuSql = " INSERT INTO tb_user_role(user_id, role_id) VALUES(?,?)";
				sql[i+1] = roleMenuSql;
				params[i+1] = new Object[2];
				params[i+1][0] = user.getId();
				params[i+1][1] = roleIds[i];				
			}
		}else {
			sql = new String[]{userSql.toString()};
			params = new Object[1][11];
			params[0][0] = user.getCompanyId(); 
			params[0][1] = user.getOfficeId(); 
			params[0][2] = user.getLoginName(); 
			params[0][3] = user.getPassword(); 
			params[0][4] = user.getNo(); 
			params[0][5] = user.getName(); 
			params[0][6] = user.getEmail(); 
			params[0][7] = user.getPhone(); 
			params[0][8] = user.getRemarks(); 
			params[0][9] = user.getDelFlag(); 
			params[0][10] = user.getId();  
		}
		return BaseDao.branch(sql, params);
	}
	
	/**
	 * @Title: findUserById   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   通过用户id查找用户
	 */
	public User findUserById(Long id){
		String sql = "SELECT * FROM tb_user WHERE id=?";
		try {
			User user = BaseDao.getObject(sql, new Object[]{id}, User.class);
			return user;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: saveUserAndRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 添加用户且添加角色
	 */
	public boolean saveUserAndRole(User user, String[] roleIds){
		long maxId = findMaxUserId();
		user.setPassword(encoding(user.getPassword().getBytes(),
				EncryptUtil.generateSalt(EncryptUtil.GENERATE_SALT_SIZE),
				EncryptUtil.DEFULT_SIZE));
		user.setId(maxId+1);
		StringBuffer userSql = new StringBuffer();
		userSql.append(" INSERT INTO tb_user(id, company_id, office_id, login_name, ")
			.append("		    password, no, name, email, phone, remarks, del_flag ) ")
			.append("	 VALUES(?,?,?,?,?,?,?,?,?,?,?)");
		String[] sql = null;
		Object[][] params = null;
		if(roleIds !=  null && roleIds.length > 0){
			sql = new String[roleIds.length+1];
			sql[0] = userSql.toString();
			params = new Object[roleIds.length+1][];
			params[0] = new Object[11];
			params[0][0] = user.getId(); 
			params[0][1] = user.getCompanyId(); 
			params[0][2] = user.getOfficeId(); 
			params[0][3] = user.getLoginName(); 
			params[0][4] = user.getPassword(); 
			params[0][5] = user.getNo(); 
			params[0][6] = user.getName(); 
			params[0][7] = user.getEmail(); 
			params[0][8] = user.getPhone(); 
			params[0][9] = user.getRemarks(); 
			params[0][10] = user.getDelFlag(); 
			for(int i=0;i<roleIds.length;i++){
				String roleMenuSql = " INSERT INTO tb_user_role(user_id, role_id) VALUES(?,?)";
				sql[i+1] = roleMenuSql;
				params[i+1] = new Object[2];
				params[i+1][0] = user.getId();
				params[i+1][1] = roleIds[i];				
			}
		}else {
			sql = new String[]{userSql.toString()};
			params = new Object[1][11];
			params[0][0] = user.getId(); 
			params[0][1] = user.getCompanyId(); 
			params[0][2] = user.getOfficeId(); 
			params[0][3] = user.getLoginName(); 
			params[0][4] = user.getPassword(); 
			params[0][5] = user.getNo(); 
			params[0][6] = user.getName(); 
			params[0][7] = user.getEmail(); 
			params[0][8] = user.getPhone(); 
			params[0][9] = user.getRemarks(); 
			params[0][10] = user.getDelFlag(); 
		}
		return BaseDao.branch(sql, params);
	}
	
	/**
	 * @Title: findMaxUserId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取当前用户的最大id
	 */
	private long findMaxUserId(){
		String sql = " SELECT max(id) AS id FROM tb_user WHERE 1=1 ";
		try {
			User user = BaseDao.getObject(sql, null, User.class);
			if(user != null && user.getId() != null){
				return user.getId();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return 0L;
	} 
	
	/**
	 * @Title: login   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   用户登录处理方法
	 * @return: User
	 */
	public User login(String loginName, String password){
		if(loginName != null && !"".equals(loginName)){
			this.loginName = loginName;
		}
		if(password != null && !"".equals(password)){
			this.password = password;
		}
		try {
			User dbUser = findUserByLoginName(this.loginName);
			if(dbUser == null){
				return null;
			}else if("0".equals(dbUser.getDelFlag())){
				//解密
				boolean b = decoding(dbUser.getPassword(), this.password, 
						EncryptUtil.GENERATE_SALT_SIZE, EncryptUtil.DEFULT_SIZE);
				return b ? dbUser : null;			
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findUserByLoginName   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   通过登录名获取用户实体
	 * @param: @param loginName
	 * @return: User      
	 * @throws DBException 
	 */
	protected User findUserByLoginName(String loginName) throws DBException{
		String sql = "SELECT * FROM tb_user WHERE login_name=?";
		User user = BaseDao.getObject(sql, new Object[]{loginName}, User.class);
		return user;
	}
	
	/**
	 * 默认加密
	 * @param inPwd 需要加密的原数据byte字节数组
	 * @param salt 附加的加密byte字节数组
	 * @param size 循环加密次数
	 * @return String encoding
	 */
	public static String encoding(byte[] inPwd, byte[] salt, int size){
		if(salt == null || salt.length == 0){
			salt = EncryptUtil.generateSalt(EncryptUtil.GENERATE_SALT_SIZE);
		}
		if(size <=0){
			size = EncryptUtil.DEFULT_SIZE;
		}
		String encodeHexSalt = EncryptUtil.encodeHex(salt);
		byte[] sha1InPwd = EncryptUtil.sha1(inPwd, salt, size);
		String encodeHexPwd = EncryptUtil.encodeHex(sha1InPwd);
		String encodingPwd = encodeHexSalt + encodeHexPwd;
		return encodingPwd;
	}
	
	/**
	 * 解密
	 * @param pwd 数据库用户加密的密码
	 * @param inPwd 对比的密码
	 * @param saltSize 加密的掩码长度
	 * @param decodingSize 解密次数
	 */
	public boolean decoding(String pwd, String inPwd, int saltSize, int decodingSize){
		boolean flag = false;
		String salt = pwd.substring(0, saltSize * 2);//解密后的掩码
		byte[] decodeSalt = EncryptUtil.decodeHex(salt);//解密随机数
		byte[] encodingInPwd = EncryptUtil.sha1(inPwd.getBytes(), decodeSalt, decodingSize);
		String encodeHexInPwd = EncryptUtil.encodeHex(encodingInPwd);
		String inPwdConding = salt + encodeHexInPwd;
		flag = pwd.equals(inPwdConding);
		return flag;
	}
	
	/**
	 * @Title: main   
	 * @Description: TODO(这里用一句话描述这个方法的作用)    加密密码测试
	 */
	public static void main(String[] args) {
		System.out.println(encoding("123".getBytes(),
				EncryptUtil.generateSalt(EncryptUtil.GENERATE_SALT_SIZE),
				EncryptUtil.DEFULT_SIZE));
	}
	
}
