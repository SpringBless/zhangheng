package com.gxwzu.zhangheng.medical.domain.payinfo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * 参合缴费配置信息表
 * @ClassName:  PayInfo   
 * @Description:TODO(这里用一句话描述这个类的作用)   tb_pay_info
 * @author: zhangheng
 * @date:   2019年5月18日 上午11:34:35     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PayInfo implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * create table tb_pay_info(
		   id bigint(11) not null primary key auto_increment COMMENT '主键',
		   years varchar(4) not null COMMENT '当前所属年份',
		   end_date datetime not null Comment '当年结束缴费时间',
		   price decimal(10,2) not null default '0.00' comment '当年缴费每人金额'
		) comment '参合缴费配置信息表';
	 */
	
	/**
	 * id bigint(11) not null primary key auto_increment COMMENT '主键',
	 */
	private Long id;
	
	/**
	 * years varchar(4) not null COMMENT '当前所属年份',
	 */
	private String years;
	
	/**
	 * end_date datetime not null Comment '当年结束缴费时间',
	 */
	private Date endDate;
	
	/**
	 * price decimal(10,2) not null default '0.00' comment '当年缴费每人金额'
	 */
	private BigDecimal price;
	
	public PayInfo(){}
	
	public PayInfo(Long id){
		this.id = id;
		this.loadDB();
	}
	
	private void loadDB(){
		try {
			PayInfo payInfo = BaseDao.getObject( "SELECT * FROM tb_pay_info WHERE id = ? ", new Object[]{this.id}, PayInfo.class);
			this.id = payInfo.getId();
			this.endDate = payInfo.getEndDate();
			this.years = payInfo.getYears();
			this.price = payInfo.getPrice();
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	public boolean add(){
		return this.addToDB();
	}
	
	private boolean addToDB() {
		/**
		 * id bigint(11) not null primary key auto_increment COMMENT '主键',
		   years varchar(4) not null COMMENT '当前所属年份',
		   end_date datetime not null Comment '当年结束缴费时间',
		   price decimal(10,2) not null default '0.00' comment '当年缴费每人金额'
		 */
		int update = BaseDao.toUpdate("INSERT INTO tb_pay_info(years, end_date, price) VALUES(?,?,?)", 
				new Object[]{this.years, this.endDate, this.price});
		return BaseDao.getBoolean(update);
	}

	public boolean update(){
		return this.updateToDB();
	}
	
	private boolean updateToDB(){
		int update = BaseDao.toUpdate("UPDATE tb_pay_info SET years=?, end_date=?,price=? WHERE id = ?", 
				new Object[]{this.years, this.endDate, this.price, this.id});
		return BaseDao.getBoolean(update);
	}
	
	public boolean del(){
		return this.delFromDB();
	}
	
	private boolean delFromDB(){
		int update = BaseDao.toUpdate("DELETE FROM tb_pay_info WHERE id = ?", new Object[]{this.id});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 通过年份获取当前年份的参合缴费配置信息
	 * @Title: findPayInfoByYear   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param year
	 * @return: PayInfo      
	 */
	public PayInfo findPayInfoByYear(String year){
		PayInfo payInfo = null;
		try {
			payInfo = BaseDao.getObject("SELECT * FROM tb_pay_info WHERE years = ?", new Object[]{year}, PayInfo.class);
		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payInfo;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "PayInfo [id=" + id + ", years=" + years + ", endDate=" + endDate + ", price=" + price + "]";
	}
	
}
