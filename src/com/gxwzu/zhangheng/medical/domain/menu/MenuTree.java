package com.gxwzu.zhangheng.medical.domain.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName:  MenuTree   
 * @Description:TODO(这里用一句话描述这个类的作用)   菜单树组件
 * @author: zhangheng
 * @date:   2019年4月8日 下午6:03:56     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MenuTree implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	private List<MenuTreeNode> nodeList;
	
	public MenuTree(boolean flag){
		if(flag){			
			nodeList = new ArrayList<MenuTreeNode>();
			Menu menu = new Menu();
			List<Menu> all = menu.findMenuAll();
			List<Menu> list = menu.getChildren(all, 1L);
			if(list != null){
				for(Menu m : list){
					MenuTreeNode node = new MenuTreeNode(m);
					nodeList.add(node);
				}
			}
		}
	}
	
	public List<MenuTreeNode> getNodeList(){
		return nodeList;
	}
	
	/**
	 * @Title: getAndChNodes   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   勾选已存在权限
	 */
	public List<MenuTreeNode> getAndChNodes(List<Menu> menus){
		if(menus != null && menus.size()>0){
			for(int i=0;i<nodeList.size();i++){
				for(Menu m : menus){
					if(nodeList.get(i).getId().longValue() == m.getId().longValue()){
						nodeList.get(i).setChecked(true);
					}
				}
			}
		}
		return this.nodeList;
	}
	
}
