package com.gxwzu.zhangheng.medical.domain.menu;

/**
 * @ClassName:  MenuDto   
 * @Description:TODO(这里用一句话描述这个类的作用)   菜单传输数据类
 * @author: zhangheng
 * @date:   2019年3月31日 下午3:36:42     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MenuDto extends Menu {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * 父级菜单名称
	 */
	private String parentName;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	@Override
	public String toString() {
		return "MenuDto [parentName=" + parentName + "]";
	}

}
