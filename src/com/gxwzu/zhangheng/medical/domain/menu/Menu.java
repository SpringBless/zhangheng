package com.gxwzu.zhangheng.medical.domain.menu;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  Menu   
 * @Description:TODO(这里用一句话描述这个类的作用)   菜单（权限）信息类
 * @author: zhangheng
 * @date:   2019年3月27日 下午4:33:10     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Menu implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * CREATE TABLE `tb_menu` (
		  `id` int(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
		  `parent_id` int(64) NOT NULL COMMENT '父级编号',
		  `name` varchar(100) NOT NULL COMMENT '名称',
		  `sort` decimal(10,0) NOT NULL COMMENT '排序',
		  `href` varchar(2000) DEFAULT NULL COMMENT '链接',
		  `target` varchar(20) DEFAULT NULL COMMENT '目标',
		  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
		  `is_show` char(1) NOT NULL COMMENT '是否在菜单中显示',
		  `lavel` int(11) NOT NULL DEFAULT '1' COMMENT '菜单级别',
		  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
		  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
		  PRIMARY KEY (`id`),
		  KEY `tb_menu_parent_id` (`parent_id`),
		  KEY `tb_menu_del_flag` (`del_flag`)
		) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='菜单表';
	 */
	
	/**
	 * `id` int(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
	 */
	private Long id;
	
	/**
	 * `parent_id` int(64) NOT NULL COMMENT '父级编号',
	 */
	private Long parentId;
	
	/**
	 * `name` varchar(100) NOT NULL COMMENT '名称',
	 */
	private String name;
	
	/**
	 * `sort` decimal(10,0) NOT NULL COMMENT '排序',
	 */
	private BigDecimal sort;
	
	/**
	 * `href` varchar(2000) DEFAULT NULL COMMENT '链接',
	 */
	private String href;
	
	/**
	 * `target` varchar(20) DEFAULT NULL COMMENT '目标',
	 */
	private String target;
	
	/**
	 * `icon` varchar(100) DEFAULT NULL COMMENT '图标',
	 */
	private String icon;
	
	/**
	 * `is_show` char(1) NOT NULL COMMENT '是否在菜单中显示',
	 */
	private String isShow;
	
	/**
	 * `lavel` int(11) NOT NULL DEFAULT '1' COMMENT '菜单级别',
	 */
	private Long lavel;
	
	/**
	 * `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
	 */
	private String remarks;
	
	/**
	 * `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
	 */
	private String delFlag;
	
	/**
	 * 上下级菜单
	 */
	private List<Menu> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSort() {
		return sort;
	}

	public void setSort(BigDecimal sort) {
		this.sort = sort;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	public Long getLavel() {
		return lavel;
	}

	public void setLavel(Long lavel) {
		this.lavel = lavel;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public Menu() {}

	@Override
	public String toString() {
		return "Menu [id=" + id + ", parentId=" + parentId + ", name=" + name + ", sort=" + sort + ", href=" + href
				+ ", target=" + target + ", icon=" + icon + ", isShow=" + isShow + ", lavel=" + lavel + ", remarks="
				+ remarks + ", delFlag=" + delFlag + "]";
	}
	
	/**
	 * @Title: findMenuByUserId   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 通过用户id获取角色，后获取菜单权限  
	 */
	public List<Menu> findMenuByUserId(Long userId) {
		StringBuffer qSql = new StringBuffer();
		qSql.append(" SELECT distinct t_m.* ")
			.append(" FROM ")
			.append("	  (SELECT * from tb_menu WHere del_flag =0) t_m, ")
			.append("	  (SELECT distinct t_r_m.menu_id as menu_id ")
			.append("		   from tb_role_menu t_r_m, ")
			.append("		       (SELECT role_id from tb_user_role where user_id = ?) t_u_r ")
			.append("		    where t_u_r.role_id=t_r_m.role_id ")
			.append("	  ) t_r_m ")
			.append(" WHERE t_r_m.menu_id=t_m.id");
		List<Menu> menuAll = null;
		try {
			menuAll = BaseDao.findAll(qSql.toString(), new Object[]{userId}, Menu.class);
		} catch (DBException e) {
			e.printStackTrace();
		}
		return menuAll;
	}
	
	/**
	 * @Title: findMenuBymenuId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  通过 菜单id获取菜单
	 * @param: @param menuId
	 * @return: Menu      
	 */
	protected Menu findMenuBymenuId(Long menuId){
		if(menuId != null && !"".equals(menuId)){
			this.id = menuId;
		}
		String sql = " SELECT * FROM tb_menu WHERE id = ?";
		try {
			Menu menu = BaseDao.getObject(sql, new Object[]{this.id}, Menu.class);
			return menu;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title:  Menu   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   根据menuid获取菜单
	 * @param:  @param menuId  
	 */
	public Menu(Long menuId){
		Menu menu = findMenuBymenuId(menuId);
		if(menu != null){
			this.id = menu.getId();
			this.parentId = menu.getParentId();
			this.name = menu.getName();
			this.sort = menu.getSort();
			this.href = menu.getHref();
			this.target = menu.getTarget();
			this.icon = menu.getIcon();
			this.isShow = menu.getIsShow();
			this.lavel = menu.getLavel();
			this.remarks = menu.getRemarks();
			this.delFlag = menu.getDelFlag();
		}
	}
	
	/**
	 * @Title: findMenuAll   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取所有菜单
	 * @return: List<Menu>      
	 */
	public List<Menu> findMenuAll(){
		try {
			String sql = " SELECT * FROM tb_menu WHERE 1=1";
			List<Menu> menuAll = BaseDao.findAll(sql, null, Menu.class);
			return menuAll; 
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: setChildren   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 初始化菜单列表  
	 * @param: @param menuList      
	 * @return: void      
	 */
	public void setChildren(List<Menu> menuList){
		this.children = menuList;
	}
	
	/**
	 * @Title: getChildren   ,parentId父级id
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获得集合菜单 
	 * @return: List<Menu>      
	 */
	public List<Menu> getChildren(Long parentId){
		List<Menu> returnMenus = new ArrayList<Menu>();
		sortMenuList(returnMenus, this.children, parentId);
		return returnMenus;
	}
	
	/**
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加权限菜单
	 */
	public boolean saveMenu(Menu menu){
		menu.setIsShow("1");
		menu.setDelFlag("0");
		if(menu.getParentId().longValue() == 0){
			menu.setLavel(1L);
		}else {
			Menu parent = findMenuBymenuId(menu.getParentId());
			menu.setLavel(parent.getLavel() + 1);
			if(parent.getLavel().longValue() > 2){
				menu.setIsShow("0");
			}
		}
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO tb_menu ")
			.append(" (parent_id, name, sort, href, target, icon, is_show, lavel, remarks, del_flag) ")
			.append(" VALUES(?,?,?,?,?,?,?,?,?,?)");
		int update = BaseDao.toUpdate(sql.toString(), new Object[]{
				menu.getParentId(), menu.getName(), menu.getSort(), menu.getHref(),
				menu.getTarget(), menu.getIcon(), menu.getIsShow(), menu.getLavel(),
				menu.getRemarks(), menu.getDelFlag()
		});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: updateMenu   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新权限菜单
	 */
	public boolean updateMenu(Menu menu){
		Long tempId = menu.getId();
		menu.setIsShow("1");
		if(menu.getParentId().longValue() == 0){
			menu.setLavel(1L);
		}else if(menu.getId().longValue() != menu.getParentId().longValue()){
			Menu parent = findMenuBymenuId(menu.getParentId());
			menu.setLavel(parent.getLavel() + 1);
			if(parent.getLavel().longValue() > 2){
				menu.setIsShow("0");
			}
		}else if(menu.getId().longValue() == menu.getParentId().longValue()){
			Menu parent = findMenuBymenuId(menu.getId());
			menu.setParentId(parent.getParentId());
		}
		menu.setId(tempId);
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE tb_menu ")
			.append(" SET parent_id=?, name=?, sort=?, href=?, target=?, icon=?, ")
			.append(" is_show=?, lavel=?, remarks=?, del_flag=? ")
			.append(" WHERE id = ? ");
		int update = BaseDao.toUpdate(sql.toString(), new Object[]{
				menu.getParentId(), menu.getName(), menu.getSort(), menu.getHref(),
				menu.getTarget(), menu.getIcon(), menu.getIsShow(), menu.getLavel(),
				menu.getRemarks(), menu.getDelFlag(), menu.getId()
		});
		return BaseDao.getBoolean(update);
	}
	/**
	 * @Title: delMenu   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除权限菜单
	 */
	public boolean delMenu(Long id){
		if(id != null){
			this.id = id;			
		}
		String sql = " UPDATE tb_menu SET del_flag = 1 WHERE id = ? OR parent_id= ? ";
		int update = BaseDao.toUpdate(sql, new Object[]{ this.id, this.id });
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: children   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取子节点/孙子节点。。。
	 * @return: List<Menu>      
	 */
	public List<Menu> children(){
		try {
			String sql = " SELECT * FROM tb_menu WHERE parent_id = ? ";
			List<Menu> childrens = BaseDao.findAll(sql, new Object[]{this.id}, Menu.class);
			return childrens;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: getChildren   ,parentId父级id
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获得集合菜单 
	 * @return: List<Menu>      
	 */
	public List<Menu> getChildren(List<Menu> menuList, Long parentId){
		if(menuList != null && menuList.size() > 0){
			this.children = menuList;
		}
		List<Menu> returnMenus = new ArrayList<Menu>();
		sortMenuList(returnMenus, this.children, parentId);
		return returnMenus;
	}
	
	
	/**
	 * 获取所有的子节点(包括儿子，孙子 /不包括自己 )
	 * @Title: getAllChildren  
	 * @param returnTreeList
	 * @param treeList
	 * @param parentId
	 */
    public Map<Long,Menu> getAllChildrenMap (Map<Long,Menu> childrenMap,
                                      List<Menu> treeList,
                                           long parentId) {  	
    	for(Menu tree:treeList){
    		//遍历出父id等于parentId的所有子节点
    		if(tree.getParentId().longValue()==parentId){
    			childrenMap.put(tree.getId(), tree);
    			getAllChildrenMap(childrenMap,treeList,tree.getId());
    		}
    	}
    	return childrenMap;
    }
	
	/**
     * 对菜单列表进行排序
     * @param menuList
     * @param parentId
     * @param cascade
     */
    public static void sortMenuList (List<Menu> returnMenus,
                                     List<Menu> menuList,
                                           Long parentId) {
    	//轮询所有菜单
        for (int i = 0; i < menuList.size(); i++) {
            Menu m = menuList.get(i);
            //找到第一级菜单
            if (m.getParentId() != null&& m.getParentId().equals(parentId)) {
                returnMenus.add(m);             
                for (Menu child : menuList) {
                	//准备递归
                    if (child.getParentId() != null && child.getParentId().equals(parentId)) {
                        //做递归
                        sortMenuList(returnMenus, menuList, m.getId());
                        break;
                    }
                }
                
            }
        }
    }
	
}
