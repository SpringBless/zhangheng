package com.gxwzu.zhangheng.medical.domain.menu;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @ClassName:  MenuTreeNode   
 * @Description:TODO(这里用一句话描述这个类的作用)   定义与zTree的节点对象有相同属性的Node类
 * @author: zhangheng
 * @date:   2019年3月28日 下午7:23:41     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class MenuTreeNode {
	
	private Long id;
	private Long pId;
	private String name;
	@JSONField(serialize=false)
	private List<MenuTreeNode>children;
	private boolean checked;
    private Long level;
    private String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getLevel() {
		return level;
	}
	public void setLevel(Long level) {
		this.level = level;
	}
	private boolean open;
	
	public MenuTreeNode(Menu menu){
		this.id =menu.getId();
		this.name =menu.getName();
		this.pId =menu.getParentId();
		this.open=true;
		this.level =menu.getLavel();
		this.url =menu.getHref();
		
	}
	
	public MenuTreeNode(Long id, String name, List<MenuTreeNode> children) {
		super();
		this.id = id;
		this.name = name;
		this.children = children;
	}
	
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<MenuTreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<MenuTreeNode> children) {
		this.children = children;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public Long getPId() {
		return pId;
	}
	public void setPId(Long pId) {
		this.pId = pId;
	}
	
	@Override
	public String toString() {
		return "MenuTreeNode [id=" + id + ", pId=" + pId + ", name=" + name + ", children=" + children + ", checked="
				+ checked + ", level=" + level + ", url=" + url + ", open=" + open + "]";
	}

}
