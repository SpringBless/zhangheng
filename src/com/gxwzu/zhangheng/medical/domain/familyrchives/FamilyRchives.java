package com.gxwzu.zhangheng.medical.domain.familyrchives;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.EntityMapper;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  FamilyRchives   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭档案管理类， table name ： tb_family_rchives
 * @author: zhangheng
 * @date:   2019年4月15日 下午10:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyRchives implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * county_code varchar(64) not null COMMENT '县级编号',
	 */
	private String countyCode;
	
	/**
	 * towns_code varchar(64) not null  COMMENT '乡镇编号',
	 */
	private String townsCode;
	
	/**
	 * village_code varchar(64) not null  COMMENT '村编号',
	 */
    private String villageCode;
    
    /**
     * group_code varchar(128) not null  COMMENT '组编号', 
     */
    private String groupCode;
    
    /**
     * family_code varchar(128) not null primary key COMMENT '主键，家庭编号',
     */
    private String familyCode;
    
    /**
     * family_property varchar(64) not null COMMENT '户属性',
     */
    private String familyProperty;
    
    /**
     * householder varchar(32) not null  COMMENT '户主姓名',
     */
    private String householder;
    
    /**
     * family_number tinyint default '1'  COMMENT '家庭人口数',
     */
    private Integer familyNumber;
    
    /**
     * agricultural tinyint default '0'  COMMENT '农业人口数',
     */
    private Integer agricultural;
    
    /**
     * address varchar(255)  COMMENT '家庭住址',
     */
    private String address;
    
    /**
     * create_time datetime  COMMENT '创建档案时间',
     */
    private Date createTime;
    
    /**
     * greffier int not null COMMENT '外键，登记员id'
     */
    private Long greffier;
    
    public FamilyRchives(){
    	
    }
    
    public FamilyRchives(String familyCode){
    	this.familyCode = familyCode;
    	try {
			this.loadDB();
		} catch (DBException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * @Title: loadDB   
     * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中加载family_code为familyCode的数据
     */
    private void loadDB() throws DBException {
    	try {			
    		FamilyRchives entity = BaseDao.getObject(
    				" SELECT * FROM tb_family_rchives WHERE family_code = ? ",
    				new Object[]{this.familyCode},
    				FamilyRchives.class);
    		if(entity != null){
    			this.countyCode = entity.getCountyCode();
    			this.townsCode = entity.getTownsCode();
    			this.villageCode = entity.getVillageCode();
    			this.groupCode = entity.getGroupCode();
    			this.familyCode = entity.getFamilyCode();
    			this.familyProperty = entity.getFamilyProperty();
    			this.householder = entity.getHouseholder();
    			this.familyNumber = entity.getFamilyNumber();
    			this.agricultural = entity.getAgricultural();
    			this.address = entity.getAddress();
    			this.createTime = entity.getCreateTime();
    			this.greffier = entity.getGreffier();
    		}
		} catch (DBException e) {
			throw new DBException("加载family_code为：" + this.familyCode + "的记录失败！");
		}
    }
    
    /**
     * @Title: updateFamilyNumberByFamilyCode   
     * @Description: TODO(这里用一句话描述这个方法的作用)   通过家庭编号，更新家庭成员数
     * @return: boolean      
     */
    public boolean updateFamilyNumberByFamilyCode(){
    	int update = BaseDao.toUpdate(" UPDATE tb_family_rchives SET family_number=? WHERE family_code=? ", new Object[]{this.familyNumber, this.familyCode});
    	return BaseDao.getBoolean(update);
    }
    
    /**
     * @Title: updateAgriculturalByFamilyCode   
     * @Description: TODO(这里用一句话描述这个方法的作用)   通过家庭编号，更新家庭成员农业人口数
     * @return: boolean      
     */
    public boolean updateAgriculturalByFamilyCode(){
    	int update = BaseDao.toUpdate(" UPDATE tb_family_rchives SET agricultural=? WHERE family_code=? ", new Object[]{this.agricultural, this.familyCode});
    	return BaseDao.getBoolean(update);
    }
    
    /**
     * @Title: add   
     * @Description: TODO(这里用一句话描述这个方法的作用)  保存数据到数据库 
     * @return: boolean      
     */
    public boolean add(){
		return this.addToDB();
    }
    
    /**
     * @Title: addToDB   
     * @Description: TODO(这里用一句话描述这个方法的作用)   保存数据到数据库 
     * @return: boolean      
     */
    private boolean addToDB() {
    	String maxcode = this.maxfamilyCode(this.groupCode);// 根据当前组编码获取最大家庭编号
    	this.familyCode = this.initMaxfamilyCode(maxcode);// 初始化家庭编号
    	StringBuffer sbf = new StringBuffer();
    	sbf.append("INSERT INTO tb_family_rchives")
    		.append(" (county_code, towns_code, village_code, group_code, family_code, ")
    		.append(" family_property, householder, family_number, agricultural, ")
    		.append(" address, create_time, greffier ) ")
    		.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		int update = BaseDao.toUpdate(sbf.toString(), 
				new Object[]{
					this.countyCode, this.townsCode, this.villageCode, this.groupCode, this.familyCode,
					this.familyProperty, this.householder, this.familyNumber, this.agricultural,
					this.address, this.createTime, this.greffier
				});
    	return BaseDao.getBoolean(update);
    }
    
    /**
     * @Title: initMaxfamilyCode   
     * @Description: TODO(这里用一句话描述这个方法的作用)   初始化最大家庭编码
     * @param: @param maxcode
     * @return: String      
     */
    private String initMaxfamilyCode(String maxcode){
    	if (maxcode != null && !"".equals(maxcode)) {
			int beginIndex = 0;
			String no = "00";
			if(maxcode.length() > 6){
				beginIndex = maxcode.length() - 2;
				no = maxcode.substring(beginIndex);
			}
			Integer number = 1;
			number = Integer.parseInt(no);
			++number;
			// 使用0补足位数
			no = String.format("%04d", number);
			maxcode = this.groupCode + no;
		} else {
			maxcode = this.groupCode + "0001";// + String.format("%02d", number);
		}
    	return maxcode;
    }
    
    /**
     * @Title: maxfamilyCode   
     * @Description: TODO(这里用一句话描述这个方法的作用) 获取最大family_code  
     * 参数：group_code组编码
     * @return: String      
     */
    private String maxfamilyCode(String groupCode){
    	String max = (String) BaseDao.getObject(new EntityMapper<String>() {

			@Override
			public String getEntity(ResultSet resultSet) throws SQLException {
				String max = resultSet.getString("family_code");
				return max;
			}
		}, "SELECT max(family_code) AS family_code FROM tb_family_rchives WHERE 1=1 AND group_code = ? ", new Object[]{groupCode});
    	return max != null && !"".equals(max) ? max : null;
    }
    
    /**
     * @Title: update   
     * @Description: TODO(这里用一句话描述这个方法的作用)   保存数据到数据库
     * @return: boolean      
     */
    public boolean update(){
    	return this.updateToDB();
    }
    
    /**
     * @Title: updateToDB   
     * @Description: TODO(这里用一句话描述这个方法的作用)  保存数据到数据库 
     * @return: boolean      
     */
    private boolean updateToDB(){
    	StringBuffer sbf = new StringBuffer();
    	sbf.append(" UPDATE tb_family_rchives SET ")
    		.append(" family_property = ?, householder = ?, family_number = ?, ")
    		.append(" agricultural = ?, address = ?, create_time = ?, ")
    		.append(" greffier = ? ")
    		.append(" WHERE family_code = ? ");
    	int update = BaseDao.toUpdate(
    			sbf.toString(),
    			new Object[]{
    				this.familyProperty, this.householder, this.familyNumber,
    				this.agricultural, this.address, this.createTime, 
    				this.greffier, this.familyCode
    			});
    	return BaseDao.getBoolean(update);
    }
    
    /**
     * @Title: del   
     * @Description: TODO(这里用一句话描述这个方法的作用) 删除数据库中 family_code为familyCode的记录  
     * @return: boolean      
     */
    public boolean del(){
    	return this.delFromDB();
    }

    /**
     * @Title: delFromDB   
     * @Description: TODO(这里用一句话描述这个方法的作用)  删除数据库中 family_code为familyCode的记录
     * @return: boolean      
     */
    private boolean delFromDB(){
    	int update = BaseDao.toUpdate("DELETE FROM tb_family_rchives WHERE family_code = ? ", 
    			new Object[]{this.familyCode});
    	return BaseDao.getBoolean(update);
    }
    
	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getTownsCode() {
		return townsCode;
	}

	public void setTownsCode(String townsCode) {
		this.townsCode = townsCode;
	}

	public String getVillageCode() {
		return villageCode;
	}

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getFamilyCode() {
		return familyCode;
	}

	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}

	public String getFamilyProperty() {
		return familyProperty;
	}

	public void setFamilyProperty(String familyProperty) {
		this.familyProperty = familyProperty;
	}

	public String getHouseholder() {
		return householder;
	}

	public void setHouseholder(String householder) {
		this.householder = householder;
	}

	public Integer getFamilyNumber() {
		return familyNumber;
	}

	public void setFamilyNumber(Integer familyNumber) {
		this.familyNumber = familyNumber;
	}

	public Integer getAgricultural() {
		return agricultural;
	}

	public void setAgricultural(Integer agricultural) {
		this.agricultural = agricultural;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getGreffier() {
		return greffier;
	}

	public void setGreffier(Long greffier) {
		this.greffier = greffier;
	}

	@Override
	public String toString() {
		return "FamilyRchives [countyCode=" + countyCode + ", townsCode=" + townsCode + ", villageCode=" + villageCode
				+ ", groupCode=" + groupCode + ", familyCode=" + familyCode + ", familyProperty=" + familyProperty
				+ ", householder=" + householder + ", familyNumber=" + familyNumber + ", agricultural=" + agricultural
				+ ", address=" + address + ", createTime=" + createTime + ", greffier=" + greffier + "]";
	}

}
