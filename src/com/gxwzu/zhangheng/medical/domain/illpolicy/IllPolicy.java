package com.gxwzu.zhangheng.medical.domain.illpolicy;

import java.io.Serializable;
import java.math.BigDecimal;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  IllPolicy   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢病封顶线类，继承自慢性病分类管理类, table name: tb_ill_policy
 * @author: zhangheng
 * @date:   2019年4月19日 上午9:47:49     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class IllPolicy extends Chronicdis implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
	 */
	private Integer id;
	
	/**
	 * `cyear` varchar(12) NOT NULL COMMENT '报销年份政策',
	 */
	private String cyear;

	/**
	 * `top_line` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '当前年份封顶线',
	 */
	private BigDecimal topLine;
	
	/**
	 * `cprop` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '当前年度报销比例',
	 */
	private BigDecimal cprop;

	public IllPolicy(){
		
	}
	
	public IllPolicy(int id){
		this.id = id;
		IllPolicy db = this.loadDB();
		if(db != null){
			this.cprop = db.getCprop();
			this.id = db.getId();
			this.setIllcode(db.getIllcode());
			this.cyear = db.getCyear();
			this.topLine = db.getTopLine();
			this.setIllname(db.getIllname());
			this.setPycode(db.getPycode());
			this.setWbcode(db.getWbcode());
		}
	}
	
	public IllPolicy queryByIllname(String illname){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT i.id AS id, i.illcode AS illcode, i.cyear AS cyear, i.top_line AS top_line, ")
			.append(" i.cprop AS cprop, c.illname AS illname, c.pycode AS pycode, c.wbcode as wbcode ")
			.append(" FROM tb_ill_policy i, tb_chronicdis c ")
			.append(" WHERE c.illname = ? AND i.illcode = c.illcode ");
		try {
			IllPolicy illPolicy = BaseDao.getObject(sql.toString(), new Object[]{illname}, IllPolicy.class);
			return illPolicy;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: loadDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   加载指定id的illpolicy
	 * @return: IllPolicy      
	 */
	private IllPolicy loadDB(){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT i.id AS id, i.illcode AS illcode, i.cyear AS cyear, i.top_line AS top_line, ")
			.append(" i.cprop AS cprop, c.illname AS illname, c.pycode AS pycode, c.wbcode as wbcode ")
			.append(" FROM tb_ill_policy i, tb_chronicdis c ")
			.append(" WHERE i.id = ? AND i.illcode = c.illcode ");
		try {
			IllPolicy illPolicy = BaseDao.getObject(sql.toString(), new Object[]{this.id}, IllPolicy.class);
			return illPolicy;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: del   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  从数据库中删除 
	 * @return: boolean      
	 */
	public boolean del(){
		return this.delToDB();
	}
	
	/**
	 * @Title: delToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中删除
	 * @return: boolean      
	 */
	private boolean delToDB(){
		int update = BaseDao.toUpdate(" DELETE FROM tb_ill_policy WHERE id=? ", new Object[]{this.id});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: update   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新数据到数据库
	 * @return: boolean      
	 */
	public boolean update(){
		return this.updateToDB();
	}
	
	/**
	 * @Title: updateToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新数据到数据库
	 * @return: boolean      
	 */
	private boolean updateToDB(){
		int update = BaseDao.toUpdate(
				" UPDATE tb_ill_policy SET illcode =?, cyear=?, top_line=?, cprop=? WHERE id=?", 
				new Object[]{
					this.getIllcode(), this.cyear, this.topLine, this.cprop, this.id
				});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 保存至数据库
	 * <p>Title: add</p>   
	 * <p>Description: </p>   
	 * @see com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis#add()
	 */
	public boolean add(){
		return this.addToDB();
	}
	
	/**
	 * @Title: addToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  保存至数据库 
	 * @return: boolean      
	 */
	private boolean addToDB(){
		int update = BaseDao.toUpdate(" INSERT INTO tb_ill_policy(illcode, cyear, top_line, cprop) VALUES(?,?,?,?)",
				new Object[]{this.getIllcode(), this.cyear, this.topLine,this.cprop});
		return BaseDao.getBoolean(update);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCyear() {
		return cyear;
	}

	public void setCyear(String cyear) {
		this.cyear = cyear;
	}

	public BigDecimal getTopLine() {
		return topLine;
	}

	public void setTopLine(BigDecimal topLine) {
		this.topLine = topLine;
	}

	public BigDecimal getCprop() {
		return cprop;
	}

	public void setCprop(BigDecimal cprop) {
		this.cprop = cprop;
	}

	@Override
	public String toString() {
		return "IllPolicy [id=" + id + ", cyear=" + cyear + ", topLine=" + topLine + ", cprop=" + cprop + "]";
	}
	
}
