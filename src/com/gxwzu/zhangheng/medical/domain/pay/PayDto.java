package com.gxwzu.zhangheng.medical.domain.pay;

import java.util.Date;

import com.gxwzu.zhangheng.medical.util.DateUtils;

/**
 * 参合交费PayDto
 */
public class PayDto extends Pay {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PayDto(){}
	
	public PayDto(Pay pay, String name){
		setId(pay.getId());
		setCardCode(pay.getCardCode());
		setFamilyCode(pay.getFamilyCode());
		setFapiao(pay.getFapiao());
		setNhCode(pay.getNhCode());
		setPlayDate(pay.getPlayDate());
		setPlayPrice(pay.getPlayPrice());
		setPlayYear(pay.getPlayYear());
		setUserId(pay.getUserId());
		this.name = name;
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	// ****************** 家庭 **********************
	/**
	 * county_code varchar(64) not null COMMENT '县级编号',
	 */
	private String countyCode;
	
	/**
	 * towns_code varchar(64) not null  COMMENT '乡镇编号',
	 */
	private String townsCode;
	
	/**
	 * village_code varchar(64) not null  COMMENT '村编号',
	 */
    private String villageCode;
    
    /**
     * group_code varchar(128) not null  COMMENT '组编号', 
     */
    private String groupCode;
    
    /**
     * family_property varchar(64) not null COMMENT '户属性',
     */
    private String familyProperty;
    
    /**
     * householder varchar(32) not null  COMMENT '户主姓名',
     */
    private String householder;
    
    /**
     * family_number tinyint default '1'  COMMENT '家庭人口数',
     */
    private Integer familyNumber;
    
    /**
     * agricultural tinyint default '0'  COMMENT '农业人口数',
     */
    private Integer agricultural;
    
    /**
     * create_time datetime  COMMENT '创建档案时间',
     */
    private Date createTime;
    
    /**
     * greffier int not null COMMENT '外键，登记员id'
     */
    private Long greffier;
    
    // ******************* 成员 *********************
	
	/**
	 * yl_code varchar(128) not null COMMENT '医疗证卡号',
	 */
	private String ylCode;
	
	/**
	 * hn_code varchar(3) not null COMMENT '户内编号,根据家庭成员增加',
	 */
	private String hnCode;
	
	/**
	 * fm_relation varchar(32) not null COMMENT '与户主关系',
	 */
	private String fmRelation;
	
	/**
	 * gender varchar(12) not null default '男' COMMENT '性别',
	 */
	private String gender;
	
	/**
	 * sf_health varchar(32) not null COMMENT '健康状况',
	 */
	private String sfHealth;
	
	/**
	 * sf_national varchar(64) not null COMMENT '民族',
	 */
	private String sfNational;
	
	/**
	 * sf_education varchar(32) default null COMMENT '文化程度',
	 */
	private String sfEducation;
	
	/**
	 * birthday date not null COMMENT '出生日期，格式：YYYY-MM-DD',
	 */
	private java.sql.Date birthday;
	
	/**
	 * r_attribute varchar(32) default null COMMENT '人员属性',
	 */
	private String rAttribute;
	
	/**
	 * hk_type varchar(3) not null default '是' COMMENT '是否农村户口',
	 */
	private String hkType;
	
	/**
	 * sf_professional varchar(32) default null COMMENT '职业（可有，无）',
	 */
	private String sfProfessional;
	
	/**
	 * workunits varchar(255) default null COMMENT '工作单位（可有，无）',
	 */
	private String workunits;
	
	/**
	 * phone varchar(11) default null COMMENT '联系电话',
	 */
	private String phone;
	
	/**
	 * address varchar(255) default null COMMENT '常住地址'
	 */
	private String address;
	
	/**
	 * COMMENT '家庭成员信息表，字段age(动态生成),'; 
	 */
	private int age;

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getTownsCode() {
		return townsCode;
	}

	public void setTownsCode(String townsCode) {
		this.townsCode = townsCode;
	}

	public String getVillageCode() {
		return villageCode;
	}

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getFamilyProperty() {
		return familyProperty;
	}

	public void setFamilyProperty(String familyProperty) {
		this.familyProperty = familyProperty;
	}

	public String getHouseholder() {
		return householder;
	}

	public void setHouseholder(String householder) {
		this.householder = householder;
	}

	public Integer getFamilyNumber() {
		return familyNumber;
	}

	public void setFamilyNumber(Integer familyNumber) {
		this.familyNumber = familyNumber;
	}

	public Integer getAgricultural() {
		return agricultural;
	}

	public void setAgricultural(Integer agricultural) {
		this.agricultural = agricultural;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getGreffier() {
		return greffier;
	}

	public void setGreffier(Long greffier) {
		this.greffier = greffier;
	}

	public String getYlCode() {
		return ylCode;
	}

	public void setYlCode(String ylCode) {
		this.ylCode = ylCode;
	}

	public String getHnCode() {
		return hnCode;
	}

	public void setHnCode(String hnCode) {
		this.hnCode = hnCode;
	}

	public String getFmRelation() {
		return fmRelation;
	}

	public void setFmRelation(String fmRelation) {
		this.fmRelation = fmRelation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSfHealth() {
		return sfHealth;
	}

	public void setSfHealth(String sfHealth) {
		this.sfHealth = sfHealth;
	}

	public String getSfNational() {
		return sfNational;
	}

	public void setSfNational(String sfNational) {
		this.sfNational = sfNational;
	}

	public String getSfEducation() {
		return sfEducation;
	}

	public void setSfEducation(String sfEducation) {
		this.sfEducation = sfEducation;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public void setBirthday(java.sql.Date birthday) {
		this.birthday = birthday;
	}

	public String getrAttribute() {
		return rAttribute;
	}

	public void setrAttribute(String rAttribute) {
		this.rAttribute = rAttribute;
	}

	public String getHkType() {
		return hkType;
	}

	public void setHkType(String hkType) {
		this.hkType = hkType;
	}

	public String getSfProfessional() {
		return sfProfessional;
	}

	public void setSfProfessional(String sfProfessional) {
		this.sfProfessional = sfProfessional;
	}

	public String getWorkunits() {
		return workunits;
	}

	public void setWorkunits(String workunits) {
		this.workunits = workunits;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		int nyear = DateUtils.getYearByUtilDate();
		int byear = DateUtils.getYearBySqlDate(birthday);
		age = nyear - byear;
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "PayDto [name=" + name + ", countyCode=" + countyCode + ", townsCode=" + townsCode + ", villageCode="
				+ villageCode + ", groupCode=" + groupCode + ", familyProperty="
				+ familyProperty + ", householder=" + householder + ", familyNumber=" + familyNumber + ", agricultural="
				+ agricultural + ", createTime=" + createTime + ", greffier=" + greffier + ", ylCode=" + ylCode + ", hnCode="
				+ hnCode + ", fmRelation=" + fmRelation + ", gender=" + gender + ", sfHealth=" + sfHealth + ", sfNational=" + sfNational + ", sfEducation="
				+ sfEducation + ", birthday=" + birthday + ", rAttribute=" + rAttribute + ", hkType=" + hkType
				+ ", sfProfessional=" + sfProfessional + ", workunits=" + workunits + ", phone=" + phone + ", address="
				+ address + ", age=" + age + "]" + super.toString();
	}
	
}
