package com.gxwzu.zhangheng.medical.domain.pay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.gxwzu.zhangheng.medical.dao.BaseDao;

/**
 * @ClassName:  Pay   
 * @Description:TODO(这里用一句话描述这个类的作用)   参合交费信息类， table name ： tb_pay
 * @author: zhangheng
 * @date:   2019年5月7日 下午3:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Pay implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * create table tb_pay(
		id bigint not null primary key auto_increment COMMENT '参合缴费编号',
		nh_code varchar(64) not null COMMENT '参合证号，外键',
		fapiao varchar(64) not null COMMENT '参合发票号',
		family_code varchar(64) not null COMMENT '家庭编号',
		card_code varchar(32) not null COMMENT '身份证号，外键',
		play_price DECIMAL(6,2) not null DEFAULT '0.00' COMMENT '缴费金额',
		play_year year not null COMMENT ' 交费年度',
		play_date datetime not null COMMENT '交费时间',
		user_id bigint not null COMMENT '操作员'
		) COMMENT '参合缴费登记';
	 */
	
	/**
	 * id bigint not null primary key auto_increment COMMENT '参合缴费编号',
	 */
	private Long id;
	
	/**
	 * nh_code varchar(64) not null COMMENT '参合证号，外键',
	 */
	private String nhCode;
	
	/**
	 * fapiao varchar(64) not null COMMENT '参合发票号',
	 */
	private String fapiao;
	
	/**
	 * family_code varchar(64) not null COMMENT '家庭编号',
	 */
	private String familyCode;
	
	/**
	 * card_code varchar(32) not null COMMENT '身份证号，外键',
	 */
	private String cardCode;
	
	/**
	 * play_price DECIMAL(6,2) not null DEFAULT '0.00' COMMENT '缴费金额',
	 */
	private BigDecimal playPrice;
	
	/**
	 * play_year year not null COMMENT ' 交费年度',
	 */
	private String playYear;
	
	/**
	 * play_date datetime not null COMMENT '交费时间',
	 */
	private Date playDate;
	
	/**
	 * user_id bigint not null COMMENT '操作员'
	 */
	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNhCode() {
		return nhCode;
	}

	public void setNhCode(String nhCode) {
		this.nhCode = nhCode;
	}

	public String getFapiao() {
		return fapiao;
	}

	public void setFapiao(String fapiao) {
		this.fapiao = fapiao;
	}

	public String getFamilyCode() {
		return familyCode;
	}

	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public BigDecimal getPlayPrice() {
		return playPrice;
	}

	public void setPlayPrice(BigDecimal playPrice) {
		this.playPrice = playPrice;
	}

	public String getPlayYear() {
		return playYear;
	}

	public void setPlayYear(String playYear) {
		this.playYear = playYear;
	}

	public Date getPlayDate() {
		return playDate;
	}

	public void setPlayDate(Date playDate) {
		this.playDate = playDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Pay [id=" + id + ", nhCode=" + nhCode + ", fapiao=" + fapiao + ", familyCode=" + familyCode
				+ ", cardCode=" + cardCode + ", playPrice=" + playPrice + ", playYear=" + playYear + ", playDate="
				+ playDate + ", userId=" + userId + "]";
	}

	public boolean add() {
		return this.addToDB();
	}
	
	private boolean addToDB(){
		StringBuffer sbf = new StringBuffer();
		sbf.append("INSERT INTO tb_pay( ")
			.append(" nh_code, fapiao, family_code, card_code, ")
			.append(" play_price, play_year, play_date, user_id ) ")
			.append("  VALUES(?,?,?,?,?,?,?,?) ");
		int update = BaseDao.toUpdate(sbf.toString(), 
				new Object[]{
						this.nhCode, this.fapiao, this.familyCode,
						this.cardCode, this.playPrice, this.playYear, 
						this.playDate, this.userId
				});
		return BaseDao.getBoolean(update);
	}
	
}
