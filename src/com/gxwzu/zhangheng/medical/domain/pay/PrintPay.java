package com.gxwzu.zhangheng.medical.domain.pay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * 打印发票数据类
 * @ClassName:  PrintPay   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月12日 上午10:46:43     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PrintPay implements Serializable{

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * 发票头信息
	 */
	private String[] title;

	/**
	 * 公司名称
	 */
	private String cName;
	
	/**
	 * 发票编号
	 */
	private String fapiao;
	
	/**
	 * 公司地址
	 */
	private String address;
	
	/**
	 * 经办人
	 */
	private String jbr;
	
	/**
	 * 经办人电话
	 */
	private String mobile;
	
	/**
	 * 签字，发票日期
	 */
	private String date;
	
	/**
	 * 数据结果集
	 */
	private List<PayDto> data;
	
	public BigDecimal getCountPrice(){
		BigDecimal bg = new BigDecimal("0");
		if(data != null){
			for(int i=0; i<data.size();i++){
				PayDto payDto = data.get(i);
				BigDecimal playPrice = payDto.getPlayPrice();
				bg = bg.add(playPrice);
			}
		}
		return bg;
	}
	
	public int getDataSize(){
		return data  != null ? data.size() : 0;
	}

	public String[] getTitle() {
		return title;
	}

	public void setTitle(String[] title) {
		this.title = title;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getFapiao() {
		return fapiao;
	}

	public void setFapiao(String fapiao) {
		this.fapiao = fapiao;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getJbr() {
		return jbr;
	}

	public void setJbr(String jbr) {
		this.jbr = jbr;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<PayDto> getData() {
		return data;
	}

	public void setData(List<PayDto> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PrintPay [title=" + Arrays.toString(title) + ", cName=" + cName + ", fapiao=" + fapiao + ", address="
				+ address + ", jbr=" + jbr + ", mobile=" + mobile + ", date=" + date + ", data=" + data + "]";
	}
	
}

