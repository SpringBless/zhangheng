package com.gxwzu.zhangheng.medical.domain.familydata;

import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.RowMapper;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * @ClassName:  FamilyInfoData   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭成员档案信息资源类， table name ： tb_family_data
 * @author: zhangheng
 * @date:   2019年4月15日 下午10:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id int not null primary key auto_increment COMMENT '主键id，自增',
	 */
	private Integer id;
	
	/**
	 * fm_relation varchar(32) not null COMMENT '与户主关系',
	 */
	private String fmRelation;

	/**
	 * gender varchar(12) not null default '男' COMMENT '性别',
	 */
	private String gender;
	
	/**
	 * sf_health varchar(32) not null COMMENT '健康状况',
	 */
	private String sfHealth;
	
	/**
	 * sf_national varchar(64) not null COMMENT '民族',
	 */
	private String sfNational;
	
	/**
	 * sf_education varchar(32) default null COMMENT '文化程度',
	 */
	private String sfEducation;
	
	/**
	 * r_attribute varchar(32) default null COMMENT '人员属性',
	 */
	private String rAttribute;
	
	/**
	 * hk_type varchar(3) not null default '是' COMMENT '是否农村户口',
	 */
	private String hkType;
	
	/**
	 * sf_professional varchar(32) default null COMMENT '职业（可有，无）'
	 */
	private String sfProfessional;
	
	/**
	 * `family_property` varchar(64) DEFAULT NULL COMMENT '户属性',
	 */
	private String familyProperty;
	
	public FamilyData(){
		
	}
	
	/**
	 * 根据字段名获取当前字段非空的数据集
	 * @param column
	 * @param conditions
	 * @return List<String>
	 */
	public List<String> findColumnList(String column){
		String sql = "SELECT distinct " + column + " FROM tb_family_data WHERE " + column + " is not null";
		List<String> list = BaseDao.findAll(new RowMapper<String>() {

			@Override
			public String mapping(ResultSet resultSet) throws SQLException {
				String rString = resultSet.getString(column);
				return rString;
			}
			
		}, sql, null);
		return list != null && list.size() > 0 ? list : null;
	}
	
	
	/**
	 * 测试用
	 * @Title: main   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param args      
	 * @return: void      
	 * @throws
	 */
	public static void main(String[] args) {
		FamilyData fData = new FamilyData();
		List<String> list = fData.findColumnList("sf_professional");
		System.out.println(list);
		
	}
	
	/**
	 * 添加指定字段的数据
	 * @param column
	 * @param value
	 * @return boolean
	 */
	public boolean addColumn(String column, String value){
		String sql = "INSERT INTO tb_family_data("+column+") VALUES(?)";
		int update = BaseDao.toUpdate(sql, new Object[]{ value});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 删除数据库中id存在的数据
	 */
	public boolean del(){
		return this.delFromDB();
	}

	/**
	 * 删除数据库中id存在的数据
	 */
	private boolean delFromDB(){
		String sql = "DELETE FROM tb_family_data WHERE id=?";
		int update = BaseDao.toUpdate(sql, new Object[]{this.id});
		return BaseDao.getBoolean(update);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFmRelation() {
		return fmRelation;
	}

	public void setFmRelation(String fmRelation) {
		this.fmRelation = fmRelation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSfHealth() {
		return sfHealth;
	}

	public void setSfHealth(String sfHealth) {
		this.sfHealth = sfHealth;
	}

	public String getSfNational() {
		return sfNational;
	}

	public void setSfNational(String sfNational) {
		this.sfNational = sfNational;
	}

	public String getSfEducation() {
		return sfEducation;
	}

	public void setSfEducation(String sfEducation) {
		this.sfEducation = sfEducation;
	}

	public String getrAttribute() {
		return rAttribute;
	}

	public void setrAttribute(String rAttribute) {
		this.rAttribute = rAttribute;
	}

	public String getHkType() {
		return hkType;
	}

	public void setHkType(String hkType) {
		this.hkType = hkType;
	}

	public String getSfProfessional() {
		return sfProfessional;
	}

	public void setSfProfessional(String sfProfessional) {
		this.sfProfessional = sfProfessional;
	}

	public String getFamilyProperty() {
		return familyProperty;
	}

	public void setFamilyProperty(String familyProperty) {
		this.familyProperty = familyProperty;
	}

	@Override
	public String toString() {
		return "FamilyInfoData [id=" + id + ", fmRelation=" + fmRelation + ", gender=" + gender + ", sfHealth="
				+ sfHealth + ", sfNational=" + sfNational + ", sfEducation=" + sfEducation + ", rAttribute="
				+ rAttribute + ", hkType=" + hkType + ", sfProfessional=" + sfProfessional + ", familyProperty="
				+ familyProperty + "]";
	}

}
