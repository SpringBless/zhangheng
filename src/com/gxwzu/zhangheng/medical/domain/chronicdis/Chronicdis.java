package com.gxwzu.zhangheng.medical.domain.chronicdis;

import java.io.Serializable;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;


/**
 * @ClassName:  Chronicdis   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病分类管理类
 * @author: zhangheng
 * @date:   2019年4月14日 上午11:40:42     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Chronicdis implements Serializable {
	
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * 疾病编码
	 */
	private String illcode;
	
	/**
	 * 疾病名称
	 */
	private String illname;
	
	/**
	 * 拼音码
	 */
	private String pycode;
	
	/**
	 * 五笔码
	 */
	private String wbcode;
	
	public Chronicdis(){
		
	}
	
	public Chronicdis(String illcode,String illname,String pycode,String wbcode){
		this.illcode =illcode;
		this.illname =illname;
		this.pycode =pycode;
		this.wbcode =wbcode;
	}
	
	/**
	 * @Title: edit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   修改
	 */
	public boolean edit(){
		return this.updateToDB();
	}
	
	/**
	 * @Title: updateToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   将修改后的数据保存至数据库
	 */
	private boolean updateToDB(){
		String  sql=" update tb_chronicdis set illname=?,pycode=?,wbcode=? where illcode=?";
		int update = BaseDao.toUpdate(sql, 
							new Object[]{
								this.illname,
								this.pycode,
								this.wbcode,
								this.illcode
							});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title:  Chronicdis   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   按iilcode加载Chronicdis
	 * @param:  @param id
	 */
	public Chronicdis(String iilcode){
		this.illcode =iilcode;
		this.load();
	}
	
	/**
	 * @Title: load   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   illcode加载Chronicdis
	 */
	private void load(){
		 String sql="select * from tb_chronicdis where illcode=?";
		 Chronicdis ill = null;
		try {
			ill = BaseDao.getObject(sql, new Object[]{this.illcode}, Chronicdis.class);
			if(ill != null){
				this.illcode= ill.getIllcode();
				this.illname = ill.getIllname();
				this.pycode = ill.getPycode();
				this.wbcode = ill.getWbcode();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: del   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除 Chronicdis 通过illcode
	 */
	public boolean del(String illcode){
	    this.illcode =illcode;
	    return this.delFromDB();
	}
	
	/**
	 * @Title: delFromDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)    删除 Chronicdis 通过illcode
	 */
	private boolean delFromDB(){
		String  sql="delete from tb_chronicdis where illcode=?";
		int update = BaseDao.toUpdate(sql, new Object[]{this.illcode});
		return BaseDao.getBoolean(update);
	}

	
	/**
	 * @Title: add   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加慢性病
	 */
	public boolean add() {
    	 return this.saveToDB();
	}
	
	/**
	 * @Title: saveToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加慢性病
	 */
	private boolean saveToDB(){
			String  sql="insert into tb_chronicdis(illcode,illname,pycode,wbcode) values(?,?,?,?) ";
			int update = BaseDao.toUpdate(sql, 
					new Object[]{
							this.illcode,
							this.illname,
							this.pycode,
							this.wbcode
					});
			return BaseDao.getBoolean(update);
	}

	public String getIllcode() {
		return illcode;
	}

	public void setIllcode(String illcode) {
		this.illcode = illcode;
	}

	public String getIllname() {
		return illname;
	}

	public void setIllname(String illname) {
		this.illname = illname;
	}

	public String getPycode() {
		return pycode;
	}

	public void setPycode(String pycode) {
		this.pycode = pycode;
	}

	public String getWbcode() {
		return wbcode;
	}

	public void setWbcode(String wbcode) {
		this.wbcode = wbcode;
	}

	@Override
	public String toString() {
		return "Chronicdis [illcode=" + illcode + ", illname=" + illname + ", pycode=" + pycode + ", wbcode=" + wbcode
				+ "]";
	}
	
}
