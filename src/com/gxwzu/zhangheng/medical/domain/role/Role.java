package com.gxwzu.zhangheng.medical.domain.role;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  Role   
 * @Description:TODO(这里用一句话描述这个类的作用)   角色信息类
 * @author: zhangheng
 * @date:   2019年3月27日 下午4:29:06     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Role implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * CREATE TABLE `tb_role` (
		  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '编号',
		  `name` varchar(100) NOT NULL COMMENT '角色名称',
		  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='角色表';
	 */
	
	/**
	 * `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '编号',
	 */
	private Long id;
	
	/**
	 * `name` varchar(100) NOT NULL COMMENT '角色名称',
	 */
	private String name;
	
	/**
	 * `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
	 */
	private String remarks;
	
	/**
     * 角色对应权限列表
     */
	private Set<Menu> menus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Set<Menu> getMenus() {
		return menus;
	}

	public void setMenus(Set<Menu> menus) {
		this.menus = menus;
	}

	public Role() {}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", remarks=" + remarks + ", menus=" + menus + "]";
	}
	
	/**
	 * @Title: findRoleById   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  roleId获取role 
	 */
	public Role findRoleById(Long id){
		String sql = "SELECT * FROM tb_role WHERE id = ?";
		Role role = null;
		try {
			role = BaseDao.getObject(sql, new Object[]{id}, Role.class);
		} catch (DBException e) {
			e.printStackTrace();
		}
		return role;
	}
	
	/**
	 * @Title: delRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   移除角色及其菜单权限，用户角色
	 */
	public boolean delRole(Long roleId){
		if(roleId != null){
			this.id = roleId;
		}
		String roleSql = " DELETE FROM tb_role WHERE id = ? ";
		String userSql = " DELETE FROM tb_user_role WHERE role_id = ? ";
		String menuSql = " DELETE FROM tb_role_menu WHERE role_id = ? ";
		boolean branch = BaseDao.branch(
				new String[]{roleSql, userSql, menuSql}, 
				new Object[]{this.id}, 
				new Object[]{this.id},
				new Object[]{this.id});
		return branch;
	}
	
	/**
	 * @Title: removeRoleMenu   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  提出角色权限 
	 */
	public boolean removeRoleMenu(Long rolId) {
		String sql = "DELETE FROM tb_role_menu WHERE role_id = ?";
		int update = BaseDao.toUpdate(sql, new Object[]{rolId});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: saveRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  添加角色 
	 */
	public boolean saveRole(Role role){
		boolean saveRole = saveRole(role, null);
		return saveRole;
	}
	
	/**
	 * @Title: saveRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  添加角色 
	 */
	public boolean saveRole(Role role, String menuIds){
		long maxId = findMaxRoleId();
		role.setId(maxId+1);
		String roleSql = " INSERT INTO tb_role(id, name, remarks) VALUES(?,?,?)";
		String[] sql = null;
		Object[][] params = null;
		if(menuIds !=  null && !"".equals(menuIds)){
			String[] split = menuIds.split(",");
			if(split != null && split.length > 0){
				sql = new String[split.length+1];
				sql[0] = roleSql;
				params = new Object[split.length+1][];
				params[0] = new Object[3];
				params[0][0] = role.getId(); 
				params[0][1] = role.getName(); 
				params[0][2] = role.getRemarks(); 
				for(int i=0;i<split.length;i++){
					String roleMenuSql = " INSERT INTO tb_role_menu(role_id, menu_id) VALUES(?,?)";
					sql[i+1] = roleMenuSql;
					params[i+1] = new Object[2];
					params[i+1][0] = role.getId();
					params[i+1][1] = split[i];					
				}
			}
		}else {
			sql = new String[]{roleSql};
			params = new Object[1][3];
			params[0][0] = role.getId();
			params[0][1] = role.getName();
			params[0][2] = role.getRemarks();
		}
		return BaseDao.branch(sql, params);
	}
	
	/**
	 * @Title: saveRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  修改角色，菜单 
	 */
	public boolean updateRoleAndMenu(Role role, String menuIds){
		String roleSql = " UPDATE tb_role SET name=?, remarks=? WHERE id = ? ";
		String[] sql = null;
		Object[][] params = null;
		if(menuIds !=  null && !"".equals(menuIds)){
			String[] split = menuIds.split(",");
			if(split != null && split.length > 0){
				sql = new String[split.length+1];
				sql[0] = roleSql;
				params = new Object[split.length+1][];
				params[0] = new Object[3];
				params[0][0] = role.getId(); 
				params[0][1] = role.getName(); 
				params[0][2] = role.getRemarks(); 
				for(int i=0;i<split.length;i++){
					String roleMenuSql = " INSERT INTO tb_role_menu(role_id, menu_id) VALUES(?,?)";
					sql[i+1] = roleMenuSql;
					params[i+1] = new Object[2];
					params[i+1][0] = role.getId();
					params[i+1][1] = split[i];					
				}
			}
		}else {
			sql = new String[]{roleSql};
			params = new Object[1][3];
			params[0][0] = role.getId();
			params[0][1] = role.getName();
			params[0][2] = role.getRemarks();
		}
		return BaseDao.branch(sql, params);
	}
	
	/**
	 * @Title: findMaxRoleId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取当前角色最大编号
	 */
	private long findMaxRoleId(){
		String sql = " SELECT max(id) AS id FROM tb_role WHERE 1=1 ";
		try {
			Role role = BaseDao.getObject(sql, null, Role.class);
			if(role != null && role.getId() != null){
				return role.getId();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return 0L;
	} 
	
	/**
	 * @Title: hasMenus   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取当前角色拥有的权限菜单
	 * @param: @param roleId
	 */
	public List<Menu> hasMenus(Long roleId){
		String sql = "SELECT distinct t_m.* FROM tb_menu t_m, (SELECT menu_id FROm tb_role_menu WHERE role_id=?) t_temp WHERE t_m.id=t_temp.menu_id";
		try {
			List<Menu> roleMenu = BaseDao.findAll(sql, new Object[]{roleId}, Menu.class);
			return roleMenu;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
}
