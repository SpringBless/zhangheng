package com.gxwzu.zhangheng.medical.domain.chrocdp;

import java.io.Serializable;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.DateUtils;

/**
 * @ClassName:  Chrocdp   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病证表:tb_chrocd_p
 * @author: zhangheng
 * @date:   2019年6月3日 下午3:43:56     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Chrocdp implements Serializable {
	
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 *  `cp_nh_code` varchar(128) NOT NULL COMMENT '农合证号、外键',
	 */
	private String cpNhCode;
	
	/**
	 * `cp_id` varchar(32) NOT NULL COMMENT '慢病证号、 主键',
	 */
	private String cpId;
	
	/**
	 *  `cp_id_card` varchar(32) NOT NULL COMMENT '身份证号、 外键',
	 */
	private String cpIdCard;
	
	/**
	 *  `cp_names` text COMMENT '疾病名称',
	 */
	private String cpNames;
	
	/**
	 *  `cp_start` datetime NOT NULL COMMENT '起始时间、 ',
	 */
	private java.util.Date cpStart;
	
	/**
	 *  `cp_end` datetime NOT NULL COMMENT '终止时间。',
	 */
	private java.util.Date cpEnd;
	
	public Chrocdp(){}
	
	public Chrocdp(String cpId){
		this.cpId = cpId;
		loadDB();
	}
	
	private void loadDB(){
		String sql = "SELECT * FROM tb_chrocd_p WHERE cp_id = ?";
		try {
			Chrocdp m = BaseDao.getObject(sql, new Object[]{this.cpId}, Chrocdp.class);
			if(m != null){
				this.cpId = m.getCpId();
				this.cpEnd = m.getCpEnd();
				this.cpIdCard = m.getCpIdCard();
				this.cpNames = m.getCpNames();
				this.cpNhCode = m.getCpNhCode();
				this.cpStart = m.getCpStart();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 保存
	 */
	public boolean add(){
		this.cpId = this.cpNhCode + DateUtils.getFormatTime();
		return add2DB();
	}
	
	/**
	 * 新增至数据库
	 */
	private boolean add2DB(){
		StringBuffer sbf = new StringBuffer();
		sbf.append("  INSERT INTO tb_chrocd_p ")
			.append("      (cp_nh_code, cp_id, cp_id_card, cp_names, cp_start, cp_end) ")
			.append(" VALUES ")
			.append("      (?,?,?,?,?,?) ");
		int update = BaseDao.toUpdate(sbf.toString(), 
				new Object[]{
					this.cpNhCode, this.cpId, this.cpIdCard, this.cpNames, this.cpStart, this.cpEnd
				});
		return BaseDao.getBoolean(update);
	}
	
	public Chrocdp queryByCardCode(String cardCode){
		String sql = "SELECT * FROM tb_chrocd_p WHERE cp_id_card = ?";
		try {
			Chrocdp m = BaseDao.getObject(sql, new Object[]{cardCode}, Chrocdp.class);
			if(m != null){
				this.cpId = m.getCpId();
				this.cpEnd = m.getCpEnd();
				this.cpIdCard = m.getCpIdCard();
				this.cpNames = m.getCpNames();
				this.cpNhCode = m.getCpNhCode();
				this.cpStart = m.getCpStart();
				return this;
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 更新至数据库
	 */
	public boolean toUpdate(){
		return update2DB();
	}
	
	/**
	 * 更新至数据库
	 */
	private boolean update2DB(){
		StringBuffer sql = new StringBuffer();
		sql.append("  UPDATE tb_chrocd_p SET ")
			.append("     cp_nh_code = ?, cp_id_card = ?, cp_names = ?, cp_start = ?, cp_end = ? ")
			.append(" WHERE ")
			.append("     cp_id = ? ");
		int update = BaseDao.toUpdate(sql.toString(), 
				new Object[]{
					this.cpNhCode, this.cpIdCard, this.cpNames, this.cpStart, this.cpEnd, this.cpId
				});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 删除
	 */
	public boolean delect2DB(){
		String sql = "DELETE FROM tb_chrocd_p WHERE cp_id = ? ";
		int update = BaseDao.toUpdate(sql, new Object[]{this.cpId});
		return BaseDao.getBoolean(update);
	}
	
	//****************** get/set *********************//

	public String getCpNhCode() {
		return cpNhCode;
	}

	public void setCpNhCode(String cpNhCode) {
		this.cpNhCode = cpNhCode;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getCpIdCard() {
		return cpIdCard;
	}

	public void setCpIdCard(String cpIdCard) {
		this.cpIdCard = cpIdCard;
	}

	public String getCpNames() {
		return cpNames;
	}

	public void setCpNames(String cpNames) {
		this.cpNames = cpNames;
	}

	public java.util.Date getCpStart() {
		return cpStart;
	}

	public void setCpStart(java.util.Date cpStart) {
		this.cpStart = cpStart;
	}

	public java.util.Date getCpEnd() {
		return cpEnd;
	}

	public void setCpEnd(java.util.Date cpEnd) {
		this.cpEnd = cpEnd;
	}

	@Override
	public String toString() {
		return "Chrocdp [cpNhCode=" + cpNhCode + ", cpId=" + cpId + ", cpIdCard=" + cpIdCard + ", cpNames=" + cpNames
				+ ", cpStart=" + cpStart + ", cpEnd=" + cpEnd + "]";
	}
	
}
