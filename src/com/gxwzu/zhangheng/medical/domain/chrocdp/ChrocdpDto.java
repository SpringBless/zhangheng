package com.gxwzu.zhangheng.medical.domain.chrocdp;

import java.io.Serializable;

/**
 * @ClassName:  ChrocdpDto   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病证表:tb_chrocd_p
 * @author: zhangheng
 * @date:   2019年6月3日 下午3:43:56     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class ChrocdpDto extends Chrocdp implements Serializable {
	
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "ChrocdpDto [userName=" + userName + "]";
	}
	
}
