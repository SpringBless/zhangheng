package com.gxwzu.zhangheng.medical.domain.apply;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  Apply   
 * @Description:TODO(这里用一句话描述这个类的作用)  报销实体：tb_apply   
 * @author: zhangheng
 * @date:   2019年6月3日 下午8:25:10     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Apply implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * `a_id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '报销记录id',
	 */
	private Long aId;
	
	/**
	 * `a_year` varchar(4) NOT NULL COMMENT '当前缴费年份',
	 */
	private String aYear;
	
	/**
	 * `a_apply` decimal(6,2) NOT NULL DEFAULT '0.00' COMMENT '当前报销金额',
	 */
	private BigDecimal aApply;
	
	/**
	 *  `a_illname` varchar(50) NOT NULL COMMENT '疾病名称',
	 */
	private String aIllname;
	
	/**
	 * `a_family_code` varchar(64) NOT NULL COMMENT '家庭编号',
	 */
	private String aFamilyCode;
	
	/**
	 * `a_id_card` varchar(32) NOT NULL COMMENT '身份证',
	 */
	private String aIdCard;
	
	/**
	 * `a_create_date` datetime NOT NULL COMMENT '报销申请时间',
	 */
	private java.util.Date aCreateDate;
	
	/**
	 * `a_apply_date` datetime NOT NULL COMMENT '报销完成时间',
	 */
	private java.util.Date aApplyDate;
	
	/**
	 * `a_status` int(3) NOT NULL COMMENT '报销状态：待审核：0，审核通过：1，审核不通过：2，已汇款：3，未回款：4',
	 */
	private Integer aStatus;
	
	/**
	 * 就诊时间
	 * `a_see_date` datetime DEFAULT NULL COMMENT '就诊时间',
	 */
	private java.util.Date aSeeDate;
	
	/**
	 * 发票号
	 * `a_fapiao` varchar(50) NOT NULL COMMENT '发票号',
	 */
	private String aFapiao;
	
	public Apply(){}
	
	public Apply(Long aId){
		this.aId = aId;
		loadDB();
	}
	
	/**
	 * @Title: loadDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 从数据库中加载apply实例   
	 */
	private void loadDB(){
		String sql = "SELECT * FROM tb_apply WHERE a_id = ?";
		try {
			Apply apply = BaseDao.getObject(sql, new Object[]{aId}, Apply.class);
			if(apply != null){
				this.aId = apply.getaId();
				this.aSeeDate = apply.getaSeeDate();
				this.aFapiao = apply.getaFapiao();
				this.aYear = apply.getaYear();
				this.aApply = apply.getaApply();
				this.aIllname = apply.getaIllname();
				this.aFamilyCode = apply.getaFamilyCode();
				this.aIdCard = apply.getaIdCard();
				this.aCreateDate = apply.getaCreateDate();
				this.aApplyDate = apply.getaApplyDate();
				this.aStatus = apply.getaStatus();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: queryApplyByYearAcard   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 根据身份证号和当前年份查询已报销记录   
	 * @param: @param year
	 * @param: @param cardCode
	 * @return: Apply      
	 */
	public List<Apply> queryApplyByYearAcard(String year, String cardCode){
		String sql = "SELECT * FROM tb_apply WHERE a_year = ? AND a_id_card = ? AND a_status = 3 ";
		try {
			List<Apply> apply = BaseDao.findAll(sql, new Object[]{ year, cardCode}, Apply.class);
			return apply;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: applyFor   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 检查已报销费用   
	 * @param: @param year
	 * @param: @param cardCode
	 * @return: BigDecimal      
	 */
	public BigDecimal applyFor(String year, String cardCode){
		List<Apply> list = queryApplyByYearAcard(year, cardCode);
		BigDecimal b = new BigDecimal("0.00");
		if(list != null && list.size() > 0){
			for(int i=0;i<list.size(); i++){
				b = b.add(list.get(i).getaApply());
			}
		}
		return b;
	}
	
	/**
	 * @Title: add   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   新增至数据库   
	 * @return: boolean      
	 */
	public boolean add(){
		return save2DB();
	}
	
	/**
	 * @Title: save2DB   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 新增至数据库   
	 * @return: boolean      
	 */
	private boolean save2DB(){
		StringBuffer sql = new StringBuffer();
		sql.append("  INSERT INTO tb_apply ")
			.append("     (a_year, a_apply, a_illname, a_family_code, a_id_card, a_create_date, a_apply_date, a_status, a_see_date, a_fapiao) ")
			.append(" VALUES ")
			.append("     (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		int update = BaseDao.toUpdate(sql.toString(), 
				new Object[]{
					aYear, aApply, aIllname, aFamilyCode, aIdCard, aCreateDate, aApplyDate, aStatus, aSeeDate, aFapiao
				});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 更新数据 
	 */
	public boolean toUpdate(){
		return update2DB();
	}
	
	/**
	 * 更新数据
	 */
	private boolean update2DB(){
		StringBuffer sql = new StringBuffer();
		sql.append("  UPDATE tb_apply SET ")
			.append("     a_year = ?, a_apply = ?, a_illname = ?, a_family_code = ?, ")
			.append("     a_id_card = ?, a_create_date = ?, a_apply_date = ?, a_status = ?, a_see_date =? , a_fapiao = ? ")
			.append(" WHERE ")
			.append("     a_id = ? ");
		int update = BaseDao.toUpdate(sql.toString(), 
				new Object[]{
					aYear, aApply, aIllname, aFamilyCode, aIdCard, aCreateDate, aApplyDate, aStatus, aSeeDate, aFapiao, aId
				});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 *  删除
	 */
	public boolean toDelete(){
		return deleteFromDB();
	}
	
	/**
	 * 删除 
	 */
	private boolean deleteFromDB(){
		String sql = "DELETE FROM tb_apply WHERE a_id = ? ";
		int update = BaseDao.toUpdate(sql, new Object[]{aId});
		return BaseDao.getBoolean(update);
	}
	
	
	//******************* get/set *********************

	public Long getaId() {
		return aId;
	}

	public void setaId(Long aId) {
		this.aId = aId;
	}

	public String getaYear() {
		return aYear;
	}

	public void setaYear(String aYear) {
		this.aYear = aYear;
	}

	public BigDecimal getaApply() {
		return aApply;
	}

	public void setaApply(BigDecimal aApply) {
		this.aApply = aApply;
	}

	public String getaIllname() {
		return aIllname;
	}

	public void setaIllname(String aIllname) {
		this.aIllname = aIllname;
	}

	public String getaFamilyCode() {
		return aFamilyCode;
	}

	public void setaFamilyCode(String aFamilyCode) {
		this.aFamilyCode = aFamilyCode;
	}

	public String getaIdCard() {
		return aIdCard;
	}

	public void setaIdCard(String aIdCard) {
		this.aIdCard = aIdCard;
	}

	public java.util.Date getaCreateDate() {
		return aCreateDate;
	}

	public void setaCreateDate(java.util.Date aCreateDate) {
		this.aCreateDate = aCreateDate;
	}

	public java.util.Date getaApplyDate() {
		return aApplyDate;
	}

	public void setaApplyDate(java.util.Date aApplyDate) {
		this.aApplyDate = aApplyDate;
	}

	public Integer getaStatus() {
		return aStatus;
	}

	public void setaStatus(Integer aStatus) {
		this.aStatus = aStatus;
	}

	public java.util.Date getaSeeDate() {
		return aSeeDate;
	}

	public void setaSeeDate(java.util.Date aSeeDate) {
		this.aSeeDate = aSeeDate;
	}

	public String getaFapiao() {
		return aFapiao;
	}

	public void setaFapiao(String aFapiao) {
		this.aFapiao = aFapiao;
	}

	@Override
	public String toString() {
		return "Apply [aId=" + aId + ", aYear=" + aYear + ", aApply=" + aApply + ", aIllname=" + aIllname
				+ ", aFamilyCode=" + aFamilyCode + ", aIdCard=" + aIdCard + ", aCreateDate=" + aCreateDate
				+ ", aApplyDate=" + aApplyDate + ", aStatus=" + aStatus + ", aSeeDate=" + aSeeDate + ", aFapiao="
				+ aFapiao + "]";
	}

}
