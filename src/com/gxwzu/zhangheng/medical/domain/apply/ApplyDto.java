package com.gxwzu.zhangheng.medical.domain.apply;

import java.math.BigDecimal;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfo;
import com.gxwzu.zhangheng.medical.exception.DBException;

public class ApplyDto extends FamilyInfo {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * `a_id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '报销记录id',
	 */
	private Long aId;
	
	/**
	 * `a_year` varchar(4) NOT NULL COMMENT '当前缴费年份',
	 */
	private String aYear;
	
	/**
	 * `a_apply` decimal(6,2) NOT NULL DEFAULT '0.00' COMMENT '当前报销金额',
	 */
	private BigDecimal aApply;
	
	/**
	 *  `a_illname` varchar(50) NOT NULL COMMENT '疾病名称',
	 */
	private String aIllname;
	
	/**
	 * `a_family_code` varchar(64) NOT NULL COMMENT '家庭编号',
	 */
	private String aFamilyCode;
	
	/**
	 * `a_id_card` varchar(32) NOT NULL COMMENT '身份证',
	 */
	private String aIdCard;
	
	/**
	 * `a_create_date` datetime NOT NULL COMMENT '报销申请时间',
	 */
	private java.util.Date aCreateDate;
	
	/**
	 * `a_apply_date` datetime NOT NULL COMMENT '报销完成时间',
	 */
	private java.util.Date aApplyDate;
	
	/**
	 * `a_status` int(3) NOT NULL COMMENT '报销状态：待审核：0，审核通过：1，审核不通过：2，已汇款：3，未回款：4',
	 */
	private Integer aStatus;
	
	/**
	 * 就诊时间
	 * `a_see_date` datetime DEFAULT NULL COMMENT '就诊时间',
	 */
	private java.util.Date aSeeDate;
	
	/**
	 * 发票号
	 * `a_fapiao` varchar(50) NOT NULL COMMENT '发票号',
	 */
	private String aFapiao;
	
	
	/*8
	 * SELECT
		    distinct t_f_i.*, t_p.*
		FROM
		    tb_family_info t_f_i, tb_apply t_p
		WHERE 
		    t_f_i.card_code = t_p.a_id_card AND t_p.a_status = 0 AND t_p.a_id=1;
	 */
	public ApplyDto loadFromDB(){
		StringBuffer sql = new StringBuffer();
		sql.append("   SELECT ")
			.append("       distinct t_f_i.*, t_p.* ")
			.append("  FROM ")
			.append("      tb_family_info t_f_i, tb_apply t_p ")
			.append("  WHERE ")
			.append("      t_f_i.card_code = t_p.a_id_card AND t_p.a_status = 0 AND t_p.a_id= ? ");
		try {
			ApplyDto t = BaseDao.getObject(sql.toString(), new Object[]{ this.aId}, ApplyDto.class);
			return t;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Apply queryApplyById(Long aId){
		String sql = "SELECT * FROM tb_apply WHERE a_id = ?";
		try {
			Apply apply = BaseDao.getObject(sql, new Object[]{aId}, Apply.class);
			return apply;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// +++++++++++++++++ get/set +++++++++++++++++

	public Long getaId() {
		return aId;
	}

	public void setaId(Long aId) {
		this.aId = aId;
	}

	public String getaYear() {
		return aYear;
	}

	public void setaYear(String aYear) {
		this.aYear = aYear;
	}

	public BigDecimal getaApply() {
		return aApply;
	}

	public void setaApply(BigDecimal aApply) {
		this.aApply = aApply;
	}

	public String getaIllname() {
		return aIllname;
	}

	public void setaIllname(String aIllname) {
		this.aIllname = aIllname;
	}

	public String getaFamilyCode() {
		return aFamilyCode;
	}

	public void setaFamilyCode(String aFamilyCode) {
		this.aFamilyCode = aFamilyCode;
	}

	public String getaIdCard() {
		return aIdCard;
	}

	public void setaIdCard(String aIdCard) {
		this.aIdCard = aIdCard;
	}

	public java.util.Date getaCreateDate() {
		return aCreateDate;
	}

	public void setaCreateDate(java.util.Date aCreateDate) {
		this.aCreateDate = aCreateDate;
	}

	public java.util.Date getaApplyDate() {
		return aApplyDate;
	}

	public void setaApplyDate(java.util.Date aApplyDate) {
		this.aApplyDate = aApplyDate;
	}

	public Integer getaStatus() {
		return aStatus;
	}

	public void setaStatus(Integer aStatus) {
		this.aStatus = aStatus;
	}

	public java.util.Date getaSeeDate() {
		return aSeeDate;
	}

	public void setaSeeDate(java.util.Date aSeeDate) {
		this.aSeeDate = aSeeDate;
	}

	public String getaFapiao() {
		return aFapiao;
	}

	public void setaFapiao(String aFapiao) {
		this.aFapiao = aFapiao;
	}

	@Override
	public String toString() {
		return "ApplyDto [aId=" + aId + ", aYear=" + aYear + ", aApply=" + aApply + ", aIllname=" + aIllname
				+ ", aFamilyCode=" + aFamilyCode + ", aIdCard=" + aIdCard + ", aCreateDate=" + aCreateDate
				+ ", aApplyDate=" + aApplyDate + ", aStatus=" + aStatus + ", aSeeDate=" + aSeeDate + ", aFapiao="
				+ aFapiao + "]" + super.toString();
	}

}
