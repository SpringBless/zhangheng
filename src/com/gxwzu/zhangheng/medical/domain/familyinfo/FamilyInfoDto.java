package com.gxwzu.zhangheng.medical.domain.familyinfo;

public class FamilyInfoDto extends FamilyInfo{

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	private boolean isPay;

	public boolean isPay() {
		return isPay;
	}

	public void setPay(boolean isPay) {
		this.isPay = isPay;
	}

	@Override
	public String toString() {
		return "FamilyInfoDto [isPay=" + isPay + "]";
	}

}
