package com.gxwzu.zhangheng.medical.domain.familyinfo;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.EntityMapper;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.DateUtils;


/**
 * @ClassName:  FamilyInfo   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭成员档案信息类， table name ： tb_family_info
 * @author: zhangheng
 * @date:   2019年4月15日 下午10:47:08     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FamilyInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * family_code varchar(128) not null COMMENT 'w外键，家庭编号', 
	 */
	private String familyCode;
	
	/**
	 * nh_code varchar(128) not null COMMENT '农合证号,依赖family_code',
	 */
	private String nhCode;
	
	/**
	 * yl_code varchar(128) not null COMMENT '医疗证卡号',
	 */
	private String ylCode;
	
	/**
	 * hn_code varchar(3) not null COMMENT '户内编号,根据家庭成员增加',
	 */
	private String hnCode;
	
	/**
	 * name varchar(32) not null COMMENT '姓名',
	 */
	private String name;
	
	/**
	 * fm_relation varchar(32) not null COMMENT '与户主关系',
	 */
	private String fmRelation;
	
	/**
	 * card_code varchar(20) not null primary key COMMENT '身份证号',
	 */
	private String cardCode;
	
	/**
	 * gender varchar(12) not null default '男' COMMENT '性别',
	 */
	private String gender;
	
	/**
	 * sf_health varchar(32) not null COMMENT '健康状况',
	 */
	private String sfHealth;
	
	/**
	 * sf_national varchar(64) not null COMMENT '民族',
	 */
	private String sfNational;
	
	/**
	 * sf_education varchar(32) default null COMMENT '文化程度',
	 */
	private String sfEducation;
	
	/**
	 * birthday date not null COMMENT '出生日期，格式：YYYY-MM-DD',
	 */
	private java.sql.Date birthday;
	
	/**
	 * r_attribute varchar(32) default null COMMENT '人员属性',
	 */
	private String rAttribute;
	
	/**
	 * hk_type varchar(3) not null default '是' COMMENT '是否农村户口',
	 */
	private String hkType;
	
	/**
	 * sf_professional varchar(32) default null COMMENT '职业（可有，无）',
	 */
	private String sfProfessional;
	
	/**
	 * workunits varchar(255) default null COMMENT '工作单位（可有，无）',
	 */
	private String workunits;
	
	/**
	 * phone varchar(11) default null COMMENT '联系电话',
	 */
	private String phone;
	
	/**
	 * address varchar(255) default null COMMENT '常住地址'
	 */
	private String address;
	
	/**
	 * COMMENT '家庭成员信息表，字段age(动态生成),'; 
	 */
	private int age;

	public FamilyInfo(){
		
	}
	
	public FamilyInfo(String cardCode){
		this.cardCode = cardCode;
		FamilyInfo f = this.loadFromDB();
		if(f != null){
			this.familyCode = f.getFamilyCode();
			this.nhCode = f.getNhCode();
			this.ylCode = f.getYlCode();
			this.hnCode = f.getHnCode();
			this.name = f.getName();
			this.fmRelation = f.getFmRelation();
			this.cardCode = f.getCardCode();
			this.gender = f.getGender();
			this.sfHealth = f.getSfHealth();
			this.sfNational = f.getSfNational();
			this.sfEducation = f.getSfEducation();
			int nyear = DateUtils.getYearByUtilDate();
			int byear = DateUtils.getYearBySqlDate(f.getBirthday());
			this.age = nyear - byear;
			this.birthday = f.getBirthday();
			this.rAttribute = f.getrAttribute();
			this.hkType = f.getHkType();
			this.sfProfessional = f.getSfProfessional();
			this.workunits = f.getWorkunits();
			this.phone = f.getPhone();
			this.address = f.getAddress();
		}
	}
	
	/**
	 * @Title: deleteByFamilyCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  通过家庭编码，删除所属家庭的全部成员 
	 * @return: boolean      
	 */
	public boolean deleteByFamilyCode(){
		int update = BaseDao.toUpdate(" DELETE FROM tb_family_info WHERE family_code = ? ", new Object[]{this.familyCode});
		return BaseDao.getBoolean(update);
	}
	
	
	/**
	 * @Title: update   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新至数据库
	 * @return: boolean      
	 */
	public boolean update(){
		return this.updateToDB();
	}
	
	/**
	 * @Title: updateToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新至数据库
	 * @return: boolean      
	 */
	private boolean updateToDB( ){
		StringBuffer sbf = new StringBuffer();
		sbf.append("UPDATE tb_family_info SET ")
			.append(" name = ?, fm_relation=?, gender=?, sf_health=?, ")
			.append(" sf_national =?, sf_education=?, birthday=?, r_attribute=?, hk_type=?, ")
			.append(" sf_professional =?, workunits=?, phone=?, address=? ")
			.append(" WHERE card_code =? ");
		int update = BaseDao.toUpdate(
				sbf.toString(), 
				new Object[]{
					this.name, this.fmRelation, this.gender, this.sfHealth,
					this.sfNational, this.sfEducation, this.birthday, this.rAttribute, this.hkType,
					this.sfProfessional, this.workunits, this.phone, this.address, this.cardCode
				});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: cardCodeInDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @return true or false,if true in db , else false not in db     
	 * @return: boolean      
	 */
	public boolean cardCodeInDB(){
		String sql = " SELECT card_code FROM tb_family_info WHERE card_code = ? ";
		String cardCode = (String) BaseDao.getObject(new EntityMapper<String>() {

			@Override
			public String getEntity(ResultSet resultSet) throws SQLException {
				String cString = resultSet.getString("card_code");
				return cString;
			}
		}, sql, new Object[]{this.cardCode});
		return cardCode != null && !"".equals(cardCode)? true : false;
	}
	
	/**
	 * @Title: nextHnCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获取当前家庭的下一个成员的户内编号 
	 * @param: @return    String nextHnCode  
	 * @return: String      
	 */
	private String nextHnCode(){
		String sql = " SELECT max(CAST(hn_code AS SIGNED )) AS hn_code FROM tb_family_info WHERE family_code = ? ";
		String maxCode = (String) BaseDao.getObject(new EntityMapper<String>() {

			@Override
			public String getEntity(ResultSet resultSet) throws SQLException {
				String hn_code = resultSet.getString("hn_code");
				return hn_code;
			}
		}, sql, new Object[]{this.familyCode});
		return this.formatHnCode(maxCode);
	}
	
	/**
	 * @Title: nextNhCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   下一个农合号
	 * @return: String      
	 */
	private String nextNhCode(){
		String sql = " SELECT max(CAST(nh_code AS SIGNED )) AS nh_code FROM tb_family_info WHERE family_code = ? ";
		String maxCode = (String) BaseDao.getObject(new EntityMapper<String>() {

			@Override
			public String getEntity(ResultSet resultSet) throws SQLException {
				String hn_code = resultSet.getString("nh_code");
				return hn_code;
			}
		}, sql, new Object[]{this.familyCode});
		return this.formatNhCode(maxCode);
	}
	
	/**
	 * @Title: nextYlCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  下一个医疗卡号   
	 * @return: String      
	 */
	private String nextYlCode(){
		String sql = " SELECT max(CAST(yl_code AS SIGNED )) AS yl_code FROM tb_family_info WHERE family_code = ? ";
		String maxCode = (String) BaseDao.getObject(new EntityMapper<String>() {

			@Override
			public String getEntity(ResultSet resultSet) throws SQLException {
				String hn_code = resultSet.getString("yl_code");
				return hn_code;
			}
		}, sql, new Object[]{this.familyCode});
		return this.formatYlCode(maxCode);
	}
	
	private String formatYlCode(String hnCode){
		if (hnCode != null && !"".equals(hnCode)) {
			Long number = 1L;
			number = Long.valueOf(hnCode);
			++number;
			// 使用0补足位数
			hnCode = "" + String.format("%09d", number);
		} else {
			hnCode = "450421" + String.format("%09d", 1);
		}
		return hnCode;
	}
	
	/**
	 * @Title: formatNhCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  农合号   
	 * @param: @param hnCode
	 * @return: String      
	 */
	private String formatNhCode(String hnCode){
		if (hnCode != null && !"".equals(hnCode)) {
			Long number = 1L;
			number = Long.valueOf(hnCode);
			++number;
			// 使用0补足位数
			hnCode = String.format("%02d", number);
		} else {
			hnCode = familyCode + "01";// + String.format("%02d", number);
		}
		return hnCode;
	}
	
	/**
	 * @Title: formatHnCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   初始化户内最大编码
	 * @param: @param hnCode
	 * @return: String      
	 */
	private String formatHnCode(String hnCode){
		if (hnCode != null && !"".equals(hnCode)) {
			Integer number = 1;
			number = Integer.parseInt(hnCode);
			++number;
			// 使用0补足位数
			hnCode = String.format("%02d", number);
		} else {
			hnCode = "01";// + String.format("%02d", number);
		}
		return hnCode;
	}

	/**
	 * 根据card_code加载数据
	 */
	public FamilyInfo load(){
		return this.loadFromDB();
	}
	
	/**
	 * 根据card_code加载数据
	 */
	private FamilyInfo loadFromDB(){
		String sql = "SELECT * FROM tb_family_info WHERE card_code = ? ";
		try {
			FamilyInfo f = BaseDao.getObject(sql, new Object[]{this.cardCode}, FamilyInfo.class);
			int nyear = DateUtils.getYearByUtilDate();
			int byear = DateUtils.getYearBySqlDate(f.getBirthday());
			f.setAge(nyear - byear);
			return f;
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 从数据库中删除记录为card_code的记录
	 */
	public boolean del(){
		return this.deleteFromDB();
	}
	
	/**
	 * 从数据库中删除记录为card_code的记录
	 */
	private boolean deleteFromDB(){
		String sql = " DELETE FROM tb_family_info WHERE card_code = ? ";
		int update = BaseDao.toUpdate(sql, new Object[]{this.cardCode});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * 添加至数据库
	 */
	public boolean add(){
		this.hnCode = this.nextHnCode();
		this.nhCode = this.nextNhCode();
		this.ylCode = this.nextYlCode();
		return this.addToDB();
	}
	
	/**
	 * 添加至数据库
	 */
	private boolean addToDB(){
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO tb_family_info( ")
			.append(" family_code, nh_code, yl_code, ")
			.append(" hn_code, name, fm_relation, ")
			.append(" card_code, gender, sf_health, ")
			.append(" sf_national, sf_education, birthday, ")
			.append(" r_attribute, hk_type, sf_professional, ")
			.append(" workunits, phone, address ) ")
			.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ")
			.append("        ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
		int update = BaseDao.toUpdate(sql.toString(), 
				new Object[]{
					this.familyCode, this.nhCode, this.ylCode,
					this.hnCode, this.name, this.fmRelation,
					this.cardCode, this.gender, this.sfHealth,
					this.sfNational, this.sfEducation, this.birthday,
					this.rAttribute, this.hkType, this.sfProfessional,
					this.workunits, this.phone, this.address
				});
		return BaseDao.getBoolean(update);
	}
	
	public String getFamilyCode() {
		return familyCode;
	}

	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}

	public String getNhCode() {
		return nhCode;
	}

	public void setNhCode(String nhCode) {
		this.nhCode = nhCode;
	}

	public String getYlCode() {
		return ylCode;
	}

	public void setYlCode(String ylCode) {
		this.ylCode = ylCode;
	}

	public String getHnCode() {
		return hnCode;
	}

	public void setHnCode(String hnCode) {
		this.hnCode = hnCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFmRelation() {
		return fmRelation;
	}

	public void setFmRelation(String fmRelation) {
		this.fmRelation = fmRelation;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSfHealth() {
		return sfHealth;
	}

	public void setSfHealth(String sfHealth) {
		this.sfHealth = sfHealth;
	}

	public String getSfNational() {
		return sfNational;
	}

	public void setSfNational(String sfNational) {
		this.sfNational = sfNational;
	}

	public String getSfEducation() {
		return sfEducation;
	}

	public void setSfEducation(String sfEducation) {
		this.sfEducation = sfEducation;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public void setBirthday(java.sql.Date birthday) {
		this.birthday = birthday;
	}

	public String getrAttribute() {
		return rAttribute;
	}

	public void setrAttribute(String rAttribute) {
		this.rAttribute = rAttribute;
	}

	public String getHkType() {
		return hkType;
	}

	public void setHkType(String hkType) {
		this.hkType = hkType;
	}

	public String getSfProfessional() {
		return sfProfessional;
	}

	public void setSfProfessional(String sfProfessional) {
		this.sfProfessional = sfProfessional;
	}

	public String getWorkunits() {
		return workunits;
	}

	public void setWorkunits(String workunits) {
		this.workunits = workunits;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "FamilyInfo [familyCode=" + familyCode + ", nhCode=" + nhCode + ", ylCode=" + ylCode + ", hnCode="
				+ hnCode + ", name=" + name + ", fmRelation=" + fmRelation + ", cardCode=" + cardCode + ", gender="
				+ gender + ", sfHealth=" + sfHealth + ", sfNational=" + sfNational + ", sfEducation=" + sfEducation
				+ ", birthday=" + birthday + ", rAttribute=" + rAttribute + ", hkType=" + hkType + ", sfProfessional="
				+ sfProfessional + ", workunits=" + workunits + ", phone=" + phone + ", address=" + address + ", age="
				+ age + "]";
	}

}
