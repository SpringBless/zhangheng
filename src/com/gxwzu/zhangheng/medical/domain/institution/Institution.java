package com.gxwzu.zhangheng.medical.domain.institution;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.EntityMapper;

/**
 * @ClassName:  Institution   
 * @Description:TODO(这里用一句话描述这个类的作用)   农合机构类
 * @author: zhangheng
 * @date:   2019年4月8日 下午5:47:54     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Institution implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * 行政区域编码，类标识符
	 */
	private String areacode;
	
	/**
	 * 经办机构编码
	 */
	private String agencode;
	
	/**
	 * 经办机构名称
	 */
	private String agenname;
	
	/**
	 * 经办机构级别，区别于行政区域级别，1表示县级农合办，2表示镇级农合点
	 */
	private Integer grade;
	
	public String getAreacode() {
		return areacode;
	}
	
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	
	public String getAgencode() {
		return agencode;
	}
	
	public void setAgencode(String agencode) {
		this.agencode = agencode;
	}
	
	public String getAgenname() {
		return agenname;
	}
	
	public void setAgenname(String agenname) {
		this.agenname = agenname;
	}
	
	public Integer getGrade() {
		return grade;
	}
	
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	
	public Institution(String areacode, String agencode, String agenname,
			Integer grade) {
		super();
		this.areacode = areacode;
		this.agencode = agencode;
		this.agenname = agenname;
		this.grade = grade;
	}
	
	public Institution(){}
	
	/**
	 * 添加农合经办点
	 * @param areacode
	 * @param agencode
	 * @param agenname
	 * @param grade
	 */
	public boolean addInst(String areacode,String agencode,String agenname,Integer grade) {
		//初始化属性
		this.areacode=areacode;
		this.agencode=agencode;
		this.agenname=agenname;
		this.grade =grade;
		//调用插入数据库私有方法
		return saveToDB();
	}
	
	/**
	 * @Title:  Institution   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   初始化农合经办点
	 */
	public Institution(String areacode){
		this.areacode = areacode;
		loadDB();
	}
	
	/**
	 * @Title: editInstitution   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   修改农合经办点
	 */
	public boolean editInstitution(){
		return updateToDB();
	}
	
	/**
	 * @Title: updateToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新至数据库
	 */
	private boolean updateToDB(){
		String sql = "UPDATE tb_institution SET agencode=?,agenname=?,grade=? WHERE areacode=?";
		int update = BaseDao.toUpdate(sql, new Object[]{
				this.agencode,
				this.agenname,
				this.grade,
				this.areacode
		});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: addInst   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加农合经办点
	 */
	public boolean addInst(){
		return saveToDB();
	}
	
	/**
	 * @Title: delInst   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除农合经办点
	 */
	public boolean delInst(String areacode){
		this.areacode = areacode;
		return delInst();
	}
	
	/**
	 * @Title: delInst   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除农合经办点
	 */
	public boolean delInst(){
		return deleteFromDB();
	}
	
	/**
	 * @Title: deleteFromDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中删除农合经办点
	 */
	private boolean deleteFromDB(){
		String sql = "DELETE FROM tb_institution WHERE areacode=?";
		int update = BaseDao.toUpdate(sql, new Object[]{this.areacode});
		return BaseDao.getBoolean(update);
	}

	/**
	 * 插入记录
	 * @param connection
	 * @throws SQLException
	 */
	private boolean saveToDB() {
		StringBuffer sqlBuff = new StringBuffer();
		sqlBuff.append(" insert into tb_institution(areacode,agencode,agenname,grade) ");
		sqlBuff.append(" values(?,?,?,?) ");
		int update = BaseDao.toUpdate(sqlBuff.toString(),
				new Object[] { this.areacode, this.agencode, this.agenname, this.grade });
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: loadDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中根据areacode加载农合经办点
	 */
	private void loadDB(){
		String sql = "SELECT * FROM tb_institution WHERE areacode=?";
		Institution institution = (Institution)BaseDao.getObject(new EntityMapper<Institution>() {
			@Override
			public Institution getEntity(ResultSet resultSet) throws SQLException {
				Institution institution = new Institution();
				institution.setAgencode(resultSet.getString("agencode"));
				institution.setAgenname(resultSet.getString("agenname"));
				institution.setAreacode(resultSet.getString("areacode"));
				institution.setGrade(resultSet.getInt("grade"));
				return institution;
			}
		}, sql, new Object[]{this.areacode});
		if(institution != null){
			this.agencode = institution.getAgencode();
			this.agenname = institution.getAgenname();
			this.areacode = institution.getAreacode();
			this.grade = institution.getGrade();
		}
	}
	
	
}
