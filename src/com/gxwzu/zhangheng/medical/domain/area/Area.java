package com.gxwzu.zhangheng.medical.domain.area;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.database.EntityMapper;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName: Area
 * @Description:TODO(这里用一句话描述这个类的作用) 行政区域类
 * @author: zhangheng
 * @date: 2019年4月8日 下午5:56:31
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Area implements Serializable {

	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 乡镇区域编码:县、镇、村、组
	 */
	private String areacode;

	/**
	 * 上一级
	 */
	private Area parent;

	/**
	 * 行政区域名称
	 */
	private String areaname;

	/**
	 * 行政区域级别：1表示县级，2表示镇级，3表示村，4表示组
	 */
	private Integer grade;

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public Area getParent() {
		return parent;
	}

	public void setParent(Area parent) {
		this.parent = parent;
	}

	public String getAreaname() {
		return areaname;
	}

	public void setAreaname(String areaname) {
		this.areaname = areaname;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Area(String areacode, Area parent, String areaname, Integer grade) {
		super();
		this.areacode = areacode;
		this.parent = parent;
		this.areaname = areaname;
		this.grade = grade;
	}

	public Area() {

	}
	
	/**
	 * @Title:  Area   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   加载行政区，进行曾删查改是，先初始化
	 */
	public Area(String areaid) throws DBException{
		try{
			this.areacode=areaid;
			loadDB(areaid);
	    }catch(SQLException e){
	    	throw new DBException("加载id="+areaid+"行政区域记录失败", e);
	    }
		
	}
	
	/**
	 * @Title:  loadAreaByGradeOrareaCode
	 * 如果areaCode为空，就只查询grade   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   加载指定grade的数据集合
	 * @param:  grade  
	 * @param:  areaCode
	 */
	private List<Area> loadAreaByGradeOrareaCode(String areaCode, int grade){
		try {
			if(areaCode != null && !"".equals(areaCode)){
				if(!areaCode.endsWith("%")){
					areaCode = areaCode + "%";
				}
				String sql = " SELECT * FROM tb_area WHERE grade = ? AND areacode LIKE ? ";
				List<Area> list = BaseDao.findAll(sql, new Object[]{ grade, areaCode }, Area.class);
				return list;
			}else {
				String sql = " SELECT * FROM tb_area WHERE grade = ? ";
				List<Area> list = BaseDao.findAll(sql, new Object[]{ grade }, Area.class);
				return list;
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @Title: findAreaByAreaCodeAndGrade   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 查询数据为areaCode且为grade数据集合
	 * @param: @param areaCode
	 * @param: @param grade
	 * @return: List<Area>      
	 */
	public List<Area> findAreaByAreaCodeAndGrade(String areaCode, int grade){
		return this.loadAreaByGradeOrareaCode(areaCode, grade);
	}
	
	/**
	 * @Title: findAreaByGrade   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   查询数据为grade数据集合
	 * @param: @param grade
	 * @return: List<Area>      
	 */
	public List<Area> findAreaByGrade(int grade){
		return this.loadAreaByGradeOrareaCode(null, grade);
	}

	/**
	 * 添加子一级行政区域
	 * @param areapid-所属行政区域
	 * @param areaname
	 * @throws Exception
	 */
	public boolean addArea(String areapid, String areaname) throws Exception {
		try {
			// 加载父节点ID的对象
			this.parent = new Area(areapid);
			this.areaname = areaname;
			if(this.parent != null && this.parent.getGrade()!= null){				
				this.grade = this.parent.getGrade() + 1;
			}else {
				this.grade = 1;
			}
			// 自动编号
			this.areacode = createAreaid();
			// 保存到数据库
			return saveToDB();
			// 提交事务
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * @Title: editArea   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   （更新）保存编辑后的行政区域
	 */
	public boolean editArea(String areaname) throws Exception {
		try {
			this.areaname = areaname;
			// 保存到数据库
			return editToDB();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * @Title: delArea   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   删除行政区域
	 */
	public boolean delArea() throws Exception {
		try {
			// 保存到数据库
			return delFromDB();
		} catch (Exception e) {
			throw e;
		} 
	}

	/**
	 * @Title: saveToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加行政区域至数据库
	 */
	private boolean saveToDB() throws SQLException {
		StringBuffer sqlBuff = new StringBuffer();
		sqlBuff.append(" insert into tb_area(areacode,areaname,grade) ");
		sqlBuff.append(" values(?,?,?) ");
		int update = BaseDao.toUpdate(sqlBuff.toString(), 
				new Object[] { this.areacode, this.areaname, this.grade });
		return BaseDao.getBoolean(update);
	}

	/**
	 * @Title: editToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   更新行政区域
	 */
	private boolean editToDB() throws SQLException {
		int update = BaseDao.toUpdate("update tb_area set areaname=?,grade=? where areacode=?", 
				new Object[]{
						this.areaname,
						this.grade,
						this.areacode
				});
		return BaseDao.getBoolean(update);
	}

	/**
	 * @Title: delFromDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 删除行政区域  
	 */
	private boolean delFromDB() throws SQLException {
		StringBuffer sqlBuff = new StringBuffer(
				"delete from tb_area where areacode like '" + this.areacode + "%' and grade>=?");
		int update = BaseDao.toUpdate(sqlBuff.toString(), new Object[]{this.grade});
		return BaseDao.getBoolean(update);
	}

	/**
	 * 加载行政区域信息
	 * @param areaid
	 */
	private void loadDB(String areaid) throws SQLException {
		Area area = (Area) BaseDao.getObject(new EntityMapper<Area>() {
			@Override
			public Area getEntity(ResultSet resultSet) throws SQLException {
				Area area = new Area();
				area.setAreacode(resultSet.getString("areacode"));
				area.setAreaname(resultSet.getString("areaname"));
				area.setGrade(resultSet.getInt("grade"));
				return area;
			}
		}, "select * from tb_area where areacode=?", new Object[]{this.areacode});
		if (area != null) {
			this.areacode = area.getAreacode();
			this.areaname = area.getAreaname();
			this.grade = area.getGrade();
		}
	}

	/**
	 * 自动生成行政区域编号
	 * @return
	 * @throws SQLException
	 */
	private String createAreaid() throws SQLException {
		try {
			String areapid = this.parent.getAreacode();
			int grade = 1;
			if(this.parent.getGrade() != null ){
				grade = this.parent.getGrade();
			}
			String sql = "select max(areacode) as areacode from tb_area where areacode like'" + areapid + "%' and grade>=" + grade;
			String maxcode = "";
			maxcode = (String)BaseDao.getObject(new EntityMapper<String>() {
				@Override
				public String getEntity(ResultSet resultSet) throws SQLException {
					return resultSet.getString("areacode");
				}
			}, sql, null);
			Integer number = 1;
			if (maxcode != null && !"".equals(maxcode)) {
				int beginIndex = 0;
				String no = "00";
				if(maxcode.length() > 6){
					beginIndex = maxcode.length() - 2;
					no = maxcode.substring(beginIndex);
				}
				number = Integer.parseInt(no);
				++number;
				// 使用0补足位数
				no = String.format("%02d", number);
				maxcode = this.parent.getAreacode() + no;
			} else {
				maxcode = "450421";// + String.format("%02d", number);
			}
			return maxcode;
		} catch (Exception e) {
			throw e;
		} 
	}

	/**
	 * @Title: children   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   获取区域子列表
	 */
	public List<Area> children() throws Exception {
		List<Area> areaList = null;
		try {
			areaList = BaseDao.findAll("select * from t_area where areacode like'?%' and grade=?", 
					new Object[]{this.areacode, this.grade + 1}, Area.class);
		} catch (DBException e) {
			throw e;
		} 
		return areaList;
	}


	@Override
	public String toString() {
		return "Area [areacode=" + areacode + ", parent=" + parent + ", areaname=" + areaname + ", grade=" + grade
				+ "]";
	}

}
