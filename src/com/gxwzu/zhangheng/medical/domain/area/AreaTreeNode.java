package com.gxwzu.zhangheng.medical.domain.area;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @ClassName:  AreaTreeNode   
 * @Description:TODO(这里用一句话描述这个类的作用)  行政区域树Node节点 
 * @author: zhangheng
 * @date:   2019年4月8日 下午5:59:42     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class AreaTreeNode implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String pid;
	
	private String name;
	
	@JSONField(serialize=false)
	private List<AreaTreeNode> children;
	
	private boolean checked;
    
	private int level;
    
	private String url;
	
	private boolean open;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AreaTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<AreaTreeNode> children) {
		this.children = children;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public String toString() {
		return "AreaTreeNode [id=" + id + ", pid=" + pid + ", name=" + name + ", children=" + children + ", checked="
				+ checked + ", level=" + level + ", url=" + url + ", open=" + open + "]";
	}

	public AreaTreeNode() {
	}
	
	public AreaTreeNode(Area area){
		this.id =area.getAreacode();
		this.name =area.getAreaname();
		this.pid =area.getParent().getAreacode();
		this.open=true;
		
	}

	public AreaTreeNode(String id, String pid, String name, List<AreaTreeNode> children, boolean checked, int level,
			String url, boolean open) {
		super();
		this.id = id;
		this.pid = pid;
		this.name = name;
		this.children = children;
		this.checked = checked;
		this.level = level;
		this.url = url;
		this.open = open;
	}
	
}
