package com.gxwzu.zhangheng.medical.domain.area;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName:  AreaTree   
 * @Description:TODO(这里用一句话描述这个类的作用)   行政区域树组件
 * @author: zhangheng
 * @date:   2019年4月8日 下午6:05:12     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class AreaTree implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	private List<AreaTreeNode> nodeList;
	
	/**
	 * 构造根节点为id 菜单树
	 * 
	 * @param id
	 */
	public AreaTree(String id) throws Exception {
		nodeList = new ArrayList<AreaTreeNode>();
		Area area = new Area(id);
		List<Area> areaList = area.children();
		for (Area m : areaList) {
			nodeList.add(tree(m, true));
		}
		
	}
	
	/**
	 * 
	 * @param node
	 * @return
	 */
	public void visit(AreaTreeNode node,List<AreaTreeNode>nodes){
		List<AreaTreeNode>children=node.getChildren();
		nodes.add(node);
		if(children!=null&&!children.isEmpty()){
			for(AreaTreeNode e:node.getChildren()){
				visit(e,nodes);
			}
		}
	}
	
	public List<AreaTreeNode>getChildNodeList(){
		List<AreaTreeNode> allNodes=new ArrayList<AreaTreeNode>();
		for(AreaTreeNode node:this.nodeList){
		
			visit(node, allNodes);
		}
		return allNodes;			
	}
	
	/**
	 * 生成权限树
	 * 
	 * @param id
	 */
	public AreaTreeNode tree(Area area,boolean recursive) throws Exception {
		try {
			AreaTreeNode node = new AreaTreeNode(area);
			List<Area> subAreaList = area.children();
			if (!subAreaList.isEmpty()) {
				if (recursive) {// 递归查询子节点
					List<AreaTreeNode> children = new ArrayList<AreaTreeNode>();
					for (Area m : subAreaList) {
						AreaTreeNode t = tree(m, true);
						children.add(t);
					}
					node.setChildren(children);
				}
			}
			return node;
		} catch (Exception e) {
			throw new Exception("recursive create the node of tree failed!", e);
		}
	}

	public List<AreaTreeNode> getNodeList() {
		return nodeList;
	}
	
}
