package com.gxwzu.zhangheng.medical.domain.medical;

import java.io.Serializable;

import com.gxwzu.zhangheng.medical.dao.BaseDao;
import com.gxwzu.zhangheng.medical.exception.DBException;

/**
 * @ClassName:  Medical   
 * @Description:TODO(这里用一句话描述这个类的作用)   卫生机构管理类
 * @author: zhangheng
 * @date:   2019年4月14日 下午1:21:44     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class Medical implements Serializable {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * `mcode` varchar(32) NOT NULL COMMENT '主键，卫生机构编码',
	 */
	private String mcode;
	
	/**
	 * `mname` varchar(128) NOT NULL COMMENT '卫生机构名称',
	 */
	private String mname;
	
	/**
	 * `address` varchar(255) DEFAULT NULL COMMENT '卫生机构地址',
	 */
	private String address;
	
	public Medical() {
	
	}
	
	/**
	 * @Title:  Medical   
	 * @Description:    TODO(这里用一句话描述这个方法的作用)   根据mcode加载数据库中的Medical
	 */
	public Medical(String mcode){
		this.mcode = mcode;
		this.loadDB();
	}

	public Medical(String mcode, String mname, String address) {
		super();
		this.mcode = mcode;
		this.mname = mname;
		this.address = address;
	}
	
	/**
	 * @Title: add   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 添加medical数据到数据库  
	 * @return: boolean      
	 */
	public boolean add(){
		return this.addToDB();
	}
	
	/**
	 * @Title: addToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加medical数据到数据库  
	 * @return: boolean      
	 */
	private boolean addToDB(){
		int update = BaseDao.toUpdate(" INSERT INTO tb_medical(mcode,mname,address) VALUES(?,?,?) ",
				new Object[]{this.mcode, this.mname, this.address });
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: del   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中删除mcode的数据
	 * @param: @param mcode
	 * @return: boolean      
	 */
	public boolean del(String mcode){
		this.mcode = mcode;
		return this.delFromDB();
	}
	
	/**
	 * @Title: delFromDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   从数据库中删除mcode的数据
	 * @return: boolean      
	 */
	private boolean delFromDB(){
		int update = BaseDao.toUpdate("DELETE FROM tb_medical WHERE mcode = ?", new Object[]{this.mcode});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: update   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  保存数据到数据库 
	 * @return: boolean      
	 */
	public boolean update(){
		return this.updateToDB();
	}
	
	/**
	 * @Title: updateToDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   保存数据到数据库
	 * @return: boolean      
	 */
	private boolean updateToDB(){
		int update = BaseDao.toUpdate(
				"UPDATE tb_medical SET mname = ?, address = ? WHERE mcode = ? ", 
				new Object[]{this.mname, this.address, this.mcode});
		return BaseDao.getBoolean(update);
	}
	
	/**
	 * @Title: loadDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   加载数据库中mcode存在的medical
	 * @return: boolean      
	 */
	private void loadDB(){
		try {
			Medical entity = BaseDao.getObject("SELECT * FROM tb_medical WHERE mcode = ?",
					new Object[]{this.mcode}, Medical.class);
			if(entity != null){
				this.mcode = entity.getMcode();
				this.mname = entity.getMname();
				this.address = entity.getAddress();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Medical [mcode=" + mcode + ", mname=" + mname + ", address=" + address + "]";
	}

	public String getMcode() {
		return mcode;
	}

	public void setMcode(String mcode) {
		this.mcode = mcode;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
