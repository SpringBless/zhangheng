package com.gxwzu.zhangheng.medical.database;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Description jdbc 封装实体映射接口类，是一个泛型接口
 * @author zhangheng
 * @date 2019-02-26 23:17:00
 */
public interface EntityMapper<T> {

	/**
	 * 实体映射方法
	 * @param object
	 * @param resultSet
	 * @throws SQLException
	 */
	T getEntity(ResultSet resultSet) throws SQLException;
	
}
