package com.gxwzu.zhangheng.medical.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.commons.dbcp.BasicDataSource;

/**
 * @Description
 * 		数据库连接工具类
 * 			步骤：
 * 				1、导jar包
 *  			2、写配置文件properties
 *  			3、静态成员变量
 *  			4、实例化properties
 *  			5、io流读取配置文件properties
 *  			6、加载配置文件properties到properties实例中
 *  			7、初始化静态成员变量
 *  			8、实例化BasicDataSource
 *  			9、设置值到basicdatasource实例
 *  			10、连接Connection
 *  classpath: in.spring.medical.common.util.MedicalDBUtil.java
 *  @author spring
 *  @date 2019-02-26 17:17:00
 */
public class DBUtil {

	private static BasicDataSource bds;
	private static String driver;
	private static String url;
	private static String username;
	private static String pwd;
	private static String initialSize;
	private static String maxIdle;
	private static String minIdle;
	private static String maxActive;
	private static String maxWait;
	
	static{
		Properties properties = new Properties();
		InputStream inputStream = DBUtil.class.getClassLoader().getResourceAsStream("jdbc/jdbc.properties");
		try {
			properties.load(inputStream);
			driver = properties.getProperty("driver");
			url = properties.getProperty("url");
			username = properties.getProperty("username");
			pwd = properties.getProperty("pwd");
			initialSize = properties.getProperty("initialSize");
			maxIdle = properties.getProperty("maxIdle");
			minIdle = properties.getProperty("minIdle");
			maxActive = properties.getProperty("maxActive");
			maxWait = properties.getProperty("maxWait");
			
			bds = new BasicDataSource();
			bds.setDriverClassName(driver);
			bds.setUrl(url);
			bds.setUsername(username);
			bds.setPassword(pwd);
			bds.setInitialSize(Integer.parseInt(initialSize));
			bds.setMaxIdle(Integer.parseInt(maxIdle));
			bds.setMinIdle(Integer.parseInt(minIdle));
			bds.setMaxActive(Integer.parseInt(maxActive));
			bds.setMaxWait(Long.parseLong(maxWait));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 得到一个Connection连接
	 * @return Connection
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		return bds.getConnection();
	}
	
	/**
	 * 获取PreparedStatement SQl执行对象
	 * @param connection
	 * @param sql
	 * @return PreparedStatement
	 * @throws SQLException
	 */
	public static PreparedStatement getPreparedStatement(Connection connection, String sql) throws SQLException{
		return connection.prepareStatement(sql);
	}
	
	/**
	 * 关闭所有连接Connection，PreparedStatement，ResultSet
	 * @param connection
	 * @param pStatement
	 * @param resultSet
	 * @return void
	 */
	public static void closeAll(Connection connection, PreparedStatement pStatement, ResultSet resultSet){
		try {
			if(resultSet != null){
				resultSet.close();
			}
			if(pStatement != null){
				pStatement.close();
			}
			if(connection != null){
				connection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 关闭Connection连接
	 * @param connection
	 * @return void
	 */
	public static void closeConnection(Connection connection){
		closeAll(connection, null, null);
	}
	
	/**
	 * 关闭PreparedStatement SQL执行对象
	 * @param pStatement
	 * @return void
	 */
	public static void closePreparedStatenment(PreparedStatement pStatement){
		closeAll(null, pStatement, null);
	}
	
	/**
	 * 关闭ResultSet 结果集对象
	 * @param resultSet
	 * @return void
	 */
	public static void closeResultSet(ResultSet resultSet){
		closeAll(null, null, resultSet);
	}
	
}
