package com.gxwzu.zhangheng.medical.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import com.gxwzu.zhangheng.medical.domain.pay.PrintPay;

/**
 * 打印发票输出
 * @ClassName:  PDFUtilsPrintFP   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月12日 上午11:27:20     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PDFUtilsPrintFP {

	public static final String TYPE_NAME = ".pdf"; 
	
	/**
	 * 创建并输出PDF
	 * @Title: outPDF   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param sheetName
	 * @param: @param data
	 * @param: @param response      
	 * @return: void      
	 */
	public static void outPDF(String fileName , PrintPay data, HttpServletResponse response){
		fileName = fileName == null || "".equals(fileName) ? UUID.randomUUID().toString().substring(0, 10) : fileName;
		OutputStream out = null;
		//设置响应头，控制浏览器下载该文件
		try {
			int idx = fileName.lastIndexOf(".");
			if(idx != -1){
				String[] split = fileName.split(".");
				fileName = split[0] + TYPE_NAME;
			}else {
				fileName = fileName + TYPE_NAME;
			}
			response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
			out = response.getOutputStream();
			BasePDFWrite basePDFWrite = new BasePDFWrite(out);
			basePDFWrite.generatePDF(data);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(out != null){
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
