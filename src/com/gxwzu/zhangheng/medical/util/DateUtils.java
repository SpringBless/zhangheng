package com.gxwzu.zhangheng.medical.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间类型数据转换工具类
 */
public class DateUtils {
	
	/**
	 * 转换时间格式
	 */
	public static final SimpleDateFormat SDF_YYYY_MM_DD =  new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 完整时间格式化
	 */
	public static final SimpleDateFormat YYYY_MM_DD_HH_MM_SS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
     * 时间格式
     */
    public static final SimpleDateFormat YYYY_MM_DD_HH_MM_SS1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    
    /**
     * 时间格式
     */
    public static final SimpleDateFormat YYYY_MM_DD_HH_MM_SS_D = new SimpleDateFormat("yyyyMMddHHmmss");
    
    /**
     * 年
     */
    public static final SimpleDateFormat YYYY = new SimpleDateFormat("yyyy");
    
    /**
     * @Title: getFormatTime   
     * @Description: TODO(这里用一句话描述这个方法的作用)  时间格式，如20190101010101 
     * @return: String      
     */
    public static String getFormatTime(){
    	return getFormatTime(null);
    }
    
    /**
     * @Title: getFormatTime   
     * @Description: TODO(这里用一句话描述这个方法的作用)  时间格式，如20190101010101 
     * @return: String      
     */
    public static String getFormatTime(java.util.Date date){
    	if(date == null){
    		date = new java.util.Date();
    	}
    	return YYYY_MM_DD_HH_MM_SS_D.format(date);
    }
    
    public static java.util.Date paseDate(String date){
    	try {
			return YYYY_MM_DD_HH_MM_SS.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return new java.util.Date();
    }
    /**
     * 参合年份
     * @Title: formartYear   
     * @Description: TODO(这里用一句话描述这个方法的作用)   
     * @param: @param date
     * @return: String      
     */
    public static String formartYear(java.util.Date date){
    	return YYYY.format(date);
    }
    
    public static String formartYear(String date){
    	Date parse = null;
    	try {
    		parse = YYYY.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return YYYY.format(parse);
    }
  
    /**
     * 获取当前时间
     * @return
     */
    public static String getyyyy_MM_dd_HH_mm_ss1(){
	    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));    //获取东八区时间
	    c.setTime(new java.util.Date());
	    return YYYY_MM_DD_HH_MM_SS1.format(c.getTime());
    }
    
	/**
	 * 格式化时间 java.util.Date
	 * @Title: formatDate   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param date
	 * @return: String      
	 */
	public static String formatDate(java.util.Date date){
		String format = YYYY_MM_DD_HH_MM_SS.format(date);
		return format;
	}
	

	/**
	 * 获取指定java.util.Date时间类型的年份
	 * @param date
	 * @return int year
	 */
	public static int getYearByUtilDate(java.util.Date date){
		Calendar calendar = Calendar.getInstance();
		if(date != null){
			calendar.setTime(date);
		}
		int year = 0;
		year = calendar.get(Calendar.YEAR);
		return year;
	}
	
	/**
	 * 获取当前java.util.Date时间类型的年份
	 * @return int year
	 */
	public static int getYearByUtilDate(){
		return getYearByUtilDate(null);
	}
	
	/**
	 * 获取指定java.sql.Date 时间类型的年份
	 * @param data
	 * @return
	 */
	public static int getYearBySqlDate(java.sql.Date date){
		return date == null ? 0:getYearBySqlDate(date.toLocalDate().toString());
	}
	
	/**
	 * 获取指定java.sql.Date 时间类型的字符串，得到年份
	 * @param data
	 * @return
	 */
	public static int getYearBySqlDate(String date){
		Calendar calendar = Calendar.getInstance();
		if(date != null){
			try {
				java.util.Date utilate = SDF_YYYY_MM_DD.parse(date);
				calendar.setTime(utilate);
			} catch (ParseException e) {
				calendar.setTime(new java.util.Date());
				e.printStackTrace();
			}
		}
		int year = 0;
		year = calendar.get(Calendar.YEAR);
		return year;
	}
	
	/**
	 * @Title: cardCode2Date   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   身份证获取出生年月
	 * @param: @param cardCode
	 * @return: java.sql.Date      
	 */
	public static java.sql.Date cardCode2Date(String cardCode){
		if(cardCode == null || "".equals(cardCode)){
			return null;
		}
		int yearStart = 6, end = 14;
		String str = cardCode.substring(yearStart, end);
		String year = str.substring(0, 4);
		String m = str.substring(4, 6);
		String d = str.substring(6, 8);
		String date = year + "-" + m + "-" + d;
		java.util.Date parse;
		try {
			parse = SDF_YYYY_MM_DD.parse(date);
			java.sql.Date date2 = new java.sql.Date(parse.getTime());
			return date2;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	} 
	
	
//	
//	public static void main(String[] args) throws ParseException {
//		//System.out.println(getYesr());
////		String str = "450923199709084052";
////		System.out.println(str.length());
////		String str1 = str.substring(6, 14);
////		
////		System.out.println(str1);
////		
////		System.out.println(str1.substring(0, 4));
////		System.out.println(str1.substring(4, 6));
////		System.out.println(str1.substring(6, 8));
//		System.out.println(formatDate(new Date()));
//	}
	
}
