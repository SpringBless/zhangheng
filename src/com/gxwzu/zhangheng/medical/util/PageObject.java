package com.gxwzu.zhangheng.medical.util;

import java.util.List;

/**
 *  分页对象
 * totalRow 某次查询的总记录数
 * pageNo 当前查询页码 
 * pageSize  每页记录条数  
 * begRow 页起始记录行数
 * endRow 页的截止记录行数 
 * data 数据集合
 * @ClassName:  PageObject   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @param <T>
 * @date:   2019年3月30日 下午2:02:58     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class PageObject<T> implements java.io.Serializable{
 
	
	private static final long serialVersionUID = 1L;
	private int totalRow;
	private int pageNo;    
	private int pageSize;
	private int begRow;
	private int endRow;
	
	private int totalPage;
	
	private List<T> data;
	
	private final int DEFAULT_PAGESIZE=20;
	private final int MAX_PAGESIZE=1000;

	public List<T> getData(){
		return data;
	}
	
	public void setData(List<T> data){
		this.data = data;
	}
	
	/**  
	 * @return the mAX_PAGESIZE  
	 */
	public int getMAX_PAGESIZE() {
		return MAX_PAGESIZE;
	}


	public PageObject(int pageNo){
		this.pageNo = pageNo;
		init();
		
	}
	
	
	public PageObject(int pageNo,int pageSize){  
 		this.pageNo = pageNo;
		this.pageSize = pageSize;
		init();
	}
	
	//获取上一页
	public int getPrePage(){
		return this.pageNo-1;
	}
	//获取下一页
	public int getNextPage(){
		return this.pageNo+1;
	}
	//判断是否是第一页	
	public boolean isFirstPage(){
		if(this.pageNo==1)
			return true;				
		else
			return false;
	}
	
	//判断是否是最后一页
	public boolean isLastPage(){
		if(this.pageNo==this.totalPage)
			return true;
		else
			return false;
	}
	
	/**
	 * 根据构造数据初始页对象
	 */
	private void init() {
		//校正totalRows && pageNo && pageSize
		if (pageNo <= 0) pageNo = 1;
		if (pageSize == 0) pageSize = DEFAULT_PAGESIZE;
		if (pageSize > MAX_PAGESIZE) pageSize = MAX_PAGESIZE;
		if (pageSize > 0){
			begRow = (pageNo - 1 ) * pageSize + 1;
			endRow = pageNo * pageSize;
		} else {
			begRow = 1;
			endRow = MAX_PAGESIZE;
		}
	}
	
	/**  
	 * @return the totalRow  
	 */
	public int getTotalRow() {
		return totalRow;
	}
	/**  
	 * 在设置总的同时将总页数算出来
	 * @param totalRow the totalRow to set  
	 */
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
		this.totalPage = this.totalRow/this.pageSize+1;
	}
	/**  
	 * @return the pageNo  
	 */
	public int getPageNo() {
		return pageNo;
	}
	/**  
	 * @param pageNo the pageNo to set  
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	/**  
	 * @return the pageSize  
	 */
	public int getPageSize() {
		return pageSize;
	}
	/**  
	 * @param pageSize the pageSize to set  
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	/**  
	 * @return the begRow  
	 */
	public int getBegRow() {
		return begRow;
	}
	/**  
	 * @param begRow the begRow to set  
	 */
	public void setBegRow(int begRow) {
		this.begRow = begRow;
	}
	/**  
	 * @return the endRow  
	 */
	public int getEndRow() {
		return endRow;
	}
	/**  
	 * @param endRow the endRow to set  
	 */
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	/**  
	 * @return the dEFAULT_PAGESIZE  
	 */
	public int getDEFAULT_PAGESIZE() {
		return DEFAULT_PAGESIZE;
	}


	public int getTotalPage() {
		return totalPage;
	}


	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	@Override
	public String toString() {
		return "PageObject [totalRow=" + totalRow + ", pageNo=" + pageNo + ", pageSize=" + pageSize + ", begRow="
				+ begRow + ", endRow=" + endRow + ", totalPage=" + totalPage + ", data=" + data + ", DEFAULT_PAGESIZE="
				+ DEFAULT_PAGESIZE + ", MAX_PAGESIZE=" + MAX_PAGESIZE + "]";
	}

}