package com.gxwzu.zhangheng.medical.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.gxwzu.zhangheng.medical.domain.apply.ApplyDto;
import com.gxwzu.zhangheng.medical.domain.pay.PayDto;

/**
 * @ClassName:  ExcelWriteUtils   
 * @Description:TODO(这里用一句话描述这个类的作用)  用于生成excel文件 
 * @author: zhangheng
 * @date:   2019年5月10日 下午1:07:10     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class ExcelWriteUtils {
	
	public static final String ID = "参合缴费编号";
	
	public static final String NH_CODE = "参合证号";
	
	public static final String FAPIAO = "参合发票号";
	
	public static final String NAME = "姓名";
	
	public static final String FAMILY_CODE = "家庭编号";
	
	public static final String CARD_CODE = "身份证号";
	
	public static final String PLAY_PRICE = "缴费金额";
	
	public static final String PLAY_YEAR = "交费年度";
	
	public static final String PLAY_DATE = "交费时间";
	
	public static final String USER_ID = "操作员";
	
	public static final String TYPE_NAME = ".xls";
	
	/**
	 * 创建并输出excel
	 * @Title: outExcel   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param sheetName
	 * @param: @param data
	 * @param: @param response      
	 * @return: void      
	 */
	public static void outExcel(String sheetName , PageObject<PayDto> data, HttpServletResponse response){
		sheetName = sheetName == null || "".equals(sheetName) ? UUID.randomUUID().toString().substring(0, 10) : sheetName;
		HSSFWorkbook wb = initData(sheetName, data);
		OutputStream out = null;
		//设置响应头，控制浏览器下载该文件
		try {
			int idx = sheetName.lastIndexOf(".");
			if(idx != -1){
				String[] split = sheetName.split(".");
				sheetName = split[0] + TYPE_NAME;
			}else {
				sheetName = sheetName + TYPE_NAME;
			}
			response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(sheetName, "UTF-8"));
			out = response.getOutputStream();
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(out != null){
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static HSSFWorkbook initData(String sheetName , PageObject<PayDto> data){
		// 第一步创建workbook
		HSSFWorkbook wb = new HSSFWorkbook();

		// 第二步创建sheet
		HSSFSheet sheet = wb.createSheet(sheetName);

		// 第三步创建行row:添加表头0行
		HSSFRow row = sheet.createRow(0);
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中

		// 第四步创建单元格
		HSSFCell cell = row.createCell(0); // 第一个单元格，ID
		cell.setCellValue(ID);
		cell.setCellStyle(style);

		cell = row.createCell(1); // 第二个单元格，NH_CODE
		cell.setCellValue(NH_CODE);
		cell.setCellStyle(style);
		
		cell = row.createCell(2); // 第三个单元格，FAPIAO
		cell.setCellValue(FAPIAO);
		cell.setCellStyle(style);
		
		cell = row.createCell(3); // 第四个单元格，NAME
		cell.setCellValue(NAME);
		cell.setCellStyle(style);
		
		cell = row.createCell(4); // 第五个单元格，FAMILY_CODE
		cell.setCellValue(FAMILY_CODE);
		cell.setCellStyle(style);
		
		cell = row.createCell(5); // 第六个单元格，CARD_CODE
		cell.setCellValue(CARD_CODE);
		cell.setCellStyle(style);
		
		cell = row.createCell(6); // 第七个单元格，PLAY_PRICE
		cell.setCellValue(PLAY_PRICE);
		cell.setCellStyle(style);
		
		cell = row.createCell(7); // 第八个单元格，PLAY_YEAR
		cell.setCellValue(PLAY_YEAR);
		cell.setCellStyle(style);
		
		cell = row.createCell(8); // 第九个单元格，PLAY_DATE
		cell.setCellValue(PLAY_DATE);
		cell.setCellStyle(style);
		
		cell = row.createCell(9); // 第十个单元格，USER_ID
		cell.setCellValue(USER_ID);
		cell.setCellStyle(style);

		if(data != null && data.getData() != null && data.getData().size() > 0){
			// 第五步插入数据
			for(int i=0;i<data.getData().size(); i++){
				PayDto payDto = data.getData().get(i);
				// 创建行
				row = sheet.createRow(i + 1);
				// 创建单元格并且添加数据
				row.createCell(0).setCellValue(payDto.getId());
				row.createCell(1).setCellValue(payDto.getNhCode());
				row.createCell(2).setCellValue(payDto.getFapiao());
				row.createCell(3).setCellValue(payDto.getName());
				row.createCell(4).setCellValue(payDto.getFamilyCode());
				row.createCell(5).setCellValue(payDto.getCardCode());
				row.createCell(6).setCellValue(payDto.getPlayPrice().toString());
				String date = payDto.getPlayYear().length() > 4 ? payDto.getPlayYear().substring(0, 4) : payDto.getPlayYear();
				row.createCell(7).setCellValue(date);
				row.createCell(8).setCellValue(DateUtils.formatDate(payDto.getPlayDate()));
				row.createCell(9).setCellValue(payDto.getUserId());
			}
		}
		return wb;
	}
	
	// ++++++++++++++++++ 报销 ++++++++++++++++++
	/**
	 * `a_id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '报销记录id',
	 */
	public static String aId = "报销记录id";
	
	/**
	 * `a_year` varchar(4) NOT NULL COMMENT '当前缴费年份',
	 */
	public static String aYear = "当前缴费年份";
	
	/**
	 * `a_apply` decimal(6,2) NOT NULL DEFAULT '0.00' COMMENT '当前报销金额',
	 */
	public static String aApply = "当前报销金额";
	
	/**
	 *  `a_illname` varchar(50) NOT NULL COMMENT '疾病名称',
	 */
	public static String aIllname = "疾病名称";
	
	/**
	 * `a_family_code` varchar(64) NOT NULL COMMENT '家庭编号',
	 */
	public static String aFamilyCode = "家庭编";
	
	/**
	 * `a_id_card` varchar(32) NOT NULL COMMENT '身份证',
	 */
	public static String aIdCard = "身份证";
	
	/**
	 * `a_create_date` datetime NOT NULL COMMENT '报销申请时间',
	 */
	public static String aCreateDate = "报销申请时间";
	
	/**
	 * `a_apply_date` datetime NOT NULL COMMENT '报销完成时间',
	 */
	public static String aApplyDate = "报销完成时间";
	
	/**
	 * `a_status` int(3) NOT NULL COMMENT '报销状态：待审核：0，审核通过：1，审核不通过：2，已汇款：3，未回款：4',
	 */
	public static String aStatus = "报销状态";
	
	/**
	 * 就诊时间
	 * `a_see_date` datetime DEFAULT NULL COMMENT '就诊时间',
	 */
	public static String aSeeDate = "就诊时间";
	
	/**
	 * 发票号
	 * `a_fapiao` varchar(50) NOT NULL COMMENT '发票号',
	 */
	public static String aFapiao = "发票号";
	
	/**
	 * nh_code varchar(128) not null COMMENT '农合证号,依赖family_code',
	 */
	public static String nhCode = "农合证号";
	
	/**
	 * yl_code varchar(128) not null COMMENT '医疗证卡号',
	 */
	public static String ylCode = "医疗证卡号";
	
	/**
	 * hn_code varchar(3) not null COMMENT '户内编号,根据家庭成员增加',
	 */
	public static String hnCode = "户内编号";
	
	/**
	 * name varchar(32) not null COMMENT '姓名',
	 */
	public static String name = "姓名";
	
	/**
	 * fm_relation varchar(32) not null COMMENT '与户主关系',
	 */
	public static  String fmRelation = "与户主关系";
	
	/**
	 * gender varchar(12) not null default '男' COMMENT '性别',
	 */
	public static String gender = "性别";
	
	/**
	 * sf_health varchar(32) not null COMMENT '健康状况',
	 */
	public static String sfHealth = "健康状况";
	
	/**
	 * sf_national varchar(64) not null COMMENT '民族',
	 */
	public static String sfNational = "民族";
	
	/**
	 * phone varchar(11) default null COMMENT '联系电话',
	 */
	public static String phone = "联系电话";
	
	/**
	 * address varchar(255) default null COMMENT '常住地址'
	 */
	public static  String address = "常住地址";
	
	/**
	 * @Title: outBaoxiaoExcel   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param sheetName
	 * @param: @param data
	 * @param: @param response      
	 * @return: void      
	 */
	public static void outBaoxiaoExcel(String sheetName , List<ApplyDto> data, HttpServletResponse response){
		sheetName = sheetName == null || "".equals(sheetName) ? UUID.randomUUID().toString().substring(0, 10) : sheetName;
		HSSFWorkbook wb = initBaoxiaoData(sheetName, data);
		OutputStream out = null;
		//设置响应头，控制浏览器下载该文件
		try {
			int idx = sheetName.lastIndexOf(".");
			if(idx != -1){
				String[] split = sheetName.split(".");
				sheetName = split[0] + TYPE_NAME;
			}else {
				sheetName = sheetName + TYPE_NAME;
			}
			response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(sheetName, "UTF-8"));
			out = response.getOutputStream();
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(out != null){
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static HSSFWorkbook initBaoxiaoData(String sheetName , List<ApplyDto> data){
		// 第一步创建workbook
		HSSFWorkbook wb = new HSSFWorkbook();

		// 第二步创建sheet
		HSSFSheet sheet = wb.createSheet(sheetName);

		// 第三步创建行row:添加表头0行
		HSSFRow row = sheet.createRow(0);
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中

		// 第四步创建单元格
		HSSFCell cell = row.createCell(0); // 第一个单元格，ID
		cell.setCellValue(aId);
		cell.setCellStyle(style);// 报销id

		cell = row.createCell(1);//当前缴费年份
		cell.setCellValue(aYear);
		cell.setCellStyle(style);
		
		cell = row.createCell(2);//当前报销金额
		cell.setCellValue(aApply);
		cell.setCellStyle(style);
		
		cell = row.createCell(3);//疾病名称
		cell.setCellValue(aIllname);
		cell.setCellStyle(style);
		
		cell = row.createCell(4);//姓名
		cell.setCellValue(name);
		cell.setCellStyle(style);
		
		cell = row.createCell(5);//身份证
		cell.setCellValue(aIdCard);
		cell.setCellStyle(style);
		
		cell = row.createCell(6);//常驻地址
		cell.setCellValue(address);
		cell.setCellStyle(style);
		
		cell = row.createCell(7);//报销申请时间
		cell.setCellValue(aApplyDate);
		cell.setCellStyle(style);
		
		cell = row.createCell(8);//报销状态
		cell.setCellValue(aStatus);
		cell.setCellStyle(style);
		
		cell = row.createCell(9);//就诊时间
		cell.setCellValue(aSeeDate);
		cell.setCellStyle(style);
		
		cell = row.createCell(10);//发票号
		cell.setCellValue(aFapiao);
		cell.setCellStyle(style);
		
		cell = row.createCell(11);//农合证号
		cell.setCellValue(nhCode);
		cell.setCellStyle(style);
		
		cell = row.createCell(12);//医疗证卡号
		cell.setCellValue(ylCode);
		cell.setCellStyle(style);
		
		cell = row.createCell(13);//与户主关系
		cell.setCellValue(fmRelation);
		cell.setCellStyle(style);
		
		cell = row.createCell(14);//家庭编号
		cell.setCellValue(aFamilyCode);
		cell.setCellStyle(style);
		
		cell = row.createCell(15);//性别
		cell.setCellValue(gender);
		cell.setCellStyle(style);
		
		cell = row.createCell(16);//健康状况
		cell.setCellValue(sfHealth);
		cell.setCellStyle(style);
		
		cell = row.createCell(17);//民族
		cell.setCellValue(sfNational);
		cell.setCellStyle(style);
		
		cell = row.createCell(18);//联系电话
		cell.setCellValue(phone);
		cell.setCellStyle(style);
		
		
		if(data != null && data.size() > 0){
			// 第五步插入数据
			for(int i=0;i<data.size(); i++){
				ApplyDto a = data.get(i);
				// 创建行
				row = sheet.createRow(i + 1);
				// 创建单元格并且添加数据
				row.createCell(0).setCellValue(a.getaId());
				row.createCell(1).setCellValue(a.getaYear());
				row.createCell(2).setCellValue(a.getaApply().toString() + "元");
				row.createCell(3).setCellValue(a.getaIllname());
				row.createCell(4).setCellValue(a.getName());
				row.createCell(5).setCellValue(a.getaIdCard());
				row.createCell(6).setCellValue(a.getaFamilyCode());
				row.createCell(7).setCellValue(DateUtils.formatDate(a.getaApplyDate()));
				
				Integer status = a.getaStatus();
				String string = null;
				if(status.intValue() == 0){
					string = "待审核";
				}
				if(status.intValue() == 1){
					string = "审核通过";
				}
				if(status.intValue() == 2){
					string = "审核不通过";
				}
				if(status.intValue() == 3){
					string = "已汇款";
				}
				if(status.intValue() == 4){
					string = "未汇款";
				}
				row.createCell(8).setCellValue(string);
				row.createCell(9).setCellValue(DateUtils.formatDate(a.getaSeeDate()));
				row.createCell(10).setCellValue(a.getaFapiao());
				row.createCell(11).setCellValue(a.getNhCode());
				row.createCell(12).setCellValue(a.getYlCode());
				row.createCell(13).setCellValue(a.getFmRelation());
				row.createCell(14).setCellValue(a.getAddress());
				row.createCell(15).setCellValue(a.getGender());
				row.createCell(16).setCellValue(a.getSfHealth());
				row.createCell(17).setCellValue(a.getSfNational());
				row.createCell(18).setCellValue(a.getPhone());
			}
		}
		return wb;
	}
	
}
