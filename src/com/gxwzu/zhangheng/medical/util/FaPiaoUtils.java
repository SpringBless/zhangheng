package com.gxwzu.zhangheng.medical.util;

import java.util.UUID;

/**
 * 发票生成工具类
 * @ClassName:  FaPiaoUtils   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月9日 下午4:07:10     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class FaPiaoUtils {

	public static String getDefaultFapiao(){
		StringBuffer fp = new StringBuffer();
		fp.append("FP_");
		fp.append(getUUID().substring(0, 4));
		String tmp = timeStep();
		int idx = tmp.length() - 8;
		fp.append(tmp.substring(idx));
		return fp.toString().toUpperCase();
	}
	
	public static String getUUID(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static String timeStep(){
		return String.valueOf(System.currentTimeMillis());
	}
	
//	public static void main(String[] args) {
//		for(int i=0;i<10;i++){			
//			System.out.println(getDefaultFapiao());
//		}
//	}
}
