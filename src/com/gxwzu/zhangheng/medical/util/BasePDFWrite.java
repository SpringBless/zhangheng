package com.gxwzu.zhangheng.medical.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.gxwzu.zhangheng.medical.domain.pay.PayDto;
import com.gxwzu.zhangheng.medical.domain.pay.PrintPay;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
 
/**
 * pdf发票生成
 * @ClassName:  BasePDFWrite   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月12日 上午10:59:39     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
public class BasePDFWrite {
 
    Document document = null;// 建立一个Document对象
    private static Font headFont ;
    private static Font keyFont ;
    private static Font textfont_H ;
    @SuppressWarnings("unused")
	private static Font textfont_B ;
    int maxWidth = 520;
 
    static{
        BaseFont bfChinese_H;
        try {
            /**
             * 新建一个字体,iText的方法 STSongStd-Light 是字体，在iTextAsian.jar 中以property为后缀
             * UniGB-UCS2-H 是编码，在iTextAsian.jar 中以cmap为后缀 H 代表文字版式是 横版， 相应的 V 代表竖版
             */
            bfChinese_H = BaseFont.createFont("STSong-Light","UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
 
            headFont = new Font(bfChinese_H, 10, Font.NORMAL);
            keyFont = new Font(bfChinese_H, 18, Font.BOLD);
            textfont_H = new Font(bfChinese_H, 10, Font.NORMAL);
            textfont_B = new Font(bfChinese_H, 12, Font.NORMAL);
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
    /**
     * 设置页面属性
     * @param file
     */
    public BasePDFWrite(File file) {
 
        //自定义纸张
       // Rectangle rectPageSize = new Rectangle(850, 600);
 
        // 定义A4页面大小
        Rectangle rectPageSize = new Rectangle(PageSize.A4);
        rectPageSize = rectPageSize.rotate();// 加上这句可以实现页面的横置
        document = new Document(rectPageSize,10, 10, 10, 10);
 
        try {
            PdfWriter.getInstance(document,new FileOutputStream(file));
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 设置页面属性
     * @param file
     */
    public BasePDFWrite(OutputStream out) {
 
        //自定义纸张
       // Rectangle rectPageSize = new Rectangle(850, 600);
 
        // 定义A4页面大小
        Rectangle rectPageSize = new Rectangle(PageSize.A4);
        rectPageSize = rectPageSize.rotate();// 加上这句可以实现页面的横置
        document = new Document(rectPageSize,10, 10, 10, 10);
 
        try {
            PdfWriter.getInstance(document,out);
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
    /**
     * 建表格(以列的数量建)
     * @param colNumber
     * @return
     */
    public PdfPTable createTable(int colNumber){
        PdfPTable table = new PdfPTable(colNumber);
        try{
            //table.setTotalWidth(maxWidth);
            //table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorder(1);
            table.setSpacingBefore(10);
            table.setWidthPercentage(100);
        }catch(Exception e){
            e.printStackTrace();
        }
        return table;
    }
 
    /**
     * 建表格(以列的宽度比建)
     * @param widths
     * @return
     */
    public PdfPTable createTable(float[] widths){
        PdfPTable table = new PdfPTable(widths);
        try{
            //table.setTotalWidth(maxWidth);
            //table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorder(1);
            table.setSpacingBefore(10);
            table.setWidthPercentage(100);
        }catch(Exception e){
            e.printStackTrace();
        }
        return table;
    }
 
 
    /**
     * 表格中单元格
     * @param value
     * @param font
     * @param align
     * @return
     */
    public PdfPCell createCell(String value,Font font,int align){
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value,font));
        return cell;
    }
 
    /**
     * 表格中单元格
     * @param value
     * @param font
     * @param align
     * @param colspan
     * @param rowspan
     * @return
     */
    public PdfPCell createCell(String value,Font font,int align_v,int align_h,int colspan,int rowspan, float padding){
        PdfPCell cell = new PdfPCell();
        cell.setPadding(padding);
        cell.setVerticalAlignment(align_v);
        cell.setHorizontalAlignment(align_h);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setPhrase(new Phrase(value,font));
        return cell;
    }
 
    /**
     * 建短语
     * @param value
     * @param font
     * @return
     */
    public Phrase createPhrase(String value,Font font){
        Phrase phrase = new Phrase();
        phrase.add(value);
        phrase.setFont(font);
        return phrase;
    }
 
    /**
     * 建段落
     * @param value
     * @param font
     * @param align
     * @return
     */
    public Paragraph createParagraph(String value,Font font,int align){
        Paragraph paragraph = new Paragraph();
        paragraph.add(new Phrase(value,font));
        paragraph.setAlignment(align);
        return paragraph;
    }
 
    /**
     * 
     * @param titile 头信息数组，index：0地址，发票，发票号
     * title = {"广西梧州市苍梧县","参合发票"，"XD201602000003"}
     * @throws Exception
     */
    public void generatePDF(String[] titile, PrintPay pay) throws Exception{
    	titile = titile != null && titile.length > 0 ? titile : new String[]{"广西梧州市苍梧县","参合发票","XD201602000003"};
    	
        //页头信息
        document.add(createParagraph(titile[0],headFont,Element.ALIGN_LEFT));
        document.add(createParagraph(titile[1],keyFont,Element.ALIGN_CENTER));
        document.add(createParagraph("发票号：" + titile[2],headFont,Element.ALIGN_RIGHT));
 
        //表格信息
        float[] widths = {4f,10f,10f,20f,15f,8f,11f,12f,10f,10f,10f,10f};
        PdfPTable table = createTable(widths);
 
        table.addCell(createCell("收款方信息", textfont_H, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,4, 10));
 
        table.addCell(createCell("名称", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        table.addCell(createCell(pay.getcName(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,9,1, 10));
 
        table.addCell(createCell("编码/税号", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        table.addCell(createCell(pay.getFapiao(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,9,1, 10));
 
        table.addCell(createCell("联系地址", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        table.addCell(createCell(pay.getAddress(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,9,1, 10));
 
        table.addCell(createCell("经办人", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,2,1, 10));
        table.addCell(createCell(pay.getJbr(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        table.addCell(createCell("经办人电话", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,2,1, 10));
        table.addCell(createCell(pay.getMobile(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,6,1, 10));
 
        if(pay.getData() != null && pay.getData().size() > 0){
        	table.addCell(createCell("参合清单", textfont_H, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1, pay.getDataSize() + 4 , 10));
        }else {
        	table.addCell(createCell("参合清单", textfont_H, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1, 4 , 10));
		}
 
        table.addCell(createCell("序号", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,1, 10));
        table.addCell(createCell("姓名", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,1, 10));
        table.addCell(createCell("身份证号", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,1, 10));
        table.addCell(createCell("家庭编号", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,2,1, 10));
        table.addCell(createCell("参合证号", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,2,1, 10));
        table.addCell(createCell("参合年份", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,1, 10));
        table.addCell(createCell("缴费时间", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,2,1, 10));
        table.addCell(createCell("操作员", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,1, 10));
        int idx = 0;
        if(pay.getData() != null){
        	for(; idx < pay.getData().size(); idx++){
        		PayDto payDto = pay.getData().get(idx);
        		table.addCell(createCell(idx + 1 + "", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        		table.addCell(createCell(payDto.getName(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        		table.addCell(createCell(payDto.getCardCode(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        		table.addCell(createCell(payDto.getFamilyCode(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        		table.addCell(createCell(payDto.getNhCode(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        		table.addCell(createCell(DateUtils.formartYear(payDto.getPlayYear()), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        		table.addCell(createCell(DateUtils.formatDate(payDto.getPlayDate()), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        		table.addCell(createCell(payDto.getUserId() + "", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));        		
        	}
        }
        
        table.addCell(createCell(idx + 1 + "", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        table.addCell(createCell("合计", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        table.addCell(createCell("", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        table.addCell(createCell("人数", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        table.addCell(createCell(idx + "", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,1,1, 10));
        table.addCell(createCell("合计金额（元）", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,2,1, 10));
        table.addCell(createCell("￥" + pay.getCountPrice() + "元", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,3,1, 10));
 
        table.addCell(createCell("确认信息", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_CENTER,1,2, 10));
 
        table.addCell(createCell("注：", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,10,1, 10));
 
        table.addCell(createCell("经办人签字：" + pay.getJbr(), textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,4,1, 10));
        table.addCell(createCell("签字日期:" + pay.getDate() , textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,4,1, 10));
        table.addCell(createCell("盖章：", textfont_H,Element.ALIGN_MIDDLE, Element.ALIGN_LEFT,3,1, 10));
 
        document.add(table);
        document.newPage();
        document.close();
    }
    
    /**
     * 
     * @param titile 头信息数组，index：0地址，发票，发票号
     * title = {"广西梧州市苍梧县","参合发票"，"XD201602000003"}
     * @throws Exception
     */
    public void generatePDF(PrintPay pay) throws Exception{
    	this.generatePDF(pay.getTitle(), pay);
    }
 
//    public static void main(String[] args) throws Exception {
//        File file = new File("C:/Users/hp.000/Desktop/test.pdf");
// 
//        file.createNewFile();
//        new BasePDFWrite(file).generatePDF(new String[]{"广西梧州市苍梧县","参合发票","XD201602000003"}, null);
//    }
    
}
