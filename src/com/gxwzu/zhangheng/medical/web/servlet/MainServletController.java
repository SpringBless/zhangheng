package com.gxwzu.zhangheng.medical.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;
import com.gxwzu.zhangheng.medical.domain.user.User;

@WebServlet("/main.do")
public class MainServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toMain   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   处理登录成功后转发至主页面
	 * @return: String      
	 */
	@ControllerAnnotation("toMain")
	public String toMain(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Menu menu = new Menu();
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		LOGGER.info(sessionUser);
		List<Menu> curentMenus = menu.findMenuByUserId(sessionUser.getId());
		LOGGER.info(curentMenus);
		request.getSession().setAttribute("SESSION_MENU", curentMenus);
		return "forward:WEB-INF/pages/main/main.jsp";
	}
	
}
