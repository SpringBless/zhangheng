package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.chrocdp.ChrocdpDtoDao;
import com.gxwzu.zhangheng.medical.domain.chrocdp.Chrocdp;
import com.gxwzu.zhangheng.medical.domain.chrocdp.ChrocdpDto;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChrocdpDtoServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病证管理控制器
 * @author: zhangheng
 * @date:   2019年4月14日 下午12:05:19     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/chrocdpdto.do")
public class ChrocdpDtoServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toChronicdisListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开慢性病证管理列表
	 */
	@ControllerAnnotation("toChrocdpDtoListPage")
	@NextMenuController
	public String toChrocdpDtoListPage(HttpServletRequest request){
		ChrocdpDtoDao dao = new ChrocdpDtoDao();
		int[] pageParams = initPageParams(request);
		PageObject<ChrocdpDto> pageObject = new PageObject<ChrocdpDto>(pageParams[0], pageParams[1]);
		pageObject = dao.findChrocdpDtoLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/chrocdpdto.do?method=toChrocdpDtoListPage");
		return "forward:WEB-INF/pages/chrocdpdto/chrocdpdto_list.jsp";
	}
	
	/**
	 * @Title: toEditChrocdpDtoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至慢性病证修改页面  
	 */
	@ControllerAnnotation("toEditChrocdpDtoPage")
	@NextMenuController
	public String toEditChrocdpDtoPage(HttpServletRequest request){
		String userName = request.getParameter("userName");
		request.setAttribute("userName", userName);
		String cpId = request.getParameter("cpId");
		Chrocdp chrocdp = new Chrocdp(cpId);
		LOGGER.info("chrocdp:" + chrocdp);
		request.setAttribute("editModle", chrocdp);
		request.setAttribute("editAction", "/sys/chrocdpdto.do?method=doEditChrocdpDto");
		return "forward:WEB-INF/pages/chrocdpdto/chrocdpdto_edit.jsp";
	}
	
	/**
	 * @Title: doEditChrocdpDto
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行慢性病证修改
	 */
	@ControllerAnnotation("doEditChrocdpDto")
	@NextMenuController
	public String doEditChrocdpDto(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Chrocdp chrocdp = GSON.fromJson(json, Chrocdp.class);
		LOGGER.info(chrocdp);
		try {
			boolean update = chrocdp.toUpdate();
			if(update){
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病证成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病证失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditChrocdpDtoPage(request);
	}
	
	/**
	 * @Title: doDeleteChrocdpDto
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除慢性病证
	 */
	@ControllerAnnotation("doDeleteChrocdpDto")
	public Map<String, Object> doDeleteChrocdpDto(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		Chrocdp chrocdp = new Chrocdp();
		chrocdp.setCpId(id);
		boolean del = chrocdp.delect2DB();
		if(del){
			map.put("code", 200);
			map.put("msg", "删除慢性病证成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除慢性病证失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddChrocdpDtoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加慢性病证页面 
	 */
	@ControllerAnnotation("toAddChrocdpDtoPage")
	@NextMenuController
	public String toAddChrocdpDtoPage(HttpServletRequest request){
		request.setAttribute("addModleAction", "/sys/chrocdpdto.do?method=doAddChrocdpDto");
		request.setAttribute("queryUserByAJAX", "/sys/FamilyInfo.do?method=queryByAjax");
		request.setAttribute("queryBinByAJAX", "/sys/chronicdis.do?method=queryBByAjax");
		return "forward:WEB-INF/pages/chrocdpdto/chrocdpdto_add.jsp";
	}
	
	/**
	 * @Title: doAddChrocdpDto  
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加慢性病证
	 */
	@ControllerAnnotation("doAddChrocdpDto")
	@NextMenuController
	public String doAddChrocdpDto(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		Chrocdp chrocdp = GSON.fromJson(json, Chrocdp.class);
		LOGGER.info(chrocdp);
		boolean add = chrocdp.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病证添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病证添加失败！');</script>");
		}
		return toAddChrocdpDtoPage(request);
	} 
	
}
