package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.payinfo.PayInfoDao;
import com.gxwzu.zhangheng.medical.domain.payinfo.PayInfo;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * 参合缴费配置信息表，控制器
 * @ClassName:  PayInfoServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年5月18日 上午11:55:56     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/payInfo.do")
public class PayInfoServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * @Title: toPayInfoList   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  参合缴费配置信息列表 
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toPayInfoList")
	@NextMenuController
	public String toPayInfoList(HttpServletRequest request){
		int[] pageParams = initPageParams(request);
		PageObject<PayInfo> pageObject = new PageObject<>(pageParams[0], pageParams[1]);
		PayInfoDao dao = new PayInfoDao();
		pageObject = dao.findPayLimit(pageObject);
		
		// 获取当前年费参合配置信息
		String year = DateUtils.formartYear(new Date());
		PayInfo payInfo = new PayInfo();
		payInfo = payInfo.findPayInfoByYear(year);
		LOGGER.info(pageObject);
		request.setAttribute("curentMSG", payInfo);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("limitPageAction", "/sys/payInfo.do?method=toPayInfoList");
		return "forward:WEB-INF/pages/payInfo/payInfo_list.jsp";
	}
	
	/**
	 * @Title: toAddPayInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至参合缴费配置信息添加界面
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toAddPayInfoPage")
	@NextMenuController
	public String toAddPayInfoPage(HttpServletRequest request){
		request.setAttribute("curentYear", DateUtils.formartYear(new Date()));
		request.setAttribute("curentDate", new Date());
		request.setAttribute("addAction", "/sys/payInfo.do?method=doAddPayInfoPage");
		return "forward:WEB-INF/pages/payInfo/payInfo_add.jsp";
	}
	
	/**
	 * @Title: doAddPayInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加  
	 * @param: @param request
	 * @param: @param parems
	 * @return: String      
	 */
	@ControllerAnnotation("doAddPayInfoPage")
	@NextMenuController
	public String doAddPayInfoPage(HttpServletRequest request, Map<String, Object> parems){
		String json = GSON.toJson(parems);
		LOGGER.info(json);
		PayInfo payInfo = GSON.fromJson(json, PayInfo.class);
		boolean add = payInfo.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加失败！');</script>");
		}
		return toAddPayInfoPage(request);
	}
	
	/**
	 * @Title: toEditPayInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至参合缴费配置信息修改界面
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toEditPayInfoPage")
	@NextMenuController
	public String toEditPayInfoPage(HttpServletRequest request){
		String payInfoId = request.getParameter("id");
		Long id = Long.valueOf(payInfoId);
		PayInfo payInfo = new PayInfo(id);
		request.setAttribute("editModel", payInfo);
		request.setAttribute("editAction", "/sys/payInfo.do?method=doEditPayInfoPage");
		return "forward:WEB-INF/pages/payInfo/payInfo_edit.jsp";
	}
	
	/**
	 * @Title: doEditPayInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行参合缴费配置信息修改
	 * @param: @param request
	 * @param: @param params
	 * @return: String      
	 */
	@ControllerAnnotation("doEditPayInfoPage")
	@NextMenuController
	public String doEditPayInfoPage(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		PayInfo payInfo = GSON.fromJson(json, PayInfo.class);
		boolean update = payInfo.update();
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改成功！');</script>");
		}else{
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改失败！');</script>");
		}
		return toEditPayInfoPage(request);
	}
	
	/**
	 * @Title: doDeletePayInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行删除  
	 * @param: @param request
	 * @return: Map<String,Object>      
	 */
	@ControllerAnnotation("doDeletePayInfo")
	public Map<String, Object> doDeletePayInfo(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
 		String ids = request.getParameter("id");
 		PayInfo payInfo = new PayInfo();
 		Long id = Long.valueOf(ids);
 		payInfo.setId(id);
 		boolean del = payInfo.del();
 		if(del){
 			map.put("code", 200);
 			map.put("msg", "删除成功");
 		}else {
 			map.put("code", 500);
 			map.put("msg", "删除失败");
		}
		return map;
	}
	
}
