package com.gxwzu.zhangheng.medical.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;

/**
 * @Description:TODO medical 站点servlet控制分发器
 * @author zhangheng
 * @date 2019-02-26 17:17:00
 */
public abstract class BaseServletController extends HttpServlet implements INextMenu {

	private static final long serialVersionUID = 1L;
	
	protected static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * Gson解析
	 */
	protected static final Gson GSON = new GsonBuilder().setDateFormat(DATE_FORMAT).serializeNulls().create();
	
	/**
	 * Log4j
	 */
	protected static final Logger LOGGER = LogManager.getLogger(BaseServletController.class);
	
	/**
	 * 默认分页大小
	 */
	protected int pageSize = 10;
	
	/**
	 * 默认分页起始页
	 */
	protected int startPage = 1;
	
	/**
	 * medical站点统一分发请求控制方法
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("客户端请求地址：" + request.getRemoteAddr());
		String method = request.getParameter("method").trim();
		if(method == null || method.isEmpty()){
			method = "execute";
		}
		LOGGER.info("客户端请求方法：" + method);
		Class<? extends BaseServletController> c = this.getClass();
		try {
			//获取请求的参数
			Map<String, String[]> parameterMap = request.getParameterMap();
			Method[] methods = c.getMethods();
			boolean methodFlag = false;
			for(Method md : methods){
				//String mdName = md.getName();
				ControllerAnnotation annotation = md.getDeclaredAnnotation(ControllerAnnotation.class);
				if(annotation.value().equals(method)){
					Class<?>[] parameterTypes = md.getParameterTypes();
					Object[] paramValue = new Object[parameterTypes.length];
					methodParam(request, response, parameterMap, parameterTypes, paramValue);//反射servlet方法参数处理
					
					// 方法响应之前，获取当前操作的菜单下的子菜单
					hasNext(request, response, md);
					
					methodResponse(request, response, md, paramValue);//反射servlet处理后的响应
					methodFlag = true;
					break;
				}
			}
			if(!methodFlag){
				request.getRequestDispatcher("/pages.do?method=to403").forward(request, response);
			}
		} catch (Exception e) {
			request.getRequestDispatcher("/pages.do?method=to500").forward(request, response);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void hasNext(HttpServletRequest request, HttpServletResponse response, Method method) {
		NextMenuController annotation = method.getDeclaredAnnotation(NextMenuController.class);
		if(annotation != null){
			LOGGER.info("获取下一节菜单节点：NextMenuController ： " + annotation);
			String tgId = request.getParameter("tgId");
			long id = tgId != null ? Long.valueOf(tgId) : 0L;
			String tgName = request.getParameter("tgName");
			List<Menu> sessionMenu = (List<Menu>)request.getSession().getAttribute("SESSION_MENU");
			List<Menu> actionMenu = new ArrayList<Menu>();//下级菜单
			for(Menu menu : sessionMenu){
				if(menu.getParentId().longValue() == id){
					actionMenu.add(menu);
				}
			}
			request.setAttribute("tgId", tgId);
			request.setAttribute("tgName", tgName);
			request.setAttribute("actionMenu", actionMenu);
		}
	}

	/**   
	 * @Title: initPageParams   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   处理页面分页数据
	 * @return: void      
	 */
	protected int[] initPageParams(HttpServletRequest request) throws NumberFormatException {
		String spage = request.getParameter("startPage");//分页起始页
		String pSize = request.getParameter("pageSize");//分页每页大小
		int stPage = startPage;
		int pgSize = pageSize;
		if(spage != null && !spage.isEmpty()){
			stPage = Integer.valueOf(spage);
		}
		if(pSize != null && !pSize.isEmpty()){
			pgSize = Integer.valueOf(pSize);
		}
		return new int[]{stPage, pgSize};
	}

	/**
	 * 处理反射servlet方法响应
	 * @param request
	 * @param response
	 * @param md
	 * @param paramValue
	 */
	private void methodResponse(HttpServletRequest request, HttpServletResponse response, Method md,
			Object[] paramValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			ServletException, IOException {
		Type type = md.getGenericReturnType();
		LOGGER.info("type:" + type);
		LOGGER.info("==:" + type.getTypeName());
		String classType = type.getTypeName();
		Object obj = md.invoke(this, paramValue);
		String result = null;
		if("java.lang.String".equals(classType)){
			result = (String) obj;
			if(result != null && !result.isEmpty()){
				result = result.replaceAll("/+", "/");
				if(result.startsWith("forward:")){
					LOGGER.info("result.startsWith(\"forward:\") ==> result:" + result);
					result = result.replace("forward:", "");
					int indexOf = result.indexOf("WEB-INF");
					if(indexOf != -1){
						result = "/" + result.substring(indexOf);
						result.replaceAll("/+", "/");
					}
					LOGGER.info("result.startsWith(\"forward:\") ==> result:" + result);
					request.getRequestDispatcher(result).forward(request, response);
					return;
				}
				if(result.startsWith("redirect:")){
					LOGGER.info("result.startsWith(\"redirect:\") ==> result:" + result);
					result = result.replace("redirect:", "");
					response.sendRedirect(result);
					return;
				}
				LOGGER.info("defult forward: ==> result:" +result);
				request.getRequestDispatcher(result).forward(request, response);
				return;
			}
		}else{
			String json = GSON.toJson(obj);
			PrintWriter writer = response.getWriter();
			writer.print(json);
			writer.flush();
			writer.close();
		}
	}

	/**
	 * 处理反射servlet匹配方法
	 * @param request
	 * @param response
	 * @param parameterMap
	 * @param parameterTypes
	 * @param paramValue
	 */
	private void methodParam(HttpServletRequest request, HttpServletResponse response,
			Map<String, String[]> parameterMap, Class<?>[] parameterTypes, Object[] paramValue) {
		for(int i=0; i< parameterTypes.length; i++){
			//根据参数名称，做某些处理
			Class<?> parameterType = parameterTypes[i];
			LOGGER.info("parameterType:" + parameterType);
			if(parameterType == HttpServletRequest.class){
				paramValue[i] = request;
				continue;
			}else if(parameterType == HttpServletResponse.class){
				paramValue[i] = response;
				continue;
			}else{
				Map<String, Object> result = new HashMap<>();
				Iterator<String> iterator = parameterMap.keySet().iterator();
				while (iterator.hasNext()) {
					String key = iterator.next().trim();
					if("method".equals(key)){
						continue;
					}
					LOGGER.info("map.get(key):" + parameterMap.get(key) + "====" + parameterMap.get(key).length);
					if(parameterMap.get(key).length == 1){
						result.put(key, parameterMap.get(key)[0].trim());
						LOGGER.info("key:" + key + ", map[key]: " + parameterMap.get(key)[0].trim() );
					}else{
						List<Object> list = new ArrayList<Object>();
						for(int j=0; j<parameterMap.get(key).length; j++){				
							LOGGER.info("key:" + key + ", map[key]: " + parameterMap.get(key)[j].trim() );
							list.add(parameterMap.get(key)[j].trim());
						}
						result.put(key, list);
					}
				}
				LOGGER.info(result);
				paramValue[i] = result;
			}
		}
	}
	
	/**
	 * http请求参数处理方法
	 * @param request
	 * @param response
	 * @param classzz
	 * @return Object
	 */
	public Object parameterToObject(HttpServletRequest request, HttpServletResponse response, Class<?> classzz){
		try {
			Map<String, String[]> map = request.getParameterMap();
			Map<String, Object> map2 = new HashMap<>();
			Iterator<String> iterator = map.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				LOGGER.info("map.get(key):" + map.get(key) + "====" + map.get(key).length);
				if(map.get(key).length == 1){
					map2.put(key, map.get(key)[0]);
					LOGGER.info("key:" + key + ", map[key]: " + map.get(key)[0] );
				}else{
					List<Object> objs = new ArrayList<Object>();
					for(int i=0; i<map.get(key).length; i++){				
						LOGGER.info("key:" + key + ", map[key]: " + map.get(key)[i] );
						objs.add(map.get(key)[i]);
					}
					map2.put(key, objs);
				}
			}
			String json = GSON.toJson(map2);
			LOGGER.info("String json = gson.toJson(map2): ==> " + json);
			Object fromJson = GSON.fromJson(json, classzz);
			return fromJson;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
