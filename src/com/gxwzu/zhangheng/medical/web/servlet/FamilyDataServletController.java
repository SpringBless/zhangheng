package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.familydata.FamilyInfoDataDao;
import com.gxwzu.zhangheng.medical.domain.familydata.FamilyData;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyInfoDataServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭人员资源管理控制器
 * @author: zhangheng
 * @date:   2019年4月16日 下午12:52:06     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/FamilyData.do")
public class FamilyDataServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toFamilyInfoDataListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至家庭人员资源管理列表
	 */
	@ControllerAnnotation("toFamilyDataListPage")
	@NextMenuController
	public String toFamilyDataListPage(HttpServletRequest request){
		FamilyInfoDataDao fiDao = new FamilyInfoDataDao();
		int[] pageParams = initPageParams(request);
		PageObject<FamilyData> pageObject = new PageObject<FamilyData>(pageParams[0], pageParams[1]);
		pageObject = fiDao.findFamilyDataLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/FamilyData.do?method=toFamilyDataListPage");
		return "forward:WEB-INF/pages/familydata/familydata_list.jsp";
	}
	
	/**
	 * @Title: doDeleteFamilyData   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除家庭人员资源
	 */
	@ControllerAnnotation("doDeleteFamilyData")
	public Map<String, Object> doDeleteFamilyData(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		FamilyData fd = new FamilyData();
		fd.setId(Integer.valueOf(id));
		boolean del = fd.del();
		if(del){
			map.put("code", 200);
			map.put("msg", "删除家庭人员资源成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除家庭人员资源失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddFamilyInfoDataPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加家庭人员资源页面 
	 */
	@ControllerAnnotation("toAddFamilyDataPage")
	@NextMenuController
	public String toAddFamilyDataPage(HttpServletRequest request){
		request.setAttribute("addAction", "/sys/FamilyData.do?method=doAddFamilyData");
		return "forward:WEB-INF/pages/familydata/familydata_add.jsp";
	}
	
	/**
	 * @Title: doAddFamilyInfoData  
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加家庭人员资源
	 */
	@ControllerAnnotation("doAddFamilyData")
	@NextMenuController
	public String doAddFamilyData(HttpServletRequest request){
		String column = request.getParameter("column");
		String value = request.getParameter("value");
		FamilyData familyInfoData = new FamilyData();
		boolean add = familyInfoData.addColumn(column, value);
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('家庭人员资源添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('家庭人员资源添加失败！');</script>");
		}
		return toAddFamilyDataPage(request);
	} 
	
}
