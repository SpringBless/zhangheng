package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.familyinfo.FamilyInfoDao;
import com.gxwzu.zhangheng.medical.domain.familydata.FamilyData;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfo;
import com.gxwzu.zhangheng.medical.domain.familyrchives.FamilyRchives;
import com.gxwzu.zhangheng.medical.domain.user.User;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyInfoServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭成员信息管理控制器
 * @author: zhangheng
 * @date:   2019年4月16日 下午12:52:06     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/FamilyInfo.do")
public class FamilyInfoServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toFamilyInfoDataListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至家庭人员管理列表
	 */
	@ControllerAnnotation("toFamilyInfoListPage")
	@NextMenuController
	public String toFamilyInfoListPage(HttpServletRequest request){
		String familyCode = request.getParameter("familyCode");
		String keyword = request.getParameter("keywords");
		int[] pageParams = initPageParams(request);
		PageObject<FamilyInfo> pageObject = new PageObject<FamilyInfo>(pageParams[0], pageParams[1]);
		FamilyInfoDao fiDao = new FamilyInfoDao();
		
		if(keyword != null && !"".equals(keyword)){
			pageObject = fiDao.findFamilyInfoLimitLike(pageObject, keyword, familyCode);
		}else {
			if(familyCode != null && !"".equals(familyCode)){				
				pageObject = fiDao.findFamilyInfoLimit(pageObject, familyCode);
			}else {
				pageObject = fiDao.findFamilyInfoLimit(pageObject);
			}
		}
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/FamilyInfo.do?method=toFamilyInfoListPage");
		return "forward:WEB-INF/pages/familyinfo/familyinfo_list.jsp";
	}
	
	/**
	 * @Title: cardCodeInDB   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   检验数据库中是否存在相同的身份证号
	 * @param: @param request
	 * @return: Map<String,Object>      
	 */
	@ControllerAnnotation("cardCodeInDB")
	public Map<String, Object> cardCodeInDB(HttpServletRequest request){
		String cardCode = request.getParameter("cardCode");
		Map<String, Object> map = new HashMap<String, Object>();
		FamilyInfo info = new FamilyInfo();
		info.setCardCode(cardCode);
		boolean flag = info.cardCodeInDB();
		map.put("flag", flag);
		return map;
	} 
	
	/**
	 * @Title: toEditFamilyInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至家庭成员信息修改页面
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toEditFamilyInfoPage")
	@NextMenuController
	public String toEditFamilyInfoPage(HttpServletRequest request){
		String cardCode = request.getParameter("cardCode");
		FamilyInfo familyInfo = new FamilyInfo(cardCode);
		request.setAttribute("model", familyInfo);
		request.setAttribute("initCodeURL", "/sys/FamilyInfo.do?method=getColumnValue");
		request.setAttribute("cardCodeAction", "/sys/FamilyInfo.do?method=cardCodeInDB");
		request.setAttribute("editAction", "/sys/FamilyInfo.do?method=doEditFamilyInfo");
		return "forward:WEB-INF/pages/familyinfo/familyinfo_edit.jsp";
	}
	
	/**
	 * @Title: doEditFamilyInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  执行家庭成员信息修改 
	 * @param: @param request
	 * @param: @param params
	 */
	@ControllerAnnotation("doEditFamilyInfo")
	@NextMenuController
	public String doEditFamilyInfo(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		FamilyInfo info = GSON.fromJson(json, FamilyInfo.class);
		String oldHk = (String)params.get("oldHk");
		oldHk = oldHk.trim();
		boolean update = info.update();
		if(update){
			FamilyRchives familyRchives = new FamilyRchives(info.getFamilyCode());
			if("0".equals(familyRchives) && "是".equals(info.getHkType())){
				familyRchives.setAgricultural(familyRchives.getAgricultural() + 1);
				boolean b = familyRchives.updateAgriculturalByFamilyCode();
				if(b){					
					request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案修改成功！');</script>");
				}else {					
					request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案修改失败！');</script>");
				}
			}else {				
				request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案修改失败！');</script>");
			}
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案修改失败！');</script>");
		}
		return toFamilyInfoListPage(request);
	}
	
	/**
	 * @Title: doDeleteFamilyInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除家庭成员档案
	 */
	@ControllerAnnotation("doDeleteFamilyInfo")
	public Map<String, Object> doDeleteFamilyInfo(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		FamilyInfo fi = new FamilyInfo(id);
		//fi.setCardCode(id);
		boolean del = fi.del();
		if(del){
			FamilyRchives familyRchives = new FamilyRchives(fi.getFamilyCode());
			familyRchives.setFamilyNumber(familyRchives.getFamilyNumber() -1);
			boolean b = familyRchives.updateFamilyNumberByFamilyCode();
			if(b && "是".equals(fi.getHkType())){
				familyRchives.setAgricultural(familyRchives.getAgricultural() - 1);
				boolean code = familyRchives.updateAgriculturalByFamilyCode();
				if (code) {
					map.put("code", 200);
					map.put("msg", "删除家庭成员档案成功！");
				}else {
					map.put("code", 500);
					map.put("msg", "删除家庭成员档案失败！");
				}
			}else {
				map.put("code", 500);
				map.put("msg", "删除家庭成员档案失败！");
			}
		}else {
			map.put("code", 500);
			map.put("msg", "删除家庭成员档案失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddFamilyInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加家庭成员页面 
	 */
	@ControllerAnnotation("toAddFamilyInfoPage")
	@NextMenuController
	public String toAddFamilyInfoPage(HttpServletRequest request){
		String familyCode = request.getParameter("familyCode");
		FamilyRchives fRchives = new FamilyRchives(familyCode);
		request.setAttribute("family", fRchives);
		request.setAttribute("familyCode", familyCode);
		request.setAttribute("initCodeURL", "/sys/FamilyInfo.do?method=getColumnValue");
		request.setAttribute("cardCodeAction", "/sys/FamilyInfo.do?method=cardCodeInDB");
		request.setAttribute("addAction", "/sys/FamilyInfo.do?method=doAddFamilyInfo");
		return "forward:WEB-INF/pages/familyinfo/familyinfo_add.jsp";
	}
	
	/**
	 * @Title: getColumnValue   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 获取资源信息  
	 * @param: @param request
	 * @return: List<String>      
	 */
	@ControllerAnnotation("getColumnValue")
	public List<String> getColumnValue(HttpServletRequest request){
		String column = request.getParameter("column");
		FamilyData fd = new FamilyData();
		List<String> list = fd.findColumnList(column.trim());
		return list;
	}
	
	/**
	 * @Title: doAddFamilyInfo
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加家庭成员档案
	 */
	@ControllerAnnotation("doAddFamilyInfo")
	@NextMenuController
	public String doAddFamilyInfo(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		FamilyInfo fi = GSON.fromJson(json, FamilyInfo.class);
		fi.setBirthday(DateUtils.cardCode2Date(fi.getCardCode()));
		boolean add = fi.add();
		if(add){
			FamilyRchives familyRchives = new FamilyRchives(fi.getFamilyCode());
			familyRchives.setFamilyNumber(familyRchives.getFamilyNumber() + 1);
			boolean b = familyRchives.updateFamilyNumberByFamilyCode();
			if(b && "是".equals(fi.getHkType())){
				familyRchives.setAgricultural(familyRchives.getAgricultural() + 1);
				boolean c = familyRchives.updateAgriculturalByFamilyCode();
				if(c){					
					request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案添加成功！');</script>");
				}else {
					request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案添加失败！');</script>");					
				}
			}else {				
				request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案添加成功！');</script>");
			}
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('家庭成员档案添加失败！');</script>");
		}
		return toAddFamilyInfoPage(request);
	} 
	
	/**
	 * @Title: queryByAjax   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  查询个人信息   
	 * @param: @param request
	 * @return: List<FamilyInfo>      
	 */
	@ControllerAnnotation("queryByAjax")
	public List<FamilyInfo> queryByAjax(HttpServletRequest request){
		User user = (User)request.getSession().getAttribute("SESSION_USER");
		String areaCode = user.getOfficeId() + "";
		String name = request.getParameter("name");
		System.out.println("name:" + name);
		FamilyInfoDao dao = new FamilyInfoDao();
		List<FamilyInfo> list = dao.queryByAjax(areaCode, name);
		return list != null ? list : new ArrayList<FamilyInfo>();
	}
	
}
