package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.area.AreaDao;
import com.gxwzu.zhangheng.medical.dao.medical.MedicalDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.domain.medical.Medical;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  MedicalServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   医疗卫生机构控制器
 * @author: zhangheng
 * @date:   2019年4月14日 下午7:39:15     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/smedi.do")
public class MedicalServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toMedicalListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开医疗卫生机构列表
	 */
	@ControllerAnnotation("toMedicalListPage")
	@NextMenuController
	public String toMedicalListPage(HttpServletRequest request){
		MedicalDao medicalDao = new MedicalDao();
		int[] pageParams = initPageParams(request);
		PageObject<Medical> pageObject = new PageObject<Medical>(pageParams[0], pageParams[1]);
		pageObject = medicalDao.findMedicalLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/smedi.do?method=toMedicalListPage");
		return "forward:WEB-INF/pages/smedi/smedi_list.jsp";
	}
	
	/**
	 * @Title: toEditMedicalPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至医疗卫生机构信息修改页面  
	 */
	@ControllerAnnotation("toEditMedicalPage")
	@NextMenuController
	public String toEditMedicalPage(HttpServletRequest request){
		String mcode = request.getParameter("mcode");
		Medical medical = new Medical(mcode);
		request.setAttribute("editModel", medical);
		request.setAttribute("editAction", "/sys/smedi.do?method=doEditMedical");
		return "forward:WEB-INF/pages/smedi/smedi_edit.jsp";
	}
	
	/**
	 * @Title: doEditMedical   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行医疗卫生机构信息修改
	 */
	@ControllerAnnotation("doEditMedical")
	public String doEditMedical(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Medical medical = GSON.fromJson(json, Medical.class);
		LOGGER.info(medical);
		boolean update = medical.update();
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('医疗卫生机构修改成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('医疗卫生机构修改失败！');</script>");
		}
		return toEditMedicalPage(request);
	}
	
	/**
	 * @Title: doDeleteMedical   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除行政区域
	 */
	@ControllerAnnotation("doDeleteMedical")
	public Map<String, Object> doDeleteMedical(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		Medical medical = new Medical();
		boolean del = medical.del(id);
		if(del){
			map.put("code", 200);
			map.put("msg", "删除医疗卫生机构成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除医疗卫生机构失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddMedicalPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加医疗卫生机构页面 
	 */
	@ControllerAnnotation("toAddMedicalPage")
	@NextMenuController
	public String toAddMedicalPage(HttpServletRequest request){
		AreaDao areaDao = new AreaDao();
		List<Area> areaAll = areaDao.findAreaAll();
		request.setAttribute("areaAll", areaAll);
		request.setAttribute("addAction", "/sys/smedi.do?method=doAddMedical");
		return "forward:WEB-INF/pages/smedi/smedi_add.jsp";
	}
	
	/**
	 * @Title: doAddMedical   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加医疗卫生机构
	 */
	@ControllerAnnotation("doAddMedical")
	@NextMenuController
	public String doAddMedical(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		Medical medical = GSON.fromJson(json, Medical.class);
		boolean add = medical.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加医疗卫生机构成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加添加医疗卫生机构失败！');</script>");
		}
		return toAddMedicalPage(request);
	}
	
}
