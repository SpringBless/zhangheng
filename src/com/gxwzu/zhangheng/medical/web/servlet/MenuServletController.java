package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.menu.MenuDao;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;
import com.gxwzu.zhangheng.medical.domain.menu.MenuDto;
import com.gxwzu.zhangheng.medical.domain.menu.MenuTree;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  MenuServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   菜单控制类
 * @author: zhangheng
 * @date:   2019年3月28日 下午3:52:00     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/menu.do")
public class MenuServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toMenuListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开菜单列表
	 */
	@ControllerAnnotation("toMenuListPage")
	@NextMenuController
	public String toMenuListPage(HttpServletRequest request){
		MenuDao menuDao = new MenuDao();
		int[] pageParams = initPageParams(request);
		PageObject<MenuDto> pageObject = new PageObject<MenuDto>(pageParams[0], pageParams[1]);
		pageObject = menuDao.findMenusLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/menu.do?method=toMenuListPage");
		return "forward:WEB-INF/pages/menu/menu_list.jsp";
	}
	
	/**
	 * @Title: toMenuAddPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   菜单添加界面
	 */
	@ControllerAnnotation("toMenuAddPage")
	@NextMenuController
	public String toMenuAddPage(HttpServletRequest request){
		MenuTree tree = new MenuTree(true);//当前所有权限树
		Menu menu = new Menu();
		List<Menu> menuList = menu.findMenuAll();
		List<Menu> list = menu.getChildren(menuList, 0L);
		request.setAttribute("menuList", list);
		request.setAttribute("menuTree", JSON.toJSONString(tree.getNodeList()));
		request.setAttribute("addMenuAction", "/sys/menu.do?method=doAddMenu");
		return "forward:WEB-INF/pages/menu/menu_add.jsp";
	}
	
	/**
	 * @Title: doAddMenu   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   添加菜单（权限）
	 */
	@ControllerAnnotation("doAddMenu")
	@NextMenuController
	public String doAddMenu(HttpServletRequest request, HttpServletResponse response, Map<String, Object> params){
		String json = GSON.toJson(params);
		Menu addMenu = GSON.fromJson(json, Menu.class);
		LOGGER.info(addMenu);
		boolean save = addMenu.saveMenu(addMenu);
		if(save){
			request.setAttribute("flag", "<script type='text/javascript'>alert('菜单添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('菜单添加失败！');</script>");
		}
		return toMenuAddPage(request);
	}
	
	/**
	 * @Title: doDelMenu   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  异步删除权限菜单 
	 */
	@ControllerAnnotation("doDelMenu")
	public Map<String, Object> doDelMenu(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
		String id = request.getParameter("id");
		Long menuId = Long.valueOf(id);
		Menu delMenu = new Menu();
		boolean del = delMenu.delMenu(menuId);
		if(del){
			map.put("code", 200);
			map.put("msg", "删除菜单成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除菜单失败！，程序内部错误！");
		}
		return map;
	}
	
	/**
	 * @Title: toMenuEditPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至菜单修改界面  
	 */
	@ControllerAnnotation("toMenuEditPage")
	@NextMenuController
	public String toMenuEditPage(HttpServletRequest request){
		Long id = Long.valueOf(request.getParameter("id"));
		Menu dbMenu = new Menu(id);
		request.setAttribute("editMenu", dbMenu);
		MenuTree tree = new MenuTree(true);//当前所有权限树
		Menu menu = new Menu();
		List<Menu> menuList = menu.findMenuAll();
		List<Menu> list = menu.getChildren(menuList, 0L);
		request.setAttribute("menuList", list);
		request.setAttribute("menuTree", JSON.toJSONString(tree.getNodeList()));
		request.setAttribute("editAction", "/sys/menu.do?method=doMenuEdit");
		return "forward:WEB-INF/pages/menu/menu_edit.jsp";
	}
	
	/**
	 * @Title: doMenuEdit   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行权限菜单修改  
	 */
	@ControllerAnnotation("doMenuEdit")
	@NextMenuController
	public String doMenuEdit(HttpServletRequest request, HttpServletResponse response, Map<String, Object> params){
		String json = GSON.toJson(params);
		Menu editMenu = GSON.fromJson(json, Menu.class);
		LOGGER.info(editMenu);
		boolean update = editMenu.updateMenu(editMenu);
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('菜单修改成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('菜单修改失败！');</script>");
		}
		return toMenuEditPage(request);
	}
	
	/**
	 * @Title: findMenuAll   测试用的
	 * @Description: TODO(这里用一句话描述这个方法的作用)  获取所有菜单，并排列 
	 * @return: List<Menu>      
	 */
	@ControllerAnnotation("findMenuAll")
	@NextMenuController
	public String findMenuAll(HttpServletRequest request){
		Menu menu = new Menu();
		List<Menu> menuAll = menu.findMenuAll();
		List<Menu> all = menu.getChildren(menuAll, 0L);
		request.setAttribute("menu", all);
		return "forward:WEB-INF/pages/test.jsp";
	}

}
