package com.gxwzu.zhangheng.medical.web.servlet;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 规划获取当前菜单的子菜单
 */
public interface INextMenu {

	void hasNext(HttpServletRequest request, HttpServletResponse response, Method method);
	
}
