package com.gxwzu.zhangheng.medical.web.servlet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.area.AreaDao;
import com.gxwzu.zhangheng.medical.dao.familyinfo.FamilyInfoDao;
import com.gxwzu.zhangheng.medical.dao.familyrchives.FamilyRchivesDao;
import com.gxwzu.zhangheng.medical.dao.pay.PayDtoDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.domain.chrocdp.Chrocdp;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfo;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfoDto;
import com.gxwzu.zhangheng.medical.domain.familyrchives.FamilyRchives;
import com.gxwzu.zhangheng.medical.domain.pay.PayDto;
import com.gxwzu.zhangheng.medical.domain.pay.PrintPay;
import com.gxwzu.zhangheng.medical.domain.payinfo.PayInfo;
import com.gxwzu.zhangheng.medical.domain.user.User;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.ExcelWriteUtils;
import com.gxwzu.zhangheng.medical.util.FaPiaoUtils;
import com.gxwzu.zhangheng.medical.util.PDFUtilsPrintFP;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  PayDtoServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   参合交费管理控制器
 * @author: zhangheng
 * @date:   2019年4月16日 下午12:52:06     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/payDto.do")
public class PayDtoServletController extends BaseServletController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * toPayDtoListPage
	 * 参合交费信息列表
	 * @param request
	 */
	@ControllerAnnotation("toPayDtoListPage")
	@NextMenuController
	public String toPayDtoListPage(HttpServletRequest request){
		int[] pageParams = initPageParams(request);
		PageObject<PayDto> pageObject = new PageObject<PayDto>(pageParams[0], pageParams[1]);
		PayDtoDao payDtoDto = new PayDtoDao();
		String keywords = request.getParameter("keywords");
		String years = request.getParameter("year");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String areaCode = request.getParameter("areacode");
		if(startDate != null && !"".equals(startDate) && endDate != null && !"".equals(endDate)){
			pageObject = payDtoDto.findPayDtoDateAndLikeLimit(pageObject, startDate, endDate);
		}else if (areaCode != null && !"".equals(areaCode)) {
			pageObject = payDtoDto.findPayDtoAreaCodeAndLikeLimit(pageObject,areaCode);
		}else if (years != null && !"".equals(years)) {
			pageObject = payDtoDto.findPayDtoYearAndLikeLimit(pageObject, years);
		}else if( keywords != null && !"".equals(keywords)){
			pageObject = payDtoDto.findPayDtoKeywordsAndLikeLimit(pageObject,keywords);
		}else {			
			pageObject = payDtoDto.findPayDtoLimit(pageObject);
		}
		List<String> allYears = payDtoDto.findAllYears();
		String year = String.valueOf(DateUtils.getYearByUtilDate());
		AreaDao areaDao = new AreaDao();
		List<Area> areaAll = areaDao.findAreaAll();
		
		//处理null
		areaCode = areaCode == null || "".equals(areaCode) ? "" : areaCode;
		years = years == null || "".equals(years) ? "" : years;
		startDate = startDate == null || "".equals(startDate) ? "" : startDate;
		endDate = endDate == null || "".equals(endDate) ? "" : endDate;
		keywords = keywords == null || "".equals(keywords) ? "" : keywords;
		
		String pageInfos = "&areacode="+areaCode+"&year="+years+"&startDate="+startDate+"&endDate="+endDate+"&keywords="+keywords;
		String curentSearch = pageInfos + "&startPage="+pageObject.getPageNo()+"&pageSize="+pageObject.getPageSize();
		request.setAttribute("pageInfos", pageInfos);
		request.setAttribute("curentSearch", curentSearch);
		request.setAttribute("areaAll", areaAll);
		request.setAttribute("curentYear", year);
		request.setAttribute("allYears", allYears);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/payDto.do?method=toPayDtoListPage");
		return "forward:WEB-INF/pages/paydto/paydto_list.jsp";
	}
	
	/**
	 * @Title: downLoadPayExcel   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  下载excel 
	 * @param: @param request
	 * @param: @param response
	 * @return: String      
	 */
	@ControllerAnnotation("downLoadPayExcel")
	public String downLoadPayExcel(HttpServletRequest request, HttpServletResponse response){
		String keywords = request.getParameter("keywords");
		String years = request.getParameter("year");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String areaCode = request.getParameter("areacode");
		int[] pageParams = initPageParams(request);
		PageObject<PayDto> pageObject = new PageObject<PayDto>(pageParams[0], pageParams[1]);
		PayDtoDao payDtoDto = new PayDtoDao();
		if(startDate != null && !"".equals(startDate) && endDate != null && !"".equals(endDate)){
			pageObject = payDtoDto.findPayDtoDateAndLikeLimit(pageObject, startDate, endDate);
		}else if (areaCode != null && !"".equals(areaCode)) {
			pageObject = payDtoDto.findPayDtoAreaCodeAndLikeLimit(pageObject,areaCode);
		}else if( keywords != null && !"".equals(keywords)){
			pageObject = payDtoDto.findPayDtoKeywordsAndLikeLimit(pageObject,keywords);
		}else if (years != null && !"".equals(years)) {
			pageObject = payDtoDto.findPayDtoYearAndLikeLimit(pageObject, years);
		}else {			
			pageObject = payDtoDto.findPayDtoLimit(pageObject);
		}
		ExcelWriteUtils.outExcel("参合缴费名单"+DateUtils.getyyyy_MM_dd_HH_mm_ss1(), pageObject, response);
		return null;
	}
	
	/**
	 * 转发至添加参合农民交费
	 * @param request
	 */
	@ControllerAnnotation("toAddPayDtoPage")
	@NextMenuController
	public String toAddPayDtoPage(HttpServletRequest request){
		String cyear = request.getParameter("curentYear");
		// 步骤
		/**需求：输入户主姓名，查找这一户所有家庭成员，选择要参合的人员，完成参合登记并打印缴费凭证单
		 * 获取家庭档案列表信息
		 */
		String keywords = request.getParameter("keywords");
		String areaCode = request.getParameter("areaCode");
		FamilyRchivesDao dao = new FamilyRchivesDao();
		int[] pageParams = initPageParams(request);
		PageObject<FamilyRchives> pageObject = new PageObject<FamilyRchives>(pageParams[0], pageParams[1]);
		if((keywords != null && !"".equals(keywords)) || (areaCode != null && !"".equals(areaCode))){
			// 关键字检索
			pageObject = dao.findFamilyRchivesLikeLimit(pageObject, keywords, areaCode);
		}else {
			// 默认检索
			pageObject = dao.findFamilyRchivesLimit(pageObject);
		}
		//当前年份
		if(cyear == null || "".equals(cyear)){
			cyear = String.valueOf(DateUtils.getYearByUtilDate());
		}
		AreaDao areaDao = new AreaDao();
		List<Area> areaAll = areaDao.findAreaAll();
		
		PayInfo payInfo = new PayInfo();
		payInfo = payInfo.findPayInfoByYear(cyear);
		
		request.setAttribute("curentTimeLong", System.currentTimeMillis());
		request.setAttribute("payInfo", payInfo);
		request.setAttribute("addPayInfoHref", "/sys/payInfo.do?method=toAddPayInfoPage");
		// 判空
		keywords = keywords  == null || "".equals(keywords) ? "" : keywords;
		areaCode = areaCode == null || "".equals(areaCode) ? "" : areaCode;
		request.setAttribute("areaAll", areaAll);
		request.setAttribute("keywords", keywords);
		request.setAttribute("areaCode", areaCode);
		request.setAttribute("curentYear", cyear);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("keywords", keywords);
		// 打印发票地址
		request.setAttribute("printURL", "/sys/payDto.do?method=downLoadFP");
		request.setAttribute("limitPageAction", "/sys/payDto.do?method=toAddPayDtoPage");
		request.setAttribute("ajaxAction", "/sys/payDto.do?method=findFamilyInfos");
		request.setAttribute("doAddAction", "/sys/payDto.do?method=doAddPayDto");
		request.setAttribute("downLoadFP", "/sys/payDto.do?method=downLoadFP");
		return "forward:WEB-INF/pages/paydto/paydto_add.jsp";
	}
	
	/**
	 * 获取已经参合的家庭成员和未参合的家庭成员
	 * @Title: findFamilyInfos   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param request
	 * @return: Map<String,Object>      
	 */
	@ControllerAnnotation("findFamilyInfos")
	public Map<String, Object> findFamilyInfos(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String familyCode = request.getParameter("familyCode");
		FamilyInfoDao dao = new FamilyInfoDao();
		List<FamilyInfoDto> list = dao.getFamilyInfoByFamilyCode(familyCode);
		map.put("all", list);
		int year = DateUtils.getYearByUtilDate();
		PayDtoDao dtoDao = new PayDtoDao();
		List<String> curentPay = dtoDao.findCodeByCurentYear(year, familyCode);
		for(int i=0;i<list.size();i++){
			if(curentPay!= null && curentPay.size() > 0){
				for(int j=0;j<curentPay.size(); j++){
					if(list.get(i).getCardCode().equals(curentPay.get(j))){
						list.get(i).setPay(true);
					}
				}
			}
		}
		map.put("pay", curentPay);
		map.put("code", 200);
		return map;
	}
	
	/**
	 * 执行添加参合缴费
	 * @Title: doAddPayDto   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("doAddPayDto")
	@NextMenuController
	public String doAddPayDto(HttpServletRequest request,HttpServletResponse response, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);//{"familyCode":"4504210203040005","price":"90.00","cardCode":"450923199709084051,450923199909084051","tgId":"119","tgName":"参合交费管理","playYear":"2019"}
		String cardCode = (String)param.get("cardCode");
		FamilyInfoDao dao = new FamilyInfoDao();
		List<FamilyInfo> list = dao.getFamilyInfoInCardCode(cardCode);
		String fapiao = (String)request.getSession().getAttribute("SESSION_FAPIAO");
		if(fapiao == null || "".equals(fapiao)){			
			fapiao = FaPiaoUtils.getDefaultFapiao();// 获取发票编号
		}
		String price = (String)param.get("price");
		BigDecimal bigDecimal = new BigDecimal(price);
		String playYear = (String)param.get("playYear");
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		// 为打印发票做准备
		//PrintPay printPay = new PrintPay();
		//List<PayDto> payList = new ArrayList<PayDto>();
		
		for(int i=0;i<list.size();i++){
			FamilyInfo info = list.get(i);
			PayDto pay = new PayDto();
			pay.setUserId(sessionUser.getId());
			pay.setNhCode(info.getNhCode());
			pay.setFapiao(fapiao);
			pay.setFamilyCode(info.getFamilyCode());
			pay.setCardCode(info.getCardCode());
			pay.setPlayPrice(bigDecimal);
			pay.setPlayYear(playYear);
			pay.setPlayDate(new Date());
			pay.setName(info.getName());
			pay.add();
			//payList.add(pay);
		}
		request.setAttribute("flag", "<script type='text/javascript'>alert('添加参合缴费成功！ ');</script>");
		return toAddPayDtoPage(request);
	}
	
	/**
	 * 打印发票
	 * @Title: downLoadFP   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param request
	 * @param: @param response
	 * @return: List<Pay>      
	 */
	@ControllerAnnotation("downLoadFP")
	public String downLoadFP(HttpServletRequest request, HttpServletResponse response,Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);//{"familyCode":"4504210203040005","price":"90.00","cardCode":"450923199709084051,450923199909084051","tgId":"119","tgName":"参合交费管理","playYear":"2019"}
		String cardCode = (String)param.get("cardCode");
		FamilyInfoDao dao = new FamilyInfoDao();
		List<FamilyInfo> list = dao.getFamilyInfoInCardCode(cardCode);
		String fapiao = FaPiaoUtils.getDefaultFapiao();// 获取发票编号
		request.getSession().setAttribute("SESSION_FAPIAO", fapiao);
		String price = (String)param.get("price");
		BigDecimal bigDecimal = new BigDecimal(price);
		String playYear = (String)param.get("playYear");
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		// 为打印发票做准备
		PrintPay printPay = new PrintPay();
		List<PayDto> payList = new ArrayList<PayDto>();
		
		for(int i=0;i<list.size();i++){
			FamilyInfo info = list.get(i);
			PayDto pay = new PayDto();
			pay.setUserId(sessionUser.getId());
			pay.setNhCode(info.getNhCode());
			pay.setFapiao(fapiao);
			pay.setFamilyCode(info.getFamilyCode());
			pay.setCardCode(info.getCardCode());
			pay.setPlayPrice(bigDecimal);
			pay.setPlayYear(playYear);
			pay.setPlayDate(new Date());
			pay.setName(info.getName());
			//pay.add();
			payList.add(pay);
		}
		printPay.setData(payList);
		printPay.setTitle(new String[]{"广西梧州市苍梧县","参合发票",fapiao});
		printPay.setFapiao(fapiao);
		printPay.setcName("慢病报销");
		printPay.setAddress("广西梧州市苍梧县");
		printPay.setJbr(sessionUser.getName());
		printPay.setMobile(sessionUser.getPhone());
		printPay.setDate(DateUtils.formatDate(new Date()));
		PDFUtilsPrintFP.outPDF("参合发票" + DateUtils.getyyyy_MM_dd_HH_mm_ss1(), printPay, response);
		return null;
	}
	
	
	//********** 接口 ***********
	
	/**
	 * @Title: queryPayDtoByYearCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 检测当年缴费情况   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("queryPayDtoByYearCode")
	@NextMenuController
	public String queryPayDtoByYearCode(HttpServletRequest request){
		String cardCode = request.getParameter("cardCode");
		String year = DateUtils.formartYear(new java.util.Date());
		PayDtoDao dao = new PayDtoDao();
		PayDto payDto = dao.queryPayDtoByYearCode(year, cardCode);
		
		Chrocdp chrocdp = new Chrocdp();
		chrocdp = chrocdp.queryByCardCode(cardCode);
		
		request.setAttribute("chrocdp", chrocdp);
		request.setAttribute("payDto", payDto);
		request.setAttribute("cardCode", cardCode);
		request.setAttribute("addAction", "/sys/apply.do?method=checkDoAddApply");
		request.setAttribute("checkAction", "/sys/payDto.do?method=queryPayDtoByYearCode");
		return "forward:WEB-INF/pages/apply/apply_add.jsp";
	}
}
