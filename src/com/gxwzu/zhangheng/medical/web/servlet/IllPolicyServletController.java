package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.chronicdis.ChronicdisDao;
import com.gxwzu.zhangheng.medical.dao.illpolicy.IllPolicyDao;
import com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis;
import com.gxwzu.zhangheng.medical.domain.illpolicy.IllPolicy;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChronicdisServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢病政策管理控制器
 * @author: zhangheng
 * @date:   2019年4月14日 下午12:05:19     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/illpolicy.do")
public class IllPolicyServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * Log4j
	 */
	private static final Logger LOGGER = LogManager.getLogger(IllPolicyServletController.class);
	
	
	/**
	 * @Title: toIllpolicyListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开慢性病政策管理列表
	 */
	@ControllerAnnotation("toIllpolicyListPage")
	@NextMenuController
	public String toIllpolicyListPage(HttpServletRequest request){
		IllPolicyDao illPolicyDao = new IllPolicyDao();
		int[] pageParams = initPageParams(request);
		PageObject<IllPolicy> pageObject = new PageObject<IllPolicy>(pageParams[0], pageParams[1]);
		pageObject = illPolicyDao.findIllPolicyLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/illpolicy.do?method=toIllpolicyListPage");
		return "forward:WEB-INF/pages/illpolicy/illpolicy_list.jsp";
	}
	
	/**
	 * @Title: toEditIllpolicyPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至慢性病政策修改页面  
	 */
	@ControllerAnnotation("toEditIllpolicyPage")
	@NextMenuController
	public String toEditIllpolicyPage(HttpServletRequest request){
		String idcode = request.getParameter("id");
		int id = Integer.valueOf(idcode);
		IllPolicy illpolicy = new IllPolicy(id);
		request.setAttribute("editModel", illpolicy);
		ChronicdisDao cDao = new ChronicdisDao();
		List<Chronicdis> list = cDao.findChronicdisAll();
		request.setAttribute("list", list);
		request.setAttribute("editAction", "/sys/illpolicy.do?method=doEditIllpolicy");
		return "forward:WEB-INF/pages/illpolicy/illpolicy_edit.jsp";
	}
	
	/**
	 * @Title: doEditIllpolicy   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行慢性病分类修改
	 */
	@ControllerAnnotation("doEditIllpolicy")
	@NextMenuController
	public String doEditIllpolicy(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		IllPolicy illPolicy = GSON.fromJson(json, IllPolicy.class);
		LOGGER.info(illPolicy);
		try {
			boolean editArea = illPolicy.update();
			if(editArea){
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病政策成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病政策失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditIllpolicyPage(request);
	}
	
	/**
	 * @Title: doDeleteIllpolicy   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除慢性病政策
	 */
	@ControllerAnnotation("doDeleteIllpolicy")
	public Map<String, Object> doDeleteIllpolicy(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String sid = request.getParameter("id");
		int id = Integer.valueOf(sid);
		IllPolicy illPolicy = new IllPolicy();
		illPolicy.setId(id);
		boolean del = illPolicy.del();
		if(del){
			map.put("code", 200);
			map.put("msg", "删除慢性病政策成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除慢性病政策失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddIllpolicyPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加慢性病政策页面 
	 */
	@ControllerAnnotation("toAddIllpolicyPage")
	@NextMenuController
	public String toAddIllpolicyPage(HttpServletRequest request){
		ChronicdisDao cDao = new ChronicdisDao();
		List<Chronicdis> list = cDao.findChronicdisAll();
		request.setAttribute("list", list);
		request.setAttribute("addAction", "/sys/illpolicy.do?method=doAddIllpolicy");
		return "forward:WEB-INF/pages/illpolicy/illpolicy_add.jsp";
	}
	
	/**
	 * @Title: doAddIllpolicy   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加慢性病政策
	 */
	@ControllerAnnotation("doAddIllpolicy")
	@NextMenuController
	public String doAddIllpolicy(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		IllPolicy illPolicy = GSON.fromJson(json, IllPolicy.class);
		LOGGER.info(illPolicy);
		boolean add = illPolicy.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病政策添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病政策添加失败！');</script>");
		}
		return toAddIllpolicyPage(request);
	} 
	
}
