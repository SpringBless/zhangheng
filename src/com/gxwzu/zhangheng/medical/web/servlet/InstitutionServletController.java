package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.area.AreaDao;
import com.gxwzu.zhangheng.medical.dao.institution.InstitutionDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.domain.institution.Institution;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  InstitutionServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   农合机构请求响应控制器
 * @author: zhangheng
 * @date:   2019年4月8日 下午9:31:03     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/institution.do")
public class InstitutionServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toInstitutionListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至行政区域列表  
	 */
	@ControllerAnnotation("toInstitutionListPage")
	@NextMenuController
	public String toInstitutionListPage(HttpServletRequest request){
		InstitutionDao institutionDao = new InstitutionDao();
		int[] pageParams = initPageParams(request);
		PageObject<Institution> pageObject = new PageObject<Institution>(pageParams[0], pageParams[1]);
		pageObject = institutionDao.findInstitutionLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("limitPageAction", "/sys/institution.do?method=toInstitutionListPage");
		return "forward:WEB-INF/pages/institution/institution_list.jsp";
	}
	
	/**
	 * @Title: toAddInstitutionPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至农合机构添加页面  
	 */
	@ControllerAnnotation("toAddInstitutionPage")
	@NextMenuController
	public String toAddInstitutionPage(HttpServletRequest request){
		AreaDao areaDao = new AreaDao();
		List<Area> areaAll = areaDao.findAreaAll();
		request.setAttribute("areaAll", areaAll);
		request.setAttribute("addAction", "/sys/institution.do?method=doAddInstitution");
		return "forward:WEB-INF/pages/institution/institution_add.jsp";
	}
	
	/**
	 * @Title: doAddInstitution   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行农合机构添加
	 */
	@ControllerAnnotation("doAddInstitution")
	@NextMenuController
	public String doAddInstitution(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Institution institution = GSON.fromJson(json, Institution.class);
		Integer grade = institution.getAreacode().length() == 6 ? 1:2;
		institution.setGrade(grade);
		boolean addInst = institution.addInst();
		if(addInst){
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加农合经办点成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加农合经办点失败！');</script>");
		}
		return toAddInstitutionPage(request);
	}
	
	/**
	 * @Title: toEditInstitutionPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至修改农合经办点页面
	 */
	@ControllerAnnotation("toEditInstitutionPage")
	@NextMenuController
	public String toEditInstitutionPage(HttpServletRequest request){
		String areacode = request.getParameter("areacode");
		Institution institution = new Institution(areacode);
		request.setAttribute("editInstitution", institution);
		request.setAttribute("editAction", "/sys/institution.do?method=doEditInstitution");
		return "forward:WEB-INF/pages/institution/institution_edit.jsp";
	}
	
	/**
	 * @Title: doEditInstitution   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行农合经办点
	 */
	@ControllerAnnotation("doEditInstitution")
	@NextMenuController
	public String doEditInstitution(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Institution institution = GSON.fromJson(json, Institution.class);
		LOGGER.info(institution);
		try {
			boolean editInstitution = institution.editInstitution();
			if(editInstitution){
				request.setAttribute("flag", "<script type='text/javascript'>alert('农合经办点修改成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('农合经办点修改失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditInstitutionPage(request);
	}
	
	/**
	 * @Title: doDeleteInstitution   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除农合经办点
	 */
	@ControllerAnnotation("doDeleteInstitution")
	public Map<String, Object> doDeleteInstitution(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String areacode = request.getParameter("id");
		Institution institution = new Institution();
		boolean delInst = institution.delInst(areacode);
		if(delInst){
			map.put("code", 200);
			map.put("msg", "删除农合经办点成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除农合经办点失败！");
		}
		return map;
	}
	
}
