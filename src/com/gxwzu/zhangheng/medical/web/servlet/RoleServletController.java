package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.role.RoleDao;
import com.gxwzu.zhangheng.medical.domain.menu.Menu;
import com.gxwzu.zhangheng.medical.domain.menu.MenuTree;
import com.gxwzu.zhangheng.medical.domain.role.Role;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  RoleServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年3月29日 下午12:18:20     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/role.do")
public class RoleServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * @Title: toRoleListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   角色管理列表
	 */
	@ControllerAnnotation("toRoleListPage")
	@NextMenuController
	public String toRoleListPage(HttpServletRequest request){
		RoleDao roleDao = new RoleDao();
		int[] pageParams = initPageParams(request);
		PageObject<Role> pageObject = new PageObject<Role>(pageParams[0], pageParams[1]);
		pageObject = roleDao.findRolesLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/role.do?method=toRoleListPage");
		return "forward:WEB-INF/pages/role/role_list.jsp";
	}
	
	/**
	 * @Title: toRoleAddPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至角色添加页面
	 */
	@ControllerAnnotation("toRoleAddPage")
	@NextMenuController
	public String toRoleAddPage(HttpServletRequest request){
		MenuTree tree = new MenuTree(true);//当前所有权限树
		request.setAttribute("menuTree", JSON.toJSONString(tree.getNodeList()));
		request.setAttribute("addRoleAction", "/sys/role.do?method=doAddRole");
		return "forward:WEB-INF/pages/role/role_add.jsp";
	}
	
	/**
	 * @Title: doAddRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行角色添加  
	 */
	@ControllerAnnotation("doAddRole")
	@NextMenuController
	public String doAddRole(HttpServletRequest request,Map<String, Object> params){
		String json = GSON.toJson(params);
		Role addRole = GSON.fromJson(json, Role.class);
		LOGGER.info(json);
		String menuIds = (String) params.get("menuIds");
		LOGGER.info(menuIds);
		boolean saveRole = addRole.saveRole(addRole, menuIds);
		if(saveRole){
			request.setAttribute("flag", "<script type='text/javascript'>alert('角色添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('角色添加失败！');</script>");
		}
		return toRoleAddPage(request);
	}
	
	/**
	 * @Title: doDelRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  异步删除角色信息 
	 */
	@ControllerAnnotation("doDelRole")
	public Map<String, Object> doDelRole(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		Long id = Long.valueOf(request.getParameter("id"));
		Role role = new Role();
		boolean delRole = role.delRole(id);
		if(delRole){
			map.put("code", 200);
			map.put("msg", "已为你移除该角色对应的权限！");
		}else {
			map.put("code", 500);
			map.put("msg", "服务器内部出现了错误，删除失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toRoleEditPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至角色修改页面
	 */
	@ControllerAnnotation("toRoleEditPage")
	@NextMenuController
	public String toRoleEditPage(HttpServletRequest request){
		Long id = Long.valueOf(request.getParameter("id"));
		Role role = new Role();
		role = role.findRoleById(id);	
		List<Menu> roleMenus = role.hasMenus(id);
		request.setAttribute("editRole", role);
		MenuTree tree = new MenuTree(true);//当前所有权限树
		request.setAttribute("menuTree", JSON.toJSONString(tree.getAndChNodes(roleMenus)));
		request.setAttribute("addRoleAction", "/sys/role.do?method=doEditRole");
		return "forward:WEB-INF/pages/role/role_edit.jsp";
	}
	
	/**
	 * @Title: doEditRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  执行角色信息修改 
	 */
	@ControllerAnnotation("doEditRole")
	@NextMenuController
	public String doEditRole(HttpServletRequest request,Map<String, Object> params){
		String json = GSON.toJson(params);
		Role editRole = GSON.fromJson(json, Role.class);
		String menuIds = (String) params.get("menuIds");
		LOGGER.info(json);
		LOGGER.info(editRole);
		editRole.removeRoleMenu(editRole.getId());
		boolean update = editRole.updateRoleAndMenu(editRole, menuIds);
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('角色修改成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('角色修改失败！');</script>");
		}
		return toRoleEditPage(request);
	}
}
