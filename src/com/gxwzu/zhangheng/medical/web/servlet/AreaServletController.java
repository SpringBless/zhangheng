package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.area.AreaDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.exception.DBException;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  AreaServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   行政区域请求响应控制器
 * @author: zhangheng
 * @date:   2019年4月8日 下午7:13:55     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/area.do")
public class AreaServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toAreaListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开行政区域列表
	 */
	@ControllerAnnotation("toAreaListPage")
	@NextMenuController
	public String toAreaListPage(HttpServletRequest request){
		AreaDao areaDao = new AreaDao();
		int[] pageParams = initPageParams(request);
		PageObject<Area> pageObject = new PageObject<Area>(pageParams[0], pageParams[1]);
		pageObject = areaDao.findAreaLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/area.do?method=toAreaListPage");
		return "forward:WEB-INF/pages/area/area_list.jsp";
	}
	
	/**
	 * @Title: toEditAreaPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 行政区域修改  
	 */
	@ControllerAnnotation("toEditAreaPage")
	@NextMenuController
	public String toEditAreaPage(HttpServletRequest request){
		String areacode = request.getParameter("areacode");
		try {
			Area area = new Area(areacode);
			request.setAttribute("editArea", area);
		} catch (DBException e) {
			e.printStackTrace();
		}
		request.setAttribute("editAction", "/sys/area.do?method=doEditArea");
		return "forward:WEB-INF/pages/area/area_edit.jsp";
	}
	
	/**
	 * @Title: doEditArea   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行行政区域修改
	 */
	@ControllerAnnotation("doEditArea")
	@NextMenuController
	public String doEditArea(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Area area = GSON.fromJson(json, Area.class);
		LOGGER.info(area);
		try {
			boolean editArea = area.editArea(area.getAreaname());
			if(editArea){
				request.setAttribute("flag", "<script type='text/javascript'>alert('行政区域修改成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('行政区域修改失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditAreaPage(request);
	}
	
	/**
	 * @Title: doDeleteArea   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除行政区域
	 */
	@ControllerAnnotation("doDeleteArea")
	public Map<String, Object> doDeleteArea(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		try {
			Area area = new Area(id);
			boolean delArea = area.delArea();
			if(delArea){
				map.put("code", 200);
				map.put("msg", "删除行政区域成功！");
			}else {
				map.put("code", 500);
				map.put("msg", "删除行政区域失败！");
			}
		} catch (DBException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * @Title: toAddAreaPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加行政区域页面 
	 */
	@ControllerAnnotation("toAddAreaPage")
	@NextMenuController
	public String toAddAreaPage(HttpServletRequest request){
		AreaDao areaDao = new AreaDao();
		List<Area> areaAll = areaDao.findAreaAll();
		request.setAttribute("areaAll", areaAll);
		request.setAttribute("addAreaAction", "/sys/area.do?method=doAddArea");
		return "forward:WEB-INF/pages/area/area_add.jsp";
	}
	
	/**
	 * @Title: doAddArea   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加行政区域  
	 */
	@ControllerAnnotation("doAddArea")
	@NextMenuController
	public String doAddArea(HttpServletRequest request){
		String areacode = request.getParameter("areacode");
		String areaname = request.getParameter("areaname");
		Area area = new Area();
		try {
			boolean addArea = area.addArea(areacode, areaname);
			if(addArea){
				request.setAttribute("flag", "<script type='text/javascript'>alert('行政区域添加成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('行政区域添加失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toAddAreaPage(request);
	}
	
	/**
	 * 行政区域
	 */
	@ControllerAnnotation("queryAreaByAJAX")
	@NextMenuController
	public List<Area> queryAreaByAJAX(HttpServletRequest request){
		AreaDao dao = new AreaDao();
		List<Area> list = dao.findAreaAll();
		return list;
	}
	
}
