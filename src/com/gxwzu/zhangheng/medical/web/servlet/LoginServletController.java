package com.gxwzu.zhangheng.medical.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.domain.user.User;

/**
 * @ClassName:  LoginController   
 * @Description:TODO(这里用一句话描述这个类的作用)   登录控制请求，响应
 * @author: zhangheng
 * @date:   2019年3月27日 下午2:57:51     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/login.do")
public class LoginServletController extends BaseServletController {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 登录处理
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("toLogin")
	public String toLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginName = request.getParameter("loginName");
		String password = request.getParameter("password");
		LOGGER.info("loginName:"+loginName + "\t password:"+password);
		if(loginName == null || loginName.isEmpty()){
			request.setAttribute("isHide", true);
			request.setAttribute("msg", "登录名或账号不能为空！");
			return login(request, response);
		}
		User user = new User();
		user.setLoginName(loginName);
		user.setPassword(password);
		User loginUser = user.login(null, null);
		if(loginUser == null){
			request.setAttribute("isHide", true);
			request.setAttribute("msg", "账号密码不匹配！！");
			return login(request, response);
		}
		request.getSession().setAttribute("SESSION_USER", loginUser);
		String contextPath = request.getContextPath();
		return "redirect:/" + contextPath + "/main.do?method=toMain";
	}
	
	/**
	 * 登出
	 * @param request
	 * @param response
	 * @return
	 */
	@ControllerAnnotation("logout")
	public String logout(HttpServletRequest request, HttpServletResponse response){
		request.getSession().invalidate();
		String contextPath = request.getContextPath();
		return "redirect:/" + contextPath + "/login.do?method=login";
	}
	
	/**
	 * 转发值登录界面
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("login")
	public String login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("action", "/login.do?method=toLogin");
		request.getSession().setAttribute("browser", "/login.do?method=toBrowserPage");
		request.setAttribute("md", "POST");
		return "forward:WEB-INF/pages/login/login.jsp";
	}
	
	/**
	 * 跳转文件上传页面
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("toFileUpload")
	public String toFileUpload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		return "forward:WEB-INF/pages/file/fileUpload.jsp";
	}
	
}
