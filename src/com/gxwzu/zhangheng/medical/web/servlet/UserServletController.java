package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.area.AreaDao;
import com.gxwzu.zhangheng.medical.dao.role.RoleDao;
import com.gxwzu.zhangheng.medical.dao.user.UserDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.domain.role.Role;
import com.gxwzu.zhangheng.medical.domain.user.User;
import com.gxwzu.zhangheng.medical.util.EncryptUtil;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  UserServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: zhangheng
 * @date:   2019年3月29日 下午6:19:31     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/user.do")
public class UserServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toUserListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至用户列表
	 */
	@ControllerAnnotation("toUserListPage")
	@NextMenuController
	public String toUserListPage(HttpServletRequest request){
		UserDao userDao = new UserDao();
		User sessionUser = (User) request.getSession().getAttribute("SESSION_USER");
		int[] pageParams = initPageParams(request);
		PageObject<User> pageObject = new PageObject<User>(pageParams[0], pageParams[1]);
		pageObject = userDao.findUsersLimit(sessionUser.getId(), pageObject);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("limitPageAction", "/sys/user.do?method=toUserListPage");
		LOGGER.info(JSON.toJSON(pageObject));
		return "forward:WEB-INF/pages/user/user_list.jsp";
	}
	
	/**
	 * @Title: toUserAddPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至用户添加页面
	 */
	@ControllerAnnotation("toUserAddPage")
	@NextMenuController
	public String toUserAddPage(HttpServletRequest request){
		AreaDao areaDao = new AreaDao();
		List<Area> list = areaDao.findAreaAll();
		request.setAttribute("areas", list);
		
		RoleDao roleDao = new RoleDao();
		List<Role> roleAll = roleDao.findRoleAll();//获取所有角色
		request.setAttribute("roleAll", roleAll);
		request.setAttribute("addUserAction", "/sys/user.do?method=doAddUser");
		return "forward:WEB-INF/pages/user/user_add.jsp";
	}
	
	/**
	 * @Title: doAddUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  执行添加用户 
	 */
	@ControllerAnnotation("doAddUser")
	@NextMenuController
	public String doAddUser(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		User user = GSON.fromJson(json, User.class);
		LOGGER.info(user);
		/**
		 * {"no":"0012","password":"123456","roleIds":["4","7"],"phone":"18577409493",
		 * "loginName":"429829320@qq.com","tgId":"20","name":"测试角色","tgName":"用户管理",
		 * "email":"429829320@qq.com","remarks":""}
		 * 
		 * User [id=null, companyId=null, officeId=null, loginName=429829320@qq.com, 
		 * password=123456, no=0012, name=测试角色, email=429829320@qq.com, 
		 * phone=18577409493, remarks=, delFlag=null, roles=null]
		 */
		String[] roleIds = request.getParameterValues("roleIds");
		LOGGER.info(Arrays.toString(roleIds));
		user.setCompanyId(1L);
		//user.setOfficeId(2L);
		user.setDelFlag("0");
		boolean save = user.saveUserAndRole(user, roleIds);
		if(save){
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加用户成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('添加用户失败！');</script>");
		}
		return toUserAddPage(request);
	}
	
	/**
	 * @Title: toUserChangPwdPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   转发至当前登录系统的用户，修改信息界面
	 */
	@ControllerAnnotation("toUserChangPwdPage")
	@NextMenuController
	public String toUserChangPwdPage(HttpServletRequest request){
		request.setAttribute("changeUserAction", "/sys/user.do?method=doChangePwd");
		return "forward:WEB-INF/pages/user/user_pwd.jsp";
	}
	
	/**
	 * @Title: doChangePwd   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  执行当前用户信息修改 
	 */

	@NextMenuController@ControllerAnnotation("doChangePwd")
	public String doChangePwd(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		User curentUser = GSON.fromJson(json, User.class);
		LOGGER.info(curentUser);
		String nPassword = request.getParameter("nPassword");
		if(nPassword != null && !"".equals(nPassword)){
			String encoding = User.encoding(
					nPassword.getBytes(), 
					EncryptUtil.generateSalt(EncryptUtil.GENERATE_SALT_SIZE),
					EncryptUtil.DEFULT_SIZE);
			curentUser.setPassword(encoding);
		}
		boolean update = curentUser.updateUserAndRole(curentUser, null);
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改信息成功！');</script>");
			request.getSession().setAttribute("SESSION_USER", curentUser);
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改信息失败！');</script>");
		}
		/**
		 * {"no":"0001","nPassword":"","delFlag":"0","companyId":"1",
		 * "password":"2548807d9b108278ce88173a76ab1621a7050a14883eac1db300ca2c","officeId":"2",
		 * "phone":"18577409493","loginName":"429829320@qq.com","tgId":"30","name":"系统超级管理员",
		 * "id":"1","tgName":"修改密码","email":"colin@163.com","remarks":"最高管理员"}
		 * 
		 * User [id=1, companyId=1, officeId=2, loginName=429829320@qq.com, 
		 * password=2548807d9b108278ce88173a76ab1621a7050a14883eac1db300ca2c, no=0001, 
		 * name=系统超级管理员, email=colin@163.com, phone=18577409493, 
		 * remarks=最高管理员, delFlag=0, roles=null]
		 */
		return toUserChangPwdPage(request);
	}
	
	/**
	 * @Title: toUserInfoPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至查看当前系统用户个人信息页面 
	 */
	@ControllerAnnotation("toUserInfoPage")
	@NextMenuController
	public String toUserInfoPage(HttpServletRequest request){
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		Set<Role> roles = sessionUser.hasRoles(sessionUser.getId());
		request.setAttribute("curentRoles", roles);
		return "forward:WEB-INF/pages/user/user_info.jsp";
	}
	
	/**
	 * @Title: toUserModifyPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至用户信息修改界面 
	 */
	@ControllerAnnotation("toUserEditPage")
	@NextMenuController
	public String toUserEditPage(HttpServletRequest request){
		Long id = Long.valueOf(request.getParameter("id"));
		User user = new User();
		user = user.findUserById(id);
		request.setAttribute("editUser", user);
		List<Long> curentRoles = user.findUserRoles(id);
		if(curentRoles == null){
			curentRoles = new ArrayList<Long>();
		}
		request.setAttribute("curentRoles", JSON.toJSON(curentRoles));//当前已有的角色
		
		AreaDao areaDao = new AreaDao();
		List<Area> list = areaDao.findAreaAll();
		request.setAttribute("areas", list);
		
		RoleDao roleDao = new RoleDao();
		List<Role> roleAll = roleDao.findRoleAll();//获取所有角色
		request.setAttribute("roleAll", roleAll);
		request.setAttribute("editUserAction", "/sys/user.do?method=doEditUser");
		return "forward:WEB-INF/pages/user/user_edit.jsp";
	}
	
	/**
	 * @Title: doEditUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行用户信息更改
	 */
	@ControllerAnnotation("doEditUser")
	@NextMenuController
	public String doEditUser(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		User user = GSON.fromJson(json, User.class);
		LOGGER.info(user);
		String nPassword = request.getParameter("nPassword");
		String[] roleIds = request.getParameterValues("roleIds");
		if(nPassword != null && !"".equals(nPassword)){//加密密码
			String encoding = User.encoding(nPassword.getBytes(),
							EncryptUtil.generateSalt(EncryptUtil.GENERATE_SALT_SIZE),
							EncryptUtil.DEFULT_SIZE);
			user.setPassword(encoding);
		}
		user.deleteRoleByUserId(user.getId());//先删除原有的角色
		boolean update = user.updateUserAndRole(user, roleIds);//更新新的用户数据，和角色
		if(update){
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改用户成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('修改用户失败！');</script>");
		}
		/**
		 * User [id=16, companyId=null, officeId=null, loginName=admin, 
		 * password=db688e70540dd363f220df5f3aaad0b820f692733c0b423da94e2b62, 
		 * no=0012, name=spring liu, email=429829320@qq.com, phone=18577409493, 
		 * remarks=111, delFlag=null, roles=null]
		 * 
		 * {"no":"0012","password":"db688e70540dd363f220df5f3aaad0b820f692733c0b423da94e2b62",
		 * "nPassword":"","roleIds":["1","5"],"phone":"18577409493","loginName":"admin","tgId":"20",
		 * "name":"spring liu","id":"16","tgName":"用户管理","email":"429829320@qq.com","remarks":"111"}
		 */
		return toUserEditPage(request);
	}
	
	/**
	 * @Title: doDeleteUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除用户
	 */
	@ControllerAnnotation("doDeleteUser")
	public Map<String, Object> doDeleteUser(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		Long userId = Long.valueOf(request.getParameter("id"));
		User delUser = new User();
		boolean del = delUser.deleteUser(userId);
		if(del){
			map.put("code", 200);
			map.put("msg", "删除用户成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除用户失败！，程序内部错误！");
		}
		LOGGER.info(map);
 		return map;
	}
	
}
