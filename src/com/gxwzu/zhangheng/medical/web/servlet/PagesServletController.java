package com.gxwzu.zhangheng.medical.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;

/**
 * @ClassName:  PagesController   
 * @Description:TODO(这里用一句话描述这个类的作用)   普通页面分发器
 * @author: zhangheng
 * @date:   2019年3月27日 下午6:00:13     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/pages.do")
public class PagesServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;

	/**
	 * 程序500错误
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("/to500")
	public String to500(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("forward:WEB-INF/pages/code/500.jsp");
		return "forward:WEB-INF/pages/code/500.jsp";
	}
	
	/**
	 * 服务器拒绝访问403
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("to403")
	public String to403(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("forward:WEB-INF/pages/code/403.jsp");
		return "forward:WEB-INF/pages/code/403.jsp";
	}
	
	/**
	 * 转发低至版本浏览器
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@ControllerAnnotation("toBrowserPage")
	public String toBrowserPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("forward:WEB-INF/pages/browser/browser.jsp");
		return "forward:WEB-INF/pages/browser/browser.jsp";
	}
	
}
