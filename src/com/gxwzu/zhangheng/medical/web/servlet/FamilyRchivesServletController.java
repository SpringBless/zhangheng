package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.familyrchives.FamilyRchivesDao;
import com.gxwzu.zhangheng.medical.domain.area.Area;
import com.gxwzu.zhangheng.medical.domain.familydata.FamilyData;
import com.gxwzu.zhangheng.medical.domain.familyinfo.FamilyInfo;
import com.gxwzu.zhangheng.medical.domain.familyrchives.FamilyRchives;
import com.gxwzu.zhangheng.medical.domain.user.User;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  FamilyRchivesServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   家庭档案管理控制器
 * @author: zhangheng
 * @date:   2019年4月16日 下午12:52:06     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/FamilyArchives.do")
public class FamilyRchivesServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toFamilyArchivesListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至家庭档案管理列表
	 */
	@ControllerAnnotation("toFamilyArchivesListPage")
	@NextMenuController
	public String toFamilyArchivesListPage(HttpServletRequest request){
		FamilyRchivesDao familyRchivesDao = new FamilyRchivesDao();
		int[] pageParams = initPageParams(request);
		PageObject<FamilyRchives> pageObject = new PageObject<FamilyRchives>(pageParams[0], pageParams[1]);
		pageObject = familyRchivesDao.findFamilyRchivesLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/FamilyArchives.do?method=toFamilyArchivesListPage");
		return "forward:WEB-INF/pages/family/family_list.jsp";
	}
	
	/**
	 * @Title: toEditFamilyArchivesPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至家庭档案管理修改页面  
	 */
	@ControllerAnnotation("toEditFamilyArchivesPage")
	@NextMenuController
	public String toEditFamilyArchivesPage(HttpServletRequest request){
		String familyCode = request.getParameter("familyCode");
		FamilyRchives familyRchives = new FamilyRchives(familyCode);
		request.setAttribute("editModel", familyRchives);
		FamilyData fData = new FamilyData();
		List<String> list = fData.findColumnList("family_property");
		request.setAttribute("familyProperty", list);
		request.setAttribute("editAction", "/sys/FamilyArchives.do?method=doEditFamilyArchives");
		request.setAttribute("initCodeURL", "/sys/FamilyArchives.do?method=findFamilyByAJAX");
		return "forward:WEB-INF/pages/family/family_edit.jsp";
	}
	
	/**
	 * @Title: findFamilyByAJAX   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 一步获取区域编码  
	 * @param: @param request
	 * @return: List<Area>      
	 */
	@ControllerAnnotation("findFamilyByAJAX")
	public List<Area> findFamilyByAJAX(HttpServletRequest request){
		String areaCode = request.getParameter("areaCode");
		String g = request.getParameter("grade");
		Integer grade = Integer.valueOf(g);
		Area area = new Area();
		List<Area> list = area.findAreaByAreaCodeAndGrade(areaCode, grade);
		return list;
	}
	
	/**
	 * @Title: doEditFamilyArchives   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行家庭档案管理修改
	 */
	@ControllerAnnotation("doEditFamilyArchives")
	@NextMenuController
	public String doEditFamilyArchives(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		FamilyRchives familyRchives = GSON.fromJson(json, FamilyRchives.class);
		familyRchives.setCreateTime(new Date());// 设置创建时间
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		familyRchives.setGreffier(sessionUser.getId());// 设置操作者
		LOGGER.info(familyRchives);
		try {
			boolean update = familyRchives.update();
			if(update){
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改家庭档案管理成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改家庭档案管理失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditFamilyArchivesPage(request);
	}
	
	/**
	 * @Title: doDeleteFamilyArchives   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除家庭档案
	 */
	@ControllerAnnotation("doDeleteFamilyArchives")
	public Map<String, Object> doDeleteFamilyArchives(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		FamilyRchives familyRchives = new FamilyRchives(id);
		boolean del = familyRchives.del();
		if(del){
			FamilyInfo info = new FamilyInfo();
			info.setFamilyCode(id);
			boolean del2 = info.deleteByFamilyCode();
			if(del2){
				map.put("code", 200);
				map.put("msg", "删除家庭档案成功！");
			}else {
				map.put("code", 500);
				map.put("msg", "删除家庭档案失败！");
			}
		}else {
			map.put("code", 500);
			map.put("msg", "删除家庭档案失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddFamilyArchivesPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加家庭档案页面 
	 */
	@ControllerAnnotation("toAddFamilyArchivesPage")
	@NextMenuController
	public String toAddFamilyArchivesPage(HttpServletRequest request){
		FamilyData fData = new FamilyData();
		List<String> list = fData.findColumnList("family_property");
		request.setAttribute("familyProperty", list);
		request.setAttribute("initCodeURL", "/sys/FamilyArchives.do?method=findFamilyByAJAX");
		request.setAttribute("addAction", "/sys/FamilyArchives.do?method=doAddFamilyArchives");
		request.setAttribute("initValueURL", "/sys/FamilyInfo.do?method=getColumnValue");
		request.setAttribute("cardCodeAction", "/sys/FamilyInfo.do?method=cardCodeInDB");
		return "forward:WEB-INF/pages/family/family_add.jsp";
	}
	
	/**
	 * @Title: doAddFamilyArchives   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加家庭档案
	 */
	@ControllerAnnotation("doAddFamilyArchives")
	@NextMenuController
	public String doAddFamilyArchives(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		FamilyRchives familyRchives = GSON.fromJson(json, FamilyRchives.class);
		familyRchives.setCreateTime(new Date());// 设置创建时间
		User sessionUser = (User)request.getSession().getAttribute("SESSION_USER");
		familyRchives.setGreffier(sessionUser.getId());// 设置操作者
		LOGGER.info(familyRchives);
		familyRchives.setAgricultural(0);
		familyRchives.setFamilyNumber(0);
		FamilyInfo info = GSON.fromJson(json, FamilyInfo.class);
		info.setAddress((String)params.get("infoAddress"));
		info.setName(familyRchives.getHouseholder());
		boolean add = familyRchives.add();
		
		if(add){
			info.setFamilyCode(familyRchives.getFamilyCode());
			info.setBirthday(DateUtils.cardCode2Date(info.getCardCode()));
			boolean add2 = info.add();
			if(add2){
				familyRchives.setFamilyNumber(1);
				familyRchives.updateFamilyNumberByFamilyCode();
				if("是".equals(info.getHkType())){
					familyRchives.setAgricultural(1);
					familyRchives.updateAgriculturalByFamilyCode();
				}
				request.setAttribute("flag", "<script type='text/javascript'>alert('家庭档案添加成功！');</script>");
			}else {	
				familyRchives.del();
				request.setAttribute("flag", "<script type='text/javascript'>alert('家庭档案添加失败！');</script>");
			}
		}else {
			familyRchives.del();
			request.setAttribute("flag", "<script type='text/javascript'>alert('家庭档案添加失败！');</script>");
		}
		return toAddFamilyArchivesPage(request);
	} 
	
}
