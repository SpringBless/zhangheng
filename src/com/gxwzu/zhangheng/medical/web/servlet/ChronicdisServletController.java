package com.gxwzu.zhangheng.medical.web.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.chronicdis.ChronicdisDao;
import com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ChronicdisServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢性病分类管理控制器
 * @author: zhangheng
 * @date:   2019年4月14日 下午12:05:19     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/chronicdis.do")
public class ChronicdisServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toChronicdisListPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  打开慢性病分类管理列表
	 */
	@ControllerAnnotation("toChronicdisListPage")
	@NextMenuController
	public String toChronicdisListPage(HttpServletRequest request){
		ChronicdisDao chronicdisDao = new ChronicdisDao();
		int[] pageParams = initPageParams(request);
		PageObject<Chronicdis> pageObject = new PageObject<Chronicdis>(pageParams[0], pageParams[1]);
		pageObject = chronicdisDao.findChronicdisLimit(pageObject);
		request.setAttribute("limitPage", pageObject);
		LOGGER.info(JSON.toJSON(pageObject));
		request.setAttribute("limitPageAction", "/sys/chronicdis.do?method=toChronicdisListPage");
		return "forward:WEB-INF/pages/chronicdis/chronicdis_list.jsp";
	}
	
	/**
	 * @Title: toEditChronicdisPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发至慢性病修改页面  
	 */
	@ControllerAnnotation("toEditChronicdisPage")
	@NextMenuController
	public String toEditChronicdisPage(HttpServletRequest request){
		String illcode = request.getParameter("illcode");
		Chronicdis chronicdis = new Chronicdis(illcode);
		request.setAttribute("editChronicdis", chronicdis);
		request.setAttribute("editAction", "/sys/chronicdis.do?method=doEditChronicdis");
		return "forward:WEB-INF/pages/chronicdis/chronicdis_edit.jsp";
	}
	
	/**
	 * @Title: doEditChronicdis   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行慢性病分类修改
	 */
	@ControllerAnnotation("doEditChronicdis")
	@NextMenuController
	public String doEditChronicdis(HttpServletRequest request, Map<String, Object> param){
		String json = GSON.toJson(param);
		LOGGER.info(json);
		Chronicdis chronicdis = GSON.fromJson(json, Chronicdis.class);
		LOGGER.info(chronicdis);
		try {
			boolean editArea = chronicdis.edit();
			if(editArea){
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病分类成功！');</script>");
			}else {
				request.setAttribute("flag", "<script type='text/javascript'>alert('修改慢性病分类失败！');</script>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toEditChronicdisPage(request);
	}
	
	/**
	 * @Title: doDeleteChronicdis   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   异步删除慢性病分类
	 */
	@ControllerAnnotation("doDeleteChronicdis")
	public Map<String, Object> doDeleteChronicdis(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String,Object>();
		String id = request.getParameter("id");
		Chronicdis chronicdis = new Chronicdis();
		boolean del = chronicdis.del(id);
		if(del){
			map.put("code", 200);
			map.put("msg", "删除慢性病分类成功！");
		}else {
			map.put("code", 500);
			map.put("msg", "删除慢性病分类失败！");
		}
		return map;
	}
	
	/**
	 * @Title: toAddChronicdisPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  转发至添加慢性病分类页面 
	 */
	@ControllerAnnotation("toAddChronicdisPage")
	@NextMenuController
	public String toAddChronicdisPage(HttpServletRequest request){
		request.setAttribute("addChronicdisAction", "/sys/chronicdis.do?method=doAddChronicdis");
		return "forward:WEB-INF/pages/chronicdis/chronicdis_add.jsp";
	}
	
	/**
	 * @Title: doAddChronicdis   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 执行添加慢性病分类
	 */
	@ControllerAnnotation("doAddChronicdis")
	@NextMenuController
	public String doAddChronicdis(HttpServletRequest request, Map<String, Object> params){
		String json = GSON.toJson(params);
		LOGGER.info(json);
		Chronicdis chronicdis = GSON.fromJson(json, Chronicdis.class);
		LOGGER.info(chronicdis);
		boolean add = chronicdis.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病分类添加成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病分类失败！');</script>");
		}
		return toAddChronicdisPage(request);
	} 
	
	/**
	 * @Title: queryBByAjax   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   慢性病
	 * @param: @param request
	 * @return: List<Chronicdis>      
	 */
	@ControllerAnnotation("queryBByAjax")
	public List<Chronicdis> queryBByAjax(HttpServletRequest request){
		ChronicdisDao dao = new ChronicdisDao();
		List<Chronicdis> list = dao.findChronicdisAll();
		return list != null ? list : new ArrayList<Chronicdis>();
	}
	
}
