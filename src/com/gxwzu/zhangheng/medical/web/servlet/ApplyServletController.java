package com.gxwzu.zhangheng.medical.web.servlet;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gxwzu.zhangheng.medical.annotation.ControllerAnnotation;
import com.gxwzu.zhangheng.medical.annotation.NextMenuController;
import com.gxwzu.zhangheng.medical.dao.apply.ApplyDtoDao;
import com.gxwzu.zhangheng.medical.dao.chronicdis.ChronicdisDao;
import com.gxwzu.zhangheng.medical.domain.apply.Apply;
import com.gxwzu.zhangheng.medical.domain.apply.ApplyDto;
import com.gxwzu.zhangheng.medical.domain.chronicdis.Chronicdis;
import com.gxwzu.zhangheng.medical.domain.illpolicy.IllPolicy;
import com.gxwzu.zhangheng.medical.util.DateUtils;
import com.gxwzu.zhangheng.medical.util.ExcelWriteUtils;
import com.gxwzu.zhangheng.medical.util.PageObject;

/**
 * @ClassName:  ApplyServletController   
 * @Description:TODO(这里用一句话描述这个类的作用)   慢病报销控制器
 * @author: zhangheng
 * @date:   2019年6月3日 下午9:25:57     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebServlet("/sys/apply.do")
public class ApplyServletController extends BaseServletController {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * @Title: toApplyPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 转发慢病报销   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toApplyPage")
	@NextMenuController
	public String toApplyPage(HttpServletRequest request){
		request.setAttribute("checkAction", "/sys/payDto.do?method=queryPayDtoByYearCode");
		return "forward:WEB-INF/pages/apply/apply_add.jsp";
	}
	
	/**
	 * @Title: checkDoAddApply   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 添加慢病报销时，检查慢病信息   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("checkDoAddApply")
	@NextMenuController
	public String checkDoAddApply(HttpServletRequest request){
		String aFapiao = request.getParameter("aFapiao");
		String aSeeDate = request.getParameter("aSeeDate");
		String aApply = request.getParameter("aApply");
		String aIllname = request.getParameter("aIllname");
		String aIdCard = request.getParameter("aIdCard");
		String aFamilyCode = request.getParameter("aFamilyCode");
		BigDecimal bApply = new BigDecimal(aApply);
		
		String year = DateUtils.formartYear(new java.util.Date());
		
		Apply apply = new Apply();
		apply.setaSeeDate(DateUtils.paseDate(aSeeDate));
		apply.setaFapiao(aFapiao);
		apply.setaYear(year);
		apply.setaApply(bApply);
		apply.setaCreateDate(new java.util.Date());
		apply.setaFamilyCode(aFamilyCode);
		apply.setaIdCard(aIdCard);
		apply.setaIllname(aIllname);
		// 报销状态：待审核：0，审核通过：1，审核不通过：2，已汇款：3，未回款：4
		apply.setaStatus(0);
		request.setAttribute("apply", apply);
		
		BigDecimal decimal = apply.applyFor(year, aIdCard);
		request.setAttribute("decimal", decimal);
		
		IllPolicy illPolicy = new IllPolicy();
		illPolicy = illPolicy.queryByIllname(aIllname);
		request.setAttribute("illPolicy", illPolicy);
		request.setAttribute("addAction", "/sys/apply.do?method=doAddApply");
		return "forward:WEB-INF/pages/apply/apply_check.jsp";
	}
	
	/**
	 * @Title: doAddApply   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  添加慢病报销   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("doAddApply")
	@NextMenuController
	public String doAddApply(HttpServletRequest request){
		String aFapiao = request.getParameter("aFapiao");
		String aSeeDate = request.getParameter("aSeeDate");
		String aYear = request.getParameter("aYear");
		String aApply = request.getParameter("aApply");
		String aIllname = request.getParameter("aIllname");
		String aIdCard = request.getParameter("aIdCard");
		String aFamilyCode = request.getParameter("aFamilyCode");
		String aCreateDate = request.getParameter("aCreateDate");
		
		Apply apply = new Apply();
		apply.setaSeeDate(DateUtils.paseDate(aSeeDate));
		apply.setaFapiao(aFapiao);
		apply.setaYear(aYear);
		apply.setaApply(new BigDecimal(aApply));
		apply.setaCreateDate(DateUtils.paseDate(aCreateDate));
		apply.setaFamilyCode(aFamilyCode);
		apply.setaIdCard(aIdCard);
		apply.setaIllname(aIllname);
		// 报销状态：待审核：0，审核通过：1，审核不通过：2，已汇款：3，未回款：4
		apply.setaStatus(0);
		boolean add = apply.add();
		if(add){
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病报销登记成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病报销登记失败！');</script>");
		}
		return toApplyPage(request);
	}
	
	/**
	 * @Title: toShenhPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   慢病报销审核列表
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toShenhPage")
	@NextMenuController
	public String toShenhPage(HttpServletRequest request){
		String areaCode = request.getParameter("areaCode");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String aStatus = request.getParameter("aStatus");
		int[] pageParams = initPageParams(request);
		
		aStatus = aStatus != null ? aStatus : "0";
		ApplyDtoDao dao = new ApplyDtoDao();
		PageObject<ApplyDto> pageObject = new PageObject<>(pageParams[0], pageParams[1]);
		pageObject = dao.findApplyLimit(pageObject, areaCode, name, startDate, endDate, aStatus);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("limitPageAction", "/sys/apply.do?method=toShenhPage");
		request.setAttribute("queryAreaByAJAX", "/sys/area.do?method=queryAreaByAJAX");
		
		areaCode = areaCode != null ? areaCode : "";
		name = name != null ? name : "";
		startDate = startDate != null ? startDate : "";
		endDate = endDate != null ? endDate : "";
		StringBuffer temp = new StringBuffer();
		temp.append("&areaCode=").append(areaCode).append("&name=").append(name)
			.append("&startDate=").append(startDate).append("&endDate=").append(endDate)
			.append("&aStatus=").append(aStatus);
		request.setAttribute("tempStr", temp.toString());
		return "forward:WEB-INF/pages/apply/apply_list.jsp";
	}
	
	/**
	 * @Title: toShenh   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 审核   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("toShenh")
	@NextMenuController
	public String toShenh(HttpServletRequest request){
		String aId = request.getParameter("aId");
		Long id = Long.valueOf(aId);
		ApplyDto dto = new ApplyDto();
		dto.setaId(id);
		dto = dto.loadFromDB();
		request.setAttribute("shenheModle", dto);
		request.setAttribute("action", "/sys/apply.do?method=doShenh");
		return "forward:WEB-INF/pages/apply/apply_shenhe.jsp";
	}
	
	/**
	 * @Title: doShenh   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   执行审核
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("doShenh")
	@NextMenuController
	public String doShenh(HttpServletRequest request){
		String type = request.getParameter("type");
		String aId = request.getParameter("aId");
		Long id = Long.valueOf(aId);
		ApplyDto dto = new ApplyDto();
		Apply apply = dto.queryApplyById(id);
		if(type != null){
			apply.setaStatus(2);
		}else{			
			apply.setaStatus(1);
		}
		apply.setaApplyDate(new java.util.Date());
		BigDecimal decimal = apply.applyFor(apply.getaYear(), apply.getaIdCard());
		
		IllPolicy illPolicy = new IllPolicy();
		illPolicy = illPolicy.queryByIllname(apply.getaIllname());
		
		//剩余可报金额
		decimal = illPolicy.getTopLine().subtract(decimal);
		int compareTo = decimal.compareTo(apply.getaApply());
		if(compareTo == -1 && decimal.compareTo(new BigDecimal("0.00")) == 1 ){
			apply.setaApply(decimal);
		}
		boolean b = apply.toUpdate();
		if(b){
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病报销审核成功！');</script>");
		}else {
			request.setAttribute("flag", "<script type='text/javascript'>alert('慢性病报销审核失败！');</script>");
		}
		return toShenhPage(request);
	}
	
	/**
	 * @Title: huikuan   
	 * @Description: TODO(这里用一句话描述这个方法的作用)  汇款   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("huikuan")
	public Map<String, Object> huikuan(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
		String aId = request.getParameter("aId");
		Long id = Long.valueOf(aId);
		ApplyDto dto = new ApplyDto();
		Apply apply = dto.queryApplyById(id);
		apply.setaStatus(3);
		boolean update = apply.toUpdate();
		if(update){
			map.put("code", 200);
			map.put("data", "汇款成功");
		}else {
			map.put("code", 500);
			map.put("data", "汇款失败");
		}
		return map;
	}
	
	/**
	 * @Title: tongjiPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 慢病报销统计   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("tongjiPage")
	@NextMenuController
	public String tongjiPage(HttpServletRequest request){
		String areaCode = request.getParameter("areaCode");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String aStatus = request.getParameter("aStatus");
		String aIllname = request.getParameter("aIllname");
		int[] pageParams = initPageParams(request);
		
		ApplyDtoDao dao = new ApplyDtoDao();
		PageObject<ApplyDto> pageObject = new PageObject<>(pageParams[0], pageParams[1]);
		pageObject = dao.findApplyLimit(pageObject, areaCode, name, startDate, endDate, aStatus,aIllname);
		request.setAttribute("limitPage", pageObject);
		request.setAttribute("limitPageAction", "/sys/apply.do?method=tongjiPage");
		request.setAttribute("queryAreaByAJAX", "/sys/area.do?method=queryAreaByAJAX");
		request.setAttribute("outExcelAction", "/sys/apply.do?method=outExcel");
		
		aStatus = aStatus != null ? aStatus : "";
		areaCode = areaCode != null ? areaCode : "";
		name = name != null ? name : "";
		startDate = startDate != null ? startDate : "";
		endDate = endDate != null ? endDate : "";
		aIllname = aIllname != null ? aIllname : "";
		StringBuffer temp = new StringBuffer();
		temp.append("&areaCode=").append(areaCode).append("&name=").append(name)
			.append("&startDate=").append(startDate).append("&endDate=").append(endDate)
			.append("&aStatus=").append(aStatus).append("&aIllname=").append(aIllname);
		request.setAttribute("tempStr", temp.toString());
		
		ChronicdisDao cDao = new ChronicdisDao();
		List<Chronicdis> list = cDao.findChronicdisAll();
		request.setAttribute("Chronicdis", list);
		return "forward:WEB-INF/pages/apply/apply_tj_list.jsp";
	}
	
	/**
	 * @Title: outExcel   
	 * @Description: TODO(这里用一句话描述这个方法的作用) 导出excel   
	 * @param: @param request
	 * @return: String      
	 */
	@ControllerAnnotation("outExcel")
	public String outExcel(HttpServletRequest request, HttpServletResponse response){
		String areaCode = request.getParameter("areaCode");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String aStatus = request.getParameter("aStatus");
		String aIllname = request.getParameter("aIllname");
		ApplyDtoDao dao = new ApplyDtoDao();
		List<ApplyDto> list = dao.findApplyLimitExcel(areaCode, name, startDate, endDate, aStatus, aIllname);
		ExcelWriteUtils.outBaoxiaoExcel("慢病报销" + DateUtils.getyyyy_MM_dd_HH_mm_ss1(), list, response);
		return null;
	}
	
}
