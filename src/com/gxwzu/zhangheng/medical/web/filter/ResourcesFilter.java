package com.gxwzu.zhangheng.medical.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gxwzu.zhangheng.medical.domain.user.User;


/**
 * @ClassName:  ResourcesFilter   
 * @Description:TODO(这里用一句话描述这个类的作用)   统一资源过滤器
 * @author: zhangheng
 * @date:   2019年3月27日 下午2:31:28     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebFilter(filterName="ResourcesFilter",urlPatterns="/*",
		initParams={
				@WebInitParam(name="statics",value="/static"),
				@WebInitParam(name="loginURI",value="/login.do"),
				@WebInitParam(name="medical",value="/medical"),
				@WebInitParam(name="file",value="/file.do"),
				@WebInitParam(name="pages",value="/pages.do")
		})
public class ResourcesFilter implements Filter {

	/**
	 * 静态资源放行
	 */
	private String statics;
	
	/**
	 * 登陆页面放行
	 */
	private String loginURI;
	
	/**
	 * 默认页面放行
	 */
	private String medical;
	
	/**
	 * 文件上传放行
	 */
	private String file;
	
	/**
	 * 提示页面放行
	 */
	private String pages;
	
	/**
	 * Log4j
	 */
	private static Logger LOGGER = LogManager.getLogger(ResourcesFilter.class);
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		statics = filterConfig.getInitParameter("statics");
		loginURI = filterConfig.getInitParameter("loginURI");
		medical = filterConfig.getInitParameter("medical");
		file = filterConfig.getInitParameter("file");
		pages = filterConfig.getInitParameter("pages");
		LOGGER.info("..ResourcesFilter.." + statics);
		LOGGER.info("..ResourcesFilter.." + loginURI);
		LOGGER.info("..ResourcesFilter.." + medical);
		LOGGER.info("..ResourcesFilter.." + file);
		LOGGER.info("..ResourcesFilter.." + pages);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String requestURI = req.getRequestURI();
		LOGGER.info("req.getRequestURI():" + requestURI);
		if(requestURI.indexOf(statics) != -1){
			LOGGER.info("public void doFilter  ==> requestURI.indexOf(statics) != -1");
			chain.doFilter(request, response);
			return;
		}
		if(requestURI.indexOf(loginURI) != -1){
			LOGGER.info("public void doFilter  ==> requestURI.indexOf(loginURI) != -1");
			chain.doFilter(request, response);
			return;
		}
		if(requestURI.indexOf(file) != -1){
			LOGGER.info("public void doFilter ==> requestURI.indexOf(file) != -1");
			chain.doFilter(request, response);
			return;
		}
		if(requestURI.indexOf(pages) != -1){
			LOGGER.info("public void doFilter ==> requestURI.indexOf(file) != -1");
			chain.doFilter(request, response);
			return;
		}
		User user = (User)req.getSession().getAttribute("SESSION_USER");
		if(user != null ){
			LOGGER.info("public void doFilter ==> user != null:" + user);
			chain.doFilter(request, response);
			return;
		}
		String contextPath = req.getContextPath();
		System.out.println("contextPath:"+contextPath);
		if(medical.equals(contextPath)){
			medical = contextPath;
		}
		if(medical.equals(requestURI) || (medical+"/").equals(requestURI)){
			LOGGER.info("public void doFilter ==> medical.equals(requestURI) || (medical+\"/\").equals(requestURI)");
			req.getRequestDispatcher("/login.do?method=login").forward(request, response);
			return;
		}
		req.getRequestDispatcher("/pages.do?method=to404").forward(request, response);
	}

	@Override
	public void destroy() {
		LOGGER.info("ResourcesFilter is destroy()");
	}

}
