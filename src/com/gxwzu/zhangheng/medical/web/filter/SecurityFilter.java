package com.gxwzu.zhangheng.medical.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gxwzu.zhangheng.medical.domain.menu.Menu;

/**
 * @ClassName:  SecurityFilter   
 * @Description:TODO(这里用一句话描述这个类的作用)   权限管理模块，权限过过滤器
 * @author: zhangheng
 * @date:   2019年3月11日 下午12:53:12     
 * @Copyright: 2019 wwww.sprringbless.xin Inc. All rights reserved.
 */
@WebFilter(filterName="SecurityFilter",urlPatterns="/sys/*")
public class SecurityFilter implements Filter {
	
	/**
	 * Log4j
	 */
	private static Logger LOGGER = LogManager.getLogger(SecurityFilter.class);
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@SuppressWarnings("unchecked")
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String requestURI = request.getRequestURI().replace("medical", "").replaceAll("/+", "/");
		LOGGER.info("requestURI:"+requestURI);
		if(requestURI == null || requestURI.length() <= 0){
			requestURI = "";
		}
		List<Menu> menuList = (List<Menu>) request.getSession().getAttribute("SESSION_MENU");
		String contextPath = request.getContextPath();
		String result = contextPath + "/pages.do?method=to403";
		if(menuList == null || !(menuList.size() >0)){//没有菜单直接拒绝访问
			response.sendRedirect(result);
			return;
		}
		boolean securityFlag = false;
		for(Menu menu : menuList){
			if(menu.getHref() == null || menu.getHref().isEmpty()){
				continue;
			}
			System.out.println(menu.getHref() + "-----" + requestURI);
			if(menu.getHref().indexOf(requestURI) != -1){
				securityFlag = true;
				chain.doFilter(request, response);
				return;
			}
		}
		if(!securityFlag){
			response.sendRedirect(result);
			return;
		}
		
	}

	@Override
	public void destroy() {
		
	}

}
